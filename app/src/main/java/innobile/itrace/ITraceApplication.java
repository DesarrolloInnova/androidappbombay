package innobile.itrace;


import android.app.Application;
import android.content.Context;
import android.os.Environment;
import java.io.File;


public class ITraceApplication extends Application {
	private static final String LOG_FOLDER_ENV_VARIABLE  = "itrace.android.logpath";
	private static final String LOG_FOLDER = "Download/iTraceLog";
	private static final String LOG_CONFIG_FILE = "logconfig.xml";
	private static ITraceApplication instance;


	
	@Override
	public void onCreate() {
		super.onCreate();
		configureLogging();
		instance=this;

	}
	

    
    private void configureLogging(){
    	
    	File logDir = new File(Environment.getExternalStorageDirectory(), LOG_FOLDER);
    	
		if (!logDir.exists() || !logDir.isDirectory()){
			logDir.mkdirs();
		}
		
		System.setProperty(LOG_FOLDER_ENV_VARIABLE, logDir.getAbsolutePath());
		
		// implementar configuración de log

    }
		


	public static Context getContext() {
		return instance.getApplicationContext();
	}

}
