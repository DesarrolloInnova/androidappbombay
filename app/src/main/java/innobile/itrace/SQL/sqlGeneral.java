package innobile.itrace.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BATMAN on 6/06/18.
 */



public class sqlGeneral extends SQLiteOpenHelper {


    public final static String dataBaseName = "General";
    public final static int version = 1;
    public final static String createMovimientos = "CREATE TABLE  General " +
            "(" +
            "   URL VARCHAR(50)," +
            "   DOCUMENTO VARCHAR(20)," +
            "   CONTEO VARCHAR(10)," +
            "   VALIDARCOD INTEGER default 0" +
            ")";



    public sqlGeneral(Context context) {
        super(context, dataBaseName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createMovimientos);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL("drop table General");
        db.execSQL(createMovimientos);
    }
}
