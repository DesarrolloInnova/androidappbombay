package innobile.itrace.SQL;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class sqlUsuarios extends SQLiteOpenHelper {


    public final static String dataBaseName = "Usuarios";
    public final static int version = 1;
    public final static String createUsuarios = "CREATE TABLE  Usuarios " +
            "(" +
            "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "   username VARCHAR(50)," +
            "   rol VARCHAR(50)," +
            "   password VARCHAR(50)" +
            ")";

    public sqlUsuarios(Context context) {
        super(context, dataBaseName, null, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createUsuarios);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table Usuarios");
        db.execSQL(createUsuarios);
    }
}
