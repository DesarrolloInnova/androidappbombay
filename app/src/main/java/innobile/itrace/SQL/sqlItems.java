package innobile.itrace.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BATMAN on 6/06/18.
 */



public class sqlItems extends SQLiteOpenHelper {


    public final static String dataBaseName = "Items";
    public final static int version = 1;
    public final static String createMovimientos = "CREATE TABLE  Items " +
            "(" +
            "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "   CODIGO VARCHAR(50)," +
            "   DESCRIPCION VARCHAR(150)," +
            "   CODIGOALTERNO VARCHAR(50)," +
            "   CANTIDAD INTEGER" +
            ")";



    public sqlItems(Context context) {
        super(context, dataBaseName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createMovimientos);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(createMovimientos);
    }
}
