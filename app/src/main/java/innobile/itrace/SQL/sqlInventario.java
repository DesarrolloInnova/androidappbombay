package innobile.itrace.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class sqlInventario extends SQLiteOpenHelper{


    public final static String dataBaseName = "InventarioActivity";
    public final static int version = 1;
    public final static String createMovimientos = "CREATE TABLE  Inventario " +
            "(" +
            "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "   USERNAME VARCHAR(50)," +
            "   DOCUMENTO VARCHAR(20)," +
            "   UBICACION VARCHAR(20)," +
            "   CODIGO VARCHAR(50)," +
            "   CONTEO VARCHAR(10)," +
            "   CANTIDAD INTEGER," +
            "   ESTADO VARCHAR(5)," +
            "   FECHA VARCHAR(30)" +
            ")";



    public sqlInventario(Context context) {
        super(context, dataBaseName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createMovimientos);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("drop table Operaciones");
        db.execSQL(createMovimientos);
    }
}
