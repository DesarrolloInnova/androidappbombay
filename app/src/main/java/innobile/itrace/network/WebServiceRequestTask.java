package innobile.itrace.network;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by danielgil on 6/7/15.
 */
public class WebServiceRequestTask extends AsyncTask<String, String, String> {

    public WebServiceRequestTask() {

    }

    final static int WEBSERVICE_TIMEOUT = 3000;


    @Override
    protected String doInBackground(String... params) {
        String result = "";
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(params[0]).openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setConnectTimeout(WEBSERVICE_TIMEOUT);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(params[1]);
            wr.flush();
            wr.close();
            int responseCode = conn.getResponseCode();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
            }
            bufferedReader.close();
            result = response.toString();
        } catch (Exception e) {
            String x = "";
        }

        return result;
    }


}
