package innobile.itrace.transport;
/**
 * Created by jaidiver on 1/29/18.
 */

public class OperationResult {

    private int result = 0;
    private String message = "";

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
