package innobile.itrace.transport;

/**
 * Created by jaidiver on 7/15/18.
 */

public class ItemWS {

    private String mae_consecutivo_producto = "";
    private long mae_consecutivo_empresa = 1;
    private String mae_referencia_producto = "";
    private String mae_nombre_producto = "";
    private String mae_talla = "";
    private String mae_codigo_color = "";
    private String mae_color = "";
    private String mae_departamento = "";
    private String mae_mundo = "";
    private String mae_genero = "";
    private String mae_marca = "";
    private String mae_maneja_lote = "";
    private String mae_maneja_serial = "";
    private String mae_codigo_unidad_medida_inventario = "";
    private String mae_codigo_unidad_medida_recepcion = "";
    private double mae_volumen_producto = 0;
    private double mae_peso_producto=0;
    private double mae_porcentaje_entrada_extra = 0;
    private String mae_epc = "";
    private String mae_ubicacion = "";

    public String getMae_consecutivo_producto() {
        return mae_consecutivo_producto;
    }

    public void setMae_consecutivo_producto(String mae_consecutivo_producto) {
        this.mae_consecutivo_producto = mae_consecutivo_producto;
    }

    public long getMae_consecutivo_empresa() {
        return mae_consecutivo_empresa;
    }

    public void setMae_consecutivo_empresa(long mae_consecutivo_empresa) {
        this.mae_consecutivo_empresa = mae_consecutivo_empresa;
    }

    public String getMae_referencia_producto() {
        return mae_referencia_producto;
    }

    public void setMae_referencia_producto(String mae_referencia_producto) {
        this.mae_referencia_producto = mae_referencia_producto;
    }

    public String getMae_nombre_producto() {
        return mae_nombre_producto;
    }

    public void setMae_nombre_producto(String mae_nombre_producto) {
        this.mae_nombre_producto = mae_nombre_producto;
    }

    public String getMae_talla() {
        return mae_talla;
    }

    public void setMae_talla(String mae_talla) {
        this.mae_talla = mae_talla;
    }

    public String getMae_codigo_color() {
        return mae_codigo_color;
    }

    public void setMae_codigo_color(String mae_codigo_color) {
        this.mae_codigo_color = mae_codigo_color;
    }

    public String getMae_color() {
        return mae_color;
    }

    public void setMae_color(String mae_color) {
        this.mae_color = mae_color;
    }

    public String getMae_departamento() {
        return mae_departamento;
    }

    public void setMae_departamento(String mae_departamento) {
        this.mae_departamento = mae_departamento;
    }

    public String getMae_mundo() {
        return mae_mundo;
    }

    public void setMae_mundo(String mae_mundo) {
        this.mae_mundo = mae_mundo;
    }

    public String getMae_genero() {
        return mae_genero;
    }

    public void setMae_genero(String mae_genero) {
        this.mae_genero = mae_genero;
    }

    public String getMae_marca() {
        return mae_marca;
    }

    public void setMae_marca(String mae_marca) {
        this.mae_marca = mae_marca;
    }

    public String getMae_maneja_lote() {
        return mae_maneja_lote;
    }

    public void setMae_maneja_lote(String mae_maneja_lote) {
        this.mae_maneja_lote = mae_maneja_lote;
    }

    public String getMae_maneja_serial() {
        return mae_maneja_serial;
    }

    public void setMae_maneja_serial(String mae_maneja_serial) {
        this.mae_maneja_serial = mae_maneja_serial;
    }

    public String getMae_codigo_unidad_medida_inventario() {
        return mae_codigo_unidad_medida_inventario;
    }

    public void setMae_codigo_unidad_medida_inventario(String mae_codigo_unidad_medida_inventario) {
        this.mae_codigo_unidad_medida_inventario = mae_codigo_unidad_medida_inventario;
    }

    public String getMae_codigo_unidad_medida_recepcion() {
        return mae_codigo_unidad_medida_recepcion;
    }

    public void setMae_codigo_unidad_medida_recepcion(String mae_codigo_unidad_medida_recepcion) {
        this.mae_codigo_unidad_medida_recepcion = mae_codigo_unidad_medida_recepcion;
    }

    public double getMae_volumen_producto() {
        return mae_volumen_producto;
    }

    public void setMae_volumen_producto(double mae_volumen_producto) {
        this.mae_volumen_producto = mae_volumen_producto;
    }

    public double getMae_peso_producto() {
        return mae_peso_producto;
    }

    public void setMae_peso_producto(double mae_peso_producto) {
        this.mae_peso_producto = mae_peso_producto;
    }

    public double getMae_porcentaje_entrada_extra() {
        return mae_porcentaje_entrada_extra;
    }

    public void setMae_porcentaje_entrada_extra(double mae_porcentaje_entrada_extra) {
        this.mae_porcentaje_entrada_extra = mae_porcentaje_entrada_extra;
    }

    public String getMae_epc() {
        return mae_epc;
    }

    public void setMae_epc(String mae_epc) {
        this.mae_epc = mae_epc;
    }

    public String getMae_ubicacion() {
        return mae_ubicacion;
    }

    public void setMae_ubicacion(String mae_ubicacion) {
        this.mae_ubicacion = mae_ubicacion;
    }
}
