package innobile.itrace.transport;


import android.content.Context;
import android.widget.Toast;

/**
 * Created by Dev1 on 10/11/15.
 */
public class T_Configuraciones {

    private int con_consecutivo = 0;
    //  private Boolean con_TipoConexion  ;
  private String con_TipoConexion  ;
    private int con_TipoImpresion ;
    private String con_URL = "";
    private String con_URLMongo = "";
    private String con_Usuario = "";

    public String getCon_Usuario() {
        return con_Usuario;
    }

    public void setCon_Usuario(String con_Usuario) {
        this.con_Usuario = con_Usuario;
    }



    public int getCon_TipoImpresion() {
        return con_TipoImpresion;
    }

    public void setCon_TipoImpresion(int con_TipoImpresion) {
        this.con_TipoImpresion = con_TipoImpresion;
    }

    public String getCon_TipoConexion() {
        return con_TipoConexion;
    }

    public void setCon_TipoConexion(String con_TipoConexion) {
        this.con_TipoConexion = con_TipoConexion;
    }



    public String getCon_URL() {
        return con_URL;
    }

    public void setCon_URL(String con_URL) {
        this.con_URL = con_URL;
    }

    public String getCon_URLMongo() {
        return con_URLMongo;
    }

    public void setCon_URLMongo(String con_URLMongo) {
        this.con_URLMongo = con_URLMongo;
    }


    public int getCon_consecutivo() {
        return con_consecutivo;
    }

    public void setCon_consecutivo(int con_consecutivo) {
        this.con_consecutivo = con_consecutivo;
    }

    public T_Configuraciones() {


    }
}
