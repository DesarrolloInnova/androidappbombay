package innobile.itrace.transport;

/**
 * Created by jaidiver on 7/14/18.
 */

public class RequestJsonUsuarios {

    private String usuario = "";

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
