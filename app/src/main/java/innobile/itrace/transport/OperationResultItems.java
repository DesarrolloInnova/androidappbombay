package innobile.itrace.transport;

import java.util.ArrayList;

/**
 * Created by jaidiver on 7/14/18.
 */

public class OperationResultItems extends OperationResult {

    private ArrayList<ItemWS> items = new ArrayList<>();

    public ArrayList<ItemWS> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemWS> items) {
        this.items = items;
    }
}
