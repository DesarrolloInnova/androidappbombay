package innobile.itrace.transport;

import java.util.ArrayList;

/**
 * Created by jaidiver on 7/12/18.
 */

public class JSONInventarioLeido {

    private ArrayList<Inventario_leido> inventarioleido = new ArrayList<Inventario_leido>();
    private String usuario = "";

    public ArrayList<Inventario_leido> getInventarioleido() {
        return inventarioleido;
    }

    public void setInventarioleido(ArrayList<Inventario_leido> inventarioleido) {
        this.inventarioleido = inventarioleido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
