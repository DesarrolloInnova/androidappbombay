package innobile.itrace.transport;

import java.util.ArrayList;

/**
 * Created by jaidiver on 7/14/18.
 */

public class OperationResultUsuarios extends OperationResult {

    private ArrayList<UsuarioWS> colResult = new ArrayList<>();

    public ArrayList<UsuarioWS> getColResult() {
        return colResult;
    }

    public void setColResult(ArrayList<UsuarioWS> colResult) {
        this.colResult = colResult;
    }
}
