package innobile.itrace.transport;

import java.util.Date;

/**
 * Created by jaidiver on 7/12/18.
 */

public class Inventario_leido {

    private int inv_codigo_empresa = 0;
    private String inv_conteo = "";
    private String inv_ubicacion = "";
    private String inv_producto = "";
    private String inv_referencia = "";
    private String inv_talla = "";
    private String inv_color = "";
    private String inv_lote = "";
    private String inv_serial = "";
    private double inv_cantidad = 0;
    private String inv_documento = "";
    private String inv_usuario = "";
    private Date inv_fecha = null;


    public int getInv_codigo_empresa() {
        return inv_codigo_empresa;
    }

    public void setInv_codigo_empresa(int inv_codigo_empresa) {
        this.inv_codigo_empresa = inv_codigo_empresa;
    }

    public String getInv_conteo() {
        return inv_conteo;
    }

    public void setInv_conteo(String inv_conteo) {
        this.inv_conteo = inv_conteo;
    }

    public String getInv_ubicacion() {
        return inv_ubicacion;
    }

    public void setInv_ubicacion(String inv_ubicacion) {
        this.inv_ubicacion = inv_ubicacion;
    }

    public String getInv_producto() {
        return inv_producto;
    }

    public void setInv_producto(String inv_producto) {
        this.inv_producto = inv_producto;
    }

    public String getInv_referencia() {
        return inv_referencia;
    }

    public void setInv_referencia(String inv_referencia) {
        this.inv_referencia = inv_referencia;
    }

    public String getInv_talla() {
        return inv_talla;
    }

    public void setInv_talla(String inv_talla) {
        this.inv_talla = inv_talla;
    }

    public String getInv_color() {
        return inv_color;
    }

    public void setInv_color(String inv_color) {
        this.inv_color = inv_color;
    }

    public String getInv_lote() {
        return inv_lote;
    }

    public void setInv_lote(String inv_lote) {
        this.inv_lote = inv_lote;
    }

    public String getInv_serial() {
        return inv_serial;
    }

    public void setInv_serial(String inv_serial) {
        this.inv_serial = inv_serial;
    }

    public double getInv_cantidad() {
        return inv_cantidad;
    }

    public void setInv_cantidad(double inv_cantidad) {
        this.inv_cantidad = inv_cantidad;
    }

    public String getInv_documento() {
        return inv_documento;
    }

    public void setInv_documento(String inv_documento) {
        this.inv_documento = inv_documento;
    }

    public String getInv_usuario() {
        return inv_usuario;
    }

    public void setInv_usuario(String inv_usuario) {
        this.inv_usuario = inv_usuario;
    }

    public Date getInv_fecha() {
        return inv_fecha;
    }

    public void setInv_fecha(Date inv_fecha) {
        this.inv_fecha = inv_fecha;
    }
}
