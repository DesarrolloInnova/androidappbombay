package innobile.itrace.transport;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Bean (Clase) que representa la estructura del activo
 */
public class IFA_activos implements Parcelable {

    private String result = "";
    private String message = "";

    private String act_codigo_activo = "";
    private String act_descripcion = "";
    private String act_codigo_barras = "";
    private String act_fecha_compra = "";
    private int act_meses_depreciacion = 0;
    private String act_area = "";
    private String act_grupo = "";
    private String act_ciclo_vida = "";
    private String act_mantenimiento = "";
    private String act_centro_costos = "";
    private double act_valor_compra = 0;
    private double act_valor_iva_compra = 0;
    private String act_cuenta_contable = "";
    private String act_nombre_cuenta_contable = "";
    private String act_ciudad = "";
    private String act_piso = "";
    private String act_ubicacion = "";
    private String act_tipo_ubicacion = "";
    private String act_serial = "";
    private String act_color = "";
    private String act_modelo = "";
    private String act_tipo_activo = "";
    private String act_material = "";
    private String act_estado = "";
    private String act_fabricante = "";
    private String act_empresa_propietaria = "";
    private String act_nombre_maquina = "";
    private String act_sistema_operativo = "";
    private String act_servidor = "";
    private String act_ultima_fecha_reporte_agente = "";
    private String act_observaciones = "";
    private String act_tag_activo = "";
    private String act_responsable = "";
    private String act_nombre_responsable = "";
    private String act_direccion_ubicacion = "";
    private String act_canal = "";
    private String act_en_red = "";
    private String act_usuario_red = "";
    private String act_fecha_ingreso_bodega = "";
    private String act_oc = "";
    private String act_service_pack = "";
    private String act_procesador = "";
    private String act_velocidad = "";
    private String act_disco_duro = "";
    private String act_total_memoria = "";
    private String act_codigo_conteo = "";

    public IFA_activos() {

    }

    public IFA_activos(String act_codigo_activo, String act_descripcion, String act_codigo_barras, String act_fecha_compra,
                       String act_meses_depreciacion, String act_area, String act_grupo, String act_ciclo_vida, String act_mantenimiento,
                       String act_centro_costos, double act_valor_compra, double act_valor_iva_compra, String act_cuenta_contable,
                       String act_nombre_cuenta_contable, String act_ciudad, String act_piso, String act_ubicacion, String act_tipo_ubicacion,
                       String act_serial, String act_color, String act_modelo, String act_tipo_activo, String act_material, String act_estado,
                       String act_fabricante, String act_empresa_propietaria, String act_nombre_maquina, String act_sistema_operativo,
                       String act_servidor, String act_ultima_fecha_reporte_agente, String act_observaciones, String act_tag_activo,
                       String act_responsable, String act_direccion_ubicacion, String act_canal, String act_en_red,
                       String act_usuario_red, String act_fecha_ingreso_bodega, String act_oc, String act_service_pack, String act_procesador,
                       String act_velocidad, String act_disco_duro, String act_total_memoria, String act_codigo_conteo) {
        this.setAct_codigo_activo(act_codigo_activo);
        this.setAct_descripcion(act_descripcion);
        this.setAct_codigo_barras(act_codigo_barras);
        this.setAct_fecha_compra(act_fecha_compra);
        try {
            this.setAct_meses_depreciacion(Integer.parseInt(act_meses_depreciacion));
        } catch (Exception ex) {
            this.setAct_meses_depreciacion(-1);
        }
        this.setAct_area(act_area);
        this.setAct_grupo(act_grupo);
        this.setAct_ciclo_vida(act_ciclo_vida);
        this.setAct_mantenimiento(act_mantenimiento);
        this.setAct_centro_costos(act_centro_costos);
        this.setAct_valor_compra(act_valor_compra);
        this.setAct_valor_iva_compra(act_valor_iva_compra);
        this.setAct_cuenta_contable(act_cuenta_contable);
        this.setAct_nombre_cuenta_contable(act_nombre_cuenta_contable);
        this.setAct_ciudad(act_ciudad);
        this.setAct_piso(act_piso);
        this.setAct_ubicacion(act_ubicacion);
        this.setAct_tipo_ubicacion(act_tipo_ubicacion);
        this.setAct_serial(act_serial);
        this.setAct_color(act_color);
        this.setAct_modelo(act_modelo);
        this.setAct_tipo_activo(act_tipo_activo);
        this.setAct_material(act_material);
        this.setAct_estado(act_estado);
        this.setAct_fabricante(act_fabricante);
        this.setAct_empresa_propietaria(act_empresa_propietaria);
        this.setAct_nombre_maquina(act_nombre_maquina);
        this.setAct_sistema_operativo(act_sistema_operativo);
        this.setAct_servidor(act_servidor);
        this.setAct_ultima_fecha_reporte_agente(act_ultima_fecha_reporte_agente);
        this.setAct_observaciones(act_observaciones);
        this.setAct_tag_activo(act_tag_activo);
        this.setAct_responsable(act_responsable);
        this.setAct_direccion_ubicacion(act_direccion_ubicacion);
        this.setAct_canal(act_canal);
        this.setAct_en_red(act_en_red);
        this.setAct_usuario_red(act_usuario_red);
        this.setAct_fecha_ingreso_bodega(act_fecha_ingreso_bodega);
        this.setAct_oc(act_oc);
        this.setAct_service_pack(act_service_pack);
        this.setAct_procesador(act_procesador);
        this.setAct_velocidad(act_velocidad);
        this.setAct_disco_duro(act_disco_duro);
        this.setAct_total_memoria(act_total_memoria);
        this.setAct_codigo_conteo(act_codigo_conteo);
    }


    protected IFA_activos(Parcel in) {
        result = in.readString();
        message = in.readString();
        act_codigo_activo = in.readString();
        act_descripcion = in.readString();
        act_codigo_barras = in.readString();
        act_fecha_compra = in.readString();
        act_meses_depreciacion = in.readInt();
        act_area = in.readString();
        act_grupo = in.readString();
        act_ciclo_vida = in.readString();
        act_mantenimiento = in.readString();
        act_centro_costos = in.readString();
        act_valor_compra = in.readDouble();
        act_valor_iva_compra = in.readDouble();
        act_cuenta_contable = in.readString();
        act_nombre_cuenta_contable = in.readString();
        act_ciudad = in.readString();
        act_piso = in.readString();
        act_ubicacion = in.readString();
        act_tipo_ubicacion = in.readString();
        act_serial = in.readString();
        act_color = in.readString();
        act_modelo = in.readString();
        act_tipo_activo = in.readString();
        act_material = in.readString();
        act_estado = in.readString();
        act_fabricante = in.readString();
        act_empresa_propietaria = in.readString();
        act_nombre_maquina = in.readString();
        act_sistema_operativo = in.readString();
        act_servidor = in.readString();
        act_ultima_fecha_reporte_agente = in.readString();
        act_observaciones = in.readString();
        act_tag_activo = in.readString();
        act_responsable = in.readString();
        act_nombre_responsable = in.readString();
        act_direccion_ubicacion = in.readString();
        act_canal = in.readString();
        act_en_red = in.readString();
        act_usuario_red = in.readString();
        act_fecha_ingreso_bodega = in.readString();
        act_oc = in.readString();
        act_service_pack = in.readString();
        act_procesador = in.readString();
        act_velocidad = in.readString();
        act_disco_duro = in.readString();
        act_total_memoria = in.readString();
        act_codigo_conteo = in.readString();
    }

    public static final Creator<IFA_activos> CREATOR = new Creator<IFA_activos>() {
        @Override
        public IFA_activos createFromParcel(Parcel in) {
            return new IFA_activos(in);
        }

        @Override
        public IFA_activos[] newArray(int size) {
            return new IFA_activos[size];
        }
    };

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAct_codigo_activo() {
        return act_codigo_activo;
    }

    public void setAct_codigo_activo(String act_codigo_activo) {
        this.act_codigo_activo = act_codigo_activo;
    }

    public String getAct_descripcion() {
        return act_descripcion;
    }

    public void setAct_descripcion(String act_descripcion) {
        this.act_descripcion = act_descripcion;
    }

    public String getAct_codigo_barras() {
        return act_codigo_barras;
    }

    public void setAct_codigo_barras(String act_codigo_barras) {
        this.act_codigo_barras = act_codigo_barras;
    }

    public String getAct_fecha_compra() {
        return act_fecha_compra;
    }

    public void setAct_fecha_compra(String act_fecha_compra) {
        this.act_fecha_compra = act_fecha_compra;
    }

    public int getAct_meses_depreciacion() {
        return act_meses_depreciacion;
    }

    public void setAct_meses_depreciacion(int act_meses_depreciacion) {
        this.act_meses_depreciacion = act_meses_depreciacion;
    }

    public String getAct_area() {
        return act_area;
    }

    public void setAct_area(String act_area) {
        this.act_area = act_area;
    }

    public String getAct_grupo() {
        return act_grupo;
    }

    public void setAct_grupo(String act_grupo) {
        this.act_grupo = act_grupo;
    }

    public String getAct_ciclo_vida() {
        return act_ciclo_vida;
    }

    public void setAct_ciclo_vida(String act_ciclo_vida) {
        this.act_ciclo_vida = act_ciclo_vida;
    }

    public String getAct_mantenimiento() {
        return act_mantenimiento;
    }

    public void setAct_mantenimiento(String act_mantenimiento) {
        this.act_mantenimiento = act_mantenimiento;
    }

    public String getAct_centro_costos() {
        return act_centro_costos;
    }

    public void setAct_centro_costos(String act_centro_costos) {
        this.act_centro_costos = act_centro_costos;
    }

    public double getAct_valor_compra() {
        return act_valor_compra;
    }

    public void setAct_valor_compra(double act_valor_compra) {
        this.act_valor_compra = act_valor_compra;
    }

    public double getAct_valor_iva_compra() {
        return act_valor_iva_compra;
    }

    public void setAct_valor_iva_compra(double act_valor_iva_compra) {
        this.act_valor_iva_compra = act_valor_iva_compra;
    }

    public String getAct_cuenta_contable() {
        return act_cuenta_contable;
    }

    public void setAct_cuenta_contable(String act_cuenta_contable) {
        this.act_cuenta_contable = act_cuenta_contable;
    }

    public String getAct_nombre_cuenta_contable() {
        return act_nombre_cuenta_contable;
    }

    public void setAct_nombre_cuenta_contable(String act_nombre_cuenta_contable) {
        this.act_nombre_cuenta_contable = act_nombre_cuenta_contable;
    }

    public String getAct_ciudad() {
        return act_ciudad;
    }

    public void setAct_ciudad(String act_ciudad) {
        this.act_ciudad = act_ciudad;
    }

    public String getAct_piso() {
        return act_piso;
    }

    public void setAct_piso(String act_piso) {
        this.act_piso = act_piso;
    }

    public String getAct_ubicacion() {
        return act_ubicacion;
    }

    public void setAct_ubicacion(String act_ubicacion) {
        this.act_ubicacion = act_ubicacion;
    }

    public String getAct_tipo_ubicacion() {
        return act_tipo_ubicacion;
    }

    public void setAct_tipo_ubicacion(String act_tipo_ubicacion) {
        this.act_tipo_ubicacion = act_tipo_ubicacion;
    }

    public String getAct_serial() {
        return act_serial;
    }

    public void setAct_serial(String act_serial) {
        this.act_serial = act_serial;
    }

    public String getAct_color() {
        return act_color;
    }

    public void setAct_color(String act_color) {
        this.act_color = act_color;
    }

    public String getAct_modelo() {
        return act_modelo;
    }

    public void setAct_modelo(String act_modelo) {
        this.act_modelo = act_modelo;
    }

    public String getAct_tipo_activo() {
        return act_tipo_activo;
    }

    public void setAct_tipo_activo(String act_tipo_activo) {
        this.act_tipo_activo = act_tipo_activo;
    }

    public String getAct_material() {
        return act_material;
    }

    public void setAct_material(String act_material) {
        this.act_material = act_material;
    }

    public String getAct_estado() {
        return act_estado;
    }

    public void setAct_estado(String act_estado) {
        this.act_estado = act_estado;
    }

    public String getAct_fabricante() {
        return act_fabricante;
    }

    public void setAct_fabricante(String act_fabricante) {
        this.act_fabricante = act_fabricante;
    }

    public String getAct_empresa_propietaria() {
        return act_empresa_propietaria;
    }

    public void setAct_empresa_propietaria(String act_empresa_propietaria) {
        this.act_empresa_propietaria = act_empresa_propietaria;
    }

    public String getAct_nombre_maquina() {
        return act_nombre_maquina;
    }

    public void setAct_nombre_maquina(String act_nombre_maquina) {
        this.act_nombre_maquina = act_nombre_maquina;
    }

    public String getAct_sistema_operativo() {
        return act_sistema_operativo;
    }

    public void setAct_sistema_operativo(String act_sistema_operativo) {
        this.act_sistema_operativo = act_sistema_operativo;
    }

    public String getAct_servidor() {
        return act_servidor;
    }

    public void setAct_servidor(String act_servidor) {
        this.act_servidor = act_servidor;
    }

    public String getAct_ultima_fecha_reporte_agente() {
        return act_ultima_fecha_reporte_agente;
    }

    public void setAct_ultima_fecha_reporte_agente(String act_ultima_fecha_reporte_agente) {
        this.act_ultima_fecha_reporte_agente = act_ultima_fecha_reporte_agente;
    }

    public String getAct_observaciones() {
        return act_observaciones;
    }

    public void setAct_observaciones(String act_observaciones) {
        this.act_observaciones = act_observaciones;
    }

    public String getAct_tag_activo() {
        return act_tag_activo;
    }

    public void setAct_tag_activo(String act_tag_activo) {
        this.act_tag_activo = act_tag_activo;
    }

    public String getAct_responsable() {
        return act_responsable;
    }

    public void setAct_responsable(String act_responsable) {
        this.act_responsable = act_responsable;
    }

    public String getAct_nombre_responsable() {
        return act_nombre_responsable;
    }

    public void setAct_nombre_responsable(String act_nombre_responsable) {
        this.act_nombre_responsable = act_nombre_responsable;
    }

    public String getAct_direccion_ubicacion() {
        return act_direccion_ubicacion;
    }

    public void setAct_direccion_ubicacion(String act_direccion_ubicacion) {
        this.act_direccion_ubicacion = act_direccion_ubicacion;
    }

    public String getAct_canal() {
        return act_canal;
    }

    public void setAct_canal(String act_canal) {
        this.act_canal = act_canal;
    }

    public String getAct_en_red() {
        return act_en_red;
    }

    public void setAct_en_red(String act_en_red) {
        this.act_en_red = act_en_red;
    }

    public String getAct_usuario_red() {
        return act_usuario_red;
    }

    public void setAct_usuario_red(String act_usuario_red) {
        this.act_usuario_red = act_usuario_red;
    }

    public String getAct_fecha_ingreso_bodega() {
        return act_fecha_ingreso_bodega;
    }

    public void setAct_fecha_ingreso_bodega(String act_fecha_ingreso_bodega) {
        this.act_fecha_ingreso_bodega = act_fecha_ingreso_bodega;
    }

    public String getAct_oc() {
        return act_oc;
    }

    public void setAct_oc(String act_oc) {
        this.act_oc = act_oc;
    }

    public String getAct_service_pack() {
        return act_service_pack;
    }

    public void setAct_service_pack(String act_service_pack) {
        this.act_service_pack = act_service_pack;
    }

    public String getAct_procesador() {
        return act_procesador;
    }

    public void setAct_procesador(String act_procesador) {
        this.act_procesador = act_procesador;
    }

    public String getAct_velocidad() {
        return act_velocidad;
    }

    public void setAct_velocidad(String act_velocidad) {
        this.act_velocidad = act_velocidad;
    }

    public String getAct_disco_duro() {
        return act_disco_duro;
    }

    public void setAct_disco_duro(String act_disco_duro) {
        this.act_disco_duro = act_disco_duro;
    }

    public String getAct_total_memoria() {
        return act_total_memoria;
    }

    public void setAct_total_memoria(String act_total_memoria) {
        this.act_total_memoria = act_total_memoria;
    }

    public String getAct_codigo_conteo() {
        return act_codigo_conteo;
    }

    public void setAct_codigo_conteo(String act_codigo_conteo) {
        this.act_codigo_conteo = act_codigo_conteo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result);
        dest.writeString(message);
        dest.writeString(act_codigo_activo);
        dest.writeString(act_descripcion);
        dest.writeString(act_codigo_barras);
        dest.writeString(act_fecha_compra);
        dest.writeInt(act_meses_depreciacion);
        dest.writeString(act_area);
        dest.writeString(act_grupo);
        dest.writeString(act_ciclo_vida);
        dest.writeString(act_mantenimiento);
        dest.writeString(act_centro_costos);
        dest.writeDouble(act_valor_compra);
        dest.writeDouble(act_valor_iva_compra);
        dest.writeString(act_cuenta_contable);
        dest.writeString(act_nombre_cuenta_contable);
        dest.writeString(act_ciudad);
        dest.writeString(act_piso);
        dest.writeString(act_ubicacion);
        dest.writeString(act_tipo_ubicacion);
        dest.writeString(act_serial);
        dest.writeString(act_color);
        dest.writeString(act_modelo);
        dest.writeString(act_tipo_activo);
        dest.writeString(act_material);
        dest.writeString(act_estado);
        dest.writeString(act_fabricante);
        dest.writeString(act_empresa_propietaria);
        dest.writeString(act_nombre_maquina);
        dest.writeString(act_sistema_operativo);
        dest.writeString(act_servidor);
        dest.writeString(act_ultima_fecha_reporte_agente);
        dest.writeString(act_observaciones);
        dest.writeString(act_tag_activo);
        dest.writeString(act_responsable);
        dest.writeString(act_nombre_responsable);
        dest.writeString(act_direccion_ubicacion);
        dest.writeString(act_canal);
        dest.writeString(act_en_red);
        dest.writeString(act_usuario_red);
        dest.writeString(act_fecha_ingreso_bodega);
        dest.writeString(act_oc);
        dest.writeString(act_service_pack);
        dest.writeString(act_procesador);
        dest.writeString(act_velocidad);
        dest.writeString(act_disco_duro);
        dest.writeString(act_total_memoria);
        dest.writeString(act_codigo_conteo);
    }
}
