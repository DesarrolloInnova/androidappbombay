package innobile.itrace.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import innobile.itrace.DAO.DAOInventario;
import innobile.itrace.DAO.daoItems;
import innobile.itrace.DAO.daoGeneral;
import innobile.itrace.modelo.ModGeneral;
import innobile.itrace.R;
import innobile.itrace.view.MenuFragment;

public class GeneralActivity extends AppCompatActivity {

    private EditText txtURL, txtDocumento, txtConteo;
    private TextView lblTotalREF;
    private Button btnGuardar, btnIniciar;
    private String username = "user";
    private Switch swValidarCodigo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);

        txtDocumento= findViewById(R.id.txtDocumento);
        txtConteo = findViewById(R.id.txtConteo);
        lblTotalREF = findViewById(R.id.lblTotalREF);
        btnGuardar = findViewById(R.id.btnGuardar);
        swValidarCodigo = findViewById(R.id.swValidarCodigo);
        swValidarCodigo.setChecked(false);

        MostrarRegistro();

        btnGuardar.setOnClickListener(view -> ActualizarRegistro());
        btnIniciar.setOnClickListener(view -> MostrarMensanjeBorrarArchivo("Advertencia","Se borraran Todos los registros del InventarioActivity. ¿Desea continuar?."));
    }

    private void MostrarRegistro() {

        ModGeneral ModGen = new ModGeneral();
        ModGen = daoGeneral.CapturarRegistros(getApplicationContext());

        txtDocumento.setText(ModGen.getDocumento());
        txtURL.setText(ModGen.getUrl());
        txtConteo.setText(ModGen.getConteo());

        if (ModGen.getValidarCod().equals(1))
        {
            swValidarCodigo.setChecked(true);
        }

        TotalRegistros();

    }

    private void TotalRegistros() {

        int TotalxCodigo =  daoItems.CapturarCantidadTotal(getApplicationContext());
        lblTotalREF.setText(String.valueOf(TotalxCodigo));
    }
    //lblTotalREF.setText();

    private void ActualizarRegistro() {

        ModGeneral ModGen = new ModGeneral();

        if(txtURL.getText().toString().trim().isEmpty()) {
            ModGen.setUrl("http://dev.ilogisticapp.com");
            txtURL.setText("http://dev.ilogisticapp.com");
        }else{
            ModGen.setUrl(txtURL.getText().toString());
        }
        ModGen.setDocumento(txtDocumento.getText().toString());
        ModGen.setConteo(txtConteo.getText().toString());

        if (swValidarCodigo.isChecked()) ModGen.setValidarCod (1);
        else  ModGen.setValidarCod (0);

        try {

            if (daoGeneral.RegistroExistente(getApplicationContext())) {

                if (!daoGeneral.ActualizarRegistros(getBaseContext(), ModGen )) {
                    Toast.makeText(getApplicationContext(), "Dato no Actualizado", Toast.LENGTH_SHORT).show();
                }else Toast.makeText(getApplicationContext(), "Dato Actualizado", Toast.LENGTH_SHORT).show();
            }else
            {
                if (!daoGeneral.GuardarRegistros(getBaseContext(), ModGen)) {
                    Toast.makeText(getApplicationContext(), "Dato no Guardado", Toast.LENGTH_SHORT).show();
                }else Toast.makeText(getApplicationContext(), "Dato  Guardado", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void MostrarMensanjeBorrarArchivo(String titulo, String mensaje){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        builder.setCancelable(false);
        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setPositiveButton("Continuar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        BorrarRegistros();


                    }
                });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void BorrarRegistros() {
        try {

            // DAOInventario DAOInv= new DAOInventario();
            DAOInventario.BorrarRegistros(getApplicationContext());
            Toast t = Toast.makeText(this, "Los datos fueron borrados",
                    Toast.LENGTH_SHORT);
            t.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
