package innobile.itrace.Activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import innobile.itrace.DAO.DAOInventario;
import innobile.itrace.DAO.daoGeneral;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.modelo.Cons;
import innobile.itrace.modelo.ModeloInventario;
import innobile.itrace.modelo.NetworkManager;
import innobile.itrace.modelo.ModGeneral;
import innobile.itrace.R;
import innobile.itrace.transport.Inventario_leido;
import innobile.itrace.transport.JSONInventarioLeido;
import innobile.itrace.transport.OperationResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class SincronizacionActivity extends AppCompatActivity implements NetworkManager.OnNetworkActionListener {

    private Button btnLecturas, btnMaestros, btnSalir;
    private String username, nombreArchivo;
    private View mProgressView;
    private View mInventarioFormView;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sincronizacion);

        Intent intent = getIntent();
       // username = intent.getExtras().getString("Usuario");

        /*
        btnLecturas = findViewById(R.id.btnLecturas);

        btnLecturas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncInventario();

            }
        });

         */

        btnMaestros = findViewById(R.id.btnMaestros);

        btnMaestros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SincronizacionActivity.this, ConfiguracionActivity.class);
                intent.putExtra("Usuario", username);
                // intent.putExtra("vdocumento",documento);
                // intent.putExtra("vurl",url);
                // intent.putExtra("vconteo",conteo );
                startActivity(intent);

            }
        });
        btnSalir = findViewById(R.id.btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(SincronizacionActivity.this, MenuActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // <- Aquí :)
                //startActivity(intent);
                finish();
            }
        });

        mInventarioFormView = findViewById(R.id.inventario_form);
        mProgressView = findViewById(R.id.inventario_progress);
    }

    /*
    private void syncInventario() {

        String errorMessage = "";
        try {

            showProgress(true);

            ArrayList<Registros> inventarioArrayList = DAOInventario.CapturasListaLecturas(this);

            if (inventarioArrayList.size() == 0) {
                showProgress(false);

                Toast.makeText(this, R.string.sync_no_lecturas, Toast.LENGTH_LONG).show();

            } else {

                String jsonInventarioLeido = getJsonInventario(inventarioArrayList);

                ModGeneral modGeneral = daoGeneral.CapturarRegistros(this);
                if(modGeneral.getUrl().trim().isEmpty()){
                    Toast.makeText(this, R.string.sync_no_lecturas, Toast.LENGTH_LONG).show();
                }else {
                    NetworkManager networkManager = NetworkManager.newInstance(this);
                    networkManager.addPostRequest(this, modGeneral.getUrl()+ Cons.urlInventario, jsonInventarioLeido);
                }

            }

        } catch (Error error) {
            showProgress(false);
            errorMessage = error.getMessage();
        } catch (Exception ex) {
            showProgress(false);
            errorMessage = ex.getMessage();
        }

        if (!errorMessage.isEmpty()) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

            dlgAlert.setMessage(getResources().getString(R.string.action_sync_error) + errorMessage);
            dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
    }
*/
    /**
     * Shows the progress UI and hides the slotmachines form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mInventarioFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mInventarioFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mInventarioFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mInventarioFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private String getJsonInventario(ArrayList<ModeloInventario> inventarioArrayList) {
        String jsonInventario = "";

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        JSONInventarioLeido jsonInventarioLeido = new JSONInventarioLeido();
        jsonInventarioLeido.setUsuario(username);
        jsonInventarioLeido.setInventarioleido(new ArrayList<Inventario_leido>());
        for (ModeloInventario modeloInventario : inventarioArrayList) {
            Inventario_leido inventario_leido = new Inventario_leido();
            inventario_leido.setInv_conteo(modeloInventario.getConteo());
            inventario_leido.setInv_ubicacion(modeloInventario.getUbicacion());
            inventario_leido.setInv_producto(modeloInventario.getCodigo());
            inventario_leido.setInv_cantidad(modeloInventario.getCantidad());
            inventario_leido.setInv_documento(modeloInventario.getDocumento());
            inventario_leido.setInv_usuario(username);
            try {
                inventario_leido.setInv_fecha(simpleDateFormat.parse(modeloInventario.getFecha()));
            }catch (Exception ex){
            }
            jsonInventarioLeido.getInventarioleido().add(inventario_leido);
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        jsonInventario = gson.toJson(jsonInventarioLeido);

        return jsonInventario;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        showProgress(false);
        Toast.makeText(this, R.string.error_operation + ": " + error.toString() + " -> " + error.getMessage() + " - " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        showProgress(false);

        int updated_yes = 1;
        try {

            OperationResult operationResult = getOperationResult(response);
            if (operationResult.getResult() == updated_yes) {
                DAOInventario.BorrarRegistros(this);
                Toast.makeText(this, R.string.succeed_operation, Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(this, R.string.error_operation + ": -> " + operationResult.getMessage(), Toast.LENGTH_LONG).show();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Error error) {
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public OperationResult getOperationResult(String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        return gson.fromJson(json, OperationResult.class);

    }
}
