package innobile.itrace.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioButton;


import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import innobile.itrace.DAO.daoGeneral;
import innobile.itrace.modelo.Cons;
import innobile.itrace.modelo.ExceptionManager;
import innobile.itrace.modelo.ModGeneral;
import innobile.itrace.modelo.ModUsuario;
import innobile.itrace.modelo.NetworkManager;
import innobile.itrace.R;
import innobile.itrace.DAO.daoUsuarios;
import innobile.itrace.modelo.ModItems;
import innobile.itrace.DAO.daoItems;
import innobile.itrace.transport.ItemWS;
import innobile.itrace.transport.OperationResultItems;
import innobile.itrace.transport.OperationResultUsuarios;
import innobile.itrace.transport.RequestJsonItems;
import innobile.itrace.transport.RequestJsonUsuarios;
import innobile.itrace.transport.UsuarioWS;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.util.Log;

public class ConfiguracionActivity extends AppCompatActivity implements NetworkManager.OnNetworkActionListener {

    boolean VRadio = false;
    private String username;
    private Button btnguardar, btnseleccionar;
    private Switch swSincronizacionLocal;
    private View mProgressView;
    private View mConfiguracionFormView;
    private int tipoSincronizacion = 0;
    private  TextView txtTotal;
    private static final String PROCESS_OK = "PROCESO OK";
    private static final String PROCESS_ERROR = "ERROR EN EL PROCESO";
    private  static ProgressDialog progressDialog;
    private int lNumeroLineas = 0;
    private int lNumeroLineasTotal = 0;
    /*private List listaNombresArchivos;
    private List listaRutasArchivos;
    private ArrayAdapter adaptador;
    private String directorioRaiz;
    private TextView carpetaActual;*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);


        progressDialog = new ProgressDialog(ConfiguracionActivity.this);
        progressDialog.setMessage("Procesando....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        Intent intent = getIntent();
        username = intent.getExtras().getString("Usuario");

        swSincronizacionLocal = findViewById(R.id.swSincronizacionLocal);

        swSincronizacionLocal.setChecked(true);

        btnguardar = findViewById(R.id.btnGuardar);

        btnseleccionar = findViewById(R.id.btnSeleccionar);

        txtTotal = findViewById(R.id.TxtTotal);

        /*btnseleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                directorioRaiz = Environment.getExternalStorageDirectory().getPath();

                verArchivosDirectorio(directorioRaiz);

            }
        });*/


        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VRadio) {
                   // CargarDatosUsuario(1000);

                    if (swSincronizacionLocal.isChecked()) {

                        CargarTotalUsuarios();
                        progressDialog.setMax(lNumeroLineas);

                        final SubirUsuarios downloadTask = new SubirUsuarios(ConfiguracionActivity.this);
                        downloadTask.execute("");

                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                downloadTask.cancel(true);
                            }
                        });



                    } else {}

                } else {
                  //  CargarDatosReferencia(1000);
                    if (swSincronizacionLocal.isChecked()) {

                        CargarTotalReferencia();
                        progressDialog.setMax(lNumeroLineas);


                        final SubirReferencias downloadTask = new SubirReferencias(ConfiguracionActivity.this);
                        downloadTask.execute("");

                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                downloadTask.cancel(true);
                            }
                        });


                    } else {}
                }

            }
        });

        mConfiguracionFormView = findViewById(R.id.configuracion_form);
        mProgressView = findViewById(R.id.configuracion_progress);



    }

    public void onRadioButtonClicked(View view) {

        // Is the button now checked?

        boolean checked = ((RadioButton) view).isChecked();

        // hacemos un case con lo que ocurre cada vez que pulsemos un botón

        switch (view.getId()) {
            case R.id.raUsuario:
                if (checked)
                    VRadio = true;
                break;
            case R.id.raReferencia:
                if (checked)
                    VRadio = false;
                break;

        }
    }

    public void CargarDatos1(int duracion) {
        final ProgressDialog dialog = ProgressDialog.show(ConfiguracionActivity.this, "", "Loading...", true, true);
        dialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                try {

                    daoUsuarios.BorrarUsuarios(getBaseContext());
                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                    final File file = new File(path, "Usuarios.txt");
                    FileInputStream FileInt = new FileInputStream(file);

                    InputStreamReader archivo = new InputStreamReader(FileInt);
                    //InputStream archivo = openFileInput("prueba_int.txt");
                    //InputStreamReader archivo = new InputStreamReader(openFileInput("prueba_int.txt"));
                    //archivo = getResources().openRawResource(R.raw.documentoprueba);
                    //InputStream archivo=getResources().openRawResource(R.raw.leccion10);
                    BufferedReader buffer = new BufferedReader(archivo);

                    try {

                        String linea = buffer.readLine();
                        ArrayList<ModUsuario> Lista = new ArrayList<ModUsuario>();

                        while ((linea) != null) {

                            String[] texto = linea.split("\t");
                            ModUsuario modelo = new ModUsuario();

                            modelo.setUsuario(texto[0]);
                            modelo.setPassword(texto[1]);
                            modelo.setRol("GEN");

                            Lista.add(modelo);
                            linea = buffer.readLine();
                        }
                        buffer.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        try {
                            if (archivo != null)
                                archivo.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {

                }


                dialog.dismiss();
            }
        }, duracion);
    }

    public void CargarDatosUsuario(int duracion) {
        if (swSincronizacionLocal.isChecked()) {

            final ProgressDialog dialog = ProgressDialog.show(ConfiguracionActivity.this, "", "Loading...", true, true);
            dialog.show();

            String errorMessage = "";

            try {
                showProgress(true);
                daoUsuarios.BorrarUsuarios(getBaseContext());
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                final File file = new File(path, "Usuarios.txt");
                FileInputStream FileInt = new FileInputStream(file);
                InputStreamReader archivo = new InputStreamReader(FileInt);

                //InputStream archivo = openFileInput("prueba_int.txt");
                // InputStreamReader archivo = new InputStreamReader(openFileInput("prueba_int.txt"));
                //  archivo = getResources().openRawResource(R.raw.documentoprueba);


                //InputStreamReader archivo = new InputStreamReader(getResources().openRawResource(R.raw.usuarios));
                BufferedReader buffer = new BufferedReader(archivo);

                try {

                    String linea = buffer.readLine();
                    ArrayList<ModUsuario> Lista = new ArrayList<ModUsuario>();

                    while ((linea) != null) {

                        String[] texto = linea.split("\t");
                        ModUsuario modelo = new ModUsuario();

                        modelo.setUsuario(texto[0]);
                        modelo.setPassword(texto[1]);
                        modelo.setRol("GEN");

                        try {

                            daoUsuarios.CrearUsuario(getBaseContext(), modelo);


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // Lista.add(modelo);
                        linea = buffer.readLine();
                    }
                    Toast.makeText(getApplicationContext(), "Usuarios Creados", Toast.LENGTH_SHORT).show();
                    buffer.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                } finally {
                    try {
                        if (archivo != null)
                            archivo.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());
                    }
                }


            }  //catch (IOException e) {
            // e.printStackTrace();
            //}
            catch (Exception ex) {
                ex.printStackTrace();
                errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
            } finally {

            }

            dialog.dismiss();

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

                dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }

        } else {

            String errorMessage = "";
            try {

                showProgress(true);

                String jsonUsuarios = getJsonUsuarios();

                ModGeneral modGeneral = daoGeneral.CapturarRegistros(this);
                if (modGeneral.getUrl().trim().isEmpty()) {
                    Toast.makeText(this, R.string.sync_no_url, Toast.LENGTH_LONG).show();
                } else {
                    tipoSincronizacion = Cons.tipoSincronizacionUsuarios;
                    NetworkManager networkManager = NetworkManager.newInstance(this);
                    networkManager.addPostRequest(this, modGeneral.getUrl() + Cons.urlUsuarios, jsonUsuarios);
                }


            } catch (Error error) {
                showProgress(false);
                errorMessage = error.getMessage();
            } catch (Exception ex) {
                showProgress(false);
                errorMessage = ex.getMessage();
            }

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

                dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
        }

    }




    private  class SubirReferencias extends AsyncTask<String, Integer, String> {



        private final WeakReference<ConfiguracionActivity> configuracionActivityWeakReference;

        public SubirReferencias (ConfiguracionActivity configuracionActivity) {
            super();
            this.configuracionActivityWeakReference= new java.lang.ref.WeakReference<ConfiguracionActivity>(configuracionActivity);
        }


        @Override
        protected String doInBackground(String... params) {
            lNumeroLineas = 0;
            String errorMessage = "";
            try {
                // showProgress(true);
                daoItems.BorrarItems(getBaseContext());
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                final File file = new File(path, "Referencias.txt");
                FileInputStream FileInt = new FileInputStream(file);
                InputStreamReader archivo = new InputStreamReader(FileInt);

                BufferedReader buffer = new BufferedReader(archivo);

                try {


                    String linea = buffer.readLine();
                    ArrayList<ModItems> Lista = new ArrayList<ModItems>();

                    while ((linea) != null) {


                        String[] texto = linea.split("\t");
                        Integer Vtex = texto.length;

                        ModItems modelo = new ModItems();

                        modelo.setCodigo(texto[0]);
                        modelo.setDescripcion(texto[1]);
                        if (Vtex.equals(3)) {

                            modelo.setCodigoAlterno(texto[2]);
                        }else
                        {
                            modelo.setCodigoAlterno("0");
                        }
                        modelo.setCantidad(0);
                        lNumeroLineas++;
                        publishProgress(lNumeroLineas);

                        try {

                            daoItems.CrearItems(getBaseContext(), modelo);




                        } catch (Exception ex) {
                            errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                            return PROCESS_ERROR;
                        }

                        // Lista.add(modelo);
                        linea = buffer.readLine();

                    }
                    Toast.makeText(getApplicationContext(), "Referencias Creadas", Toast.LENGTH_SHORT).show();

                    buffer.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return PROCESS_ERROR;

                } finally {
                    try {
                        if (archivo != null)
                            archivo.close();
                    } catch (IOException e) {
                        errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());
                        return PROCESS_ERROR;
                    }
                }


            }  //catch (IOException e) {
            // e.printStackTrace();
            //}
            catch (Exception ex) {

                errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                return PROCESS_ERROR;
            } finally {

            }

            // dialog.dismiss();

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ConfiguracionActivity.this);

                dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }

            return PROCESS_OK;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.d("onpostexecute", (configuracionActivityWeakReference.get() != null) + "");
            if (configuracionActivityWeakReference.get() != null && !configuracionActivityWeakReference.get().isFinishing()) {

                progressDialog.dismiss();
                txtTotal.setText(String.valueOf(lNumeroLineasTotal));
               // ActualizarRegistrosSubidosRef();

            /*if (PROCESS_OK.equals(result)) {
                Toast.makeText(this, "Process OK " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Process ERROR " + result, Toast.LENGTH_LONG).show();
            }*/
                /*AlertDialog alertDialog = new AlertDialog.Builder(configuracionActivityWeakReference.get()).create();
                alertDialog.setTitle(result);
                alertDialog.setMessage("On post execute");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();*/
            }

        }
    }

    private  class SubirUsuarios extends AsyncTask<String, Integer, String> {



        private final WeakReference<ConfiguracionActivity> configuracionActivityWeakReference;

        public SubirUsuarios (ConfiguracionActivity configuracionActivity) {
            super();
            this.configuracionActivityWeakReference= new java.lang.ref.WeakReference<ConfiguracionActivity>(configuracionActivity);
        }


        @Override
        protected String doInBackground(String... params) {
            lNumeroLineas = 0;
            String errorMessage = "";

            try {
                showProgress(true);
                daoUsuarios.BorrarUsuarios(getBaseContext());
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                final File file = new File(path, "Usuarios.txt");
                FileInputStream FileInt = new FileInputStream(file);
                InputStreamReader archivo = new InputStreamReader(FileInt);

                //InputStream archivo = openFileInput("prueba_int.txt");
                // InputStreamReader archivo = new InputStreamReader(openFileInput("prueba_int.txt"));
                //  archivo = getResources().openRawResource(R.raw.documentoprueba);


                //InputStreamReader archivo = new InputStreamReader(getResources().openRawResource(R.raw.usuarios));
                BufferedReader buffer = new BufferedReader(archivo);

                try {

                    String linea = buffer.readLine();
                    ArrayList<ModUsuario> Lista = new ArrayList<ModUsuario>();

                    while ((linea) != null) {

                        String[] texto = linea.split("\t");
                        ModUsuario modelo = new ModUsuario();

                        modelo.setUsuario(texto[0]);
                        modelo.setPassword(texto[1]);
                        modelo.setRol("GEN");
                        lNumeroLineas++;
                        publishProgress(lNumeroLineas);

                        try {

                            daoUsuarios.CrearUsuario(getBaseContext(), modelo);


                        } catch (Exception ex) {
                            ex.printStackTrace();
                            return PROCESS_ERROR;
                        }

                        // Lista.add(modelo);
                        linea = buffer.readLine();
                    }
                    Toast.makeText(getApplicationContext(), "Usuarios Creados", Toast.LENGTH_SHORT).show();
                    buffer.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                    return PROCESS_ERROR;
                } finally {
                    try {
                        if (archivo != null)
                            archivo.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());
                        return PROCESS_ERROR;
                    }
                }


            }  //catch (IOException e) {
            // e.printStackTrace();
            //}
            catch (Exception ex) {

                errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                return PROCESS_ERROR;
            } finally {

            }

            // dialog.dismiss();

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ConfiguracionActivity.this);

                dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }

            return PROCESS_OK;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            Log.d("onpostexecute", (configuracionActivityWeakReference.get() != null) + "");
            if (configuracionActivityWeakReference.get() != null && !configuracionActivityWeakReference.get().isFinishing()) {

                progressDialog.dismiss();
                ActualizarRegistrosSubidosUsu();

            /*if (PROCESS_OK.equals(result)) {
                Toast.makeText(this, "Process OK " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Process ERROR " + result, Toast.LENGTH_LONG).show();
            }*/
                /*AlertDialog alertDialog = new AlertDialog.Builder(configuracionActivityWeakReference.get()).create();
                alertDialog.setTitle(result);
                alertDialog.setMessage("On post execute");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();*/
            }

        }
    }

    public void CargarTotalReferencia(){
        String errorMessage = "";

        try {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            final File file = new File(path, "Referencias.txt");
            FileInputStream FileInt = new FileInputStream(file);
            InputStreamReader archivo = new InputStreamReader(FileInt);

            BufferedReader buffer = new BufferedReader(archivo);

            try {


                String linea = buffer.readLine();
                while ((linea)!=null) {
                    lNumeroLineas++;
                    lNumeroLineasTotal++;
                    linea = buffer.readLine();
                }


                buffer.close();
            } catch (Exception ex) {
                ex.printStackTrace();

            } finally {
                try {
                    if (archivo != null)
                        archivo.close();
                } catch (IOException e) {
                    errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());

                }
            }


        }  //catch (IOException e) {
        // e.printStackTrace();
        //}
        catch (Exception ex) {

            errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
            // showProgress(false);
        } finally {

        }


        if (!errorMessage.isEmpty()) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

            dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
            dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
    }


    public void CargarTotalUsuarios(){
        String errorMessage = "";

        try {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            final File file = new File(path, "Usuarios.txt");
            FileInputStream FileInt = new FileInputStream(file);
            InputStreamReader archivo = new InputStreamReader(FileInt);

            BufferedReader buffer = new BufferedReader(archivo);

            try {


                String linea = buffer.readLine();
                while ((linea)!=null) {
                    lNumeroLineas++;
                    linea = buffer.readLine();
                }
                buffer.close();
            } catch (Exception ex) {
                ex.printStackTrace();

            } finally {
                try {
                    if (archivo != null)
                        archivo.close();
                } catch (IOException e) {
                    errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());

                }
            }


        }  //catch (IOException e) {
        // e.printStackTrace();
        //}
        catch (Exception ex) {

            errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
            // showProgress(false);
        } finally {

        }


        if (!errorMessage.isEmpty()) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

            dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
            dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
    }




    public void CargarDatosReferencia(int duracion) {
        if (swSincronizacionLocal.isChecked()) {

            btnguardar.setEnabled(false);
            final ProgressDialog dialog = ProgressDialog.show(ConfiguracionActivity.this, "", "Loading...", true, true);
            dialog.show();

            String errorMessage = "";

            try {
                // showProgress(true);
                daoItems.BorrarItems(getBaseContext());
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                final File file = new File(path, "Referencias.txt");
                FileInputStream FileInt = new FileInputStream(file);
                InputStreamReader archivo = new InputStreamReader(FileInt);

                //InputStream archivo = openFileInput("prueba_int.txt");
                // InputStreamReader archivo = new InputStreamReader(openFileInput("prueba_int.txt"));
                //  archivo = getResources().openRawResource(R.raw.documentoprueba);


                //InputStreamReader archivo = new InputStreamReader(getResources().openRawResource(R.raw.referencias));
                BufferedReader buffer = new BufferedReader(archivo);

                try {

                    String linea = buffer.readLine();
                    ArrayList<ModItems> Lista = new ArrayList<ModItems>();
                    // protressStatus = 0;
                    while ((linea) != null) {


                        String[] texto = linea.split("\t");
                        ModItems modelo = new ModItems();

                        modelo.setCodigo(texto[0]);
                        modelo.setDescripcion(texto[1]);
                        modelo.setCodigoAlterno(texto[2]);
                        modelo.setCantidad(0);
                        // progressBarH.setProgress(protressStatus);
                        //  Total.setText(String.valueOf(protressStatus));
                        //Barra();

                        try {

                            daoItems.CrearItems(getBaseContext(), modelo);



                        } catch (Exception ex) {
                            errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                        }

                        // Lista.add(modelo);
                        linea = buffer.readLine();

                    }
                    Toast.makeText(getApplicationContext(), "Referencias Creadas", Toast.LENGTH_SHORT).show();
                    // showProgress(false);
                    //ActualizarRegistrosSubidos();
                    btnguardar.setEnabled(true);
                    buffer.close();
                } catch (Exception ex) {
                    ex.printStackTrace();

                } finally {
                    try {
                        if (archivo != null)
                            archivo.close();
                    } catch (IOException e) {
                        errorMessage = e.getMessage() + " -- " + ExceptionManager.getStackTraceString(e.getStackTrace());

                    }
                }


            }  //catch (IOException e) {
            // e.printStackTrace();
            //}
            catch (Exception ex) {

                errorMessage = ex.getMessage() + " -- " + ExceptionManager.getStackTraceString(ex.getStackTrace());
                // showProgress(false);
            } finally {

            }

            dialog.dismiss();

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

                dlgAlert.setMessage(getResources().getString(R.string.action_usuarios_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }

        } else {

            String errorMessage = "";
            try {

                showProgress(true);

                String jsonItems = getJsonItems();

                ModGeneral modGeneral = daoGeneral.CapturarRegistros(this);
                if (modGeneral.getUrl().trim().isEmpty()) {
                    Toast.makeText(this, R.string.sync_no_url, Toast.LENGTH_LONG).show();
                } else {
                    tipoSincronizacion = Cons.tipoSincronizacionItems;
                    NetworkManager networkManager = NetworkManager.newInstance(this);
                    networkManager.addPostRequest(this, modGeneral.getUrl() + Cons.urlItems, jsonItems);
                }


            } catch (Error error) {
                showProgress(false);
                errorMessage = error.getMessage();
            } catch (Exception ex) {
                showProgress(false);
                errorMessage = ex.getMessage();
            }

            if (!errorMessage.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

                dlgAlert.setMessage(getResources().getString(R.string.action_referencias_error) + errorMessage);
                dlgAlert.setTitle(getResources().getString(R.string.action_save_error_title));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
        }

    }

    /**
     * Shows the progress UI and hides the slotmachines form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mConfiguracionFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mConfiguracionFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mConfiguracionFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mConfiguracionFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        showProgress(false);
        Toast.makeText(this, R.string.error_operation + ": " + error.toString() + " -> " + error.getMessage() + " - " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        showProgress(false);

        int updated_yes = 1;
        try {

            if (tipoSincronizacion == Cons.tipoSincronizacionUsuarios) {

                OperationResultUsuarios operationResult = getOperationResultUsuarios(response);
                if (operationResult.getResult() == updated_yes) {

                    daoUsuarios.BorrarUsuarios(this);
                    for (UsuarioWS usuarioWS : operationResult.getColResult()) {
                        ModUsuario modUsuario = new ModUsuario();
                        modUsuario.setUsuario(usuarioWS.getUsuario());
                        modUsuario.setPassword(usuarioWS.getClave());

                        daoUsuarios.CrearUsuario(this, modUsuario);
                    }
                    Toast.makeText(this, R.string.succeed_operation, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(this, R.string.error_operation + ": -> " + operationResult.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            if (tipoSincronizacion == Cons.tipoSincronizacionItems) {

                OperationResultItems operationResult = getOperationResultItems(response);
                if (operationResult.getResult() == updated_yes) {

                    daoItems.BorrarItems(this);
                    for (ItemWS itemWS : operationResult.getItems()) {
                        ModItems modItems = new ModItems();
                        modItems.setCodigo(itemWS.getMae_consecutivo_producto());
                        modItems.setDescripcion(itemWS.getMae_nombre_producto());

                        daoItems.CrearItems(this, modItems);
                    }
                    Toast.makeText(this, R.string.succeed_operation, Toast.LENGTH_LONG).show();
                    //ActualizarRegistrosSubidos();
                } else {
                    Toast.makeText(this, R.string.error_operation + ": -> " + operationResult.getMessage(), Toast.LENGTH_LONG).show();
                }
            }


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Error error) {
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public OperationResultUsuarios getOperationResultUsuarios(String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        return gson.fromJson(json, OperationResultUsuarios.class);

    }

    public OperationResultItems getOperationResultItems(String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        return gson.fromJson(json, OperationResultItems.class);

    }

    private String getJsonItems() {
        String jsonInventario = "";

        RequestJsonItems requestJsonItems = new RequestJsonItems();
        requestJsonItems.setUsuario(username);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        jsonInventario = gson.toJson(requestJsonItems);

        return jsonInventario;
    }

    private String getJsonUsuarios() {
        String jsonUsuarios = "";

        RequestJsonUsuarios requestJsonUsuarios = new RequestJsonUsuarios();
        requestJsonUsuarios.setUsuario(username);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        jsonUsuarios = gson.toJson(requestJsonUsuarios);

        return jsonUsuarios;
    }

    private void ActualizarRegistrosSubidosRef() {


        int TotalSubido= daoItems.CapturarCantidadTotal(getApplicationContext());
        txtTotal.setText(String.valueOf(TotalSubido));
    }

    private void ActualizarRegistrosSubidosUsu() {


        int TotalSubido= daoUsuarios.CapturarCantidadTotal(getApplicationContext());
        txtTotal.setText(String.valueOf(TotalSubido));
    }


    /**
     * Muestra los archivos del directorio pasado como parametro en un listView
     *
     * @param rutaDirectorio
     */

    /*private void verArchivosDirectorio(String rutaDirectorio) {

        carpetaActual.setText("Estas en: " + rutaDirectorio);
        listaNombresArchivos = new ArrayList();
        listaRutasArchivos = new ArrayList();
        File directorioActual = new File(rutaDirectorio);
        File[] listaArchivos = directorioActual.listFiles();

        int x = 0;

        // Si no es nuestro directorio raiz creamos un elemento que nos
        // permita volver al directorio padre del directorio actual
        if (!rutaDirectorio.equals(directorioRaiz)) {
            listaNombresArchivos.add("../");
            listaRutasArchivos.add(directorioActual.getParent());
            x = 1;
        }

        // Almacenamos las rutas de todos los archivos y carpetas del directorio
        for (File archivo : listaArchivos) {
            listaRutasArchivos.add(archivo.getPath());
        }

        // Ordenamos la lista de archivos para que se muestren en orden alfabetico
       // Collections.sort(listaRutasArchivos, String.CASE_INSENSITIVE_ORDER);


        // Recorredos la lista de archivos ordenada para crear la lista de los nombres
        // de los archivos que mostraremos en el listView
        for (int i = x; i < listaRutasArchivos.size(); i++){
            File archivo = new File(listaRutasArchivos.get(i));
            if (archivo.isFile()) {
                listaNombresArchivos.add(archivo.getName());
            } else {
                listaNombresArchivos.add("/" + archivo.getName());
            }
        }

        // Si no hay ningun archivo en el directorio lo indicamos
        if (listaArchivos.length < 1) {
            listaNombresArchivos.add("No hay ningun archivo");
            listaRutasArchivos.add(rutaDirectorio);
        }


        // Creamos el adaptador y le asignamos la lista de los nombres de los
        // archivos y el layout para los elementos de la lista
        adaptador = new ArrayAdapter(this, R.layout.text_view_lista_archivos, listaNombresArchivos);
        setListAdapter(adaptador);
    }*/





    }

