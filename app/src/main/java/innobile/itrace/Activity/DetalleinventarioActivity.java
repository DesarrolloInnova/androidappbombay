package innobile.itrace.Activity;

import android.content.Intent;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import innobile.itrace.DAO.DAOInventario;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.ModeloInventario;
import innobile.itrace.Adapter.AdapterInv;
import innobile.itrace.R;

import java.util.ArrayList;

public class DetalleinventarioActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private TextView  txtConteo;
    private ListView MovimientosListaView;
    ArrayList<Registros> ListInventario;
    private static AdapterInv adapterInv;
    private DAOInventario daoInventario;
    SwipeRefreshLayout swipeRefreshLayout;




    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalleinventario);

        txtConteo = findViewById(R.id.txtConteo);
        recyclerView = (RecyclerView) findViewById(R.id.list);

        daoInventario = new DAOInventario();
        Button buttonSugerencia = findViewById(R.id.botonSugerencia);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        NumeroDeRegistros();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarListado();
                NumeroDeRegistros();

                swipeRefreshLayout.setRefreshing(false);
            }
        });




        buttonSugerencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Ingrese la cantidad que desea borrar y deslice el datos al que desea aplicar el borrado", Toast.LENGTH_LONG).show();
            }
        });

    }




    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AdapterInv.ViewHolder) {
            // get the removed item code
            int id = ListInventario.get(viewHolder.getAdapterPosition()).get_id();

            // backup of removed item for undo purpose
            final Registros deletedItem = ListInventario.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapterInv.removeItem(viewHolder.getAdapterPosition());
            EditText cantidad = findViewById(R.id.plain_text_input);

            try {

                if(cantidad.getText().toString().length() > 0){
                    daoInventario.BorrarRegistroPorId(this, id, Integer.parseInt(cantidad.getText().toString()));
                }else{
                    daoInventario.BorrarRegistroPorId(this, id, 0);
                }


            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), "La cantidad a borrar no tiene datos", Toast.LENGTH_SHORT).show();
            }

        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        cargarListado();
    }

    //CARGAR LA TABLA DE INVENTARIO EN UNA LISTA.
    public void cargarListado() {

        ListInventario = DAOInventario.CapturasListaLecturas(getApplicationContext());
        adapterInv = new AdapterInv(ListInventario);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapterInv);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


    }

    public void NumeroDeRegistros(){
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, Utilidades.TABLA_REGISTRO, null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Long numRows = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_REGISTRO);
        TextView textView6 = findViewById(R.id.txtConteo);
        textView6.setText(String.valueOf(numRows));
    }



}
