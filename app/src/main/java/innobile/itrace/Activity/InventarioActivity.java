package innobile.itrace.Activity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.SampleAppActivity;


public class InventarioActivity  extends SampleAppActivity {

    private String V_URL = null, V_URL_MON = null;
    private String V_TipoConexion  = null;
    private  ConexionSQLiteHelper conn;
    private  SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);


        //Instanciando la conexión a la base de datos Sqlite
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_PRODUCTOS, null, 1);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL=  t_configuraciones.getCon_URL();
        V_URL_MON =  t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();

        traerDatosProductos();

    }


    public void traerDatosProductos(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String Ruta = null;


        db = conn.getWritableDatabase();

        //URL para realizar la peticion
        Ruta = V_URL_MON + "/api/producto/list";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {

                        Toast.makeText(InventarioActivity.this,String.valueOf(jsonArray.length()) , Toast.LENGTH_SHORT).show();


                        try {
                            for(int i = 0; i < jsonArray.length(); i++) {
                                //Se recorre cada uno de los elementos del json y se insertan en la base de datos SQLite
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ContentValues values = new ContentValues();
                                values.put(Utilidades.CODIGO_PRODUCTO,  jsonObject.getString("Codigo").trim());
                                values.put(Utilidades.DESCRIPCION_PRODUCTO,  jsonObject.getString("Descripcion").trim());
                                db.insert(Utilidades.TABLA_PRODUCTOS, null, values);
                            }

                            //Al terminar se cierra la conexión con la base de datos
                            Toast.makeText(InventarioActivity.this, "Los datos se guardaron correctamente", Toast.LENGTH_SHORT).show();

                            motrarDatosSqlite();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } ,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(InventarioActivity.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        queue.add(request);
    }


    public void motrarDatosSqlite(){

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_PRODUCTOS;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM  '" + myTable + "' where Codigo = 5400879085378" , null);


        if (c.moveToFirst()) {
            do {
                String prioridad = c.getString(0);
                String central = c.getString(1);

                Toast.makeText(InventarioActivity.this,prioridad + " " + central , Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }else{
            Toast.makeText(InventarioActivity.this, "Sin registros ", Toast.LENGTH_SHORT).show();
        }


    }

}