package innobile.itrace.Adapter;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.ModeloInventario;
import innobile.itrace.R;

public class AdapterInv extends RecyclerView.Adapter<AdapterInv.ViewHolder> implements Filterable {


    private List<Registros> mValuesFiltered;
    private List<Registros> mValues;

    // View lookup cache
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        TextView posicion;
        TextView codigo;
        TextView cantidad;
        TextView ubicacion;
        public Registros mItem;
        public RelativeLayout viewBackground, viewForeground;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            posicion = view.findViewById(R.id.positionText);
            codigo = view.findViewById(R.id.codigo);
            cantidad = view.findViewById(R.id.cantidad);
            ubicacion = view.findViewById(R.id.ubicacion);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);

        }
    }

    public AdapterInv(ArrayList<Registros> items) {

        mValues = items;
        mValuesFiltered = items;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValuesFiltered.get(position);
        holder.posicion.setText(String.format(" %d ", position + 1));
       /// holder.codigo.setText(String.valueOf(holder.mItem.getId()));
        String data = String.valueOf(holder.mItem.getId());
        holder.codigo.setText(data);
        holder.cantidad.setText(String.valueOf(holder.mItem.getReferencia()));
        holder.ubicacion.setText(String.valueOf(holder.mItem.getCantidad()));
    }

    @Override
    public int getItemCount() {

        return mValuesFiltered.size();
    }

    public void removeItem(int position) {
        mValues.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();


                FilterResults filterResults = new FilterResults();
                filterResults.values = mValuesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mValuesFiltered = (ArrayList<Registros>
                        ) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


}
