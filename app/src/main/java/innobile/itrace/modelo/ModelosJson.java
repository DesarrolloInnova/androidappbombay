package innobile.itrace.modelo;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;

/**
 * Created by Dev1 on 7/04/15.
 */
public class ModelosJson {

    private String V_URL,V_URLMON  = null;
    private String V_TipoConexion  = null;

    public ModelosJson() {

    }



// Valida un usuario en el APÏ NodeJS

    public String Login(String Cod_Usuario, String password, Context context) throws Exception {
        String respuesta = "";
        JSONObject jsonUserPassword = null;

        try {
            if (Cod_Usuario.isEmpty() || password.isEmpty()) {
                return respuesta = "-1";
            }
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            V_URL=  t_configuraciones.getCon_URL();
            V_URLMON =  t_configuraciones.getCon_URLMongo();
            V_TipoConexion =  t_configuraciones.getCon_TipoConexion();



            String Ruta = null;
            jsonUserPassword = new JSONObject();

            if (V_TipoConexion.trim().equals(Cons.SERVER) ) {
                jsonUserPassword.put("Cod_Usuario", Cod_Usuario);
                jsonUserPassword.put("password", password);

                Ruta = V_URL + "/Autenticacion";

                          }
            if (V_TipoConexion.trim().equals(Cons.NUBE) ) {

                jsonUserPassword.put("Usuario", Cod_Usuario);
                jsonUserPassword.put("Password", password);

                Ruta = V_URLMON + "/api/Usuario/login";
            }

            respuesta = WebServicesOperationsModel.setDataHttpRequest(Ruta, jsonUserPassword.toString(), 30000);
            //respuesta = WebServicesOperationsModel.setDataHttpRequest("http://innobile.zapto.org:8090/iTrace/webresources/authentication", jsonUserPassword.toString(), 30000);


        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }


//trae datos de la tabla de movimentos

    public String BuscarMovimientos(String Tipo_Documento,  Context context) throws Exception {
        String respuesta = "";
        JSONObject jsonBuscarMovimiento = null;

        try {
            if (Tipo_Documento.isEmpty() ) {
                return respuesta = "-1";
            }

            jsonBuscarMovimiento = new JSONObject();

            jsonBuscarMovimiento.put("Cod_Empresa", "01");
            jsonBuscarMovimiento.put("Tipo_Documento", Tipo_Documento);
            jsonBuscarMovimiento.put("Estado", "IN");

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            V_URL=  t_configuraciones.getCon_URL();

            respuesta = WebServicesOperationsModel.setDataHttpRequest(V_URL + "/BuscarMovimientos", jsonBuscarMovimiento.toString(), 30000);


        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }
}
