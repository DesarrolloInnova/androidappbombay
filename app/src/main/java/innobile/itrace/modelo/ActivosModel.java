package innobile.itrace.modelo;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.IFA_activos;
import innobile.itrace.transport.T_Configuraciones;

/**
 * Created by Dev1 on 9/11/15.
 */
public class ActivosModel {

    public ActivosModel() {

    }

    /**
     * Obtiene un activo consultado por el parámetro dado
     *
     * @param context
     * @param codigo
     * @return
     * @throws Exception
     */
    public String GetActivoFromRF(Context context, String codigo) throws Exception {
        String respuesta = "";
        JSONObject jsonListados = null;
        try {

            if (codigo.isEmpty()) {
                return "-1";
            }

            jsonListados = new JSONObject();

            jsonListados.put("data", codigo);

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresourcesitrace/getassets", jsonListados.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (OutOfMemoryError memoryEx) {
            throw memoryEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }

    /**
     * Obtiene los listados para la actividad de activos
     *
     * @param context
     * @return
     * @throws Exception
     */
    public String GetListadosActivosFromRF(Context context) throws Exception {
        String respuesta = "";
        JSONObject jsonListados = null;
        try {

            jsonListados = new JSONObject();

            jsonListados.put("idsLists", "1,3,6,2,5,11,15,4,14,13,12,8");

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresources/lists", jsonListados.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }

    /**
     * Obtiene el listado de áreas
     *
     * @param context
     * @return
     * @throws Exception
     */
    public String GetListadoAreasFromRF(Context context) throws Exception {
        String respuesta = "";
        JSONObject jsonListados = null;
        try {

            jsonListados = new JSONObject();

            jsonListados.put("idsLists", "1");

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresources/lists", jsonListados.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }

    /**
     * Obtiene un activo consultado por el parámetro dado
     *
     * @param context
     * @param area
     * @return
     * @throws Exception
     */
    public String GetReporteInventarioPorAreaFromRF(Context context, String area) throws Exception {
        String respuesta = "";
        JSONObject jsonReporteInventario = null;
        try {

            jsonReporteInventario = new JSONObject();

            jsonReporteInventario.put("area", area);

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresourcesitrace/getreports", jsonReporteInventario.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }

    /**
     * Obtiene un activo consultado por el parámetro dado
     *
     * @param context
     * @param cedulaResponsable
     * @return
     * @throws Exception
     */
    public String GetResponsableFromRF(Context context, String cedulaResponsable) throws Exception {
        String respuesta = "";
        JSONObject jsonListados = null;
        try {

            jsonListados = new JSONObject();

            jsonListados.put("cedula_responsable", cedulaResponsable);

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresourcesitrace/getResponsable", jsonListados.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;
    }

    /**
     * Envía los datos de un activo al restful para su respectiva grabada o actualización
     *
     * @param context
     * @param areaLeida
     * @param responsableLeido
     * @param inventario
     * @param ifa_activos
     * @return
     * @throws Exception
     */
    public String SetActivoRF(Context context, String areaLeida, String responsableLeido, String inventario, String usuario, IFA_activos ifa_activos) throws Exception {

        String respuesta = "";
        JSONObject jsonActivo = null;
        try {

            jsonActivo = new JSONObject();

            jsonActivo.put("usuario", usuario);
            jsonActivo.put("area_leida", areaLeida);
            jsonActivo.put("responsable_leido", responsableLeido);
            jsonActivo.put("inventario", inventario);
            jsonActivo.put("codigo_conteo", ifa_activos.getAct_codigo_conteo().trim());
            jsonActivo.put("codigo_activo", ifa_activos.getAct_codigo_activo().trim());
            jsonActivo.put("descripcion", ifa_activos.getAct_descripcion().trim());
            jsonActivo.put("codigo_barras", ifa_activos.getAct_codigo_barras().trim());
            jsonActivo.put("fecha_compra", ifa_activos.getAct_fecha_compra());
            jsonActivo.put("meses_depreciacion", String.valueOf(ifa_activos.getAct_meses_depreciacion()));
            jsonActivo.put("area", ifa_activos.getAct_area().trim());
            jsonActivo.put("grupo", ifa_activos.getAct_grupo().trim());
            jsonActivo.put("ciclo_vida", ifa_activos.getAct_ciclo_vida().trim());
            jsonActivo.put("mantenimiento", ifa_activos.getAct_mantenimiento().trim());
            jsonActivo.put("centro_costos", ifa_activos.getAct_centro_costos().trim());
            jsonActivo.put("valor_compra", String.valueOf(ifa_activos.getAct_valor_compra()));
            jsonActivo.put("valor_iva_compra", String.valueOf(ifa_activos.getAct_valor_iva_compra()));
            jsonActivo.put("cuenta_contable", ifa_activos.getAct_cuenta_contable().trim());
            jsonActivo.put("nombre_cuenta_contable", ifa_activos.getAct_nombre_cuenta_contable().trim());
            jsonActivo.put("ciudad", ifa_activos.getAct_ciudad().trim());
            jsonActivo.put("piso", ifa_activos.getAct_piso().trim());
            jsonActivo.put("ubicacion", ifa_activos.getAct_ubicacion().trim());
            jsonActivo.put("tipo_ubicacion", ifa_activos.getAct_tipo_ubicacion().trim());
            jsonActivo.put("serial", ifa_activos.getAct_serial().trim());
            jsonActivo.put("color", ifa_activos.getAct_color().trim());
            jsonActivo.put("modelo", ifa_activos.getAct_modelo().trim());
            jsonActivo.put("tipo_activo", ifa_activos.getAct_tipo_activo().trim());
            jsonActivo.put("material", ifa_activos.getAct_material().trim());
            jsonActivo.put("estado", ifa_activos.getAct_estado().trim());
            jsonActivo.put("fabricante", ifa_activos.getAct_fabricante().trim());
            jsonActivo.put("empresa_propietaria", ifa_activos.getAct_empresa_propietaria().trim());
            jsonActivo.put("nombre_maquina", ifa_activos.getAct_nombre_maquina().trim());
            jsonActivo.put("sistema_operativo", ifa_activos.getAct_sistema_operativo().trim());
            jsonActivo.put("servidor", ifa_activos.getAct_servidor().trim());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            if (!ifa_activos.getAct_ultima_fecha_reporte_agente().trim().isEmpty()) {
                Date parsedDate = dateFormat.parse(ifa_activos.getAct_ultima_fecha_reporte_agente().trim());
                long time = parsedDate.getTime() / 1000;
                jsonActivo.put("ultima_fecha_reporte_agente", time);
                //jsonActivo.put("ultima_fecha_reporte_agente", ifa_activos.getAct_ultima_fecha_reporte_agente());
            } else {
                jsonActivo.put("ultima_fecha_reporte_agente", "");
            }

            jsonActivo.put("observaciones", ifa_activos.getAct_observaciones());
            jsonActivo.put("tag_activo", ifa_activos.getAct_tag_activo());
            jsonActivo.put("responsable", ifa_activos.getAct_responsable());
            jsonActivo.put("direccion_ubicacion", ifa_activos.getAct_direccion_ubicacion());
            jsonActivo.put("canal", ifa_activos.getAct_canal());
            jsonActivo.put("en_red", ifa_activos.getAct_en_red());
            jsonActivo.put("usuario_red", ifa_activos.getAct_usuario_red());
            jsonActivo.put("fecha_ingreso_bodega", ifa_activos.getAct_fecha_ingreso_bodega());
            jsonActivo.put("orden_compra", ifa_activos.getAct_oc());
            jsonActivo.put("service_pack", ifa_activos.getAct_service_pack());
            jsonActivo.put("procesador", ifa_activos.getAct_procesador());
            jsonActivo.put("velocidad", ifa_activos.getAct_velocidad());
            jsonActivo.put("disco_duro", ifa_activos.getAct_disco_duro());
            jsonActivo.put("total_memoria", ifa_activos.getAct_total_memoria());

            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            respuesta = WebServicesOperationsModel.setDataHttpRequest(t_configuraciones.getCon_URL() + "/webresources/saveasset", jsonActivo.toString(), 30000);

        } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
            throw connectionEx;
        } catch (ParseException ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error: " + ex.getMessage());
            throw ex;
        }
        return respuesta;

    }
}
