package innobile.itrace.modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.entidades.RegistrosCompletos;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


public class DAOInventario {
    public final static int CANTIDAD = 0;
    public static ArrayList<Registros> getListaxID(Context context) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);
        ArrayList<Registros> Lista = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
               Lista.add(new Registros( c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4), c.getString(5)));
            } while (c.moveToNext());
        }
        db.close();
        return Lista;
    }

    public static Long CapturarCantidadCodigosLeidos(Context context, String Codigo) {
        Long cant = 0L;
        try{
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

            SQLiteDatabase db = conn.getWritableDatabase();

            String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

             cant = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + myTable + " where id = " + Codigo, null);


        }catch (Exception error){
            Toast.makeText(context,"El registro no se encontro", Toast.LENGTH_SHORT).show();
        }

        return cant;

    }

    public static JSONArray  LocalData(Context context) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_OBTENERDATOS, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_OBTENERDATOS; //Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);
        ArrayList<RegistrosCompletos> Lista = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Lista.add(new RegistrosCompletos( c.getString(0)));
            } while (c.moveToNext());
        }
        db.close();
        JSONArray mJSONArray = new JSONArray(Arrays.asList(Lista));
        return mJSONArray;
    }

    public static String GetJsonData(Context context) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_OBTENERDATOS, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_OBTENERDATOS;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);

        String json = "";

        if (c.moveToFirst()) {
            do {
                json = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();
        return json;
    }

}
