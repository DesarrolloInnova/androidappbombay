package innobile.itrace.modelo;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;

/**
 * Created by jaidiver on 12/22/17.
 */

public class NetworkManager {

    private static NetworkManager networkManager = null;
    private static OnNetworkActionListener mListener = null;

    private NetworkManager() {

    }

    public static NetworkManager newInstance(Context context) {
        if (networkManager == null) {
            networkManager = new NetworkManager();
        }

        if (context instanceof OnNetworkActionListener) {
            mListener = (OnNetworkActionListener) context;

        } else {
            throw new RuntimeException(context.toString() + " debe implementar OnNetworkActionListener");
        }

        return networkManager;
    }

    public void addRequest(Context context, String url) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, onPostsLoaded, onPostsError);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(600000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void addPostRequest(Context context, String url, final String jsonContent) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, onPostsLoaded, onPostsError) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonContent == null ? null : jsonContent.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Codificación no soportada mientras obtenía los bytes de %s usando %s", jsonContent, "utf-8");
                    return null;
                }
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(600000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private final Response.Listener<String> onPostsLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            mListener.onResponse(response);
        }
    };

    private final Response.ErrorListener onPostsError = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mListener.onErrorResponse(error);
        }
    };

    /**
     * Interface definition for a callback to be invoked when an
     * network action has been executed.
     */
    public interface OnNetworkActionListener {

        void onResponse(String response);

        void onErrorResponse(VolleyError error);

    }
}
