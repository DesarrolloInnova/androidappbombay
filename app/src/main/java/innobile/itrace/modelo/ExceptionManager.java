package innobile.itrace.modelo;

/**
 * Created by jaidiver on 8/8/18.
 */

public class ExceptionManager {

    public static String getStackTraceString(StackTraceElement[] stackTrace){
        String stackTrace1 = "";
        String stackTrace2 = "";
        String stackTrace3 = "";
        String stackTrace4 = "";
        if (stackTrace.length > 0) {
            stackTrace1 = "\t|->Clase: " + stackTrace[0].getClassName() + " - MÈtodo: " + stackTrace[0].getMethodName() + " - Linea: " + String.valueOf(stackTrace[0].getLineNumber());
            System.out.println(stackTrace1);
        }
        if (stackTrace.length > 1) {
            stackTrace2 = "\t|->Clase: " + stackTrace[1].getClassName() + " - MÈtodo: " + stackTrace[1].getMethodName() + " - Linea: " + String.valueOf(stackTrace[1].getLineNumber());
            System.out.println(stackTrace2);
        }
        if (stackTrace.length > 2) {
            stackTrace3 = "\t|->Clase: " + stackTrace[2].getClassName() + " - MÈtodo: " + stackTrace[2].getMethodName() + " - Linea: " + String.valueOf(stackTrace[2].getLineNumber());
            System.out.println(stackTrace3);
        }
        if (stackTrace.length > 3) {
            stackTrace4 = "\t|->Clase: " + stackTrace[3].getClassName() + " - MÈtodo: " + stackTrace[3].getMethodName() + " - Linea: " + String.valueOf(stackTrace[3].getLineNumber());
            System.out.println(stackTrace4);
        }
        return stackTrace1+stackTrace2+stackTrace3+stackTrace4;
    }
}
