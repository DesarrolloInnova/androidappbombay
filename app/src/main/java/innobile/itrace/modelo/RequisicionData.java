package innobile.itrace.modelo;

public class RequisicionData {
    String ean;
    int cantidad;
    String observaciones;
    int nr_documento;
    String fecha;

    public RequisicionData(String ean, int cantidad, String observaciones, int nr_documento, String fecha) {
        this.ean = ean;
        this.cantidad = cantidad;
        this.observaciones = observaciones;
        this.nr_documento = nr_documento;
        this.fecha = fecha;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getNr_documento() {
        return nr_documento;
    }

    public void setNr_documento(int nr_documento) {
        this.nr_documento = nr_documento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }




}
