package innobile.itrace.modelo;

public class ImpresionDetalleModelo {
    String Cod_Barras;
    String Descripcion;
    String Cantidad;

    public ImpresionDetalleModelo(String cod_Barras, String descripcion, String cantidad) {
        Cod_Barras = cod_Barras;
        Descripcion = descripcion;
        Cantidad = cantidad;
    }

    public ImpresionDetalleModelo() {

    }


    public String getCod_Barras() {
        return Cod_Barras;
    }

    public void setCod_Barras(String cod_Barras) {
        Cod_Barras = cod_Barras;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCantidad() {
        return Cantidad;
    }

    public void setCantidad(String cantidad) {
        Cantidad = cantidad;
    }
}



