package innobile.itrace.modelo;

/**
 * Created by BATMAN on 6/06/18.
 */

public class ModItems {

    private int id;
    private String Codigo;
    private String Descripcion;
    private String codigoAlterno;
    private int Cantidad;

    public ModItems() {
    }

    public ModItems(int id, String Codigo, String Descripcion, int Cantidad) {

        this.setId(id);
        this.setCodigo(Codigo);
        this.setDescripcion(Descripcion);
        this.setCantidad(Cantidad);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCodigoAlterno() {
        return codigoAlterno;
    }

    public void setCodigoAlterno(String codigoAlterno) {
        this.codigoAlterno = codigoAlterno;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }
}
