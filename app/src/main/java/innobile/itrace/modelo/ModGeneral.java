package innobile.itrace.modelo;

/**
 * Created by BATMAN on 6/06/18.
 */

public class ModGeneral {


    private String Url = "";
    private String Documento = "";
    private String Conteo = "";
    private Integer ValidarCod = 0;

    public Integer getValidarCod() {
        return ValidarCod;
    }

    public void setValidarCod(Integer validarCod) {
        ValidarCod = validarCod;
    }


    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String documento) {
        Documento = documento;
    }

    public String getConteo() {
        return Conteo;
    }

    public void setConteo(String conteo) {
        Conteo = conteo;
    }


    public ModGeneral() {
    }


    public ModGeneral(String Url, String Documento, String Conteo, Integer ValidarCod) {

        this.Url = Url;
        this.Documento = Documento;
        this.Conteo = Conteo;
        this.ValidarCod = ValidarCod;
    }


}
