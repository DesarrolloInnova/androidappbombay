package innobile.itrace.modelo;

public class Encabezado {



    int codigo_documento;
    String prioridad;
    String central_costo;
    String responsable;

    public Encabezado(int codigo_documento, String prioridad, String central_costo, String responsable) {
        this.codigo_documento = codigo_documento;
        this.prioridad = prioridad;
        this.central_costo = central_costo;
        this.responsable = responsable;
    }

    public int getCodigo_documento() {
        return codigo_documento;
    }

    public void setCodigo_documento(int codigo_documento) {
        this.codigo_documento = codigo_documento;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getCentral_costo() {
        return central_costo;
    }

    public void setCentral_costo(String central_costo) {
        this.central_costo = central_costo;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }




}
