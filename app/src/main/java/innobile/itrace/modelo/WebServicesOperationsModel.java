package innobile.itrace.modelo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by jaidiver on 4/1/15.
 */
public class WebServicesOperationsModel {

    public static String setDataHttpRequest(String url, String dataPost, int timeOut) throws IOException {

        String result = "";

        try {
            HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setConnectTimeout(timeOut);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(dataPost);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine()) != null){
                response.append(line);
            }
            bufferedReader.close();

            result = response.toString();
        } catch(ConnectException connectEx){
            throw connectEx;
        } catch(OutOfMemoryError memoryEx){
            throw memoryEx;
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            throw ex;
        }

        return result;
    }
}
