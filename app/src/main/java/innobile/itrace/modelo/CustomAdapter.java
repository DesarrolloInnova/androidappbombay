package innobile.itrace.modelo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteBlobTooBigException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;

import innobile.itrace.R;
import innobile.itraceandroid.OrdeCompra;

/**
 * Created by hardik on 9/1/17.
 */
public class CustomAdapter extends BaseAdapter {

    private Context context;
    public static ArrayList<Model> modelArrayList;
    public static int Cantidad;
    public static String[] datos;
    public static String[] datosCounter;
    public static int cantidadTotal = 0;
    public static int comprobarIngreso;
    public static ArrayList<String> arrayModificaciones = new ArrayList<String>();


    public CustomAdapter(Context context, ArrayList<Model> modelArrayList, int Cantidad) {

        this.context = context;
        this.modelArrayList = modelArrayList;
        this.Cantidad = Cantidad;
        datosCounter = new String[0];
        datosCounter = new String[CustomAdapter.modelArrayList.size()];


        //Reiniciando la lista a su valor original
        arrayModificaciones = new ArrayList<String>();


        //Enviando la cantidad a la cantidad restante
        OrdeCompra.actualizarCantidad(Integer.parseInt(String.valueOf(CustomAdapter.Cantidad)));


        for (int i = 0; i < CustomAdapter.modelArrayList.size(); i++) {
            datosCounter[i] = CustomAdapter.modelArrayList.get(i).getAnimal();
            //Toast.makeText(context,"DATO: " + datosCounter[i], Toast.LENGTH_SHORT).show();
        }

        cantidadTotal = 0;
        //Obteniendo la cantidad total
        //Sumando los datos para obtener la cantidad total de los elementos
        for (int i = 0; i < datosCounter.length; i++) {
            datos = datosCounter[i].split("    ");
            if (!datos[1].equals("registro")) {
                cantidadTotal = Integer.parseInt(datos[1]) + cantidadTotal;
            } else {
                cantidadTotal = 0;
            }


        }


    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lv_item, null, true);

            holder.checkBox = (CheckBox) convertView.findViewById(R.id.remision_seleccionada);
            holder.tRemision = (TextView) convertView.findViewById(R.id.t_remision);

            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder) convertView.getTag();
        }


        holder.checkBox.setText("N° ");
        holder.tRemision.setText(modelArrayList.get(position).getAnimal());
        holder.checkBox.setChecked(modelArrayList.get(position).getSelected());
        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(position);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                View tempview = (View) holder.checkBox.getTag(R.integer.btnplusview);
                Integer pos = (Integer) holder.checkBox.getTag();
                //Toast.makeText(context, "Checkbox "+pos+" clicked!", Toast.LENGTH_SHORT).show();

                if (modelArrayList.get(pos).getSelected()) {
                    modelArrayList.get(pos).setSelected(false);
                } else {
                    modelArrayList.get(pos).setSelected(true);
                }


                if (CustomAdapter.modelArrayList.get(pos).getSelected() == false && CustomAdapter.modelArrayList.size() == 1) {
                    String dato = OrdeCompra.recuperarNuevaCantidad(context, datos[0], arrayModificaciones);
                    OrdeCompra.actualizarCantidad((Integer.parseInt(dato) + OrdeCompra.obtenerCantidad()));
                } else if (Integer.parseInt(datos[1]) >= CustomAdapter.Cantidad && CustomAdapter.modelArrayList.size() == 1) {
                    //Se actualizar la cantidad actual
                    arrayModificaciones.add(datos[0]);
                    //Se almacena el dato con la cantidad restada
                    arrayModificaciones.add(String.valueOf(Integer.parseInt(datos[1]) - (Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad())));
                    //Se actualiza el dato con la direrencia de la resta
                    OrdeCompra.actualizarCantidad(0);
                } else if (CustomAdapter.modelArrayList.size() > 1) {

                    //Se extraen los datos para obtener la cantidad del producto actual
                    datos = CustomAdapter.modelArrayList.get(pos).getAnimal().split("    ");

                    //Se encarga de recuperar la cantidad al quitar el check en el checkbox
                    if (CustomAdapter.modelArrayList.get(pos).getSelected() == false) {
                        String dato = OrdeCompra.recuperarNuevaCantidad(context, datos[0], arrayModificaciones);
                        OrdeCompra.actualizarCantidad((Integer.parseInt(dato) + OrdeCompra.obtenerCantidad()));
                    }

                    //Este condicional se ejecuta cuando la cantidad a descontar es mayor a la cantidad del producto
                    else if (OrdeCompra.obtenerCantidad() >= Integer.parseInt(datos[1]) && CustomAdapter.Cantidad < cantidadTotal && OrdeCompra.obtenerCantidad() != 0) {
                        //Se actualizar la cantidad actual
                        arrayModificaciones.add(datos[0]);
                        //Se almacena el dato con la cantidad restada
                        arrayModificaciones.add(String.valueOf(datos[1]));
                        //Se actualiza el dato con la direrencia de la resta

                        OrdeCompra.actualizarCantidad(OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1]));
                        comprobarIngreso = 0;
                    }

                    //Este condicional se ejecuta cuando la cantidad del producto es mayor a la cantidad a descontar
                    else if (Integer.parseInt(datos[1]) >= OrdeCompra.obtenerCantidad() && OrdeCompra.obtenerCantidad() != 0) {

                        //Se actualizar la cantidad actual
                        arrayModificaciones.add(datos[0]);
                        //Se almacena el dato con la cantidad restada
                        arrayModificaciones.add(String.valueOf(Integer.parseInt(datos[1]) - (Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad())));
                        //Se actualiza el dato con la direrencia de la resta
                        OrdeCompra.actualizarCantidad(0);
                        comprobarIngreso = 1;
                    }

                    //Este condicional se ejecuta cuando las dos cantidades son iguales

                    else if (OrdeCompra.obtenerCantidad() == Integer.parseInt(datos[1]) && CustomAdapter.modelArrayList.get(pos).getSelected() && OrdeCompra.obtenerCantidad() != 0) {


                        //Se actualiza el dato con la direrencia de la resta
                        OrdeCompra.actualizarCantidad(Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad());

                        //Se almacena el codigo del documento
                        arrayModificaciones.add(datos[0]);
                        //Se almacena el dato con la cantidad restada
                        arrayModificaciones.add(String.valueOf((Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad()) - Integer.parseInt(datos[1])));
                        comprobarIngreso = 2;

                    }
                    //Se muestra el mensaje de alerta si no es posible restar la cantidad
                    else {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        holder.checkBox.setChecked(false);
                        modelArrayList.get(pos).setSelected(false);
                        alertDialog.setTitle("Mmmm");
                        alertDialog.setMessage("Quieres ingresar una cantidad mayor al total de cantidad en los pedidos");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }

                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    holder.checkBox.setChecked(false);
                    modelArrayList.get(pos).setSelected(false);
                    alertDialog.setTitle("Mmmm");
                    alertDialog.setMessage("Quieres ingresar una cantidad mayor al total de cantidad en los pedidos");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

            }
        });
        return convertView;
    }


    private class ViewHolder {

        protected CheckBox checkBox;
        private TextView tRemision;

    }

    public static String mensajePrueba(Context context, String codigo) {
        String resultadoBusqueda = "0";
        for (int i = 0; i < arrayModificaciones.size(); i++) {
            if (codigo.equals(arrayModificaciones.get(i))) {
                resultadoBusqueda = arrayModificaciones.get(i + 1);
            }
        }
        return resultadoBusqueda;
    }
}


/**
 * Ideas de codigo a reutilizar
 * Autor: CarlosDnl
 * Fecha: 12/12/2019
 **/

 /*

                    Toast.makeText(context, "Cantidad total: " + String.valueOf(cantidadTotal), Toast.LENGTH_SHORT).show();

                   if(cantidadTotal - CustomAdapter.Cantidad < 0){
                       AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                       holder.checkBox.setChecked(false);
                       alertDialog.setTitle("Mmmm");
                       alertDialog.setMessage("Quieres ingresar una cantidad mayor al total de cantidad en los pedidos");
                       alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                               new DialogInterface.OnClickListener() {
                                   public void onClick(DialogInterface dialog, int which) {
                                       dialog.dismiss();
                                   }
                               });
                       alertDialog.show();
                   }
                   else{

                       for (int i = 0; i < CustomAdapter.modelArrayList.size(); i++){
                           if(CustomAdapter.modelArrayList.get(i).getSelected()) {
                               datos = CustomAdapter.modelArrayList.get(i).getAnimal().split(" ");
                            //   CustomAdapter.Cantidad = Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad();
                               cantidadDiferencia =  OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1]);

                           }
                       }

                       OrdeCompra.actualizarCantidad(cantidadDiferencia);

                     //  Toast.makeText(context, String.valueOf(Integer.parseInt(datos[1]) - CustomAdapter.Cantidad), Toast.LENGTH_SHORT).show();

                   }


                    /*
                    for (int i = 0; i < CustomAdapter.modelArrayList.size(); i++){
                        if(CustomAdapter.modelArrayList.get(i).getSelected()) {
                            datos = CustomAdapter.modelArrayList.get(i).getAnimal().split(" ");
                            cambiarCantidad = String.valueOf(Integer.parseInt(datos[1]) - CustomAdapter.Cantidad);
                            cambiarCantidad = datos[0] + " " + cambiarCantidad + " " + datos[2];
                            holder.tvAnimal.setText(cambiarCantidad);
                            // Toast.makeText(context,CustomAdapter.modelArrayList.get(i).getAnimal()  , Toast.LENGTH_SHORT).show();
                        }
                    }*/





   /*
                    datos = CustomAdapter.modelArrayList.get(pos).getAnimal().split(" ");

                    if(CustomAdapter.modelArrayList.get(pos).getSelected() == false){

                      //  comprobarCantidad =  Integer.parseInt(datos[1]) + OrdeCompra.obtenerCantidad();

                        if(comprobarIngreso || Integer.parseInt(datos[1]) <= OrdeCompra.obtenerCantidad()  || CustomAdapter.Cantidad >= Integer.parseInt(datos[1])){
                          //  datos = CustomAdapter.modelArrayList.get(pos).getAnimal().split(" ");
                           // String dato = OrdeCompra.recuperarNuevaCantidad(context, datos[0], arrayModificaciones);
                           // Toast.makeText(context, "asd: " + dato, Toast.LENGTH_SHORT).show();
                            OrdeCompra.actualizarCantidad( Integer.parseInt(datos[1]) + OrdeCompra.obtenerCantidad());
                        }else if(Integer.parseInt(datos[1]) > OrdeCompra.obtenerCantidad()){
                            datos = CustomAdapter.modelArrayList.get(pos).getAnimal().split(" ");
                            String dato = OrdeCompra.recuperarNuevaCantidad(context, datos[0], arrayModificaciones);
                         //   Toast.makeText(context, "asd: " + dato, Toast.LENGTH_SHORT).show();
                            OrdeCompra.actualizarCantidad( Integer.parseInt(datos[1]) - Integer.parseInt(dato));
                        }
                    }
                    else if(Integer.parseInt(datos[1]) - CustomAdapter.Cantidad < 0 && cantidadTotal < CustomAdapter.Cantidad || OrdeCompra.obtenerCantidad() <= 0){
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        holder.checkBox.setChecked(false);
                        modelArrayList.get(pos).setSelected(false);
                        alertDialog.setTitle("Mmmm");
                        alertDialog.setMessage("La cantidad a restar es mayor a la cantidad del Pedido");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                   else if(Integer.parseInt(datos[1]) - CustomAdapter.Cantidad < 0 && cantidadTotal < CustomAdapter.Cantidad || OrdeCompra.obtenerCantidad() <= 0){
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        holder.checkBox.setChecked(false);
                        modelArrayList.get(pos).setSelected(false);
                        alertDialog.setTitle("Mmmm");
                        alertDialog.setMessage("La cantidad a restar es mayor a la cantidad del Pedido");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    else if(OrdeCompra.obtenerCantidad()== Integer.parseInt(datos[1])){
                        comprobarIngreso = false;
                       // Toast.makeText(context, "Se va a restar la cantidad y el dato es igual", Toast.LENGTH_SHORT).show();
                        arrayModificaciones.add(datos[0]);
                        arrayModificaciones.add(String.valueOf( OrdeCompra.obtenerCantidad()));
                        OrdeCompra.actualizarCantidad(  OrdeCompra.obtenerCantidad() -  Integer.parseInt(datos[1]));

                    }
                    else if(cantidadTotal > CustomAdapter.Cantidad && OrdeCompra.obtenerCantidad() > 0) {
                       // Toast.makeText(context, "Se va a restar la cantidad", Toast.LENGTH_SHORT).show();
                        if(Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad() == 0  || OrdeCompra.obtenerCantidad() >= Integer.parseInt(datos[1])){
                            OrdeCompra.actualizarCantidad(OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1]));
                            arrayModificaciones.add(datos[0]);
                            arrayModificaciones.add("0");
                            comprobarIngreso = true;
                        }
                        else{
                            OrdeCompra.actualizarCantidad(OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1]));
                            arrayModificaciones.add(datos[0]);
                            arrayModificaciones.add(String.valueOf(OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1])));
                            comprobarIngreso = true;
                        }

                    }

                    else if(Integer.parseInt(datos[1]) > OrdeCompra.obtenerCantidad() && CustomAdapter.modelArrayList.get(pos).getSelected()){

                        if(Integer.parseInt(datos[1]) >= OrdeCompra.obtenerCantidad() ){
                            comprobarIngreso = false;
                            arrayModificaciones.add(datos[0]);
                            arrayModificaciones.add(String.valueOf(Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad()));
                            cantidadPrevia =  Integer.parseInt(datos[1]) - OrdeCompra.obtenerCantidad();
                            OrdeCompra.actualizarCantidad(Integer.parseInt("0"));

                        }else {
                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                            holder.checkBox.setChecked(false);
                            modelArrayList.get(pos).setSelected(false);
                            alertDialog.setTitle("Mmmm");
                            alertDialog.setMessage("La cantidad a restar es mayor a la cantidad del Pedido");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    }
                    else {
                        Toast.makeText(context, "Algún BUG sin corregir" , Toast.LENGTH_SHORT).show();
                    }



                   // Toast.makeText(context, "La cantidad será almacenada con lista mayor a 0" , Toast.LENGTH_SHORT).show();

                    /*

                    if(CustomAdapter.Cantidad >= Integer.parseInt(datos[1]) && OrdeCompra.obtenerCantidad() >= Integer.parseInt(datos[1]) ){
                        OrdeCompra.actualizarCantidad(OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1]));
                        Toast.makeText(context, "El dato será guardado con una lista mayor a uno" , Toast.LENGTH_SHORT).show();
                    }
                    else if(OrdeCompra.obtenerCantidad() > 0){
                        OrdeCompra.actualizarCantidad(0);
                    }
*/


// Toast.makeText(context,"Nueva cantidad" + String.valueOf(Integer.parseInt( OrdeCompra.obtenerCantidad() - Integer.parseInt(datos[1])) ), Toast.LENGTH_SHORT).show();
// int actualizarCantidad = OrdeCompra.obtenerCantidad() - ( Integer.parseInt(datos[1]) );
// OrdeCompra.actualizarCantidad(actualizarCantidad);


                /*
                for (int i = 0; i < datos.length; i++ ){
                    Toast.makeText(context,datos[i], Toast.LENGTH_SHORT).show();
                }*/
