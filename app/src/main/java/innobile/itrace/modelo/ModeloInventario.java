package innobile.itrace.modelo;

public class ModeloInventario {

    private int id;
    private String userName;
    private String Documento;
    private String Ubicacion;
    private String Codigo;
    private String Conteo;
    private int Cantidad;
    private String Estado;
    private String fecha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String documento) {
        Documento = documento;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getConteo() {
        return Conteo;
    }

    public void setConteo(String conteo) {
        Conteo = conteo;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public ModeloInventario() {
    }


    public ModeloInventario(int id, String userName, String Documento, String Ubicacion, String Codigo, String Conteo, int Cantidad, String Estado, String fecha) {

        this.id = id;
        this.userName = userName;
        this.Documento = Documento;
        this.Ubicacion = Ubicacion;
        this.Codigo = Codigo;
        this.Conteo = Conteo;
        this.Cantidad = Cantidad;
        this.Estado = Estado;
        this.fecha = fecha;

    }

}
