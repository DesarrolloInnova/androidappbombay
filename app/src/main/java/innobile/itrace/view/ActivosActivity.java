package innobile.itrace.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import innobile.itrace.R;

public class ActivosActivity extends FragmentContainerActivityToolbarNav {

    private ActivosFragment activosFragment = null;
    private Bundle arguments;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.limpiarCampos) {
            try {
                ActivosFragment activosFragment = new ActivosFragment();
                activosFragment.LimpiarCamposDeActivos(this);
            }
            catch(Exception ex){
                Toast.makeText(this, "Ocurrio un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    Fragment createFragment() {
        activosFragment = ActivosFragment.newInstance();
        arguments = new Bundle();
        arguments.putInt(String.valueOf(FragmentContainerActivityToolbarNav.NAVIGATION_TYPE), FragmentContainerActivityToolbarNav.NAVIGATION_TYPE_BACK);
        activosFragment.setArguments(arguments);
        return activosFragment;
    }


}
