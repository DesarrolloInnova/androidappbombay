package innobile.itrace.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataAccessJson;
import innobile.itrace.modelo.ActivosModel;
import innobile.itrace.transport.IFA_activos;

/**
 * Modificado Alex  12/08/2019
 */
public class ActivosFragment extends Fragment implements View.OnClickListener, View.OnKeyListener {


    private TextView mlblCodigoActivo = null;
/*
    private TextView mlblSerial = null;
    private TextView mlblArea = null;
    private TextView mlblDescripcion = null;
    private TextView mlblCodigoDeBarras = null;
    private TextView mlblFechaDeCompra = null;
    private TextView mlblMesesDepreciacion = null;
    private TextView mlblGrupo = null;
    private TextView mlblCicloDeVida = null;
    private TextView mlblMantenimiento = null;
*/


    //private static Spinner mListCiudades = null;
    private static Spinner mListEstados = null;

/*
    private static Spinner mListTiposDeUbicaciones = null;
    private static Spinner mListUbicaciones = null;
    private static Spinner mListFabricantes = null;
    private static Spinner mListGrupos = null;
    private static Spinner mListEmpresasPropietarias = null;
*/
    private static EditText mBusqueda = null;
    private static EditText mCodigoConteo = null;
    private static EditText mCodigoDeActivo = null;  // CODIGO ACTIVO
    private static EditText mDescripcion = null;  // DESCRIPCION
   // private static EditText mCodigoDeBarras = null;
    //private static EditText mArea = null;
    private static EditText mCiclosDeVida = null;  // BLOQUE
    private static EditText mMantenimiento = null;

    private static double mValorDeCompra = 0;
    private static double mValorIvaDeCompra = 0;
    private static EditText mCuentaContable = null;  //CEDULA RESPONSABLE
    private static EditText mNombreResponsable = null;   // RESPONSABLE
    private static EditText mNombreCuentaContable = null; // ID RECURSOS HUM
    private static EditText mCentrosDeCostos = null; // CENTRO DE COSTOS
    private static EditText mPiso = null;
    private static EditText mUbicaciones = null;
    private static EditText mCiudad = null;
    // private static EditText mTiposDeUbicaciones = null;
    private static EditText mSerial = null;   //SERIAL
    private static EditText mColor = null;  // MARCA
    private static EditText mModelo = null;  //MODELO
    private static EditText mTipoDeActivo = null;  //NOMBRE
    //private static EditText mEstado = null;  // ESTADO
    // private static EditText mFabricantes = null;
    private static EditText mObservaciones = null;


    private static Switch sResponsable = null;
    private static Switch sUbicacion = null;
/*
    private static EditText mDireccionUbicacion = null;
    private static EditText mEnRed = null;
    private static EditText mUsuarioDeRed = null;
    private static EditText mCedulaResponsable = null;
    private static EditText mNombreDeLaMaquina = null;
    private static EditText mSistemasOperativos = null; //SO
    private static EditText mServidor = null; //DIRIP
    private static EditText mOrdenDeCompra = null; //MODELO PROCE
    private static EditText mServicePack= null; //  VERSION PRO
    private static EditText mProcesador = null; // TECNOLOGIA PROCE
    private static EditText mVelocidad = null; //  VELOCIDAD PROC
    private static EditText mDiscoDuro = null; // DISCO DURO
    private static EditText mTotalDeMemoria = null; // MEMORIA
    private static EditText mBloque = null;
    private static EditText midRecuersosH = null;
    private static EditText mUbicacion = null;
    private static EditText mMarca = null;

    private static EditText mEnRed = null;  //

*/

    //Campos que no se ven en el formulario de activos, pero se utilizan para recibir datos y enviar los mismos

/*
    private static String mFechaDeCompra = "";
    private static String mListMesesDepreciacion = "";

    private static String mMaterial = "";
    private static String mRfidTag = "";

    private static String mUltimaFechaReporteDeAgente = "";
    private static String mCanal = "";
    private static String mFechaIngresoBodega = "";

    private static String mListCiclosDeVida = "";
    private static String mListMantenimiento = "";
    private static String mListCentrosDeCostos = "";
    private static String mListSistemasOperativos = "";

    private static String mServidor = "";
    private static String mCuentaContable = "";
    private static String mNombreCuentaContable = "";
    private static String mColor = "";
    private static String mOrdenDeCompra = "";
    private static String mServicePack = "";
    private static String mProcesador = "";
    private static String mVelocidad = "";
    private static String mDiscoDuro = "";
    private static String mTotalDeMemoria = "";

*/

/*
    private EditText mFechaDeCompra = null;
    private Spinner mListMesesDepreciacion = null;
    private Spinner mListCiclosDeVida = null;
    private Spinner mListMantenimiento = null;
    private Spinner mListCentrosDeCostos = null;
    private EditText mValorDeCompra = null;
    private EditText mValorIvaDeCompra = null;
    private EditText mCuentaContable = null;
    private EditText mNombreCuentaContable = null;
    private EditText mColor = null;
    private EditText mMaterial = null;
    private Spinner mListSistemasOperativos = null;
    private EditText mServidor = null;
    private EditText mUltimaFechaReporteDeAgente = null;
    private EditText mRfidTag = null;
    private EditText mCanal = null;
    private EditText mFechaIngresoBodega = null;
    private EditText mOrdenDeCompra = null;
    private EditText mServicePack = null;
    private EditText mProcesador = null;
    private EditText mVelocidad = null;
    private EditText mDiscoDuro = null;
    private EditText mTotalDeMemoria = null;
*/


   // EditText busquedaCedulaResponsable = null;
    private ImageButton mIbtnBuscar = null;
   // private ImageButton mIbtnBuscarArea = null;

    private Calendar myCalendar = null;
    private int requestListadoActivos = 1;
    private int requestListadoAreas = 1;
    private static IFA_activos ifa_activos = null;
    private String areaLeida = "";

    public static ActivosFragment newInstance() {
        ActivosFragment fragment = new ActivosFragment();
        return fragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_activos_fab, container, false);
        setHasOptionsMenu(false);

        ifa_activos = null;

        mIbtnBuscar = (ImageButton) root.findViewById(R.id.ibtnBuscar);
        mIbtnBuscar.setOnClickListener(this);

        //mIbtnBuscarArea = (ImageButton) root.findViewById(R.id.ibtnBuscarArea);
        //mIbtnBuscarArea.setOnClickListener(this);



        // TextView
        Spannable wordtoSpan ;
        mlblCodigoActivo =  root.findViewById(R.id.lblCodigoActivo);
        wordtoSpan = new SpannableString(mlblCodigoActivo.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblCodigoActivo.setText(wordtoSpan);
/*
        mlblSerial =  root.findViewById(R.id.lblSerial);
        mlblSerial =  root.findViewById(R.id.lblSerial);
        Spannable wordtoSpan = new SpannableString(mlblSerial.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblSerial.setText(wordtoSpan);




        mlblArea = root.findViewById(R.id.lblArea);
        wordtoSpan = new SpannableString(mlblArea.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblArea.setText(wordtoSpan);
*/

// initiate a Switch
        sResponsable = root.findViewById(R.id.swMantenerResp);
        sUbicacion  = root.findViewById(R.id.swMantenerUbicacion);
// check current state of a Switch (true or false).

        // spinner

        //mListEmpresasPropietarias =  root.findViewById(R.id.listEmpresasPropietarias);
        //mListGrupos =  root.findViewById(R.id.listGrupos);
        mListEstados =  root.findViewById(R.id.listEstados);
        //mListCiudades = (Spinner) root.findViewById(R.id.listCiudades);
        //mListUbicaciones = (Spinner) root.findViewById(R.id.listUbicaciones);
        //mListTiposDeUbicaciones = (Spinner) root.findViewById(R.id.listTiposUbicaciones);
        // mListFabricantes = (Spinner) root.findViewById(R.id.listFabricantes);

        // EditText
        //mCodigoDeBarras = (EditText) root.findViewById(R.id.txtCodigoBarras); // Codigo de barras / Placa
        mCodigoConteo = root.findViewById(R.id.txtCodigoConteo);

        mCodigoDeActivo = (EditText) root.findViewById(R.id.txtCodigoActivo);
        mDescripcion = (EditText) root.findViewById(R.id.txtDescripcion);
        mCiclosDeVida = (EditText) root.findViewById(R.id.txtBloque); // BLOQUE
        mCentrosDeCostos = (EditText) root.findViewById(R.id.txtCenCo); // CENTRO DE COSTOS
        mCuentaContable= (EditText) root.findViewById(R.id.txtCedResponsable);  //CEDULA RESPONSABLE
        mNombreCuentaContable = (EditText) root.findViewById(R.id.txtRecursosH);; // ID RECURSOS HUM
        mPiso = (EditText) root.findViewById(R.id.txtPiso);  // PISO
        mSerial = (EditText) root.findViewById(R.id.txtSerial); // SERIAL
        mCiudad = (EditText) root.findViewById(R.id.txtCiudad); // CIUDAD
        mColor = (EditText) root.findViewById(R.id.txtMarca);  // MARCA
        mModelo = (EditText) root.findViewById(R.id.txtModelo);  // MODELO
        mTipoDeActivo = (EditText) root.findViewById(R.id.txtNombre);  // NOMBRE

        //mEstado = (EditText) root.findViewById(R.id.txtTipoActivo);  // ESTADO

        mObservaciones = (EditText) root.findViewById(R.id.txtObservaciones); // OBSERVACIONES

        mNombreResponsable = (EditText) root.findViewById(R.id.txtResponsable);  // RESPONSABLE


        //mArea = (EditText) root.findViewById(R.id.txtArea);

        //mEnRed = (EditText) root.findViewById(R.id.txtEnRed);
        //mUsuarioDeRed = (EditText) root.findViewById(R.id.txtUsuarioDeRed);


        // mDireccionUbicacion = (EditText) root.findViewById(R.id.txtDireccionUbicacion);
        //mCedulaResponsable = (EditText) root.findViewById(R.id.txtCedulaResponsable);
        //mNombreDeLaMaquina = (EditText) root.findViewById(R.id.txtNombreMaquina);

        //mSistemasOperativos = (EditText) root.findViewById(R.id.txtSO); //SO
        //mServidor = (EditText) root.findViewById(R.id.txtDIRIP); //DIRIP
        //mOrdenDeCompra = (EditText) root.findViewById(R.id.txtModProc); //MODELO PROCE
        //mServicePack = (EditText) root.findViewById(R.id.txtVProc); //  VERSION PRO
        //mProcesador = (EditText) root.findViewById(R.id.txtProTec); // TECNOLOGIA PROCE
        //mVelocidad = (EditText) root.findViewById(R.id.txtProVel);  //  VELOCIDAD PROC
        //mDiscoDuro = (EditText) root.findViewById(R.id.txtDisDur); // DISCO DURO
        //mTotalDeMemoria = (EditText) root.findViewById(R.id.txtMemo);  // MEMORIA

        //mEnRed = (EditText) root.findViewById(R.id.txtEnRed); //


      // mMantenimiento = (EditText) root.findViewById(R.id.txtDiad); // DIADEMA SI O NO
        //mFabricantes = (EditText) root.findViewById(R.id.txtFabricantes);
        //mTiposDeUbicaciones = (EditText) root.findViewById(R.id.txtTiposUbicaciones);
        mUbicaciones = (EditText) root.findViewById(R.id.txtUbicaciones);

        mBusqueda = (EditText) root.findViewById(R.id.txtBusqueda);
        mBusqueda.setOnKeyListener(this);

        // mNombreResponsable = (EditText) root.findViewById(R.id.txtNombreResponsable);
        // mTotalBancosMemoria = (EditText) root.findViewById(R.id.txtDiad); // DIADEMA
        // mBancosUsuados = (EditText) root.findViewById(R.id.txtMarDia); // MARCA DIAMEMA
        //mBancosLibres = (EditText) root.findViewById(R.id.txtCantPu); // CANTIDAD PUERTOS
        // Campos que se


        //mlblGrupo = (TextView) root.findViewById(R.id.lblGrupo);
        //mlblDescripcion = (TextView) root.findViewById(R.id.lblDescripcion);
        //mlblCodigoDeBarras = (TextView) root.findViewById(R.id.lblCodigoBarras);
        //mlblFechaDeCompra = (TextView) root.findViewById(R.id.lblFechaCompra);
        //mlblMesesDepreciacion = (TextView) root.findViewById(R.id.lblMesesDepreciacion);

        //mListMesesDepreciacion = (Spinner) root.findViewById(R.id.listMesesDepreciacion);

        /*mlblCicloDeVida = (TextView) root.findViewById(R.id.lblCicloVida);
        wordtoSpan = new SpannableString(mlblCicloDeVida.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblCicloDeVida.setText(wordtoSpan);*/

        /*mlblMantenimiento = (TextView) root.findViewById(R.id.lblMantenimiento);
        wordtoSpan = new SpannableString(mlblMantenimiento.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblMantenimiento.setText(wordtoSpan);*/

        //mListCiclosDeVida = (Spinner) root.findViewById(R.id.listCiclosVida);
        //mListMantenimiento = (Spinner) root.findViewById(R.id.listMantenimiento);
        //mListCentrosDeCostos = (Spinner) root.findViewById(R.id.listCentrosCostos);
        //mValorDeCompra = (EditText) root.findViewById(R.id.txtValorCompra);
        //mValorIvaDeCompra = (EditText) root.findViewById(R.id.txtValorIvaCompra);
        //mCuentaContable = (EditText) root.findViewById(R.id.txtCuentaContable);
        //mNombreCuentaContable = (EditText) root.findViewById(R.id.txtNombreCuentaContable);
        //mColor = (EditText) root.findViewById(R.id.txtColor);
        //mMaterial = (EditText) root.findViewById(R.id.txtMaterial);
        //mListSistemasOperativos = (Spinner) root.findViewById(R.id.listSistemasOperativos);
        //mServidor = (EditText) root.findViewById(R.id.txtServidor);
        //mUltimaFechaReporteDeAgente = (EditText) root.findViewById(R.id.txtUltimaFechaReporteAgente);
        //mRfidTag = (EditText) root.findViewById(R.id.txtRfidTag);
        //mCanal = (EditText) root.findViewById(R.id.txtCanal);
        //mFechaIngresoBodega = (EditText) root.findViewById(R.id.txtFechaIngresoBodega);
        //mOrdenDeCompra = (EditText) root.findViewById(R.id.txtOrdenDeCompra);
        //mServicePack = (EditText) root.findViewById(R.id.txtServicePack);
        //mProcesador = (EditText) root.findViewById(R.id.txtProcesador);
        //mVelocidad = (EditText) root.findViewById(R.id.txtVelocidad);
        //mDiscoDuro = (EditText) root.findViewById(R.id.txtDiscoDuro);
        //mTotalDeMemoria = (EditText) root.findViewById(R.id.txtTotalDeMemoria);

        //Se obtiene parámetro desde la actividad de gestión de inventario(Donde se selecciona el área)
        areaLeida = getActivity().getIntent().getStringExtra("areaLeida");

        //Se declara el listener para el campo de texto de la cédula del responsable
       // busquedaCedulaResponsable = (EditText) root.findViewById(R.id.txtCedulaResponsable);

 /*
        busquedaCedulaResponsable.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            private Handler mHandler = new Handler();

            Runnable mFilterTask = new Runnable() {

                @Override
                public void run() {
                    if (!mCedulaResponsable.getText().toString().trim().equals("")) {
                        new GetNombreResponsableFromRF().execute(getActivity(), mCedulaResponsable.getText().toString().trim());
                    } else {
                        mNombreResponsable.setText("");
                    }
                }
            };

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(getActivity(), "Se ingreso el codigo: " + busquedaCedulaResponsable.getText().toString(), Toast.LENGTH_SHORT).show();
                mHandler.removeCallbacks(mFilterTask);
                mHandler.postDelayed(mFilterTask, 1000);
            }
        });
*/


        //Se declara el listener para el botón flotante guardar
        FloatingActionButton floatingActionButton = (FloatingActionButton) root.findViewById(R.id.fab_activos);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(mCodigoDeActivo.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "El código de activo es obligatorio", Toast.LENGTH_SHORT).show();
                    mCodigoDeActivo.requestFocus();
                    return;
                }
/*

                if (TextUtils.isEmpty(mSerial.getText().toString().trim())) {
                    //Muestra un mensaje en pantalla si el código del activo es vacío
                    Toast.makeText(getActivity(), "El serial de activo es obligatorio", Toast.LENGTH_SHORT).show();
                    mSerial.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(mArea.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "El área es obligatoria", Toast.LENGTH_SHORT).show();
                    mArea.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(((GetListadosActivosAsyncTask.ObjetosClase) mListCiclosDeVida.getSelectedItem()).getId())) {
                    Toast.makeText(getActivity(), "El ciclo de vida es obligatorio", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(((GetListadosActivosAsyncTask.ObjetosClase) mListMantenimiento.getSelectedItem()).getId())) {
                    Toast.makeText(getActivity(), "El mantenimiento es obligatorio", Toast.LENGTH_SHORT).show();
                    return;
                }
*/
               // double valorDeCompra = 0;
               // double valorIvaDeCompra = 0;
               // double velocidad = 0;
                //double discoDuro = 0;
                //double totalDeMemoria = 0;

                /*if (!mValorDeCompra.getText().toString().trim().isEmpty()) {
                    try {
                        valorDeCompra = Double.parseDouble(mValorDeCompra.getText().toString().trim());
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(), "El valor de compra debe ser numérico", Toast.LENGTH_SHORT).show();
                        mValorDeCompra.setFocusable(true);
                        return;
                    }
                }*/
                /*if (!mValorIvaDeCompra.getText().toString().trim().isEmpty()) {
                    try {
                        valorIvaDeCompra = Double.parseDouble(mValorIvaDeCompra.getText().toString().trim());
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(), "El valor del iva de compra debe ser numérico", Toast.LENGTH_SHORT).show();
                        mValorIvaDeCompra.setFocusable(true);
                        return;
                    }
                }*/
                /*if (!mVelocidad.getText().toString().trim().isEmpty()) {
                    try {
                        velocidad = Double.parseDouble(mVelocidad.getText().toString().trim());
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(), "La velocidad debe ser numérico", Toast.LENGTH_SHORT).show();
                        mVelocidad.setFocusable(true);
                        return;
                    }
                }*/
                /*if (!mDiscoDuro.getText().toString().trim().isEmpty()) {
                    try {
                        discoDuro = Double.parseDouble(mDiscoDuro.getText().toString().trim());
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(), "El disco duro debe ser numérico", Toast.LENGTH_SHORT).show();
                        mDiscoDuro.setFocusable(true);
                        return;
                    }
                }*/
                /*if (!mTotalDeMemoria.getText().toString().trim().isEmpty()) {
                    try {
                        totalDeMemoria = Double.parseDouble(mTotalDeMemoria.getText().toString().trim());
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(), "El total de memoria debe ser numérico", Toast.LENGTH_SHORT).show();
                        mTotalDeMemoria.setFocusable(true);
                        return;
                    }
                }*/


                String estado = "";
                try {
                    estado = ((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    estado = ((ObjetosClase) mListEstados.getSelectedItem()).getId().trim();
                }
 /*
                String ciudad = "";
                try {
                    ciudad = ((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    ciudad = ((ObjetosClase) mListCiudades.getSelectedItem()).getId().trim();
                }



                String empresaPropietaria = "";
                try {
                    empresaPropietaria = ((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    empresaPropietaria = ((ObjetosClase) mListEmpresasPropietarias.getSelectedItem()).getId().trim();
                }

                String grupo = "";
                try {
                    grupo = ((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    grupo = ((ObjetosClase) mListGrupos.getSelectedItem()).getId().trim();
                }


                String ubicacion = "";
                try {
                    ubicacion = ((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    ubicacion = ((ObjetosClase) mListUbicaciones.getSelectedItem()).getId().trim();
                }

                String tipoUbicacion = "";
                try {
                    tipoUbicacion = ((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    tipoUbicacion = ((ObjetosClase) mListTiposDeUbicaciones.getSelectedItem()).getId().trim();
                }



                String fabricante = "";
                try {
                    fabricante = ((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getSelectedItem()).getId().trim();
                } catch (Exception ex) {
                    fabricante = ((ObjetosClase) mListFabricantes.getSelectedItem()).getId().trim();
                }
*/

                IFA_activos ifa_activos = new IFA_activos(mCodigoDeActivo.getText().toString().trim(),
                        mDescripcion.getText().toString().trim(),
                        "", //mCodigoDeBarras.getText().toString(),
                        "", //mFechaDeCompra.trim(),
                        "0",//meses depreciacion
                        //mListMesesDepreciacion.trim(),
                        areaLeida,//mArea.getText().toString().trim(),
                        "", //grupo,
                        mCiclosDeVida.getText().toString().trim(),
                       "", // mMantenimiento.getText().toString().trim(),
                        mCentrosDeCostos.getText().toString().trim(),
                        //mListCiclosDeVida.trim(),
                        //mListMantenimiento.trim(),
                        //mListCentrosDeCostos.trim(),
                        mValorDeCompra,
                        mValorIvaDeCompra,
                        //mCuentaContable.trim(),
                        //mNombreCuentaContable.trim(),
                        mCuentaContable.getText().toString().trim(),
                        mNombreCuentaContable.getText().toString().trim(),
                        mCiudad.getText().toString().trim(), //ciudad,
                        mPiso.getText().toString().trim(),
                        mUbicaciones.getText().toString().trim(),   //ubicacion,
                        "", //mTiposDeUbicaciones.getText().toString().trim(),    //tipoUbicacion,
                        mSerial.getText().toString().trim(),
                        mColor.getText().toString().trim(),  //mColor.trim(),
                        mModelo.getText().toString().trim(),
                        mTipoDeActivo.getText().toString().trim(),
                        "", //mMaterial.trim(),
                        estado,
                        "", //mFabricantes.getText().toString().trim(),   // fabricante,
                        "", //empresaPropietaria,
                        "", //mNombreDeLaMaquina.getText().toString().trim(),
                        "", //mSistemasOperativos.getText().toString().trim(),     //mListSistemasOperativos.trim(),
                        "", //mServidor.getText().toString().trim(),   //mServidor.trim(),
                       "", // mUltimaFechaReporteDeAgente.trim(),
                        mObservaciones.getText().toString().trim(),
                        "", //mRfidTag.trim(),
                        mNombreResponsable.getText().toString().trim(),
                        "", //mDireccionUbicacion.getText().toString().trim(),
                        "", //mCanal.trim(),
                        "", //mEnRed.getText().toString().trim(),
                        "", //mUsuarioDeRed.getText().toString().trim(),
                        "", //mFechaIngresoBodega.trim(),
                        "", //mOrdenDeCompra.getText().toString().trim(),  //mOrdenDeCompra.trim(),
                        "", // mServicePack.getText().toString().trim(),  //mServicePack.trim(),
                        "", // mProcesador.getText().toString().trim(),   //mProcesador.trim(),
                        "", // mVelocidad.getText().toString().trim(),    //String.valueOf(velocidad),
                        "", // mDiscoDuro.getText().toString().trim(),    //String.valueOf(discoDuro),
                        "", //mTotalDeMemoria.getText().toString().trim(),    //String.valueOf(totalDeMemoria),
                        mCodigoConteo.getText().toString().trim()
                );

                /*
                IFA_activos ifa_activos = new IFA_activos(mCodigoDeActivo.getText().toString().trim(),
                        mDescripcion.getText().toString().trim(), mCodigoDeBarras.getText().toString(),
                        mFechaDeCompra.getText().toString().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListMesesDepreciacion.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListAreas.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListCiclosDeVida.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListMantenimiento.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListCentrosDeCostos.getSelectedItem()).getId().trim(),
                        valorDeCompra,
                        valorIvaDeCompra,
                        mCuentaContable.getText().toString().trim(),
                        mNombreCuentaContable.getText().toString().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getSelectedItem()).getId().trim(),
                        mPiso.getText().toString().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getSelectedItem()).getId().trim(),
                        mSerial.getText().toString().trim(),
                        mColor.getText().toString().trim(),
                        mModelo.getText().toString().trim(),
                        mTipoDeActivo.getText().toString().trim(),
                        mMaterial.getText().toString().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getSelectedItem()).getId().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getSelectedItem()).getId().trim(),
                        mNombreDeLaMaquina.getText().toString().trim(),
                        ((GetListadosActivosAsyncTask.ObjetosClase) mListSistemasOperativos.getSelectedItem()).getId().trim(),
                        mServidor.getText().toString().trim(),
                        mUltimaFechaReporteDeAgente.getText().toString().trim(),
                        mObservaciones.getText().toString().trim(),
                        mRfidTag.getText().toString().trim(),
                        "",
                        mDireccionUbicacion.getText().toString().trim(),
                        mCanal.getText().toString().trim(),
                        mEnRed.getText().toString().trim(),
                        mUsuarioDeRed.getText().toString().trim(),
                        mFechaIngresoBodega.getText().toString().trim(),
                        mOrdenDeCompra.getText().toString().trim(),
                        mServicePack.getText().toString().trim(),
                        mProcesador.getText().toString().trim(),
                        String.valueOf(velocidad),
                        String.valueOf(discoDuro),
                        String.valueOf(totalDeMemoria)
                );
                 */

                ActivosModel activosModel = new ActivosModel();
                try {
                    if (areaLeida == null) {
                        new SetActivoAsyncTask().execute(getActivity(), areaLeida, "", Boolean.FALSE.toString(), Cons.USUARIO_LOGUEADO, ifa_activos);
                    } else {
                        new SetActivoAsyncTask().execute(getActivity(), areaLeida, "", Boolean.TRUE.toString(), Cons.USUARIO_LOGUEADO, ifa_activos);
                    }
                } catch (Exception e) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                    dlgAlert.setMessage("Ocurrió un error al guardar el activo");
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);

                }
            }
        });

        myCalendar = Calendar.getInstance();


/*
        //Se declara un DateTimePickerDialog para ser usado en los EditText que lo requieran
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                ActualizarDateTimePickerFechaDeCompra();
            }

        };

*/

        //Se inicializa el listener para el EditText de fecha de compra
        /*mFechaDeCompra.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mFechaDeCompra.setText("");
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mFechaDeCompra.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                mFechaDeCompra.setText("");
                return true;
            }
        });
        */

        //Evita que se abra el SoftKeyBoard(Teclado) al precionar en el EditText mFechaDeCompra
        //mFechaDeCompra.setInputType(InputType.TYPE_NULL);

        if (savedInstanceState == null) {
            new GetListadosActivosAsyncTask().execute(getActivity());
        }

        /*
        Se usa para no mostrar el teclado(Softkeyboard) al abrir la ventana.
        Se necesita ademas que la actividad en el manifest contenga: android:windowSoftInputMode="adjustResize"
         */
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        //inputMethodManager.hideSoftInputFromWindow(mFechaDeCompra.getWindowToken(), 0);

        return root;
    }


    

    /**
     * Infla el menú, osea se encarga de mostrar el menú(xml) en la actividad
     *
     * @param menu
     * @return
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        //Sirva para controlar lo que hace el botón del menú
        try {
            switch (item.getItemId()) {
                case R.id.menu_menu:
                    Intent intent = new Intent(getActivity(), ConfiguracionesMenuActivity.class);
                    this.startActivity(intent);
                    break;
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return true;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        try {
            super.onViewStateRestored(savedInstanceState);
            if (savedInstanceState != null) {



                ArrayList arrayListEstados = new ArrayList<>();
                arrayListEstados = savedInstanceState.getParcelableArrayList("estados");
                String valueSeleccionadoEstado = savedInstanceState.getString("estadoSeleccionado");


/*

                ArrayList arrayListCiudades = new ArrayList<>();
                arrayListCiudades = savedInstanceState.getParcelableArrayList("ciudades");
                String valueSeleccionadoCiudades = savedInstanceState.getString("ciudadSeleccionada");


                ArrayList arrayListEmpresasPropietarias = new ArrayList<>();
                arrayListEmpresasPropietarias = savedInstanceState.getParcelableArrayList("empresasPropietarias");
                String valueSeleccionadoEmpresaPropietaria = savedInstanceState.getString("empresaPropietariaSeleccionada");

                ArrayList arrayListGrupos = new ArrayList<>();
                arrayListGrupos = savedInstanceState.getParcelableArrayList("grupos");
                String valueSeleccionadoGrupo = savedInstanceState.getString("grupoSeleccionado");

                ArrayList arrayListUbicaciones = new ArrayList<>();
                arrayListUbicaciones = savedInstanceState.getParcelableArrayList("ubicaciones");
                String valueSeleccionadoUbicacion = savedInstanceState.getString("ubicacionSeleccionada");

                ArrayList arrayListTiposUbicaciones = new ArrayList<>();
                arrayListTiposUbicaciones = savedInstanceState.getParcelableArrayList("tiposUbicaciones");
                String valueSeleccionadoTipoUbicacion = savedInstanceState.getString("tipoUbicacionSeleccionada");


                ArrayList arrayListFabricantes = new ArrayList<>();
                arrayListFabricantes = savedInstanceState.getParcelableArrayList("fabricantes");
                String valueSeleccionadoFabricante = savedInstanceState.getString("fabricanteSeleccionado");


                LinkedList listadoCiudades = new LinkedList();
                for (Object ciudad : arrayListCiudades) {
                    String[] datosCiudad = ((String) ciudad).split("~");
                    listadoCiudades.add(new ObjetosClase(datosCiudad[0], datosCiudad[1]));
                }

                ArrayAdapter spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoCiudades);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListCiudades.setAdapter(spinner_adapter);
                mListCiudades.setSelection(Integer.parseInt(valueSeleccionadoCiudades));

*/

                LinkedList listadoEstados = new LinkedList();
                for (Object estado : arrayListEstados) {
                    String[] datosEstado = ((String) estado).split("~");
                    listadoEstados.add(new ObjetosClase(datosEstado[0], datosEstado[1]));
                }
                ArrayAdapter spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoEstados);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListEstados.setAdapter(spinner_adapter);
                mListEstados.setSelection(Integer.parseInt(valueSeleccionadoEstado));

/*

                LinkedList listadoEmpresaPropietaria = new LinkedList();
                for (Object empresaPropietaria : arrayListEmpresasPropietarias) {
                    String[] datosEmpresaPropietaria = ((String) empresaPropietaria).split("~");
                    listadoEmpresaPropietaria.add(new ObjetosClase(datosEmpresaPropietaria[0], datosEmpresaPropietaria[1]));
                }
                spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoEmpresaPropietaria);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListEmpresasPropietarias.setAdapter(spinner_adapter);
                mListEmpresasPropietarias.setSelection(Integer.parseInt(valueSeleccionadoEmpresaPropietaria));



                LinkedList listadoGrupos = new LinkedList();
                for (Object grupo : arrayListGrupos) {
                    String[] datosGrupo = ((String) grupo).split("~");
                    listadoGrupos.add(new ObjetosClase(datosGrupo[0], datosGrupo[1]));
                }
                spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoGrupos);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListGrupos.setAdapter(spinner_adapter);
                mListGrupos.setSelection(Integer.parseInt(valueSeleccionadoGrupo));


                LinkedList listadoUbicaciones = new LinkedList();
                for (Object ubicacion : arrayListUbicaciones) {
                    String[] datosUbicacion = ((String) ubicacion).split("~");
                    listadoUbicaciones.add(new ObjetosClase(datosUbicacion[0], datosUbicacion[1]));
                }
                spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoUbicaciones);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListUbicaciones.setAdapter(spinner_adapter);
                mListUbicaciones.setSelection(Integer.parseInt(valueSeleccionadoUbicacion));


                LinkedList listadoTiposUbicaciones = new LinkedList();
                for (Object tipoUbicacion : arrayListTiposUbicaciones) {
                    String[] datosTipoUbicacion = ((String) tipoUbicacion).split("~");
                    listadoTiposUbicaciones.add(new ObjetosClase(datosTipoUbicacion[0], datosTipoUbicacion[1]));
                }
                spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoTiposUbicaciones);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListTiposDeUbicaciones.setAdapter(spinner_adapter);
                mListTiposDeUbicaciones.setSelection(Integer.parseInt(valueSeleccionadoTipoUbicacion));


                LinkedList listadoFabricantes = new LinkedList();
                for (Object fabricante : arrayListFabricantes) {
                    String[] datosFabricante = ((String) fabricante).split("~");
                    listadoFabricantes.add(new ObjetosClase(datosFabricante[0], datosFabricante[1]));
                }
                spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoFabricantes);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mListFabricantes.setAdapter(spinner_adapter);
                mListFabricantes.setSelection(Integer.parseInt(valueSeleccionadoFabricante));

*/

            }
        } catch (Exception ex) {
            String xx = "";
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        try {

            super.onSaveInstanceState(outState);
            ArrayList arrayListCiudades = new ArrayList<>();
            ArrayList arrayListEstados = new ArrayList<>();

/*
            ArrayList arrayListEmpresasPropietarias = new ArrayList<>();
            ArrayList arrayListUbicaciones = new ArrayList<>();
            ArrayList arrayListGrupos = new ArrayList<>();
            ArrayList arrayListTiposUbicaciones = new ArrayList<>();
            ArrayList arrayListFabricantes = new ArrayList<>();
*/





            for (int index = 0; index < mListEstados.getCount(); index++) {
                try {
                    arrayListEstados.add(((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getItemAtPosition(index)).getId().trim() + "~" + mListEstados.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListEstados.add(((ObjetosClase) mListEstados.getItemAtPosition(index)).getId().trim() + "~" + mListEstados.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("estados", (ArrayList<? extends Parcelable>) arrayListEstados);
            outState.putString("estadoSeleccionado", String.valueOf(mListEstados.getSelectedItemId()));


/*


            for (int index = 0; index < mListCiudades.getCount(); index++) {
                try {
                    arrayListCiudades.add(((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getItemAtPosition(index)).getId().trim() + "~" + mListCiudades.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListCiudades.add(((ObjetosClase) mListCiudades.getItemAtPosition(index)).getId().trim() + "~" + mListCiudades.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("ciudades", (ArrayList<? extends Parcelable>) arrayListCiudades);
            outState.putString("ciudadSeleccionada", String.valueOf(mListCiudades.getSelectedItemId()));



            for (int index = 0; index < mListEmpresasPropietarias.getCount(); index++) {
                try {
                    arrayListEmpresasPropietarias.add(((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getItemAtPosition(index)).getId().trim() + "~" + mListEmpresasPropietarias.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListEmpresasPropietarias.add(((ObjetosClase) mListEmpresasPropietarias.getItemAtPosition(index)).getId().trim() + "~" + mListEmpresasPropietarias.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("empresasPropietarias", (ArrayList<? extends Parcelable>) arrayListEmpresasPropietarias);
            outState.putString("empresaPropietariaSeleccionada", String.valueOf(mListEmpresasPropietarias.getSelectedItemId()));


            for (int index = 0; index < mListGrupos.getCount(); index++) {
                try {
                    arrayListGrupos.add(((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getItemAtPosition(index)).getId().trim() + "~" + mListGrupos.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListGrupos.add(((ObjetosClase) mListGrupos.getItemAtPosition(index)).getId().trim() + "~" + mListGrupos.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("grupos", (ArrayList<? extends Parcelable>) arrayListGrupos);
            outState.putString("grupoSeleccionado", String.valueOf(mListGrupos.getSelectedItemId()));


            for (int index = 0; index < mListUbicaciones.getCount(); index++) {
                try {
                    arrayListUbicaciones.add(((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getItemAtPosition(index)).getId().trim() + "~" + mListUbicaciones.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListUbicaciones.add(((ObjetosClase) mListUbicaciones.getItemAtPosition(index)).getId().trim() + "~" + mListUbicaciones.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("ubicaciones", (ArrayList<? extends Parcelable>) arrayListEmpresasPropietarias);
            outState.putString("ubicacionSeleccionada", String.valueOf(mListUbicaciones.getSelectedItemId()));


            for (int index = 0; index < mListTiposDeUbicaciones.getCount(); index++) {
                try {
                    arrayListTiposUbicaciones.add(((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getItemAtPosition(index)).getId().trim() + "~" + mListTiposDeUbicaciones.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListTiposUbicaciones.add(((ObjetosClase) mListTiposDeUbicaciones.getItemAtPosition(index)).getId().trim() + "~" + mListTiposDeUbicaciones.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("tiposUbicaciones", (ArrayList<? extends Parcelable>) arrayListTiposUbicaciones);
            outState.putString("tipoUbicacionSeleccionada", String.valueOf(mListTiposDeUbicaciones.getSelectedItemId()));


            for (int index = 0; index < mListFabricantes.getCount(); index++) {
                try {
                    arrayListFabricantes.add(((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getItemAtPosition(index)).getId().trim() + "~" + mListFabricantes.getItemAtPosition(index));
                } catch (Exception ex) {
                    arrayListFabricantes.add(((ObjetosClase) mListFabricantes.getItemAtPosition(index)).getId().trim() + "~" + mListFabricantes.getItemAtPosition(index));
                }
            }
            outState.putParcelableArrayList("fabricantes", (ArrayList<? extends Parcelable>) arrayListFabricantes);
            outState.putString("fabricanteSeleccionado", String.valueOf(mListFabricantes.getSelectedItemId()));
*/

        } catch (Exception ex) {
            String xx = "";
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtnBuscar:
                new GetActivoFromRF().execute(getActivity(), mBusqueda.getText().toString().trim());
                break;
           // case R.id.ibtnBuscarArea:
            //    new GetAreasAsyncTask().execute(getActivity(), mArea.getText().toString().trim());
           //     break;
        }

        getActivity().onBackPressed();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER) && !mBusqueda.getText().toString().equals("")) {
            new GetActivoFromRF().execute(getActivity(), mBusqueda.getText().toString().trim());
            return true;
        }

        mBusqueda.requestFocus();
        return false;
    }

    public class ObjetosClase {
        String id;
        String nombre;

        //Constructor
        public ObjetosClase(String id, String nombre) {
            super();
            this.id = id;
            this.nombre = nombre;
        }

        @Override
        public String toString() {
            return nombre;
        }

        public String getId() {
            return id;
        }
    }

    /**
     * Encargado de gestionar el Intent que hemos recibido de la actividad ListadoActivosActivity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if ((requestCode == requestListadoActivos) && (resultCode == -1)) {//RESULT_OK
                String jsonActivos = data.getDataString();
                ifa_activos = new DataAccessJson().parserJsonActivoRequest(jsonActivos);

                if (ifa_activos != null) {

                    //DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                    mCodigoConteo.setText(ifa_activos.getAct_codigo_conteo());
                    mCodigoDeActivo.setText(ifa_activos.getAct_codigo_activo());
                    mDescripcion.setText(ifa_activos.getAct_descripcion());
                    //mCodigoDeBarras.setText(ifa_activos.getAct_codigo_barras());
                    //Date fecha = ifa_activos.getAct_fecha_compra();
                    //String strDate = df.format(fecha);
                    //mFechaDeCompra.setText(ifa_activos.getAct_fecha_compra());
                    //mFechaDeCompra = ifa_activos.getAct_fecha_compra();
                   // mCedulaResponsable.setText(ifa_activos.getAct_responsable().trim());
                    //mArea.setText(ifa_activos.getAct_area());
                    //mListCiclosDeVida = ifa_activos.getAct_ciclo_vida();
                    //mListMantenimiento = ifa_activos.getAct_mantenimiento();
                    //mListCentrosDeCostos = ifa_activos.getAct_centro_costos();

                   // mMantenimiento.setText(ifa_activos.getAct_mantenimiento());



                    //mValorDeCompra.setText(String.valueOf(ifa_activos.getAct_valor_compra()));
                   // mValorDeCompra = ifa_activos.getAct_valor_compra();
                    //mValorIvaDeCompra.setText(String.valueOf(ifa_activos.getAct_valor_iva_compra()));
                   // mValorIvaDeCompra = ifa_activos.getAct_valor_iva_compra();



                    mSerial.setText(ifa_activos.getAct_serial());
                    mColor.setText(ifa_activos.getAct_color());
                    //mColor = ifa_activos.getAct_color();
                    mModelo.setText(ifa_activos.getAct_modelo());
                    mTipoDeActivo.setText(ifa_activos.getAct_tipo_activo());
                    //mEstado.setText(ifa_activos.getAct_estado());

                    //mMaterial.setText(ifa_activos.getAct_material());
                    //mMaterial = ifa_activos.getAct_material();
                   // mTiposDeUbicaciones.setText(ifa_activos.getAct_tipo_ubicacion());




                    if (!sResponsable.isChecked())

                    {

                        mCuentaContable.setText(ifa_activos.getAct_cuenta_contable()); //CEDULA RESPONSABLE
                        mNombreResponsable.setText(ifa_activos.getAct_responsable());  // RESPONSABLE
                        mNombreCuentaContable.setText(ifa_activos.getAct_nombre_cuenta_contable());  // ID RECURSOS HUM
                        mCentrosDeCostos.setText(ifa_activos.getAct_centro_costos());  // CENTRO DE COSTOS


                    }


                    if (!sUbicacion.isChecked())
                    {
                        mCiudad.setText(areaLeida);// mCiudad.setText(ifa_activos.getAct_ciudad());
                        mPiso.setText(ifa_activos.getAct_piso());
                        mUbicaciones.setText(ifa_activos.getAct_ubicacion());
                        mCiclosDeVida.setText(ifa_activos.getAct_ciclo_vida());

                    }



                    String estadoValue = String.valueOf(ifa_activos.getAct_estado());
                    for (int index = 0; index < mListEstados.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                                mListEstados.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                                mListEstados.setSelection(index);
                                break;
                            }
                        }
                    }


/*


                    String ciudadValue = String.valueOf(ifa_activos.getAct_ciudad());
                    for (int index = 0; index < mListCiudades.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                                mListCiudades.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                                mListCiudades.setSelection(index);
                                break;
                            }
                        }
                    }


                    String grupoValue = String.valueOf(ifa_activos.getAct_grupo());
                    for (int index = 0; index < mListGrupos.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                                mListGrupos.setSelection(index);
                                break;
                            }
                        } catch (Exception ex) {
                            if (((ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                                mListGrupos.setSelection(index);
                                break;
                            }
                        }
                    }


                    String empresaPropietariaValue = String.valueOf(ifa_activos.getAct_empresa_propietaria());
                    for (int index = 0; index < mListEmpresasPropietarias.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                                mListEmpresasPropietarias.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                                mListEmpresasPropietarias.setSelection(index);
                                break;
                            }
                        }
                    }
*/

                    /*String mesesDepreciacionValue = String.valueOf(ifa_activos.getAct_meses_depreciacion());
                    for (int index = 0; index < mListMesesDepreciacion.getCount(); index++) {
                        if (((GetListadosActivosAsyncTask.ObjetosClase) mListMesesDepreciacion.getAdapter().getItem(index)).id.equals(mesesDepreciacionValue)) {
                            mListMesesDepreciacion.setSelection(index);
                            break;
                        }
                    }*/
                    //mListMesesDepreciacion = String.valueOf(ifa_activos.getAct_meses_depreciacion());


                    /*String cicloDeVidaValue = String.valueOf(ifa_activos.getAct_ciclo_vida());
                    for (int index = 0; index < mListCiclosDeVida.getCount(); index++) {
                        if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiclosDeVida.getAdapter().getItem(index)).id.equals(cicloDeVidaValue)) {
                            mListCiclosDeVida.setSelection(index);
                            break;
                        }
                    }*/

                    /*String mantenimientoValue = String.valueOf(ifa_activos.getAct_mantenimiento());
                    for (int index = 0; index < mListMantenimiento.getCount(); index++) {
                        if (((GetListadosActivosAsyncTask.ObjetosClase) mListMantenimiento.getAdapter().getItem(index)).id.equals(mantenimientoValue.toUpperCase())) {
                            mListMantenimiento.setSelection(index);
                            break;
                        }
                    }*/


                    /*String centroDeCostosValue = String.valueOf(ifa_activos.getAct_centro_costos());
                    for (int index = 0; index < mListCentrosDeCostos.getCount(); index++) {
                        if (((GetListadosActivosAsyncTask.ObjetosClase) mListCentrosDeCostos.getAdapter().getItem(index)).id.equals(centroDeCostosValue)) {
                            mListCentrosDeCostos.setSelection(index);
                            break;
                        }
                    }*/



/*
                    String ubicacionValue = String.valueOf(ifa_activos.getAct_ubicacion());
                    for (int index = 0; index < mListUbicaciones.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                                mListUbicaciones.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                                mListUbicaciones.setSelection(index);
                                break;
                            }
                        }
                    }

                    String tipoDeUbicacionesValue = String.valueOf(ifa_activos.getAct_tipo_ubicacion());
                    for (int index = 0; index < mListTiposDeUbicaciones.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                                mListTiposDeUbicaciones.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                                mListTiposDeUbicaciones.setSelection(index);
                                break;
                            }
                        }
                    }

                    String fabricanteValue = String.valueOf(ifa_activos.getAct_fabricante());
                    for (int index = 0; index < mListFabricantes.getCount(); index++) {
                        //if (((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                        try {
                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                                mListFabricantes.setSelection(index);
                                break;
                            }
                        }catch(Exception ex){
                            if (((ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                                mListFabricantes.setSelection(index);
                                break;
                            }
                        }
                    }
  */
                   // mNombreDeLaMaquina.setText(ifa_activos.getAct_nombre_maquina());
                   // mFabricantes.setText(ifa_activos.getAct_fabricante());


                    /*String sistemaOperativoValue = String.valueOf(ifa_activos.getAct_sistema_operativo());
                    for (int index = 0; index < mListSistemasOperativos.getCount(); index++) {
                        if (((GetListadosActivosAsyncTask.ObjetosClase) mListSistemasOperativos.getAdapter().getItem(index)).id.equals(sistemaOperativoValue)) {
                            mListSistemasOperativos.setSelection(index);
                            break;
                        }
                    }*/

                   // mListSistemasOperativos = ifa_activos.getAct_sistema_operativo();
                  //  mSistemasOperativos.setText(ifa_activos.getAct_sistema_operativo());
                  //  mServidor.setText(ifa_activos.getAct_servidor());
                   // mServidor = ifa_activos.getAct_servidor();

                    try {
                        //long unixSeconds = 1372339860;
                        long unixSeconds = Long.parseLong(ifa_activos.getAct_ultima_fecha_reporte_agente());
                        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss"); // the format of your date
                        //sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
                       // String formattedDate = sdf.format(date);
                       // System.out.println(formattedDate);
                        //mUltimaFechaReporteDeAgente.setText(formattedDate);
                      // mUltimaFechaReporteDeAgente = formattedDate;
                    } catch (Exception ex) {
                        //mUltimaFechaReporteDeAgente.setText("");
                      //  mUltimaFechaReporteDeAgente = "";
                    }

                    //mUltimaFechaReporteDeAgente.setText(ifa_activos.getAct_ultima_fecha_reporte_agente());
                    mObservaciones.setText(ifa_activos.getAct_observaciones());
                    //mRfidTag.setText(ifa_activos.getAct_tag_activo());
                   // mRfidTag = ifa_activos.getAct_tag_activo();
                   // mDireccionUbicacion.setText(ifa_activos.getAct_direccion_ubicacion());
                    //mCanal.setText(ifa_activos.getAct_canal());
                   // mCanal = ifa_activos.getAct_canal();
                   // mEnRed.setText(ifa_activos.getAct_en_red());
                   // mUsuarioDeRed.setText(ifa_activos.getAct_usuario_red());
                    //mFechaIngresoBodega.setText(ifa_activos.getAct_fecha_ingreso_bodega());
                   // mFechaIngresoBodega = ifa_activos.getAct_fecha_ingreso_bodega();
                   // mOrdenDeCompra.setText(ifa_activos.getAct_oc());
                    //mOrdenDeCompra = ifa_activos.getAct_oc();
                   // mServicePack.setText(ifa_activos.getAct_service_pack());
                    //mServicePack = ifa_activos.getAct_service_pack();
                  //  mProcesador.setText(ifa_activos.getAct_procesador());
                    //mProcesador = ifa_activos.getAct_procesador();
                  //  mVelocidad.setText(ifa_activos.getAct_velocidad());
                    //mVelocidad = ifa_activos.getAct_velocidad();
                  //  mDiscoDuro.setText(ifa_activos.getAct_disco_duro());
                    //mDiscoDuro = ifa_activos.getAct_disco_duro();
                  //  mTotalDeMemoria.setText(ifa_activos.getAct_total_memoria());
                    //mTotalDeMemoria = ifa_activos.getAct_total_memoria();
/*
                    // Se deshabilitan los campos que no se puedan modificar
                    mSerial.setEnabled(false);
                    mCodigoDeActivo.setEnabled(false);
                    mModelo.setEnabled(false);
                    mListEmpresasPropietarias.setEnabled(false);
                   // mListFabricantes.setEnabled(false);
                    mCodigoDeBarras.setEnabled(false);
                    mDescripcion.setEnabled(false);
                   // mNombreResponsable.setEnabled(false);
*/
                    mCodigoConteo.requestFocus();
                }
            }

            if ((requestCode == requestListadoAreas) && (resultCode == -1)) {//RESULT_OK
                String jsonArea = data.getDataString();
                JSONObject jSONActivo = new JSONObject(jsonArea);
                String area = jSONActivo.getString("area");

                //mArea.setText(area);
                //mArea.requestFocus();

 /*
                if (!TextUtils.isEmpty(mBusqueda.getText().toString().trim())) {
                    // Se deshabilitan los campos que no se puedan modificar
                    mSerial.setEnabled(false);
                    mCodigoDeActivo.setEnabled(false);
                    mModelo.setEnabled(false);
                    mListEmpresasPropietarias.setEnabled(false);
                  //  mListFabricantes.setEnabled(false);
                    mCodigoDeBarras.setEnabled(false);
                    mDescripcion.setEnabled(false);
                  //  mNombreResponsable.setEnabled(false);
                }
 */
            }
        } catch (JSONException jsonError) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            dlgAlert.setMessage("Ocurrió un error al devolver activo");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        } catch (Exception e) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            dlgAlert.setMessage("Ocurrió un error al devolver activo");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para obtener activo filtrado por su respectivo
     * cógido de búsqueda de la actividad de activos
     */
    private class GetActivoFromRF extends AsyncTask<Object, Integer, ArrayList<Object>> {
        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        ArrayList<Object> arrayListListadosActivos = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected ArrayList<Object> doInBackground(Object... objects) {
            context = (Context) objects[0];
            String busqueda = (String) objects[1];
            ArrayList<Object> arrayListObjRespuesta = new ArrayList<>();

            String resultado = "1";

            ActivosModel activosModel = new ActivosModel();
            try {

                String jsonActivo = activosModel.GetActivoFromRF(context, busqueda);

                if (!jsonActivo.equals("-1")) {

                    ArrayList<IFA_activos> arraylistIFA_activos = new DataAccessJson().parserJsonActivo(jsonActivo);
                    if (arraylistIFA_activos != null && !arraylistIFA_activos.isEmpty()) {
                        arrayListObjRespuesta.add(arraylistIFA_activos);
                    }
                } else {
                    ArrayList<IFA_activos> arraylistIFA_activos = new ArrayList<>();
                    IFA_activos ifa_activos = new IFA_activos();
                    ifa_activos.setResult(jsonActivo);
                    arraylistIFA_activos.add(ifa_activos);
                    arrayListObjRespuesta.add(arraylistIFA_activos);
                }
            } catch (OutOfMemoryError memoryEx) {
                resultado = "2";
                ArrayList<IFA_activos> arraylistIFA_activos = new ArrayList<>();
                IFA_activos ifa_activos = new IFA_activos();
                ifa_activos.setResult(resultado);
                arraylistIFA_activos.add(ifa_activos);
                arrayListObjRespuesta.add(arraylistIFA_activos);
            } catch (Exception ex) {
                //Log.e("Error", "Ocurrió un error consultando activo: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
                resultado = "2";
                ArrayList<IFA_activos> arraylistIFA_activos = new ArrayList<>();
                IFA_activos ifa_activos = new IFA_activos();
                ifa_activos.setResult(resultado);
                arraylistIFA_activos.add(ifa_activos);
                arrayListObjRespuesta.add(arraylistIFA_activos);
            }

            return arrayListObjRespuesta;
        }


        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Consultando activo...");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<Object> arrayListObject) {
            try {

                switch (((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getResult()) {
                    //Cuando no se ha ingresado nada en la búsqueda
                    case "-1":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        //Muestra un mensaje en pantalla
                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage("Debe ingresar un código para la búsqueda");
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        //Se posiciona el foco en el campo de búsqueda
                        mBusqueda.requestFocus();
                        break;

                    //Cuando no se encontró ningún activo
                    case "0":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        //Muestra un mensaje en pantalla
                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getMessage());
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        mBusqueda.setText("");
                        mBusqueda.requestFocus();

                        break;

                    //Cuando encontró mas de un activo
                    case "1":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        //Entra por el if cuando solo encuentra un activo
                        if (((ArrayList) arrayListObject.get(0)).size() == 1) {

                            //DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                            mCodigoDeActivo.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_codigo_activo());
                            mDescripcion.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_descripcion());
                            //mCodigoDeBarras.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_codigo_barras());
                            //Date fecha = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fecha_compra();
                            //String strDate = df.format(fecha);
                            //mFechaDeCompra.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fecha_compra());
                           // mFechaDeCompra = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fecha_compra();

                          //  mCedulaResponsable.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_responsable().trim());
                           // mNombreResponsable.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_nombre_responsable().trim());

                            /*String mesesDepreciacionValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_meses_depreciacion());
                            for (int index = 0; index < mListMesesDepreciacion.getCount(); index++) {
                                if (((GetListadosActivosAsyncTask.ObjetosClase) mListMesesDepreciacion.getAdapter().getItem(index)).id.equals(mesesDepreciacionValue)) {
                                    mListMesesDepreciacion.setSelection(index);
                                    break;
                                }
                            }*/
                           // mListMesesDepreciacion = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_meses_depreciacion());

                            //mArea.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_area());


                            if (!sResponsable.isChecked())

                            {


                                mNombreResponsable.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_responsable());

                                mCuentaContable.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_cuenta_contable());
                                //mCuentaContable = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_cuenta_contable();
                                mNombreCuentaContable.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_nombre_cuenta_contable());
                                //mNombreCuentaContable = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_nombre_cuenta_contable();

                                // mListCentrosDeCostos = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_centro_costos();
                                mCentrosDeCostos.setText( ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_centro_costos());


                            }


                            if (!sUbicacion.isChecked())
                            {


                                // mListCiclosDeVida = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ciclo_vida();
                                mCiclosDeVida.setText( ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ciclo_vida());

                                mCiudad.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_piso());
                                mPiso.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_piso());
                                mUbicaciones.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ubicacion());

                            }



                            /*String mantenimientoValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_mantenimiento());
                            for (int index = 0; index < mListMantenimiento.getCount(); index++) {
                                if (((GetListadosActivosAsyncTask.ObjetosClase) mListMantenimiento.getAdapter().getItem(index)).id.equals(mantenimientoValue.toUpperCase())) {
                                    mListMantenimiento.setSelection(index);
                                    break;
                                }
                            }*/
                            //mListMantenimiento = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_mantenimiento();
                            //mMantenimiento.setText( ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_mantenimiento());

                            /*String centroDeCostosValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_centro_costos());
                            for (int index = 0; index < mListCentrosDeCostos.getCount(); index++) {
                                if (((GetListadosActivosAsyncTask.ObjetosClase) mListCentrosDeCostos.getAdapter().getItem(index)).id.equals(centroDeCostosValue)) {
                                    mListCentrosDeCostos.setSelection(index);
                                    break;
                                }
                            }*/

                            //mValorDeCompra.setText(String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_valor_compra()));
                            mValorDeCompra = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_valor_compra();
                            //mValorIvaDeCompra.setText(String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_valor_iva_compra()));
                            mValorIvaDeCompra = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_valor_iva_compra();





                            mSerial.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_serial());
                            mColor.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_color());
                            //mColor = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_color();
                            mModelo.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_modelo());
                            mTipoDeActivo.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_tipo_activo());

                            //mMaterial.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_material());
                            //mMaterial = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_material();
                          //  mTiposDeUbicaciones.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_tipo_ubicacion());

                            String estadoValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_estado());
                            for (int index = 0; index < mListEstados.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                                        mListEstados.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                                        mListEstados.setSelection(index);
                                        break;
                                    }
                                }
                            }


 /*
                            String ciudadValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ciudad());
                            for (int index = 0; index < mListCiudades.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                                        mListCiudades.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                                        mListCiudades.setSelection(index);
                                        break;
                                    }
                                }
                            }


                            String grupoValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_grupo());
                            for (int index = 0; index < mListGrupos.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                                        mListGrupos.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                                        mListGrupos.setSelection(index);
                                        break;
                                    }
                                }
                            }

                            String cicloDeVidaValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ciclo_vida());
                            for (int index = 0; index < mListCiclosDeVida.getCount(); index++) {
                                if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiclosDeVida.getAdapter().getItem(index)).id.equals(cicloDeVidaValue)) {
                                    mListCiclosDeVida.setSelection(index);
                                    break;
                                }
                            }

                            String ubicacionValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ubicacion());
                            for (int index = 0; index < mListUbicaciones.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                                        mListUbicaciones.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                                        mListUbicaciones.setSelection(index);
                                        break;
                                    }
                                }
                            }

                            String tipoDeUbicacionesValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_tipo_ubicacion());
                            for (int index = 0; index < mListTiposDeUbicaciones.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                                        mListTiposDeUbicaciones.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                                        mListTiposDeUbicaciones.setSelection(index);
                                        break;
                                    }
                                }
                            }



                            String fabricanteValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fabricante());
                            for (int index = 0; index < mListFabricantes.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                                        mListFabricantes.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                                        mListFabricantes.setSelection(index);
                                        break;
                                    }
                                }
                            }

                            String empresaPropietariaValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_empresa_propietaria());
                            for (int index = 0; index < mListEmpresasPropietarias.getCount(); index++) {
                                try {
                                    if (((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                                        mListEmpresasPropietarias.setSelection(index);
                                        break;
                                    }
                                } catch (Exception ex) {
                                    if (((ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                                        mListEmpresasPropietarias.setSelection(index);
                                        break;
                                    }
                                }
                            }
*/
                            //    mNombreDeLaMaquina.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_nombre_maquina());
                          //  mFabricantes.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fabricante());
                            /*String sistemaOperativoValue = String.valueOf(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_sistema_operativo());
                            for (int index = 0; index < mListSistemasOperativos.getCount(); index++) {
                                if (((GetListadosActivosAsyncTask.ObjetosClase) mListSistemasOperativos.getAdapter().getItem(index)).id.equals(sistemaOperativoValue)) {
                                    mListSistemasOperativos.setSelection(index);
                                    break;
                                }
                            }*/
                            //mServidor.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_servidor());
                          //  mSistemasOperativos.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_sistema_operativo());

                          //  mServidor.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_servidor());
                            //mServidor = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_servidor();

                            try {
                                //long unixSeconds = 1372339860;
                                long unixSeconds = Long.parseLong(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ultima_fecha_reporte_agente());
                                Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss"); // the format of your date
                                //sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
                               // String formattedDate = sdf.format(date);
                               // System.out.println(formattedDate);
                                //mUltimaFechaReporteDeAgente.setText(formattedDate);
                             //   mUltimaFechaReporteDeAgente = formattedDate;
                            } catch (Exception ex) {
                                //mUltimaFechaReporteDeAgente.setText("");
                             //   mUltimaFechaReporteDeAgente = "";
                            }

                            //mUltimaFechaReporteDeAgente.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_ultima_fecha_reporte_agente());
                            mObservaciones.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_observaciones());
                            //mRfidTag.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_tag_activo());
                           // mRfidTag = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_tag_activo();
                         //   mDireccionUbicacion.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_direccion_ubicacion());
                            //mCanal.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_canal());
                          //  mCanal = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_canal();
                         //   mEnRed.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_en_red());
                         //   mUsuarioDeRed.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_usuario_red());
                            //mFechaIngresoBodega.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fecha_ingreso_bodega());
                          //  mFechaIngresoBodega = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_fecha_ingreso_bodega();
                         //   mOrdenDeCompra.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_oc());
                            //mOrdenDeCompra = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_oc();
                         //   mServicePack.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_service_pack());
                            //mServicePack = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_service_pack();
                         //   mProcesador.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_procesador());
                            //mProcesador = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_procesador();
                         //   mVelocidad.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_velocidad());
                            //mVelocidad = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_velocidad();
                         //   mDiscoDuro.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_disco_duro());
                            //mDiscoDuro = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_disco_duro();
                         //   mTotalDeMemoria.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_total_memoria());
                            //mTotalDeMemoria = ((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_total_memoria();
                            mCodigoConteo.setText(((IFA_activos) ((ArrayList) arrayListObject.get(0)).get(0)).getAct_codigo_conteo().trim());

                            //Cierra el SoftKeyboard(teclado)
                            View view = getActivity().getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }

                            // Se deshabilitan los campos que no se puedan modificar
                           /*
                            mSerial.setEnabled(false);
                            mCodigoDeActivo.setEnabled(false);
                            mModelo.setEnabled(false);
                            mListEmpresasPropietarias.setEnabled(false);
                            mListFabricantes.setEnabled(false);
                            mCodigoDeBarras.setEnabled(false);
                            mDescripcion.setEnabled(false);
                            mNombreResponsable.setEnabled(false);
                            */
                           // mNombreResponsable.setEnabled(false);

                            mCodigoConteo.requestFocus();

                        } else {//Si nó es porque la consulta de activos devolvió mas de un registro lo cual hay que mostrarlo en un ListView
                            try {
                                Intent listadoActivosIntent = new Intent(getActivity(), ListadoActivosActivity.class);
                                ArrayList<IFA_activos> mListadoActivos = ((ArrayList) arrayListObject.get(0));
                                listadoActivosIntent.putParcelableArrayListExtra("listadoDeActivos", (ArrayList<? extends Parcelable>) mListadoActivos);
                                startActivityForResult(listadoActivosIntent, requestListadoActivos);
                            } catch (Exception ex) {
                                //Muestra un mensaje en pantalla
                                dlgAlert = new AlertDialog.Builder(context);
                                dlgAlert.setMessage("Ocurrió un error mostrando listado de activos");
                                dlgAlert.setTitle("iTrace");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.setCancelable(true);
                                dlgAlert.create().show();
                            }
                        }

                        break;

                    case "2":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        //Muestra un mensaje en pantalla
                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage("Ocurrió un error consultando el activo");
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        break;

                    default:
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }
                        break;
                }
            } catch (Exception ex) {
                Log.e("Error", "Ocurrió un error consultando activo: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
            }
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para obtener el listado de areas
     */
    private class GetAreasAsyncTask extends AsyncTask<Object, Integer, Integer> {

        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        ArrayList<Object> arrayListListadosActivos = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected Integer doInBackground(Object... params) {
            context = (Context) params[0];
            Integer resultado = 1;

            try {
                ActivosModel activosModel = new ActivosModel();
                String respuesta = activosModel.GetListadoAreasFromRF(context);

                arrayListListadosActivos = new ArrayList<Object>();
                arrayListListadosActivos = DataAccessJson.parserJsonListadosActivos(respuesta);

            } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
                resultado = 2;
                //return 2;
            } catch (Exception ex) {
                resultado = 3;
                //return 4;
            }

            return resultado;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Cargando...");
            this.dialog.show();
        }


        @Override
        protected void onPostExecute(final Integer result) {

            switch (result) {

                case 1:
                    if (!arrayListListadosActivos.isEmpty()) {
                        if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_FAIL)) {

                            if (this.dialog.isShowing()) {
                                this.dialog.dismiss();
                            }
                            //Muestra un mensaje en pantalla
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                            dlgAlert.setMessage((String) arrayListListadosActivos.get(1));
                            dlgAlert.setTitle("iTrace Móvil");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        } else {
                            if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_SUCCESS)) {

                                try {
                                    Intent listadoAreasIntent = new Intent(getActivity(), ListadoAreasActivity.class);
                                    listadoAreasIntent.putParcelableArrayListExtra("listadoDeAreas", (ArrayList<? extends Parcelable>) arrayListListadosActivos.get(2));
                                    listadoAreasIntent.putExtra("SIN_TODAS", 1);
                                    startActivityForResult(listadoAreasIntent, requestListadoAreas);
                                    if (this.dialog.isShowing()) {
                                        this.dialog.dismiss();
                                    }
                                } catch (Exception ex) {
                                    if (this.dialog.isShowing()) {
                                        this.dialog.dismiss();
                                    }
                                    //Muestra un mensaje en pantalla
                                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                                    dlgAlert.setMessage("Ocurrió un error al consultar las áreas");
                                    dlgAlert.setTitle("iTrace Móvil");
                                    dlgAlert.setPositiveButton("OK", null);
                                    dlgAlert.setCancelable(true);
                                    dlgAlert.create().show();
                                    return;
                                }
                            }
                        }
                    }
                    break;

                case 2:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error en la comunicación");
                    dlgAlert.setTitle("iLogistica Movil");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;

                case 3:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error al consultar listados para los activos");
                    dlgAlert.setTitle("iLogistica Movil");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;
            }
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para obtener los listados de la actividad de activos
     */
    private class GetListadosActivosAsyncTask extends AsyncTask<Object, Integer, Integer> {

        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        ArrayList<Object> arrayListListadosActivos = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected Integer doInBackground(Object... params) {
            context = (Context) params[0];
            Integer resultado = 1;

            try {
                ActivosModel activosModel = new ActivosModel();
                String respuesta = activosModel.GetListadosActivosFromRF(context);

                arrayListListadosActivos = new ArrayList<Object>();
                arrayListListadosActivos = DataAccessJson.parserJsonListadosActivos(respuesta);

            } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
                resultado = 2;
                //return 2;
            } catch (Exception ex) {
                resultado = 3;
                //return 4;
            }

            return resultado;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Cargando...");
            this.dialog.show();
        }


        @Override
        protected void onPostExecute(final Integer result) {

            Intent data = null;

            switch (result) {

                case 1:
                    if (!arrayListListadosActivos.isEmpty()) {
                        if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_FAIL)) {

                            if (this.dialog.isShowing()) {
                                this.dialog.dismiss();
                            }
                            //Muestra un mensaje en pantalla
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                            dlgAlert.setMessage((String) arrayListListadosActivos.get(1));
                            dlgAlert.setTitle("iLogistica Movil");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        } else {
                            if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_SUCCESS)) {

                               // LinkedList listadoGrupos = null;
                               // LinkedList listadoCiclosDeVida = null;
                               // LinkedList listadoCentrosDeCostos = null;
                               // LinkedList listadoCiudades = null;
                                LinkedList listadoUbicaciones = null;
                               // LinkedList listadoTiposDeUbicaciones = null;
                                LinkedList listadoEstados = null;
                                LinkedList listadoFabricantes = null;
                               // LinkedList listadoEmpresasPropietarias = null;
                               // LinkedList listadoSistemasOperativos = null;
                               // LinkedList listadoMesesDepreciacion = null;
                               // LinkedList listadoMantenimiento = null;

                                try {

                                   // listadoGrupos = new LinkedList();
                                   // listadoCiudades = new LinkedList();
                                    //listadoUbicaciones = new LinkedList();
                                    listadoEstados = new LinkedList();
                                    //listadoFabricantes = new LinkedList();
                                    //listadoEmpresasPropietarias = new LinkedList();
     /*
                                    listadoCiclosDeVida = new LinkedList();
                                    listadoCentrosDeCostos = new LinkedList();
                                    listadoTiposDeUbicaciones = new LinkedList();

                                    listadoSistemasOperativos = new LinkedList();
                                    listadoMesesDepreciacion = new LinkedList();
                                    listadoMantenimiento = new LinkedList();


                                   // listadoGrupos.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "3":
                                                listadoGrupos.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }

                                    listadoCiudades.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "5":
                                                listadoCiudades.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }

*/

                                    listadoEstados.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "4":
                                                listadoEstados.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }




/*

                                    listadoEmpresasPropietarias.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "13":
                                                listadoEmpresasPropietarias.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }

                                    listadoFabricantes.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "14":
                                                listadoFabricantes.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }
                                    listadoUbicaciones.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "11":
                                                listadoUbicaciones.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }


                                    listadoCiclosDeVida.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "6":
                                                listadoCiclosDeVida.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }

                                    listadoCentrosDeCostos.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "2":
                                                listadoCentrosDeCostos.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }


                                    listadoTiposDeUbicaciones.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "15":
                                                listadoTiposDeUbicaciones.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }



                                    listadoSistemasOperativos.add(new ObjetosClase("", "Seleccione un opción"));

                                    for (int index = 0; index < ((ArrayList) arrayListListadosActivos.get(2)).size(); index++) {
                                        String[] textSplit = ((ArrayList) arrayListListadosActivos.get(2)).get(index).toString().split("~");

                                        switch (textSplit[0]) {
                                            case "12":
                                                listadoSistemasOperativos.add(new ObjetosClase(textSplit[2], textSplit[1]));
                                                break;
                                        }
                                    }

                                    listadoMesesDepreciacion.add(new ObjetosClase("-1", "Seleccione un opción"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("0", "0 Meses"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("12", "12 Meses"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("24", "24 Meses"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("60", "60 Meses"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("120", "120 Meses"));
                                    listadoMesesDepreciacion.add(new ObjetosClase("240", "240 Meses"));

                                    listadoMantenimiento.add(new ObjetosClase("", "Seleccione un opción"));
                                    listadoMantenimiento.add(new ObjetosClase("SI", "SI"));
                                    listadoMantenimiento.add(new ObjetosClase("NO", "NO"));
*/
                                    //Creamos el adaptador


                                    ArrayAdapter spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoEstados);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListEstados.setAdapter(spinner_adapter);

/*

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoCiudades);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListCiudades.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoGrupos);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListGrupos.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoCiclosDeVida);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListCiclosDeVida.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoCentrosDeCostos);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListCentrosDeCostos.setAdapter(spinner_adapter);


                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoUbicaciones);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListUbicaciones.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoTiposDeUbicaciones);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListTiposDeUbicaciones.setAdapter(spinner_adapter);


                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoFabricantes);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListFabricantes.setAdapter(spinner_adapter);


                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoEmpresasPropietarias);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListEmpresasPropietarias.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoSistemasOperativos);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListSistemasOperativos.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoMesesDepreciacion);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListMesesDepreciacion.setAdapter(spinner_adapter);

                                    spinner_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listadoMantenimiento);
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mListMantenimiento.setAdapter(spinner_adapter);
*/
                                    if (ifa_activos != null) {




                                        String estadoValue = String.valueOf(ifa_activos.getAct_estado());
                                        for (int index = 0; index < mListEstados.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListEstados.getAdapter().getItem(index)).id.equals(estadoValue)) {
                                                mListEstados.setSelection(index);
                                                break;
                                            }
                                        }

/*

                                        String ciudadValue = String.valueOf(ifa_activos.getAct_ciudad());
                                        for (int index = 0; index < mListCiudades.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiudades.getAdapter().getItem(index)).id.equals(ciudadValue)) {
                                                mListCiudades.setSelection(index);
                                                break;
                                            }
                                        }
                                        String mesesDepreciacionValue = String.valueOf(ifa_activos.getAct_meses_depreciacion());
                                        for (int index = 0; index < mListMesesDepreciacion.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListMesesDepreciacion.getAdapter().getItem(index)).id.equals(mesesDepreciacionValue)) {
                                                mListMesesDepreciacion.setSelection(index);
                                                break;
                                            }
                                        }

                                        String grupoValue = String.valueOf(ifa_activos.getAct_grupo());
                                        for (int index = 0; index < mListGrupos.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListGrupos.getAdapter().getItem(index)).id.equals(grupoValue)) {
                                                mListGrupos.setSelection(index);
                                                break;
                                            }
                                        }

                                        String cicloDeVidaValue = String.valueOf(ifa_activos.getAct_ciclo_vida());
                                        for (int index = 0; index < mListCiclosDeVida.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListCiclosDeVida.getAdapter().getItem(index)).id.equals(cicloDeVidaValue)) {
                                                mListCiclosDeVida.setSelection(index);
                                                break;
                                            }
                                        }

                                        String mantenimientoValue = String.valueOf(ifa_activos.getAct_mantenimiento());
                                        for (int index = 0; index < mListMantenimiento.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListMantenimiento.getAdapter().getItem(index)).id.equals(mantenimientoValue.toUpperCase())) {
                                                mListMantenimiento.setSelection(index);
                                                break;
                                            }
                                        }

                                        String centroDeCostosValue = String.valueOf(ifa_activos.getAct_centro_costos());
                                        for (int index = 0; index < mListCentrosDeCostos.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListCentrosDeCostos.getAdapter().getItem(index)).id.equals(centroDeCostosValue)) {
                                                mListCentrosDeCostos.setSelection(index);
                                                break;
                                            }
                                        }

                                        String ubicacionValue = String.valueOf(ifa_activos.getAct_ubicacion());
                                        for (int index = 0; index < mListUbicaciones.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListUbicaciones.getAdapter().getItem(index)).id.equals(ubicacionValue)) {
                                                mListUbicaciones.setSelection(index);
                                                break;
                                            }
                                        }

                                        String tipoDeUbicacionesValue = String.valueOf(ifa_activos.getAct_tipo_ubicacion());
                                        for (int index = 0; index < mListTiposDeUbicaciones.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListTiposDeUbicaciones.getAdapter().getItem(index)).id.equals(tipoDeUbicacionesValue)) {
                                                mListTiposDeUbicaciones.setSelection(index);
                                                break;
                                            }
                                        }



                                        String fabricanteValue = String.valueOf(ifa_activos.getAct_fabricante());
                                        for (int index = 0; index < mListFabricantes.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListFabricantes.getAdapter().getItem(index)).id.equals(fabricanteValue)) {
                                                mListFabricantes.setSelection(index);
                                                break;
                                            }
                                        }


                                        String empresaPropietariaValue = String.valueOf(ifa_activos.getAct_empresa_propietaria());
                                        for (int index = 0; index < mListEmpresasPropietarias.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListEmpresasPropietarias.getAdapter().getItem(index)).id.equals(empresaPropietariaValue)) {
                                                mListEmpresasPropietarias.setSelection(index);
                                                break;
                                            }
                                        }

                                        String sistemaOperativoValue = String.valueOf(ifa_activos.getAct_sistema_operativo());
                                        for (int index = 0; index < mListSistemasOperativos.getCount(); index++) {
                                            if (((GetListadosActivosAsyncTask.ObjetosClase) mListSistemasOperativos.getAdapter().getItem(index)).id.equals(sistemaOperativoValue)) {
                                                mListSistemasOperativos.setSelection(index);
                                                break;
                                            }
                                        }
*/
                                    }
                                } catch (Exception ex) {
                                    throw ex;
                                }

                                if (this.dialog.isShowing()) {
                                    this.dialog.dismiss();
                                }
                            }
                        }
                    }
                    break;

                case 2:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    data = new Intent();
                    data.setData(Uri.parse("error"));
                    getActivity().setResult(getActivity().RESULT_OK, data);
                    getActivity().finish();

                    break;

                case 3:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    data = new Intent();
                    data.setData(Uri.parse("error"));
                    getActivity().setResult(getActivity().RESULT_OK, data);
                    getActivity().finish();

                    break;
            }
        }


        public class ObjetosClase {
            String id;
            String nombre;

            //Constructor
            public ObjetosClase(String id, String nombre) {
                super();
                this.id = id;
                this.nombre = nombre;
            }

            @Override
            public String toString() {
                return nombre;
            }

            public String getId() {
                return id;
            }
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para obtener activo filtrado por su respectivo
     * cógido de búsqueda de la actividad de activos
     */
 /*
    private class GetNombreResponsableFromRF extends AsyncTask<Object, Integer, String> {
        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected String doInBackground(Object... objects) {
            context = (Context) objects[0];
            String cedulaResponsable = (String) objects[1];

            String nombreResponsable = "";
            ActivosModel activosModel = null;
            try {
                activosModel = new ActivosModel();

                String jsonResponsable = activosModel.GetResponsableFromRF(context, cedulaResponsable);

                nombreResponsable = new DataAccessJson().parserJsonNombreResponsable(jsonResponsable);
            } catch (Exception ex) {
                //Log.e("Error", "Ocurrió un error consultando activo: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
                nombreResponsable = "-1";
            }
            return nombreResponsable;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(String nombreResponsable) {
            try {

                if (nombreResponsable.equals("-1")) {
                    //Muestra un mensaje en pantalla
                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error al consultar el responsable");
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    //Se posiciona el foco en el campo de cédula del responsable
                    mCedulaResponsable.requestFocus();
                } else {
                    mNombreResponsable.setText(nombreResponsable.trim());
                    mNombreResponsable.setEnabled(false);
                }
            } catch (Exception ex) {
                Log.e("Error", "Ocurrió un error consultando nombre del responsable: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
            }
        }
    }
*/
    /**
     * Clase contenedora de una tarea asincrónica para grabar o actualizar los datos de un activo
     */
    private class SetActivoAsyncTask extends AsyncTask<Object, Integer, Integer> {

        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        AlertDialog.Builder dlgAlert;
        Context context;
        String areaLeida = "";
        String responsableLeido = "";
        String inventario = "";
        String usuario = "";
        IFA_activos ifa_activos = null;
        ArrayList<String> arrayListRespuesta = new ArrayList<>();
        //String fechaDeCompra = mFechaDeCompra.getText().toString().trim();
       // String fechaDeCompra = mFechaDeCompra.trim();
        //String mesesDeDepreciacion = ((GetListadosActivosAsyncTask.ObjetosClase) mListMesesDepreciacion.getSelectedItem()).getId();
        String mesesDeDepreciacion =  "0";//mListMesesDepreciacion;

        @Override
        protected Integer doInBackground(Object... params) {
            context = (Context) params[0];
            areaLeida = (String) params[1];
            responsableLeido = (String) params[2];
            inventario = (String) params[3];
            usuario = (String) params[4];
            ifa_activos = (IFA_activos) params[5];

            try {
                ActivosModel activosModel = new ActivosModel();
                String respuesta = activosModel.SetActivoRF(context, areaLeida, responsableLeido, inventario, usuario, ifa_activos);

                arrayListRespuesta = DataAccessJson.parserJsonRespuestaSetActivoRF(respuesta);

            } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
                arrayListRespuesta = new ArrayList<>();
                arrayListRespuesta.add("2");
            } catch (ParseException ex) {
                arrayListRespuesta.add("3");
            } catch (Exception ex) {
                arrayListRespuesta.add("4");
            }

            return 0;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Guardando activo...");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            /*if (integer != null) {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }

                Toast.makeText(getActivity(), "Los Meses de Depreciación es obligatorio", Toast.LENGTH_SHORT).show();

                return;
            }*/
            switch (arrayListRespuesta.get(0)) {
                case "0":
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }
                    //Muestra un mensaje en pantalla
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage(arrayListRespuesta.get(1));
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    break;

                case "1":
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    LimpiarCamposDeActivos(null);

                    Toast.makeText(getActivity(), arrayListRespuesta.get(1), Toast.LENGTH_SHORT).show();

                    //Cierra el SoftKeyboard(teclado)
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    // Se habilitan los campos despues del guardado
                    mSerial.setEnabled(true);
                    mCodigoDeActivo.setEnabled(true);
                    mModelo.setEnabled(true);
                    //mListEmpresasPropietarias.setEnabled(true);
                    //mListFabricantes.setEnabled(true);
                    //mCodigoDeBarras.setEnabled(true);
                    mDescripcion.setEnabled(true);

                    break;

                case "2":
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error en la comunicación");
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;

                case "3":
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Última fecha de reporte del agente tiene un formato de fecha incorrecto");
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;

                case "4":
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error al grabar el activo");
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;
            }

            // Se habilita el campo de codigo del activo y el nombre del responsable
            mCodigoDeActivo.setEnabled(true);
           // mNombreResponsable.setEnabled(true);
        }
    }

/*
    private void ActualizarDateTimePickerFechaDeCompra() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mFechaDeCompra.setText(sdf.format(myCalendar.getTime()));
        mFechaDeCompra = sdf.format(myCalendar.getTime());
    }
*/

    public void LimpiarCamposDeActivos(Context context) {
        try {
            if (context == null) {
                LimpiarTodosLosCamposDeActivos();
            } else {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                LimpiarTodosLosCamposDeActivos();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Desea limpiar los campos?").setPositiveButton("Sí", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "Ocurrio un error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void LimpiarTodosLosCamposDeActivos() {
        mBusqueda.setText("");
        mCodigoDeActivo.setText("");
        mCodigoConteo.setText("");
        mDescripcion.setText("");
       // mCodigoDeBarras.setText("");
        //mFechaDeCompra.setText("");
        //mFechaDeCompra = "";
        //mListMesesDepreciacion.setSelection(0);
     //  mListMesesDepreciacion = "";
       // mArea.setText("");
       // mListGrupos.setSelection(0);

        //mListMantenimiento.setSelection(0);
        //mListMantenimiento = "";
       // mMantenimiento.setText("");

        //mValorDeCompra.setText("");
        mValorDeCompra = 0;
        //mValorIvaDeCompra.setText("");
        mValorIvaDeCompra = 0;


        mSerial.setText("");
        mColor.setText("");
        //mColor = "";
        mModelo.setText("");
        mTipoDeActivo.setText("");
        //mMaterial.setText("");
       // mMaterial = "";
        mListEstados.setSelection(0);
      //  mFabricantes.setText("");
        // mListFabricantes.setSelection(0);
       // mListEmpresasPropietarias.setSelection(0);
      //  mNombreDeLaMaquina.setText("");
        //mListSistemasOperativos.setSelection(0);
        //mListSistemasOperativos = "";
       // mSistemasOperativos.setText("");
        //mServidor.setText("");
      //  mServidor.setText("");
        //mUltimaFechaReporteDeAgente.setText("");
       // mUltimaFechaReporteDeAgente = "";
        mObservaciones.setText("");

        //mRfidTag.setText("");
      //  mRfidTag = "";
      //  mCedulaResponsable.setText("");
       // mNombreResponsable.setText("");
      //  mDireccionUbicacion.setText("");
        //mCanal.setText("");
      //  mCanal = "";
       // mEnRed.setText("");
      //  mUsuarioDeRed.setText("");
        //mFechaIngresoBodega.setText("");
      //  mFechaIngresoBodega = "";
       // mOrdenDeCompra.setText("");
        //mOrdenDeCompra = "";
      //  mServicePack.setText("");
        //mServicePack = "";
       // mProcesador.setText("");
        //mProcesador = "";
      //  mVelocidad.setText("");
        //mVelocidad = "";
      //  mDiscoDuro.setText("");
        //mDiscoDuro = "";
      //  mTotalDeMemoria.setText("");
        //mTotalDeMemoria = "";
        mBusqueda.requestFocus();

        mSerial.setEnabled(true);
        mCodigoDeActivo.setEnabled(true);
        mModelo.setEnabled(true);
        //mListEmpresasPropietarias.setEnabled(true);
        // mListFabricantes.setEnabled(true);
        //mCodigoDeBarras.setEnabled(true);
        mDescripcion.setEnabled(true);

        if (!sResponsable.isChecked())

        {

            //mCuentaContable.setText("");
            mCuentaContable.setText("");  //CEDULA RESPONSABLE
            //mNombreCuentaContable.setText("");
            mNombreCuentaContable.setText("");  // ID RECURSOS HUM
            mNombreResponsable.setText(""); // RESPONSABLE

            //mListCentrosDeCostos.setSelection(0);
            //mListCentrosDeCostos = "";
            mCentrosDeCostos.setText(""); // CENTRO DE COSTOS


        }


        if (!sUbicacion.isChecked())
        {


            mCiclosDeVida.setText(ifa_activos.getAct_ciclo_vida());

            // mListCiudades.setSelection(0);
            mPiso.setText("");
            mCiudad.setText("");
            // mListUbicaciones.setSelection(0);
            // mListTiposDeUbicaciones.setSelection(0);
            mUbicaciones.setText("");
            // mTiposDeUbicaciones.setText("");
            //mListCiclosDeVida.setSelection(0);
            // mListCiclosDeVida = "";
            mCiclosDeVida.setText("");
        }

    }

}
