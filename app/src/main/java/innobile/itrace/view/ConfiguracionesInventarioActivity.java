package innobile.itrace.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.TipoConexion;

/**
 * Abre una ventana para las configuraciones de iTrace
 * Se implementa View.OnClickListener para los eventos de los botones cuando sean pulsados
 */
public class ConfiguracionesInventarioActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTxtUrl, mTxtUrlMongo = null;
    private Button mCancelar = null;
    private Button mGuardar = null;
    private Spinner mSpTipoImpresion = null;
   // private Switch mSwValidarConexion = null;
    private TextView MostrarMac = null;
    private TextView MostrarLicencia = null;
    private RadioButton Rb_Local, Rb_Nube, Rb_Server;

    private String patronBusqueda;
    private String patronEncripta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_inventario);
        patronBusqueda = "GlxnHMbcIfñK4LeUNFO1ms2PtDvSVkWXqirYTaghjZyo";
        patronEncripta = "eahdmifsWJBrWZD5CLpvMXQuVjkXqY6AoKyNzUgÑOPRt";

        mTxtUrl = (TextView) findViewById(R.id.txtUrl);
        mTxtUrlMongo = (TextView) findViewById(R.id.txtUrlMongo);
        mCancelar = (Button) findViewById(R.id.btnCancelar);
        mGuardar = (Button) findViewById(R.id.btnGuardar);
        mSpTipoImpresion = (Spinner) findViewById(R.id.spTipoImpresion);
       // mSwValidarConexion = (Switch) findViewById(R.id.SwValidarConexion);
        Rb_Local = (RadioButton) findViewById(R.id.Rb_Local);
        Rb_Nube= (RadioButton) findViewById(R.id.Rb_Nube);
        Rb_Server= (RadioButton) findViewById(R.id.Rb_Server);


        /**
         * Se encarga de comprobar el estado de los RadioButton
         * */

        comprobarEstado();



        //Se inicializan los listeners para los botones
        mCancelar.setOnClickListener(this);
        mGuardar.setOnClickListener(this);
        mTxtUrl.setText("http://192.168.1.70:3000");
        mTxtUrlMongo.setText("https://webapi.innova-id.com");
       // mSwValidarConexion.setChecked(true);

        poblarSpinner();
        ConsultarConfiguraciones();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancelar:
                cancelarConfiguraciones();
                break;

            case R.id.btnGuardar:
                guardarConfiguraciones();
                setResult(RESULT_OK);
                finish();
                break;
        }
    }

    /**
     * Finaliza la actividad al precionar el botón cancelar
     */
    private void cancelarConfiguraciones() {
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * Consulta las configuraciones para poner en los respectivos campos de la actividad
     */
    private void ConsultarConfiguraciones() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Cargando configuraciones...");
        dialog.show();

        try {
            //CLASE QUE DEVUELVE LOS DATOS DE LA TABLA CONFIGURACIONES
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(this);
            ArrayList<T_Configuraciones> arrayListT_Configuraciones = t_configuraciones_dbManager.ConsultarConfiguraciones();
            for (T_Configuraciones configuracion : arrayListT_Configuraciones) {
                switch (configuracion.getCon_consecutivo()) {
                    case Cons.CONFIGURACION_URL:
                        mTxtUrl.setText(configuracion.getCon_URL());
                        mTxtUrlMongo.setText(configuracion.getCon_URLMongo());
                        mSpTipoImpresion.setSelection(configuracion.getCon_TipoImpresion());
                        //Local =1 , Nube = 2, Sever = 3
                        if (configuracion.getCon_TipoConexion().equals(Cons.LOCAL))
                        {Rb_Local.isChecked();
                        }else
                        if (configuracion.getCon_TipoConexion().equals(Cons.NUBE))
                        {Rb_Nube.isChecked();
                        }else {Rb_Nube.isChecked();}
                        //mSwValidarConexion.setChecked(configuracion.getCon_TipoConexion());
                        break;
                }
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error consultando las configuraciones: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            //Muestra un mensaje en pantalla
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Ocurrió un error consultando las configuraciones");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }

    private void guardarConfiguraciones() {
        //Barra de progreso
        ProgressDialog dialog = null;

        try {

            dialog = new ProgressDialog(ConfiguracionesInventarioActivity.this);
            //Pone el mensaje y muestra el ProgressBar
            dialog.setMessage("Guardando configuraciones...");
            dialog.show();

            if (!mTxtUrl.getText().toString().trim().isEmpty()) {
                T_Configuraciones t_configuraciones = new T_Configuraciones();
                t_configuraciones.setCon_consecutivo(1);
                t_configuraciones.setCon_URL(mTxtUrl.getText().toString().trim());
                t_configuraciones.setCon_URLMongo(mTxtUrlMongo.getText().toString().trim());
                t_configuraciones.setCon_TipoImpresion( mSpTipoImpresion.getSelectedItemPosition());
                t_configuraciones.setCon_Usuario("");


                if (Rb_Local.isChecked())
                { t_configuraciones.setCon_TipoConexion(Cons.LOCAL);
                    TipoConexion.TIPO_CONEXION = "1";
                }else if (Rb_Nube.isChecked())
                {t_configuraciones.setCon_TipoConexion(Cons.NUBE);
                    TipoConexion.TIPO_CONEXION = "2";
                }else if(Rb_Server.isChecked()) {t_configuraciones.setCon_TipoConexion(Cons.SERVER);
                        TipoConexion.TIPO_CONEXION = "3";
                }else {
                    TipoConexion.TIPO_CONEXION = "3";
                }


                /*
                if (mSwValidarConexion.isChecked() == true)
                t_configuraciones.setCon_TipoConexion(true);
                else t_configuraciones.setCon_TipoConexion(false);
                */
                T_Configuraciones_DBManager mT_configuraciones_dbManager = new T_Configuraciones_DBManager(this);
                mT_configuraciones_dbManager.ActualizarConfiguraciones(t_configuraciones);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            } else {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Muestra un mensaje en pantalla
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ConfiguracionesInventarioActivity.this);
                dlgAlert.setMessage("El campo URL no puede estar vacío");
                dlgAlert.setTitle("iTrace");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
            }
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error al guardar las configuraciones: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            //Muestra un mensaje en pantalla
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ConfiguracionesInventarioActivity.this);
            dlgAlert.setMessage("Ocurrió un error al guardar las configuraciones");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }

    public void poblarSpinner(){

        String[] datos = new String[] {"Scan-Print-Scan", "Scan-Print-Print"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos);
        mSpTipoImpresion.setAdapter(adapter);
    }

    /**
     * Se encarga de comprobar cuàl de los RadionButton està seleccionado
     * para guardar su estado.
     *
     * @param TIPO_CONEXION string tiene el dato del valor seleccionado

     */

    public void comprobarEstado(){
        if(TipoConexion.TIPO_CONEXION.equals("1")){
            Rb_Local.setChecked(true);
        }
        else if(TipoConexion.TIPO_CONEXION.equals("2")){
            Rb_Nube.setChecked(true);
        }
        else if(TipoConexion.TIPO_CONEXION.equals("3")){
            Rb_Server.setChecked(true);
        }
    }
}

