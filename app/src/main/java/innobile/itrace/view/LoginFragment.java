package innobile.itrace.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.SQLite_User;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataAccessJson;
import innobile.itrace.modelo.ModelosJson;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.GetDataAdapter;


public class LoginFragment extends Fragment implements View.OnClickListener {

    private EditText mUser = null;
    private EditText mPassword = null;
    private Button mSignIn = null;
    private Switch Loginoffline = null;
    private Button Comprobar = null;
    private RequestQueue queue;

    private List<GetDataAdapter> mlstRequisicionData = new ArrayList<>();

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        setHasOptionsMenu(false);
        queue = new Volley().newRequestQueue(getContext());
        mUser = (EditText) root.findViewById(R.id.txtUser);
        mPassword = (EditText) root.findViewById(R.id.txtPassword);
        mSignIn = (Button) root.findViewById(R.id.btnSignIn);
        mSignIn.setOnClickListener(this);


        //ingreso tabla usuario local
        ComprobarUsuario(mUser.getText().toString(), mPassword.getText().toString());

        return root;


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);


        //Sirva para controlar lo que hace el botón del menú
        // ABRE EL FORMULARIO CONFIGURACION
        try {
            switch (item.getItemId()) {
                case R.id.menu_menu:
                    Intent intent = new Intent(getActivity(), ConfiguracionesMenuActivity.class);
                    this.startActivity(intent);
                    break;
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            //cuando se presiona el boton ingreso
            case R.id.btnSignIn:
                try {
                    //Ejecuta la tarea asincrónica DataUploadAsyncTask
                    new ValidarUsuario().execute(getActivity(), mUser.getText().toString(), mPassword.getText().toString());

                } catch (Exception ex) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                    dlgAlert.setMessage("Ocurrió un error: " + ex.getMessage());
                    dlgAlert.setTitle("iTrace");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }

                break;
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para la validación de usuario
     */
    private class ValidarUsuario extends AsyncTask<Object, Integer, Integer> {

        //Barra de progresoa
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        ArrayList<String> arrayLogin = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected Integer doInBackground(Object... params) {
            //Looper.prepare();
            context = (Context) params[0];
            String user = params[1].toString();
            String password = params[2].toString();
            Integer resultado = 0;

            try {
                //EJECUTA JSON PARA VALIDAR USUARIO Y CONTRASEÑA
                ModelosJson ModeloUsuario = new ModelosJson();
                String respuesta = ModeloUsuario.Login(user, password, context);

                if (!respuesta.equals("-1")) {
                    arrayLogin = new ArrayList<String>();
                    arrayLogin = DataAccessJson.JSONValidarUsuario(respuesta);
                } else {
                    resultado = 0;
                }
            } catch (ConnectException | SocketTimeoutException | UnknownHostException | MalformedURLException | FileNotFoundException connectionEx) {
                resultado = 2;
            } catch (Exception ex) {
                resultado = 3;
            }

            return resultado;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Autenticando...");
            this.dialog.show();
        }


        @Override
        protected void onPostExecute(final Integer result) {

            try {


                switch (result) {

                    case 0:

                        if (!arrayLogin.isEmpty()) {

                            //si el valor devuelto en el array es un 0 no lo deja entrar
                            if (arrayLogin.get(0).equals(Cons.RESPONSE_FAIL)) {

                                if (this.dialog.isShowing()) {
                                    this.dialog.dismiss();
                                }
                                //Muestra un mensaje en pantalla
                                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                                dlgAlert.setMessage(arrayLogin.get(1));
                                dlgAlert.setTitle("iTrace");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.setCancelable(true);
                                dlgAlert.create().show();
                            } else {
                                //si el valor devuelto en el array es un 1  deja entrar
                                if (arrayLogin.get(0).equals(Cons.RESPONSE_SUCCESS)) {

                                    if (this.dialog.isShowing()) {
                                        this.dialog.dismiss();
                                    }

                                    Cons.Usuario = mUser.getText().toString();
                                    Cons.Empresa = arrayLogin.get(2);
                                    try {

                                        Cons.Tipo_Producto = arrayLogin.get(3);
                                        Cons.id_Usuario = arrayLogin.get(4);

                                    } catch (Exception e) {
                                        Toast.makeText(context, "El usuario no tiene codigo de empresa", Toast.LENGTH_SHORT).show();
                                    }


                                    Intent menuIntent = new Intent(context, MenuActivity.class);
                                    //menuIntent.putExtra("USUARIO_LOGEADO", mUser.getText().toString());
                                    startActivity(menuIntent);
                                }
                            }
                        }
                        break;

                    case 1:
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage("Debe ingresar el usuario y contraseña");
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        break;

                    case 2:
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }
                        boolean band = ComprobarUsuario(mUser.getText().toString(), mPassword.getText().toString());
                        if (band) {
                            Cons.Usuario = mUser.getText().toString();
                            // ABRE INSTANCIA DEL FORMULARIO MENU
                            Intent menuIntent = new Intent(context, MenuActivity.class);
                            menuIntent.putExtra("USUARIO_LOGEADO", mUser.getText().toString());
                            startActivity(menuIntent);

                        } else {
                            dlgAlert = new AlertDialog.Builder(context);
                            dlgAlert.setMessage("El usuario o clave ingresadas son incorrectas. Verifique nuevamente la información");
                            dlgAlert.setTitle("iTrace");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        }

                        break;

                    case 3:
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage("Ocurrió un error al autenticar el usuario");
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + result);
                }

            }catch (NullPointerException e){
                Toasty.error(context, "Es necesario ingresar todos los datos", Toasty.LENGTH_SHORT).show();
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            }
        }
    }

    public Boolean ComprobarUsuario(String Username, String password) {
        Boolean band = false;
        String fecha = "";
        String ThisUsername = "", ThisPassword = "";
        SQLite_User conn = new SQLite_User(getContext(), Utilidades.TABLA_USUARIO, null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_USUARIO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "' WHERE " + Utilidades.CAMPO_USERNAME + "= '" + Username + "'", null);


        if (c.moveToFirst()) {
            do {
                ThisUsername = c.getString(0);
                ThisPassword = c.getString(1);
                fecha = c.getString(2);
            } while (c.moveToNext());
        }

        // Toast.makeText(getContext(),ThisUsername, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getContext(),fecha, Toast.LENGTH_SHORT).show();
        db.close();


        if (Username.equals(ThisUsername) && password.equals(ThisPassword)) {
            band = true;
        }

        return band;

    }


}
