package innobile.itrace.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itrace.transport.IFA_activos;

/**
 * Created by Dev1 on 12/11/15.
 */
public class ListadoAreasActivity extends Activity {
    private ListView mLvAreas;
    private ArrayList<String> listViewAreasValues = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_areas);

        mLvAreas = (ListView) findViewById(R.id.lv_areas);

        mLvAreas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String json = (String) mLvAreas.getAdapter().getItem(i);
                    String jsonActivo = listViewAreasValues.get(i);

                    Intent data = new Intent();
                    data.setData(Uri.parse(jsonActivo));
                    setResult(RESULT_OK, data);
                    finish();
                } catch (Exception ex) {
                    String x = "";
                }
            }

        });

        JSONObject jsonListados = new JSONObject();
        try {
            listViewAreasValues = new ArrayList<>();
            ArrayList<String> listViewAreasText = new ArrayList<>();

            if (getIntent() != null && getIntent().hasExtra("listadoDeAreas")) {

                if(!getIntent().hasExtra("SIN_TODAS")){
                    jsonListados.put("area", "TODAS");
                    listViewAreasText.add("TODAS");
                    listViewAreasValues.add(jsonListados.toString());
                }

                ArrayList arrayListAreas = getIntent().getParcelableArrayListExtra("listadoDeAreas");
                for (int index = 0; index < arrayListAreas.size(); index++) {

                    jsonListados = new JSONObject();

                    String[] fila = arrayListAreas.get(index).toString().split("~");

                    jsonListados.put("area", fila[1]);

                    listViewAreasText.add(fila[2]);
                    listViewAreasValues.add(jsonListados.toString());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listViewAreasText);
                mLvAreas.setAdapter(adapter);
            }
        } catch (JSONException ex) {
            String x = "";
        } catch (Exception ex) {
            String x = "";
        }
    }
}
