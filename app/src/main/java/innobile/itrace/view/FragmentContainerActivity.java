package innobile.itrace.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import innobile.itrace.R;


/**
 * Created by danielgil on 3/23/15.
 */
public abstract class FragmentContainerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        FragmentManager fm= getSupportFragmentManager();
        Fragment fragment=fm.findFragmentById(R.id.fragment_container_content);
        if (fragment == null){
            fragment=createFragment();
            fm.beginTransaction().add(R.id.fragment_container_content,fragment).addToBackStack("fragBack").commit();
        }

    }

    abstract Fragment createFragment();
}
