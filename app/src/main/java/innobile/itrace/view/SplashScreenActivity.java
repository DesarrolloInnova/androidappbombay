package innobile.itrace.view;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;

import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


public class SplashScreenActivity extends FragmentContainerActivity {

        private SplashScreenFragment splashScreenFragment=null;
        private Bundle arguments;

        @Override
        Fragment createFragment() {
            splashScreenFragment = SplashScreenFragment.newInstance();
            arguments = new Bundle();
            splashScreenFragment.setArguments(arguments);
            return splashScreenFragment;

        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event)
        {

            if (keyCode == KeyEvent.KEYCODE_BACK)
            {
                splashScreenFragment.onKeyDown(keyCode);
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }


}
