package innobile.itrace.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class LoginActivity extends FragmentContainerActivityToolbarNav {

    private LoginFragment loginFragment = null;
    private Bundle arguments;

    @Override
    Fragment createFragment() {
        loginFragment = LoginFragment.newInstance();
        arguments = new Bundle();
        loginFragment.setArguments(arguments);
        return loginFragment;
    }
}
