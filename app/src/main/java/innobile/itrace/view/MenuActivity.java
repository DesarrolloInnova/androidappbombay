package innobile.itrace.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MenuActivity extends FragmentContainerActivityToolbarNav {

    private MenuFragment menuFragment = null;
    private Bundle arguments;

    @Override
    Fragment createFragment() {
        menuFragment = MenuFragment.newInstance();
        arguments = new Bundle();
        menuFragment.setArguments(arguments);
        return menuFragment;
    }
}
