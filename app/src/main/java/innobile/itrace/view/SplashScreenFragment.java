package innobile.itrace.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import innobile.itrace.R;


public class SplashScreenFragment extends Fragment {

    private boolean isRunning;

    public static SplashScreenFragment newInstance() {
        SplashScreenFragment fragment = new SplashScreenFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_splashscreen, container, false);
        setHasOptionsMenu(false);

        isRunning = true;

        startSplash();

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    /**
     * Starts the count down timer for 3-seconds. It simply sleeps the thread
     * for 3-seconds.
     */
    private void startSplash() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Thread.sleep(3000);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    if(getActivity() == null)
                        return;


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            doFinish();
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * If the app is still running than this method will start the MainActivity
     * and finish the Splash.
     */
    private synchronized void doFinish() {

        if (isRunning) {
            isRunning = false;
            /*
            Intent i = new Intent(getActivity(), LoginActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            */
            getActivity().finish();

            // Luego que termine de ejecutarse el Splash se abre una ventana de bievenida (HomeActivity)
            Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
            startActivity(homeIntent);
        }
    }


    public void onKeyDown(int keyCode) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isRunning = false;
            getActivity().finish();

        }

    }


}
