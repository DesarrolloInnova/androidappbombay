package innobile.itrace.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataAccessJson;
import innobile.itrace.modelo.ActivosModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class GestionarInventarioActivityFragment extends Fragment implements View.OnClickListener {

    private int requestListadoAreas = 1;

    private Button mButtonCancel = null;
    private Button mButtonNext = null;
    private Button mButtonReporteDeInventario = null;
    private TextView mlblArea = null;
    private EditText mTxtArea = null;
    private ImageButton mIbtnBuscarAreas = null;

    public static GestionarInventarioActivityFragment newInstance() {
        GestionarInventarioActivityFragment fragment = new GestionarInventarioActivityFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_gestionar_inventario, container, false);
        setHasOptionsMenu(false);
        mIbtnBuscarAreas = (ImageButton) root.findViewById(R.id.ibtnBuscarAreas);
        mIbtnBuscarAreas.setOnClickListener(this);
        mTxtArea = (EditText) root.findViewById(R.id.txtArea);
        mButtonCancel = (Button) root.findViewById(R.id.btnCancel);
        mButtonReporteDeInventario = (Button) root.findViewById(R.id.btnReporteDeInventario);
        mlblArea = (TextView) root.findViewById(R.id.lblArea);
        Spannable wordtoSpan = new SpannableString(mlblArea.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mlblArea.setText(wordtoSpan);

        mButtonNext = (Button) root.findViewById(R.id.btnNext);
        mButtonNext.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);
        mButtonReporteDeInventario.setOnClickListener(this);

        //new GetAreasAsyncTask().execute(getActivity());
        return root;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent nextIntent = null;
        switch (v.getId()) {
            case R.id.btnCancel:
                getActivity().finish();
                break;
            case R.id.btnNext:
                // Valida que se haya saleccionado un área
                if (TextUtils.isEmpty(mTxtArea.getText().toString().trim()) || mTxtArea.getText().toString().trim().equals("TODAS")) {
                    Toast.makeText(getActivity(), "El área es obligatoria", Toast.LENGTH_SHORT).show();
                    return;
                }
                nextIntent = new Intent(getActivity(), ActivosActivity.class);
                nextIntent.putExtra("areaLeida", mTxtArea.getText().toString().trim());
                this.startActivity(nextIntent);

                break;
            case R.id.btnReporteDeInventario:
                if (TextUtils.isEmpty(mTxtArea.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "El área es obligatoria", Toast.LENGTH_SHORT).show();
                    return;
                }
                nextIntent = new Intent(getActivity(), ReporteInventarioActivity.class);
                nextIntent.putExtra("areaLeida", mTxtArea.getText().toString().trim());
                this.startActivity(nextIntent);

                break;

            case R.id.ibtnBuscarAreas:
                new GetAreasAsyncTask().execute(getActivity());

                break;
        }
    }

    /**
     * Clase contenedora de una tarea asincrónica para obtener el listado de areas
     */
    private class GetAreasAsyncTask extends AsyncTask<Object, Integer, Integer> {

        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(getActivity());
        ArrayList<Object> arrayListListadosActivos = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected Integer doInBackground(Object... params) {
            context = (Context) params[0];
            Integer resultado = 1;

            try {
                ActivosModel activosModel = new ActivosModel();
                String respuesta = activosModel.GetListadoAreasFromRF(context);

                arrayListListadosActivos = new ArrayList<Object>();
                arrayListListadosActivos = DataAccessJson.parserJsonListadosActivos(respuesta);

            } catch (ConnectException | SocketTimeoutException | UnknownHostException connectionEx) {
                resultado = 2;
                //return 2;
            } catch (Exception ex) {
                resultado = 3;
                //return 4;
            }

            return resultado;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Cargando...");
            this.dialog.show();
        }


        @Override
        protected void onPostExecute(final Integer result) {

            switch (result) {

                case 1:
                    if (!arrayListListadosActivos.isEmpty()) {
                        if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_FAIL)) {

                            if (this.dialog.isShowing()) {
                                this.dialog.dismiss();
                            }
                            //Muestra un mensaje en pantalla
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                            dlgAlert.setMessage((String) arrayListListadosActivos.get(1));
                            dlgAlert.setTitle("iTrace Móvil");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        } else {
                            if (arrayListListadosActivos.get(0).equals(Cons.RESPONSE_SUCCESS)) {

                                try {
                                    Intent listadoAreasIntent = new Intent(getActivity(), ListadoAreasActivity.class);
                                    listadoAreasIntent.putParcelableArrayListExtra("listadoDeAreas", (ArrayList<? extends Parcelable>) arrayListListadosActivos.get(2));
                                    startActivityForResult(listadoAreasIntent, requestListadoAreas);
                                    if (this.dialog.isShowing()) {
                                        this.dialog.dismiss();
                                    }
                                } catch (Exception ex) {
                                    if (this.dialog.isShowing()) {
                                        this.dialog.dismiss();
                                    }
                                    //Muestra un mensaje en pantalla
                                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                                    dlgAlert.setMessage("Ocurrió un error al consultar las áreas");
                                    dlgAlert.setTitle("iTrace Móvil");
                                    dlgAlert.setPositiveButton("OK", null);
                                    dlgAlert.setCancelable(true);
                                    dlgAlert.create().show();
                                    return;
                                }
                            }
                        }
                    }
                    break;

                case 2:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error en la comunicación");
                    dlgAlert.setTitle("iLogistica Movil");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;

                case 3:
                    if (this.dialog.isShowing()) {
                        this.dialog.dismiss();
                    }

                    dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage("Ocurrió un error al consultar listados para los activos");
                    dlgAlert.setTitle("iLogistica Movil");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    break;
            }
        }


        public class ObjetosClase {
            String id;
            String nombre;

            //Constructor
            public ObjetosClase(String id, String nombre) {
                super();
                this.id = id;
                this.nombre = nombre;
            }

            @Override
            public String toString() {
                return nombre;
            }

            public String getId() {
                return id;
            }
        }
    }

    /**
     * Encargado de gestionar el Intent que hemos recibido de la actividad ListadoAreasActivity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if ((requestCode == requestListadoAreas) && (resultCode == -1)) {//RESULT_OK
                String jsonArea = data.getDataString();
                JSONObject jSONActivo = new JSONObject(jsonArea);
                String area = jSONActivo.getString("area");

                mTxtArea.setText(area);
            }
        } catch (JSONException jsonError) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            dlgAlert.setMessage("Ocurrió un error al devolver el área");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        } catch (Exception e) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            dlgAlert.setMessage("Ocurrió un error al devolver el área");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }
}
