package innobile.itrace.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itrace.data.DataAccessJson;
import innobile.itrace.modelo.ActivosModel;

/**
 * Created by Dev1 on 12/11/15.
 */
public class ReporteInventarioActivity extends Activity {
    private ListView mLvReporteInventario;
    private ArrayList<String> listViewActivosValues = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reporte_inventario);

        mLvReporteInventario = (ListView) findViewById(R.id.lvReporteInventario);

        String area = getIntent().getStringExtra("areaLeida");

        new GetReporteInventarioPorAreaFromRF().execute(this, area);

    }



    /**
     * Clase contenedora de una tarea asincrónica para obtener activo filtrado por su respectivo
     * cógido de búsqueda de la actividad de activos
     */
    private class GetReporteInventarioPorAreaFromRF extends AsyncTask<Object, Integer, ArrayList<String>> {
        //Barra de progreso
        private final ProgressDialog dialog = new ProgressDialog(ReporteInventarioActivity.this);
        ArrayList<String> arraylistReporteInventario = null;
        AlertDialog.Builder dlgAlert;
        Context context;

        @Override
        protected ArrayList<String> doInBackground(Object... objects) {
            context = (Context) objects[0];
            String area = (String) objects[1];

            ActivosModel activosModel = new ActivosModel();
            try {

                String jsonReporteInventario = activosModel.GetReporteInventarioPorAreaFromRF(context, area);

                arraylistReporteInventario = new DataAccessJson().parserJsonReporteInventario(jsonReporteInventario);
            } catch (Exception ex) {
                arraylistReporteInventario = new ArrayList<>();
                arraylistReporteInventario.add("0");
            }

            return arraylistReporteInventario;
        }

        @Override
        protected void onPreExecute() {
            //Pone el mensaje y muestra el ProgressBar
            this.dialog.setMessage("Consultando reporte de inventario...");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<String> arrayListReportInventario) {
            try {

                switch (arrayListReportInventario.get(0)) {
                    //Cuando no se ha ingresado nada en la búsqueda
                    case "0":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }

                        //Muestra un mensaje en pantalla
                        dlgAlert = new AlertDialog.Builder(context);
                        dlgAlert.setMessage("Ocurrió un error consultando reporte de inventario");
                        dlgAlert.setTitle("iTrace");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        break;

                    //Cuando encontró mas de un activo
                    case "1":
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }
                        try {

                            ArrayList<String> listViewReporteInventarioText = new ArrayList<>();

                            String[] activos_leidos = arrayListReportInventario.get(2).split("~");
                            String[] activos_no_leidos = arrayListReportInventario.get(3).split("~");
                            //String[] activos_leidos_en_diferente_area = arrayListReportInventario.get(4).split("~");

                            listViewReporteInventarioText.add(activos_leidos[0] + ":   " + activos_leidos[1]);
                            listViewReporteInventarioText.add(activos_no_leidos[0] + ":   " + activos_no_leidos[1]);
                            //listViewReporteInventarioText.add(activos_leidos_en_diferente_area[0] + ":   " + activos_leidos_en_diferente_area[1]);

                            ArrayAdapter<String> adapter = new ArrayAdapter<>(ReporteInventarioActivity.this, android.R.layout.simple_list_item_1, listViewReporteInventarioText);
                            mLvReporteInventario.setAdapter(adapter);

                        } catch (Exception ex) {
                            //Muestra un mensaje en pantalla
                            dlgAlert = new AlertDialog.Builder(context);
                            dlgAlert.setMessage("Ocurrió un error mostrando reporte de inventario");
                            dlgAlert.setTitle("iTrace");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        }

                        break;

                    default:
                        if (this.dialog.isShowing()) {
                            this.dialog.dismiss();
                        }
                        break;
                }
            } catch (Exception ex) {
                Log.e("Error", "Ocurrió un error consultando reporte de inventario: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
            }
        }
    }
}
