package innobile.itrace.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Config;
import android.view.View;
import android.widget.Toast;

import innobile.itrace.R;

/**
 * Created by danielgil on 5/22/15.
 */
public abstract class FragmentContainerActivityToolbarNav extends AppCompatActivity {

    public static final int NAVIGATION_TYPE = 100;
    public static final int NAVIGATION_TYPE_DRAWERTOGGLE = 101;
    public static final int NAVIGATION_TYPE_BACK = 102;
    private Bundle arguments;
    int cont = 0;

    /***********************************/

    int PROFILE = R.mipmap.ic_launcher;

    //String TITLES[] = {"Mis Registros", "Mis Premios","Salir"};
    String TITLES[] = {"Configuracion","Cerrar sesión"};
    //int ICONS[] = {R.drawable.ic_records, R.drawable.ic_money,R.drawable.ic_exit};
    int ICONS[] = {R.drawable.calibrate,R.drawable.exit};
    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle

    int fragment_navigation_type=0;
    FragmentManager fm;
    FragmentContainerActivityToolbarNav instance=this;
    /**********************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the container123
        setContentView(R.layout.activity_fragment_container_toolbar_navdrawer);

        // get fragment manager

        fm = getSupportFragmentManager();


        // get the fragment which supports the content
        Fragment fragment=fm.findFragmentById(R.id.fragment_container_content);
        if (fragment == null){
            fragment=createFragment();
            fragment_navigation_type=fragment.getArguments().getInt(String.valueOf(this.NAVIGATION_TYPE), NAVIGATION_TYPE_DRAWERTOGGLE);
            fm.beginTransaction().add(R.id.fragment_container_content, fragment).commit();
            //fm.beginTransaction().add(R.id.fragment_container_content, fragment).addToBackStack("fragBack").commit();
        }

        // get the toolbar
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        //toolbar.setLogo(R.drawable.itrace);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_forward);

        // get the recycler
        mRecyclerView = (RecyclerView)findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View
        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        // create the adapter for the navigation drawer
        mAdapter = new NavigationDrawerAdapter(TITLES, ICONS,PROFILE);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout)findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view

        if (fragment_navigation_type==NAVIGATION_TYPE_DRAWERTOGGLE) {
            mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, Drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                    // open I am not going to put anything here)
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    // Code here will execute once drawer is closed
                }


            }; // Drawer Toggle Object Made
            Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
            mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
            // Setting toolbar as the ActionBar with setSupportActionBar() call
        }else {

            //toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            /*
            fm.addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {
                            // Update your UI here.
                            Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();

                        }
                    });
                */
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    instance.onBackPressed();
                    /*
                    if (fm.findFragmentByTag("fragBack") != null) {


                    }
                    else {

                        return;
                    }
                    if (fm.getBackStackEntryCount() != 0) {
                        Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
                        Fragment frag = fm.findFragmentByTag("fragBack");
                        FragmentTransaction transac = getSupportFragmentManager().beginTransaction().remove(frag);
                        transac.commit();
                    }
                */

                }
            });


        }
    }

    abstract Fragment createFragment();

    @Override
    public void onBackPressed() {


        if(cont == 0){
            Toast.makeText(getApplicationContext(), "Presione de nuevo para salir", Toast.LENGTH_SHORT).show();
            cont++;
        }else{
            super.onBackPressed();
        }

        new CountDownTimer(6000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                cont = 0;
            }
        }.start();

    }
}

