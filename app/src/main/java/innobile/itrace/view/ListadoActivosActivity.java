package innobile.itrace.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.transport.IFA_activos;

/**
 * Created by Dev1 on 12/11/15.
 */
public class ListadoActivosActivity extends Activity {
    private ListView mLvActivos;
    private ArrayList<String> listViewActivosValues = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_activos);

        mLvActivos = (ListView) findViewById(R.id.lv_activos);

        mLvActivos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    String json = (String) mLvActivos.getAdapter().getItem(i);
                    String jsonActivo = listViewActivosValues.get(i);

                    Intent data = new Intent();
                    data.setData(Uri.parse(jsonActivo));
                    setResult(RESULT_OK, data);
                    finish();
                } catch (Exception ex) {
                    String x = "";
                }
            }

        });

        JSONObject jsonListados = null;
        try {
            listViewActivosValues = new ArrayList<>();
            ArrayList<String> listViewActivosText = new ArrayList<>();

            if (getIntent() != null && getIntent().hasExtra("listadoDeActivos")) {

                ArrayList<IFA_activos> arrayListIFA_activos = getIntent().getParcelableArrayListExtra("listadoDeActivos");
                for (IFA_activos activo : arrayListIFA_activos) {

                    jsonListados = new JSONObject();

                    jsonListados.put("codigo_activo", activo.getAct_codigo_activo());
                    jsonListados.put("descripcion", activo.getAct_descripcion());
                    jsonListados.put("codigo_barras", activo.getAct_codigo_barras());
                    jsonListados.put("fecha_compra", activo.getAct_fecha_compra());
                    jsonListados.put("meses_depreciacion", activo.getAct_meses_depreciacion());
                    jsonListados.put("area", activo.getAct_area());
                    jsonListados.put("grupo", activo.getAct_grupo());
                    jsonListados.put("ciclo_vida", activo.getAct_ciclo_vida());
                    jsonListados.put("mantenimiento", activo.getAct_mantenimiento());
                    jsonListados.put("centro_costos", activo.getAct_centro_costos());
                    jsonListados.put("valor_compra", activo.getAct_valor_compra());
                    jsonListados.put("valor_iva_compra", activo.getAct_valor_iva_compra());
                    jsonListados.put("cuenta_contable", activo.getAct_cuenta_contable());
                    jsonListados.put("nombre_cuenta_contable", activo.getAct_nombre_cuenta_contable());
                    jsonListados.put("ciudad", activo.getAct_ciudad());
                    jsonListados.put("piso", activo.getAct_piso());
                    jsonListados.put("ubicacion", activo.getAct_ubicacion());
                    jsonListados.put("tipo_ubicacion", activo.getAct_tipo_ubicacion());
                    jsonListados.put("serial", activo.getAct_serial());
                    jsonListados.put("color", activo.getAct_color());
                    jsonListados.put("modelo", activo.getAct_modelo());
                    jsonListados.put("tipo_activo", activo.getAct_tipo_activo());
                    jsonListados.put("material", activo.getAct_material());
                    jsonListados.put("estado", activo.getAct_estado());
                    jsonListados.put("fabricante", activo.getAct_fabricante());
                    jsonListados.put("empresa_propietaria", activo.getAct_empresa_propietaria());
                    jsonListados.put("nombre_maquina", activo.getAct_nombre_maquina());
                    jsonListados.put("sistema_operativo", activo.getAct_sistema_operativo());
                    jsonListados.put("servidor", activo.getAct_servidor());
                    jsonListados.put("ultima_fecha_reporte_agente", activo.getAct_ultima_fecha_reporte_agente());
                    jsonListados.put("observaciones", activo.getAct_observaciones());
                    jsonListados.put("tag_activo", activo.getAct_tag_activo());
                    jsonListados.put("responsable", activo.getAct_responsable());
                    jsonListados.put("direccion_ubicacion", activo.getAct_direccion_ubicacion());
                    jsonListados.put("canal", activo.getAct_canal());
                    jsonListados.put("en_red", activo.getAct_en_red());
                    jsonListados.put("usuario_red", activo.getAct_usuario_red());
                    jsonListados.put("fecha_ingreso_bodega", activo.getAct_fecha_ingreso_bodega());
                    jsonListados.put("orden_compra", activo.getAct_oc());
                    jsonListados.put("service_pack", activo.getAct_service_pack());
                    jsonListados.put("procesador", activo.getAct_procesador());
                    jsonListados.put("velocidad", activo.getAct_velocidad());
                    jsonListados.put("disco_duro", activo.getAct_disco_duro());
                    jsonListados.put("total_memoria", activo.getAct_total_memoria());
                    jsonListados.put("codigo_conteo", activo.getAct_codigo_conteo());

                    listViewActivosText.add("Codigo de barras: " + activo.getAct_codigo_barras().trim() + "  Serial: " + activo.getAct_serial().trim() + "  Descripción: " + activo.getAct_descripcion().trim() + "  Codigo de activo: " + activo.getAct_codigo_activo().trim());
                    listViewActivosValues.add(jsonListados.toString());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listViewActivosText);
                mLvActivos.setAdapter(adapter);
                String x = "";
            }
        } catch (JSONException ex) {
            String x = "";
        } catch (Exception ex) {
            String x = "";
        }
    }
}
