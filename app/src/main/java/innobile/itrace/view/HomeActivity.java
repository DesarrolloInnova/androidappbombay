package innobile.itrace.view;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.SQLite_User;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


public class HomeActivity extends AppCompatActivity {

    private Button mLogin = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        RegistrarE1();
        RegistrarE2();
        RegistrarE3();
        RegistrarE4();


        mLogin = (Button) findViewById(R.id.btnLogin);

        /**
         * Configuramos los botones con la propietad setOnClickListener para no tener la necesidad de
         * utilizar el onClick de la vista que no es lo mas adecuado.
         */
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se le pasa como primer parámetro la actividad en la que estamos y como segundo
                //parámetro la actividad a la que quiero ir
                Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(loginIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void RegistrarE1(){
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        SQLite_User conn = new SQLite_User(getApplicationContext(), Utilidades.TABLA_USUARIO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_USERNAME, "E1");
        values.put(Utilidades.CAMPO_PASSWORD, "123");
        values.put(Utilidades.CAMPO_DATATIME, hourdateFormat.format(date));
        db.insert(Utilidades.TABLA_USUARIO, null, values);
        db.close();
    }

    public void RegistrarE2(){
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        SQLite_User conn = new SQLite_User(getApplicationContext(), Utilidades.TABLA_USUARIO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_USERNAME, "E2");
        values.put(Utilidades.CAMPO_PASSWORD, "123");
        values.put(Utilidades.CAMPO_DATATIME, hourdateFormat.format(date));
        db.insert(Utilidades.TABLA_USUARIO, null, values);
        db.close();
    }

    public void RegistrarE3(){
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        SQLite_User conn = new SQLite_User(getApplicationContext(), Utilidades.TABLA_USUARIO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_USERNAME, "E3");
        values.put(Utilidades.CAMPO_PASSWORD, "123");
        values.put(Utilidades.CAMPO_DATATIME, hourdateFormat.format(date));
        db.insert(Utilidades.TABLA_USUARIO, null, values);
        db.close();
    }

    public void RegistrarE4(){
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        SQLite_User conn = new SQLite_User(getApplicationContext(), Utilidades.TABLA_USUARIO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_USERNAME, "E4");
        values.put(Utilidades.CAMPO_PASSWORD, "123");
        values.put(Utilidades.CAMPO_DATATIME, hourdateFormat.format(date));
        db.insert(Utilidades.TABLA_USUARIO, null, values);
        db.close();
    }

}
