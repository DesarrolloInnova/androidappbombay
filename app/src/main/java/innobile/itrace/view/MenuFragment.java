package innobile.itrace.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itraceandroid.Bombay.ConsultaProductos;
import innobile.itraceandroid.Consultar_Traslados;
import innobile.itraceandroid.Diamante.LecturaCedulas;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.GlobalDatoDetalle;
import innobile.itraceandroid.IngresarProducto;
import innobile.itraceandroid.Marmol.ConsultarPicking;
import innobile.itraceandroid.MenuInventario;
import innobile.itraceandroid.MenuRecepcion;
import innobile.itraceandroid.ProyectoAnterior.ActividadGeneral;
import innobile.itraceandroid.RegistrarResponsable;
import innobile.itraceandroid.Retina.InventarioRetina;
import innobile.itraceandroid.SincronizaDatos;
import innobile.itraceandroid.TrasladosTM.AsistenteApp;
import innobile.itraceandroid.TrasladosTM.InformacionMovimientos;
import innobile.itraceandroid.TrasladosTM.Reportes_Traslados;
import innobile.itraceandroid.TrasladosTM.consultarActivosFijos;


public class MenuFragment extends Fragment implements View.OnClickListener {

    T_Configuraciones_DBManager mT_configuraciones_dbManager = null;
    private CardView mIbtnInventario = null;
    private CardView mIbtnGeneral = null;
    private CardView mIbtnTraslados = null;
    private CardView mIbtnSincronizar = null;
    private CardView mIbtConexion = null;
    private CardView ibtRecepcion = null;
    private CardView ibtnConsultarActivosFijos = null;
    private CardView ibtninventarioActivosFijos = null;
    private CardView ibtnChatBot = null;
    private CardView ibtnregistrar_responsable = null;
    private CardView btnReportes = null;
    private CardView btnInventario_Retina = null;
    private CardView modulo_picking = null;
    private CardView modulo_leer_cedula = null;


    private int requestActivos = 1;

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == requestActivos) && (resultCode == -1)) {//RESULT_OK
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            dlgAlert.setMessage("Ocurrió un error en la comunicación");
            dlgAlert.setTitle("iTrace");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_menu, container, false);
        setHasOptionsMenu(false);


        mIbtnInventario = root.findViewById(R.id.modulo_inventario_fajate);
        mIbtnInventario.setOnClickListener(this);

        mIbtnGeneral = root.findViewById(R.id.ibtnGeneral);
        mIbtnGeneral.setOnClickListener(this);

        mIbtnTraslados = root.findViewById(R.id.ibtnTraslados);
        mIbtnTraslados.setOnClickListener(this);

        mIbtnSincronizar = root.findViewById(R.id.ibtnSincronizar);
        mIbtnSincronizar.setOnClickListener(this);

        mIbtConexion = root.findViewById(R.id.ibtnConexion);
        mIbtConexion.setOnClickListener(this);

        ibtRecepcion = root.findViewById(R.id.ibtnRecepcion);
        ibtRecepcion.setOnClickListener(this);

        ibtnConsultarActivosFijos = root.findViewById(R.id.modulo_consultar_activos_fijos);
        ibtnConsultarActivosFijos.setOnClickListener(this);

        ibtninventarioActivosFijos = root.findViewById(R.id.ibtninventarioActivosFijos);
        ibtninventarioActivosFijos.setOnClickListener(this);

        ibtnChatBot = root.findViewById(R.id.ibtnChatBot);
        ibtnChatBot.setOnClickListener(this);


        ibtnregistrar_responsable = root.findViewById(R.id.ibtnregistrar_responsable);
        ibtnregistrar_responsable.setOnClickListener(this);

        btnReportes = root.findViewById(R.id.modulo_reportes);
        btnReportes.setOnClickListener(this);


        btnInventario_Retina = root.findViewById(R.id.modulo_inventario_retina);
        btnInventario_Retina.setOnClickListener(this);

        modulo_picking = root.findViewById(R.id.modulo_picking);
        modulo_picking.setOnClickListener(this);

        modulo_leer_cedula = root.findViewById(R.id.modulo_leer_cedula);
        modulo_leer_cedula.setOnClickListener(this);


        //Se encarga de ocultar o motrar los elementos de la vista principal del menú
        mIbtConexion.setVisibility(View.GONE);
        mIbtnSincronizar.setVisibility(View.GONE);
        mIbtnInventario.setVisibility(View.GONE);
        mIbtnGeneral.setVisibility(View.GONE);
        ibtRecepcion.setVisibility(View.GONE);
       // btnInventario_Retina.setVisibility(View.GONE);
        modulo_picking.setVisibility(View.GONE);
         mIbtnTraslados.setVisibility(View.GONE);
       ibtnregistrar_responsable.setVisibility(View.GONE);
       ibtninventarioActivosFijos.setVisibility(View.GONE);
         ibtnConsultarActivosFijos.setVisibility(View.GONE);
        modulo_leer_cedula.setVisibility(View.GONE);
        ibtnChatBot.setVisibility(View.GONE);
         btnReportes.setVisibility(View.GONE);
       // btnReportes.setOnClickListener(this);

        try {
            //Crea la base de datos T_Configuraciones
            mT_configuraciones_dbManager = new T_Configuraciones_DBManager(getActivity());
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error creando base de datos T_Configuraciones: ---> " + ex.getMessage() + " ---> " + ex.getStackTrace());
        }
        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    /**
     * Infla el menú, osea se encarga de mostrar el menú(xml) en la actividad
     *
     * @param menu
     * @return
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        //Sirva para controlar lo que hace el botón del menú
        try {
            switch (item.getItemId()) {
                case R.id.menu_menu:
                    Intent intent = new Intent(getActivity(), ConfiguracionesMenuActivity.class);
                    this.startActivity(intent);
                    break;
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return true;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.modulo_inventario_fajate:
                Intent inventarioIntent = new Intent(getActivity(), MenuInventario.class);
                startActivity(inventarioIntent);
                break;

            case R.id.ibtnGeneral:
                Intent generalIntent = new Intent(getActivity(), ConsultaProductos.class);
                GlobalDatoDetalle.comprobarTraerDetalle = "0";
                startActivity(generalIntent);
                break;

            case R.id.ibtnTraslados:
                Intent trasladosIntent = new Intent(getActivity(), Consultar_Traslados.class);
                Global.TIPO_DOCUMENTO = "AF";
                startActivity(trasladosIntent);
                break;

            case R.id.ibtnSincronizar:
                Intent sincronizacion = new Intent(getActivity(), SincronizaDatos.class);
                startActivityForResult(sincronizacion, requestActivos);
                break;

            case R.id.ibtnConexion:
                Intent conectar = new Intent(getActivity(), ActividadGeneral.class);
                startActivity(conectar);
                break;

            case R.id.ibtnRecepcion:
                Intent recepcion = new Intent(getActivity(), MenuRecepcion.class);
                startActivity(recepcion);
                break;

            case R.id.modulo_consultar_activos_fijos:
                Intent ibtnConsultarActivosFijos = new Intent(getActivity(), consultarActivosFijos.class);
                startActivity(ibtnConsultarActivosFijos);
                break;

            case R.id.ibtninventarioActivosFijos:
                Intent ibtninventarioActivosFijos = new Intent(getActivity(), IngresarProducto.class);
                startActivity(ibtninventarioActivosFijos);
                break;

            case R.id.ibtnChatBot:
                Intent ibtnChatBot = new Intent(getActivity(), InformacionMovimientos.class);
                startActivity(ibtnChatBot);
                break;

            case R.id.ibtnregistrar_responsable:
                Intent ibtnregistrar_responsable = new Intent(getActivity(), RegistrarResponsable.class);
                startActivity(ibtnregistrar_responsable);
                break;

            case R.id.modulo_reportes:
                Intent btnReportes = new Intent(getActivity(), Reportes_Traslados.class);
                startActivity(btnReportes);
                break;

            case R.id.modulo_picking:
                Intent intentPicking = new Intent(getActivity(), ConsultarPicking.class);
                startActivity(intentPicking);
                break;


            case R.id.modulo_inventario_retina:
                Intent intentRetina = new Intent(getActivity(), InventarioRetina.class);
                startActivity(intentRetina);
                break;


            case R.id.modulo_leer_cedula:
                Intent intentLeerCedula = new Intent(getActivity(), LecturaCedulas.class);
                startActivity(intentLeerCedula);
                break;

        }
    }
}