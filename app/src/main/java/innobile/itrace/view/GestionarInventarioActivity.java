package innobile.itrace.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import innobile.itrace.R;

public class GestionarInventarioActivity  extends FragmentContainerActivityToolbarNav {

    private GestionarInventarioActivityFragment mGestionarInventarioActivityFragment  = null;
    private Bundle arguments;

    @Override
    Fragment createFragment() {
        mGestionarInventarioActivityFragment = GestionarInventarioActivityFragment.newInstance();
        arguments = new Bundle();
        arguments.putInt(String.valueOf(FragmentContainerActivityToolbarNav.NAVIGATION_TYPE),FragmentContainerActivityToolbarNav.NAVIGATION_TYPE_BACK);
        mGestionarInventarioActivityFragment.setArguments(arguments);
        return mGestionarInventarioActivityFragment;
    }

}
