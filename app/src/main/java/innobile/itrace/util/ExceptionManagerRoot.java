package innobile.itrace.util;

/**
 * Clase principal base para la definición de errores
 *
 * @version 1.0.0
 */
public class ExceptionManagerRoot extends Exception {

    // Codigos de representación de mensajes
    public static final int COD_WARNING = 100;
    public static final int COD_ERROR = 200;
    public static final int COD_INFORMATION = 300;
    public static final int COD_SUCCED = 400;
    // Codigo de error del sistema
    private int codigoErrorSistema;
    // Codigo de error del usuario
    private int codigoErrorUsuario;
    // Mensaje de error para el sistema (logs)
    private String mensajeErrorSistema;
    // Mensaje de error para el usuario
    private String mensajeErrorUsuario;

    // Constructores
    /**
     * Constructor de la clase
     *
     */
    public ExceptionManagerRoot() {
        setCodigoErrorSistema(0);
        setCodigoErrorUsuario(0);
        setMensajeErrorSistema("");
        setMensajeErrorUsuario("");
    }

    /**
     * Constructor de la clase
     *
     * @param errorSistema Codigo de error para el sistema
     * @param errorUsuario Codigo de error para el usuario
     * @param mensajeUsuario Mensaje de error para el usuario
     * @param mensajeSistema Mensaje de error para el sistema
     */
    public ExceptionManagerRoot(int errorSistema, int errorUsuario, String mensajeUsuario, String mensajeSistema) {
        setCodigoErrorSistema(errorSistema);
        setCodigoErrorUsuario(errorUsuario);
        setMensajeErrorSistema(mensajeSistema);
        setMensajeErrorUsuario(mensajeUsuario);


    }

    /**
     * Devuelve el codigo de error del sistema
     *
     * @return codigo de error del sistema
     */
    public int getCodigoErrorSistema() {
        return codigoErrorSistema;
    }

    /**
     * Actualiza el codigo de error actual del sistema
     *
     * @param codigoErrorSistema Codigo de error para el sistema
     */
    public void setCodigoErrorSistema(int codigoErrorSistema) {
        this.codigoErrorSistema = codigoErrorSistema;
    }

    /**
     * Devuelve el codigo de error actual para el usuario
     *
     * @return Codigo de error para el usuario
     */
    public int getCodigoErrorUsuario() {
        return codigoErrorUsuario;
    }

    /**
     * Actualiza el codigo de error actual para el usuario
     *
     * @param codigoErrorUsuario
     */
    public void setCodigoErrorUsuario(int codigoErrorUsuario) {
        this.codigoErrorUsuario = codigoErrorUsuario;
    }

    /**
     * Devuelve el mensaje de error para el sistema
     *
     * @return Mensaje de error para el sistema
     */
    public String getMensajeErrorSistema() {
        return mensajeErrorSistema;
    }

    /**
     * Actualiza el mensaje de error para el sistema
     *
     * @param mensajeErrorSistema Mensaje de error para el sistema
     */
    public void setMensajeErrorSistema(String mensajeErrorSistema) {
        this.mensajeErrorSistema = mensajeErrorSistema;
    }

    /**
     * Devulve el mensaje de error para el usuario
     *
     * @return Mensaje de error para el usuario
     */
    public String getMensajeErrorUsuario() {
        return mensajeErrorUsuario;
    }

    /**
     * Actualiza el mensaje de error para el usuario
     *
     * @param mensajeErrorUsuario Mensaje de error para el usuario
     */
    public void setMensajeErrorUsuario(String mensajeErrorUsuario) {
        this.mensajeErrorUsuario = mensajeErrorUsuario;
    }
}
