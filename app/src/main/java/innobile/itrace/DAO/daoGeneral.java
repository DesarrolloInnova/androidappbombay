package innobile.itrace.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import innobile.itrace.modelo.ModGeneral;
import innobile.itrace.SQL.sqlGeneral;

/**
 * Created by BATMAN on 6/06/18.
 */

public class daoGeneral {


    public final static String TABLE_NAME = "General";
    public final static String URL = "URL";
    public final static String DOCUMENTO = "DOCUMENTO";;
    public final static String CONTEO = "CONTEO";
    public final static String VALIDARCOD = "VALIDARCOD";



    public static boolean ActualizarRegistros(Context context, ModGeneral Modelo) {

        SQLiteDatabase db = new sqlGeneral(context).getWritableDatabase();


        try {
            ContentValues c = new ContentValues();


            c.put(URL, Modelo.getUrl());
            c.put(DOCUMENTO, Modelo.getDocumento());
            c.put(CONTEO, Modelo.getConteo());
            c.put(VALIDARCOD, Modelo.getValidarCod());

            db.update(TABLE_NAME,c, null, null);
            //System.out.println("Acabó de insertar " + c.get(ID));
            db.close();
            return true;
        } catch (Exception e) {
            System.out.println("No inserto Dato: " + e);
            return false;

        }

    }


    public static boolean RegistroExistente(Context context) {
        SQLiteDatabase db = new sqlGeneral(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' ", null);

        boolean result= c.moveToFirst();

        db.close();
        return result;
    }


    public static boolean GuardarRegistros(Context context, ModGeneral Modelo) {

        SQLiteDatabase db = new sqlGeneral(context).getWritableDatabase();


        try {
            ContentValues c = new ContentValues();


            c.put(URL, Modelo.getUrl());
            c.put(DOCUMENTO, Modelo.getDocumento());
            c.put(CONTEO, Modelo.getConteo());
            c.put(VALIDARCOD, Modelo.getValidarCod());

            db.insert(TABLE_NAME, null, c);
            db.close();
            return true;
        } catch (Exception e) {
            System.out.println("No inserto Dato: " + e);
            return false;

        }

    }


    public static ModGeneral CapturarRegistros(Context context) {
        SQLiteDatabase db = new sqlGeneral(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' ", null);

        ModGeneral modelo =new ModGeneral();

        if (c.moveToFirst()) {


            modelo.setDocumento(c.getString(c.getColumnIndex(DOCUMENTO)));
            modelo.setUrl(c.getString(c.getColumnIndex(URL)));
            modelo.setConteo(c.getString(c.getColumnIndex(CONTEO)));
            modelo.setValidarCod(c.getInt(c.getColumnIndex(VALIDARCOD)));


        }
        db.close();
        return modelo;
    }
}
