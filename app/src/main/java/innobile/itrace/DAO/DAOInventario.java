package innobile.itrace.DAO;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.slf4j.helpers.Util;

import innobile.itrace.Activity.InventarioActivity;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.ModeloInventario;
import innobile.itrace.SQL.sqlInventario;
import innobile.itrace.modelo.RequisicionData;

import java.util.ArrayList;

public class DAOInventario {


    public final static String TABLE_NAME = "Inventario";
    public final static String ID = "id";
    public final static String USERNAME = "USERNAME";
    public final static String DOCUMENTO = "DOCUMENTO";
    public final static String UBICACION = "UBICACION";
    public final static String CODIGO = "CODIGO";
    public final static String CONTEO = "CONTEO";
    public final static String CANTIDAD = "CANTIDAD";
    public final static String ESTADO = "ESTADO";
    public final static String FECHA = "FECHA";


    public static ArrayList<Registros> getListaxID(Context context) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);
        ArrayList<Registros> Lista = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Lista.add(new Registros( c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getString(5)));
            } while (c.moveToNext());
        }
        db.close();
        return Lista;
    }

    public static boolean RegistroExitente(Context context, String Ubicacion, String Codigo) {
        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' where " + UBICACION + " = '" + Ubicacion + "' AND " + CODIGO + " = '" + Codigo + "' COLLATE NOCASE", null);
        boolean result = c.moveToFirst();

        db.close();
        return result;
    }

    public static int CapturarCantidadCodigosLeidos(Context context, String Codigo) {
        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT SUM(" + CANTIDAD + ") FROM '" + TABLE_NAME + "' where " + CODIGO + " = '" + Codigo + "' COLLATE NOCASE", null);
        ArrayList<String> ListaMovID = new ArrayList<String>();
        int cant = 0;
        if (c.moveToFirst()) {
            cant = c.getInt(0);
        }
        db.close();
        return cant;
    }

    public static int CapturarCantidadTotalLeidos(Context context) {
        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT SUM(" + CANTIDAD + ") FROM '" + TABLE_NAME + "'", null);
        ArrayList<String> ListaMovID = new ArrayList<String>();
        int cant = 0;
        if (c.moveToFirst()) {
            cant = c.getInt(0);
        }
        db.close();
        return cant;
    }

    public static ArrayList<Registros> CapturasListaLecturas(Context context) {

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'  order by "  + Utilidades.CAMPO_PRIMARYID + " DESC ", null);
        ArrayList<Registros> Lista = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Lista.add(new Registros( c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getString(5)));
            } while (c.moveToNext());
        }
        db.close();
        return Lista;
    }

    public static ModeloInventario GetMovimientoXCodigo(Context context, int id, String Codigo) {
        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' WHERE id = " + id + "  AND Codigo = '" + Codigo + "' COLLATE NOCASE", null);

        ModeloInventario modelo = new ModeloInventario();

        if (c.moveToFirst()) {

            modelo.setId(c.getInt(c.getColumnIndex(ID)));
            modelo.setUserName(c.getString(c.getColumnIndex(USERNAME)));
            modelo.setDocumento(c.getString(c.getColumnIndex(DOCUMENTO)));
            modelo.setUbicacion(c.getString(c.getColumnIndex(UBICACION)));
            modelo.setCodigo(c.getString(c.getColumnIndex(CODIGO)));
            modelo.setConteo(c.getString(c.getColumnIndex(CONTEO)));
            modelo.setCantidad(c.getInt(c.getColumnIndex(CANTIDAD)));
            modelo.setEstado(c.getString(c.getColumnIndex(ESTADO)));
            modelo.setFecha(c.getString(c.getColumnIndex(FECHA)));

        }
        db.close();
        return modelo;
    }

    public static boolean GuardarRegistros(Context context, ModeloInventario Modelo) {

        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();


        try {
            ContentValues c = new ContentValues();

            c.put(USERNAME, Modelo.getUserName());
            c.put(DOCUMENTO, Modelo.getDocumento());
            c.put(UBICACION, Modelo.getUbicacion());
            c.put(CODIGO, Modelo.getCodigo());
            c.put(CONTEO, Modelo.getConteo());
            c.put(CANTIDAD, Modelo.getCantidad());
            c.put(ESTADO, Modelo.getEstado());
            c.put(FECHA, Modelo.getFecha());

            db.insert(TABLE_NAME, null, c);
            System.out.println("Acabó de insertar " + c.get(ID));
            db.close();
            return true;
        } catch (Exception e) {
            System.out.println("No inserto Dato: " + e);
            return false;

        }

    }

    public static boolean BorrarRegistros(Context context) {

        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        int a = db.delete(TABLE_NAME, null, null);

        return a > 0;

    }

    public static boolean BorrarRegistrosCodigo(Context context, String NumDocumento) {

        SQLiteDatabase db = new sqlInventario(context).getWritableDatabase();

        int a = db.delete(TABLE_NAME, DOCUMENTO + " = '" + NumDocumento + "'", null);

        return a > 0;


    }

    public static void BorrarRegistroPorId(Context context, int id, int valor) {


            int ValorActual = 0;
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

            SQLiteDatabase db = conn.getWritableDatabase();

            String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

            Cursor c = db.rawQuery("SELECT Cantidad FROM '" + myTable + "'", null);




            if (c.moveToFirst()) {
                do {
                    ValorActual = c.getInt(0);
                } while (c.moveToNext());
            }

        if(ValorActual == 1 || ValorActual - valor == 0){

            ConexionSQLiteHelper conna = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);
            SQLiteDatabase dbs = conna.getWritableDatabase();

            dbs.delete(Utilidades.TABLA_REGISTRO, Utilidades.CAMPO_PRIMARYID + " = " + id, null);
            Toast.makeText(context,"Los datos fueron eliminados", Toast.LENGTH_SHORT).show();
        }



            if(ValorActual - valor < 0){
                new AlertDialog.Builder(context)
                        .setTitle(":( Houston, tenemos un problema")
                        .setMessage("La cantidad que intenta borrar es mayor a la registrada en la base de datos")
                        .setCancelable(false)
                        .setPositiveButton("ok", null).show();
            }
            else {
                if(ValorActual > 1 || valor == 0  ){


                    ConexionSQLiteHelper conne = new ConexionSQLiteHelper(context, Utilidades.TABLA_REGISTRO, null, 1);

                    SQLiteDatabase dbr = conne.getWritableDatabase();



                    // Cursor c = db.rawQuery("UPDATE " + myTable + " SET Cantidad =  Cantidad - " +  valor + " WHERE id = " + id , null);
                    Cursor cl = dbr.rawQuery("UPDATE " + myTable + " SET Cantidad = Cantidad  - " + valor + " WHERE " + Utilidades.CAMPO_PRIMARYID + " = " + id , null);
                    cl.moveToFirst();
                    cl.close();

                    Toast.makeText(context,"Deslice la pantalla para ver los cambios", Toast.LENGTH_SHORT).show();
                }

            }
    }


    public static ArrayList<RequisicionData> CapturasListaRequisicion(Context context, int codigo_requisicion) {

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.REQUISICION_TABLA, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();

        String myTable = Utilidades.REQUISICION_TABLA;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "' WHERE " + Utilidades.REQUISICION_DATA_DOCUMENTO + " = " + codigo_requisicion + " order by "  + Utilidades.REQUISICION_DATA_FECHA + " DESC "  , null);
        ArrayList<RequisicionData> Lista = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Lista.add(new RequisicionData( c.getString(0),c.getInt(1),c.getString(2),c.getInt(3),c.getString(4)));
            } while (c.moveToNext());
        }
        db.close();
        return Lista;
    }


}



