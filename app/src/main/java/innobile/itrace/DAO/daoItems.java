package innobile.itrace.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import innobile.itrace.modelo.ModItems;
import innobile.itrace.SQL.sqlItems;

import java.util.ArrayList;

/**
 * Created by BATMAN on 6/06/18.
 */

public  class daoItems {

    public final static String TABLE_NAME = "Items";
    public final static String ID = "id";
    public final static String CODIGO = "CODIGO";
    public final static String DESCRIPCION = "DESCRIPCION";
    public final static String CODIGOALTERNO = "CODIGOALTERNO";
    public final static String CANTIDAD = "CANTIDAD";


    public static ArrayList<ModItems> getListaxID(Context context) {
        SQLiteDatabase db = new sqlItems(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "'", null);
        ArrayList<ModItems> Lista = new ArrayList<ModItems>();

        if (c.moveToFirst()) {
            do {
                ModItems modelo = new ModItems();

                modelo.setId(c.getInt(c.getColumnIndex(ID)));

                modelo.setCodigo(c.getString(c.getColumnIndex(CODIGO)));
                modelo.setDescripcion(c.getString(c.getColumnIndex(DESCRIPCION)));

                modelo.setCantidad(c.getInt(c.getColumnIndex(CANTIDAD)));

                Lista.add(modelo);

            } while (c.moveToNext());
        }
        db.close();
        return Lista;
    }
    public static ModItems ConsultarRegistroItem(Context context, String Codigo) {
        SQLiteDatabase db = new sqlItems(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' WHERE CODIGO = '" + Codigo + "' OR CODIGOALTERNO = '"+ Codigo +"' COLLATE NOCASE", null);
        ModItems modItems=new ModItems();
        if(c.moveToFirst()){
            modItems.setId(c.getInt(c.getColumnIndex(ID)));
            modItems.setCodigo(c.getString(c.getColumnIndex(CODIGO)));
            modItems.setDescripcion(c.getString(c.getColumnIndex(DESCRIPCION)));
            modItems.setCodigoAlterno(c.getString(c.getColumnIndex(CODIGOALTERNO)));
            modItems.setCantidad(c.getInt(c.getColumnIndex(CANTIDAD)));
        }

        db.close();
        return modItems;
    }

    public static boolean RegistroExistente(Context context, String Codigo) {
        SQLiteDatabase db = new sqlItems(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "' WHERE  CODIGO = '" + Codigo + "' OR CODIGOALTERNO = '"+ Codigo +"' COLLATE NOCASE", null);

        boolean result= c.moveToFirst();

        db.close();
        return result;
    }

    public static int CapturarCantidadTotal(Context context) {
        SQLiteDatabase db = new sqlItems(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT COUNT(" + ID + ") FROM '" + TABLE_NAME + "'", null);
        ArrayList<String> ListaMovID = new ArrayList<String>();
        int cant = 0;
        if (c.moveToFirst()) {
            cant = c.getInt(0);
        }
        db.close();
        return cant;
    }



    public static boolean CrearItems(Context context, ModItems Modelo) {

        SQLiteDatabase db = new sqlItems(context).getWritableDatabase();


        try {
            ContentValues c = new ContentValues();

            c.put(CODIGO, Modelo.getCodigo());
            c.put(DESCRIPCION, Modelo.getDescripcion());
            c.put(CODIGOALTERNO, Modelo.getCodigoAlterno());
            c.put(CANTIDAD, Modelo.getCantidad());

            db.insert(TABLE_NAME, null, c);
            System.out.println("Acabó de insertar " + c.get(ID));
            db.close();
            return true;
        } catch (Exception e) {
            System.out.println("No inserto Dato: " + e);
            return false;

        }

    }


    public static boolean BorrarItems(Context context) {

        SQLiteDatabase db = new  sqlItems(context).getWritableDatabase();

        int a = db.delete(TABLE_NAME, null, null);
        //db.execSQL("DELETE FROM Usuarios WHERE nombre=?", args);
        return a > 0;


    }
}
