package innobile.itrace.DAO;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import innobile.itrace.modelo.ModUsuario;
import innobile.itrace.SQL.sqlUsuarios;

import java.util.ArrayList;
import java.util.List;

public class daoUsuarios {

    public final static String TABLE_NAME = "Usuarios";
    public final static String ID = "id";
    public final static String USERNAME = "username";
    public final static String ROL = "rol";
    public final static String PASSWORD = "password";


    public static List<ModUsuario> GetListaUsuarios(Context context){
        SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);
        List<ModUsuario> usersList = new ArrayList<>();
        while (c.moveToNext()){
            ModUsuario modUsuario = new ModUsuario();
            modUsuario.setIdUsuario(c.getInt(c.getColumnIndex(ID)));
            modUsuario.setUsuario(c.getString(c.getColumnIndex(USERNAME)));
            modUsuario.setUsuario(c.getString(c.getColumnIndex(ROL)));
            modUsuario.setPassword(c.getString(c.getColumnIndex(PASSWORD)));

            usersList.add(modUsuario);
        }
        db.close();
        return usersList;
    }

    public static boolean CrearUsuario(Context context, ModUsuario modUsuario){
        SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();

        ContentValues newItem = new ContentValues();
        // newItem.put("ID", ModUsuario.getIdUsuario());
        newItem.put("USERNAME", modUsuario.getUsuario());
        newItem.put("PASSWORD", modUsuario.getPassword());
        newItem.put("ROL", modUsuario.getRol());

        long inserted = db.insert(TABLE_NAME, null, newItem);
        db.close();
        return inserted > 0;
    }

    public static ModUsuario GetUsuarioXUsername(Context context, String username){
        SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();
        String[] where = new String[]{
                username
        };
        Cursor c = db.rawQuery("SELECT * FROM Usuarios WHERE username like(?) ",where);
        ModUsuario modUsuario = new ModUsuario();
        if (c.moveToFirst()){
            modUsuario.setIdUsuario(c.getInt(c.getColumnIndex(ID)));
            modUsuario.setUsuario(c.getString(c.getColumnIndex(USERNAME)));
            modUsuario.setPassword(c.getString(c.getColumnIndex(PASSWORD)));

        }
        db.close();
        return modUsuario;
    }

    public static int CapturarCantidadTotal(Context context) {
        SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT COUNT(" + ID+ ") FROM '" + TABLE_NAME + "'", null);
        ArrayList<String> ListaMovID = new ArrayList<String>();
        int cant = 0;
        if (c.moveToFirst()) {
            cant = c.getInt(0);
        }
        db.close();
        return cant;
    }

    public static ModUsuario GetUsuarioRol(Context context, String username){
        SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();
        String[] where = new String[]{
                username
        };
        Cursor c = db.rawQuery("SELECT * FROM Usuarios WHERE username like(?) ",where);
        ModUsuario modUsuario = new ModUsuario();
        if (c.moveToFirst()){
            modUsuario.setIdUsuario(c.getInt(c.getColumnIndex(ID)));
            modUsuario.setUsuario(c.getString(c.getColumnIndex(USERNAME)));
            modUsuario.setPassword(c.getString(c.getColumnIndex(PASSWORD)));
            modUsuario.setRol(c.getString(c.getColumnIndex(ROL)));
        }
        db.close();
        return modUsuario;
    }

    public static boolean BorrarUsuarios(Context context){

        try {
            SQLiteDatabase db = new sqlUsuarios(context).getWritableDatabase();
            db.execSQL("DELETE FROM '"+TABLE_NAME+"'");
            db.close();
            return true;
        } catch (SQLException e) {
            return false;
        }


    }


}
