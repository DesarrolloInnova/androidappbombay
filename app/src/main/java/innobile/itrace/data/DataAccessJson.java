package innobile.itrace.data;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import innobile.itrace.transport.IFA_activos;
import innobile.itraceandroid.GetDataAdapter;

/**
 * Created by Dev1 on 6/04/15.
 */
public class DataAccessJson {



    public static String createJson() throws JSONException {
        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put("valor 1", "Valor 1");
            jsonObject.put("valor 2", "Valor 2");

            jsonArray.put("lista1");
            jsonArray.put("lista2");

            jsonObject.put("array", jsonArray);
        } catch (JSONException jsonError) {

        }

        return null;
    }

    /**
     * Parsea un Json con los datos de un activo para la actividad de activos para convertirlo en un ArrayList<IFA_activos>
     *
     * @param jsonActivo
     * @return
     */
    public static ArrayList<IFA_activos> parserJsonActivo(String jsonActivo) throws Exception {
        ArrayList<IFA_activos> arrayListIFA_activos = new ArrayList<>();
        IFA_activos ifa_activos = null;
        try {
            //SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            //Date fecha = null;

            JSONArray jSONListado = null;
            JSONObject jSONActivo = new JSONObject(jsonActivo);

            ifa_activos = new IFA_activos();

            ifa_activos.setResult(jSONActivo.getString("result"));
            ifa_activos.setMessage(jSONActivo.getString("message"));


            if (jSONActivo.has("result")) {

                if (ifa_activos.getResult().equals("0")) {
                    arrayListIFA_activos.add(ifa_activos);
                    return arrayListIFA_activos;
                } else {

                    boolean tieneListados = jSONActivo.has("listados");

                    if (tieneListados) {

                        jSONListado = jSONActivo.getJSONArray("listados");

                        for (int i = 0; i < jSONListado.length(); i++) {
                            JSONObject c = jSONListado.getJSONObject(i);

                            ifa_activos.setAct_area(c.getString("area"));
                            ifa_activos.setAct_centro_costos(c.getString("centro_costos"));
                            ifa_activos.setAct_ciclo_vida(c.getString("ciclo_vida"));
                            ifa_activos.setAct_ciudad(c.getString("ciudad"));
                            ifa_activos.setAct_codigo_activo(c.getString("codigo_activo"));
                            ifa_activos.setAct_codigo_barras(c.getString("codigo_barras"));
                            ifa_activos.setAct_color(c.getString("color"));
                            ifa_activos.setAct_cuenta_contable(c.getString("cuenta_contable"));
                            ifa_activos.setAct_descripcion(c.getString("descripcion"));
                            ifa_activos.setAct_empresa_propietaria(c.getString("empresa_propietaria"));
                            ifa_activos.setAct_estado(c.getString("estado"));
                            ifa_activos.setAct_fabricante(c.getString("fabricante"));
                            //fecha = formatoDelTexto.parse(c.getString("fecha_compra"));
                            ifa_activos.setAct_fecha_compra(c.getString("fecha_compra"));
                            ifa_activos.setAct_grupo(c.getString("grupo"));
                            ifa_activos.setAct_mantenimiento(c.getString("mantenimiento"));
                            ifa_activos.setAct_material(c.getString("material"));
                            ifa_activos.setAct_meses_depreciacion(c.getInt("meses_depreciacion"));
                            ifa_activos.setAct_modelo(c.getString("modelo"));
                            ifa_activos.setAct_nombre_cuenta_contable(c.getString("nombre_cuenta_contable"));
                            ifa_activos.setAct_nombre_maquina(c.getString("nombre_maquina"));
                            ifa_activos.setAct_observaciones(c.getString("observaciones"));
                            ifa_activos.setAct_piso(c.getString("piso"));
                            ifa_activos.setAct_responsable(c.getString("cedula_responsable"));
                            ifa_activos.setAct_nombre_responsable(c.getString("nombre_responsable"));
                            ifa_activos.setAct_serial(c.getString("serial"));
                            ifa_activos.setAct_servidor(c.getString("servidor"));
                            ifa_activos.setAct_sistema_operativo(c.getString("sistema_operativo"));
                            ifa_activos.setAct_tag_activo(c.getString("tag_activo"));
                            ifa_activos.setAct_tipo_activo(c.getString("tipo_activo"));
                            ifa_activos.setAct_tipo_ubicacion(c.getString("tipo_ubicacion"));
                            ifa_activos.setAct_responsable(c.getString("cedula_responsable"));
                            ifa_activos.setAct_nombre_responsable(c.getString("nombre_responsable"));
                            ifa_activos.setAct_ubicacion(c.getString("ubicacion"));
                            ifa_activos.setAct_ultima_fecha_reporte_agente(c.getString("ultima_fecha_reporte_agente"));
                            ifa_activos.setAct_valor_compra(c.getDouble("valor_compra"));
                            ifa_activos.setAct_valor_iva_compra(c.getDouble("valor_iva_compra"));
                            ifa_activos.setAct_direccion_ubicacion(c.getString("direccion_ubicacion"));
                            ifa_activos.setAct_canal(c.getString("canal"));
                            ifa_activos.setAct_en_red(c.getString("en_red"));
                            ifa_activos.setAct_usuario_red(c.getString("usuario_red"));
                            ifa_activos.setAct_fecha_ingreso_bodega(c.getString("fecha_ingreso_bodega"));
                            ifa_activos.setAct_oc(c.getString("orden_compra"));
                            ifa_activos.setAct_service_pack(c.getString("service_pack"));
                            ifa_activos.setAct_procesador(c.getString("procesador"));
                            ifa_activos.setAct_velocidad(c.getString("velocidad"));
                            ifa_activos.setAct_disco_duro(c.getString("disco_duro"));
                            ifa_activos.setAct_total_memoria(c.getString("total_memoria"));
                            ifa_activos.setAct_codigo_conteo(c.getString("codigo_conteo"));

                            arrayListIFA_activos.add(ifa_activos);

                            ifa_activos = new IFA_activos();
                        }
                    } else {
                        arrayListIFA_activos.add(ifa_activos);
                    }
                }
            }
        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error al parsear los activos: " + jsonError.getMessage());
            throw  jsonError;
        }

        return arrayListIFA_activos;
    }

    /**
     * Parsea un Json con los datos de un activo para la actividad de activos para convertirlo en un IFA_activos
     *
     * @param jsonActivo
     * @return
     */
    public static IFA_activos parserJsonActivoRequest(String jsonActivo) throws Exception {
        IFA_activos ifa_activos = null;
        try {
            //SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            //Date fecha = null;

            JSONArray jSONListado = null;
            JSONObject jSONActivo = new JSONObject(jsonActivo);
            if (jSONActivo.has("codigo_activo")) {

                ifa_activos = new IFA_activos();

                ifa_activos.setAct_codigo_conteo(jSONActivo.getString("codigo_conteo"));
                ifa_activos.setAct_codigo_activo(jSONActivo.getString("codigo_activo"));
                ifa_activos.setAct_descripcion(jSONActivo.getString("descripcion"));
                ifa_activos.setAct_codigo_barras(jSONActivo.getString("codigo_barras"));
                //fecha = formatoDelTexto.parse(jSONActivo.getString("fecha_compra"));
                ifa_activos.setAct_fecha_compra(jSONActivo.getString("fecha_compra"));
                ifa_activos.setAct_meses_depreciacion(jSONActivo.getInt("meses_depreciacion"));
                ifa_activos.setAct_area(jSONActivo.getString("area"));
                ifa_activos.setAct_grupo(jSONActivo.getString("grupo"));
                ifa_activos.setAct_ciclo_vida(jSONActivo.getString("ciclo_vida"));
                ifa_activos.setAct_mantenimiento(jSONActivo.getString("mantenimiento"));
                ifa_activos.setAct_centro_costos(jSONActivo.getString("centro_costos"));
                ifa_activos.setAct_valor_compra(jSONActivo.getDouble("valor_compra"));
                ifa_activos.setAct_valor_iva_compra(jSONActivo.getDouble("valor_iva_compra"));
                ifa_activos.setAct_cuenta_contable(jSONActivo.getString("cuenta_contable"));
                ifa_activos.setAct_nombre_cuenta_contable(jSONActivo.getString("nombre_cuenta_contable"));
                ifa_activos.setAct_ciudad(jSONActivo.getString("ciudad"));
                ifa_activos.setAct_piso(jSONActivo.getString("piso"));
                ifa_activos.setAct_ubicacion(jSONActivo.getString("ubicacion"));
                ifa_activos.setAct_tipo_ubicacion(jSONActivo.getString("tipo_ubicacion"));
                ifa_activos.setAct_serial(jSONActivo.getString("serial"));
                ifa_activos.setAct_color(jSONActivo.getString("color"));
                ifa_activos.setAct_modelo(jSONActivo.getString("modelo"));
                ifa_activos.setAct_tipo_activo(jSONActivo.getString("tipo_activo"));
                ifa_activos.setAct_material(jSONActivo.getString("material"));
                ifa_activos.setAct_estado(jSONActivo.getString("estado"));
                ifa_activos.setAct_fabricante(jSONActivo.getString("fabricante"));
                ifa_activos.setAct_empresa_propietaria(jSONActivo.getString("empresa_propietaria"));
                ifa_activos.setAct_nombre_maquina(jSONActivo.getString("nombre_maquina"));
                ifa_activos.setAct_sistema_operativo(jSONActivo.getString("sistema_operativo"));
                ifa_activos.setAct_servidor(jSONActivo.getString("servidor"));
                ifa_activos.setAct_ultima_fecha_reporte_agente(jSONActivo.getString("ultima_fecha_reporte_agente"));
                ifa_activos.setAct_observaciones(jSONActivo.getString("observaciones"));
                ifa_activos.setAct_tag_activo(jSONActivo.getString("tag_activo"));
                ifa_activos.setAct_responsable(jSONActivo.getString("responsable"));
                ifa_activos.setAct_direccion_ubicacion(jSONActivo.getString("direccion_ubicacion"));
                ifa_activos.setAct_canal(jSONActivo.getString("canal"));
                ifa_activos.setAct_en_red(jSONActivo.getString("en_red"));
                ifa_activos.setAct_usuario_red(jSONActivo.getString("usuario_red"));
                ifa_activos.setAct_fecha_ingreso_bodega(jSONActivo.getString("fecha_ingreso_bodega"));
                ifa_activos.setAct_oc(jSONActivo.getString("orden_compra"));
                ifa_activos.setAct_service_pack(jSONActivo.getString("service_pack"));
                ifa_activos.setAct_procesador(jSONActivo.getString("procesador"));
                ifa_activos.setAct_velocidad(jSONActivo.getString("velocidad"));
                ifa_activos.setAct_disco_duro(jSONActivo.getString("disco_duro"));
                ifa_activos.setAct_total_memoria(jSONActivo.getString("total_memoria"));
            }
        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error al parsear el activo: " + jsonError.getMessage());
            throw jsonError;
        }

        return ifa_activos;
    }

    /**
     * Parsea un Json con los listados para la actividad de activos para convertirlo en un ArrayList
     *
     * @param jsonListadosactivos
     * @return
     */
    public static ArrayList<Object> parserJsonListadosActivos(String jsonListadosactivos) {
        ArrayList<Object> arrayListListadoActivos = null;
        ArrayList<String> arrayListListado = new ArrayList<String>();
        try {

            JSONArray jSONListado = null;
            JSONObject jSONListadoActivos = new JSONObject(jsonListadosactivos);
            if (jSONListadoActivos.has("result")) {

                arrayListListadoActivos = new ArrayList<Object>();

                arrayListListadoActivos.add(jSONListadoActivos.getString("result"));
                arrayListListadoActivos.add(jSONListadoActivos.getString("message"));

                jSONListado = jSONListadoActivos.getJSONArray("listados");

                for (int i = 0; i < jSONListado.length(); i++) {
                    JSONObject c = jSONListado.getJSONObject(i);

                    String codigo = c.getString("codigo");
                    String texto = c.getString("texto");
                    String valor = c.getString("valor");

                    arrayListListado.add(codigo + "~" + texto + "~" + valor);
                }

                arrayListListadoActivos.add(arrayListListado);
            }

        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error: " + jsonError.getMessage());
        }

        return arrayListListadoActivos;
    }

    /**
     * Parsea un Json que contiene el nombre del responsable consultado por la cédula
     *
     * @param jsonResponsable
     * @return
     */
    public static String parserJsonNombreResponsable(String jsonResponsable) throws ParseException {
        String nombreResponsable = "";
        try {
            JSONObject jSONResponsable = new JSONObject(jsonResponsable);

            if (jSONResponsable.has("nombre_responsable")) {
                nombreResponsable = jSONResponsable.getString("nombre_responsable");
            }
        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error al parsear el responsable: " + jsonError.getMessage());
        }

        return nombreResponsable;
    }

    /**
     * Parsea un Json con los datos de un reporte de inventario filtrado por area
     *
     * @param jsonReporteInventario
     * @return
     */
    public static ArrayList<String> parserJsonReporteInventario(String jsonReporteInventario) throws Exception {
        ArrayList<String> arrayListReporteInventario = new ArrayList<>();
        try {

            JSONArray jSONListado = null;
            JSONObject jSONActivo = new JSONObject(jsonReporteInventario);

            arrayListReporteInventario.add(jSONActivo.getString("result"));
            arrayListReporteInventario.add(jSONActivo.getString("message"));

            if (jSONActivo.has("result")) {

                boolean tieneReportes = jSONActivo.has("reportes");

                if (tieneReportes) {

                    jSONListado = jSONActivo.getJSONArray("reportes");

                    for (int i = 0; i < jSONListado.length(); i++) {
                        JSONObject c = jSONListado.getJSONObject(i);

                        arrayListReporteInventario.add("Cantidad de activos leídos" + "~" + String.valueOf(c.getInt("Cantidad de activos leídos")));
                        arrayListReporteInventario.add("Cantidad de activos no leídos" + "~" + String.valueOf(c.getInt("Cantidad de activos no leídos")));
                        //arrayListReporteInventario.add("Cantidad de activos leídos en diferente área" + "~" + String.valueOf(c.getInt("Cantidad de activos leídos en diferente área")));
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Error", "Ocurrió un error al parsear el reporte de activos: " + ex.getMessage());
            throw ex;
        }




        return arrayListReporteInventario;
    }

    /**
     * Parsea un Json con los datos de un activo para la actividad de activos para convertirlo en un IFA_activos
     *
     * @param jsonActivo
     * @return
     */
    public static ArrayList parserJsonRespuestaSetActivoRF(String jsonActivo) throws Exception {
        ArrayList<String> arrayListRespuesta = new ArrayList<>();
        try {
            //SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            //Date fecha = null;

            JSONArray jSONListado = null;
            JSONObject jSONActivo = new JSONObject(jsonActivo);
            if (jSONActivo.has("result")) {
                arrayListRespuesta.add(jSONActivo.getString("result"));
                arrayListRespuesta.add(jSONActivo.getString("message"));
            }
        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error al parsear el activo: " + jsonError.getMessage());
            throw jsonError;
        }

        return arrayListRespuesta;
    }

    /**
     * Parsea un Json para convertirlo en un ArrayList
     *
     * @param jsonValidateUser
     * @return
     */
    public static ArrayList<String> JSONValidarUsuario(String jsonValidateUser) {
        ArrayList<String> arrayListValidateUser = null;
        try {

            //creo Objeto Json para entrar al Output
            JSONObject JOutput = null;

            //Creo Objeto Json para traer Datos devueltos por el API
            JSONObject JSONUsuario = new JSONObject(jsonValidateUser);

            //Valido que exita el request output


            if (JSONUsuario.has("user")) {

                arrayListValidateUser = new ArrayList<String>();

                JOutput  = JSONUsuario.getJSONObject("user");


                arrayListValidateUser.add(JOutput.optString("Respuesta"));
                arrayListValidateUser.add(JOutput.optString("Mensaje"));
                arrayListValidateUser.add(JOutput.getJSONObject("Empresa").optString("_id"));
                arrayListValidateUser.add(JOutput.getJSONObject("Empresa").optString("Tipo_Producto"));
                arrayListValidateUser.add(JOutput.optString("Usuario"));
            }

            /*
            arrayListValidateUser = new ArrayList<String>();

            if (JSONUsuario.has("output")) {



                JOutput  = JSONUsuario.getJSONObject("output");

                arrayListValidateUser.add(JOutput.optString("Respuesta"));
                arrayListValidateUser.add(JOutput.optString("Mensaje"));
            }*/else{
                arrayListValidateUser.add(JOutput.optString(""));
            }


        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error: " + jsonError.getMessage());
        }

        return arrayListValidateUser;


    }


    public static List<GetDataAdapter> JSONBuscarMovimientos(String jsonValidateUser) {
        List<GetDataAdapter> ListaMovimientos = new ArrayList<>();
        //List<GetDataAdapter> ListaMovimientos = null;
        try {

            ListaMovimientos.clear();
            JSONArray jSONListado = null;


            //Creo Objeto Json para traer Datos devueltos por el API
            JSONObject JSONMovimientos = new JSONObject(jsonValidateUser);

            //Busco el Array Recordset.
            jSONListado  = JSONMovimientos.getJSONArray("recordset");

            //recorro los datos del Array y los almaceno en la lista
            for (int i = 0; i < jSONListado.length(); i++) {
                JSONObject c = jSONListado.getJSONObject(i);

                GetDataAdapter requisicionData = new GetDataAdapter();
                requisicionData.setCodigo(c.getString("Nro_Dcto"));
                requisicionData.setFecha(c.getString("Fecha"));
                ListaMovimientos.add(requisicionData);

            }


        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error: " + jsonError.getMessage());
        }

        return ListaMovimientos;


    }

    public static ArrayList<String> parserJsonValidateUserOriginal(String jsonValidateUser) {
        ArrayList<String> arrayListValidateUser = null;
        try {

            JSONObject jSONUsuario = new JSONObject(jsonValidateUser);

            if (jSONUsuario.has("result")) {

                arrayListValidateUser = new ArrayList<String>();

                arrayListValidateUser.add(jSONUsuario.getString("result"));
                arrayListValidateUser.add(jSONUsuario.getString("message"));
            }


        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error: " + jsonError.getMessage());
        }

        return arrayListValidateUser;
    }

    public static ArrayList<String> parserJsonValidateUserModelo(String jsonValidateUser) {
        ArrayList<String> arrayListValidateUser = null;
        try {
            JSONArray jSONListado = null;
            //JSONArray jsonArray = new JSONArray(jsonValidateUser);
            //JSONObject mJsonObject = jsonArray.getJSONObject(0);
            JSONObject mJsonObject = new JSONObject(jsonValidateUser);
            arrayListValidateUser = new ArrayList<String>();

            /*
            jSONListado  = mJsonObject.getJSONArray("recordset");


            for (int i = 0; i < jSONListado.length(); i++) {
                JSONObject c = jSONListado.getJSONObject(i);

                String codigo = c.optString("respuesta");
                String texto = c.optString("mensaje");


                arrayListValidateUser.add(codigo + "~" + texto );
            }
            */
            JSONObject mJsonObject1 = null;
            mJsonObject1  = mJsonObject.getJSONObject("output");
            String codigo =mJsonObject1 .optString("Respuesta");
            String texto = mJsonObject1 .optString("Mensaje");
            arrayListValidateUser.add(codigo + "~" + texto );

        } catch (JSONException jsonError) {
            Log.e("Error", "Ocurrió un error: " + jsonError.getMessage());
        }

        return arrayListValidateUser;


    }
}
