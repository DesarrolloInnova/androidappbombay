package innobile.itrace.data;

/**
 * Clase contenedora de contantes
 */
public class Cons {

    public static String USUARIO_LOGUEADO = "";

    public static final String RESPONSE_FAIL = "0";
    public static final String RESPONSE_SUCCESS = "1";

    //CONFIGURACIONES
    public static final int CONFIGURACION_URL = 1;
    public static String Usuario = "";
    public static String  Empresa = "";
    public static  String Tipo_Producto = "";
    public static String id_Usuario = "";

    //CONEXION
    public static final String LOCAL = "1";
    public static final String NUBE = "2";
    public static final String SERVER = "3";
}
