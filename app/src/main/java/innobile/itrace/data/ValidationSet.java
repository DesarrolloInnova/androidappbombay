package innobile.itrace.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dev1 on 6/04/15.
 */
public final class ValidationSet {
    // Elementos no permitidos en contraseñas
    private final static int[] ELEMENTS_NOT_ALLOWED_PASSWORD = {241, 209}; /* Ñ , ñ */

    // Elementos no permitidos strings
    private final static String[] ELEMENTS_NOT_ALLOWED = {"\"", "'"}; /* " */

    // Validación del abecedario
    private final static int INIT_ELEMENT = 97; /* LETRA a */

    private final static int FINAL_ELEMENT = 122; /* LETRA z */

    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Verifica que la cadena sea un email válido
     *
     * @param email String que representa el email
     * @return Verdadero si corresponde a un email válido, falso de lo contrario
     */
    public final static boolean validateEmail(String email) {

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Verifica que el caracter sea número
     *
     * @param caracter Caracter que representa un número
     * @return verdadero si es un número, falso de lo contrario
     */
    public final static boolean isNumber(char caracter) {
        if (caracter >= 48 && caracter <= 57) {
            return true;
        }
        return false;
    }

    /**
     * Verifica si una cadena es un número
     *
     * @param cadena
     * @return
     */
    public final static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Verifica que la cadena pueda representar dinero
     *
     * @param money Cadena que representa dinero
     * @return Verdadero si puede representar dinero, falso de lo contrario
     */
    public final static boolean isMoney(String money) {
        try {
            // produce una excepcion si no es valida la conversion
            Float aux = new Float(money);
            // si pasa eso significa que es valido el formato
            if (aux == null) {
                return false;
            }
            if (aux.floatValue() <= 0) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Verifica que pueda utilizarse como clave
     *
     * @param password String que representa una clave
     * @return verdadero si puede utilizarse como clave, falso de lo contrario
     */
    public final static boolean validatePassword(String password) {

        for (int i = 0; i < password.length(); i++) {
            if (!(password.charAt(i) >= INIT_ELEMENT
                    && password.charAt(i) <= FINAL_ELEMENT
                    && isAllowChar(password.charAt(i))
                    || isNumber(password.charAt(i)))) {

                return false;
            }
        }
        return true;
    }

    /**
     * Verifica que el caracter esté permitido
     *
     * @param caracter Caracter a evaluar
     * @return Verdadero si es permitido, falso de lo contrario
     */
    private static final boolean isAllowChar(char caracter) {
        for (int i = 0; i < ELEMENTS_NOT_ALLOWED_PASSWORD.length; i++) {
            if (ELEMENTS_NOT_ALLOWED_PASSWORD[i] == caracter) {
                return false;
            }
        }
        return true;
    }

    /**
     * Elimina los acentos de un texto
     * @param cadena
     * @return
     */
    public static final String clearAccent(String cadena) {

        return cadena.replace("Á", "A").replace("É", "E").replace("Í", "I").replace("Ó", "O")
                .replace("Ú", "U").replace("á", "a").replace("é", "e").replace("í", "i")
                .replace("ó", "o").replace("ú", "u");
    }

    /**
     * Limpia una cadena de caracteres no deseables
     *
     * @param cadena
     * @return cadena sin caracteres no deseables
     */
    public static final String clearString(String cadena) {
        for (int i = 0; i < ELEMENTS_NOT_ALLOWED.length; i++) {
            cadena = cadena.replace(ELEMENTS_NOT_ALLOWED[i], "");
        }

        return cadena;
    }

    /**
     * Evalua si la cadena tiene una longitud en el rango descrito.
     *
     * @param word Cadena a evaluar
     * @param min Longitud mínima
     * @param max Longitud m xima
     * @return Verdadero si es está dentro del rango, falso de lo contrario
     */
    public static final boolean betweenLength(String word, int min, int max) {
        if (word.length() >= min && word.length() <= max) {
            return true;
        }

        return false;
    }

    /**
     * Elimina los caracteres de salto de línea de un string
     *
     * @param textPlain Cadena
     * @return Cadena modificada
     */
    public static final String nl2br(String textPlain) {

        if (textPlain == null) {  //1
            return null;          //2
        }

        StringBuffer cadenaSalida = new StringBuffer(); //3

        for (int i = 0; i < textPlain.length(); i++) {  //4
            char c = textPlain.charAt(i);     //5
            if (c == 13) {                    //6
                cadenaSalida.append("<br>");   //7
            } else {
                cadenaSalida.append(c);        //8
            }
            //9
        }
        for (int i = 0; i < cadenaSalida.length(); i++) {   //10
            char c = cadenaSalida.charAt(i);                //11
            if (c == 13 | c == '\n') {                      //12
                cadenaSalida.deleteCharAt(i);               //13
            }
            //14
        }

        return new String(cadenaSalida);                      //15

    }

    /**
     * Elimina cualquier etiqueta html reemplazandola por otro valor. Ej. < por
     * &lt;
     * @p
     *
     * aram textPlain
     * @return Devuelve cadena modificada
     */
    public final static String clearHTML(String textPlain) {

        if (textPlain == null) {
            return null;
        }

        StringBuffer cadenaSalida = new StringBuffer();

        for (int i = 0; i < textPlain.length(); i++) {
            char c = textPlain.charAt(i);
            if (c == 13) {
                cadenaSalida.append("<br>");
            } else if (c == '<') {
                cadenaSalida.append("&lt;");
            } else if (c == '>') {
                cadenaSalida.append("&gt;");
            } else if (c == '\n') {
                cadenaSalida.append("\n<br>");
            } else {
                cadenaSalida.append(c);
            }
        }
        return new String(cadenaSalida);
    }

    /**
     * Verifica que la cadena pueda considerarse como un nombre de usuario
     * correcto
     *
     * @param login Cadena a evaluar
     * @return Verdadero si cumple requisitos, falso de lo contrario
     */
    public static boolean isLogin(String login) {
        for (int i = 0; i < login.length(); i++) {
            if (!(login.charAt(i) >= INIT_ELEMENT
                    && login.charAt(i) <= FINAL_ELEMENT
                    && isAllowChar(login.charAt(i))
                    || isNumber(login.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Dado dos cadenas y un delimitador, devuelve una nueva cadena con el valor
     * de los dos strings que se entregaron como parámetros separados por una
     * delimitador.
     *
     * @param s1 Primer Cadena
     * @param delim Delimitador de separación
     * @param s2 Segunda cadena
     */
    public static String addWithDelim(String s1, String delim, String s2) {
        if (s1.equals("")) {
            return s2;
        } else {
            return s1 + delim + s2;
        }
    }

    /**
     * Arregla un valor a partir de un ancho con el complemento descrito. La
     * cadena de complemento se inserta en el inicio (parte izquierda) del valor
     *
     * @param valor Cadena a ser modificada
     * @param ancho Ancho fijo que debe tener el valor
     * @param complemento Cadena complemento a ser insertada en el valor
     *
     * @return Cadena de ancho fijo con caracter de complemento
     */
    public static String fixFieldToLengthLeft(String valor, int ancho, String complemento) {
        String valorTmp = "";
        if (ancho < 0) {
            return valor;
        }
        if (valor.length() > ancho) {
            valor = valor.substring(valor.length() - ancho, valor.length());
        } else {
            int restante = ancho - valor.length();
            for (int i = 1; i <= restante; i++) {
                valorTmp = complemento + valorTmp;
            }
        }
        return valorTmp + valor;
    }

    /**
     * Arregla un valor a partir de un ancho con el complemento descrito. La
     * cadena de complemento se inserta al final (parte derecha) del valor
     *
     * @param valor Cadena a ser modificada
     * @param ancho Ancho fijo que debe tener el valor
     * @param complemento Cadena complemento a ser insertada en el valor
     *
     * @return Cadena de ancho fijo con caracter de complemento
     */
    public static String fixFieldToLengthRight(String valor, int ancho, String complemento) {
        String valorTmp = "";
        if (ancho < 0) {
            return valor;
        }
        if (valor.length() > ancho) {
            valor = valor.substring(ancho);
        } else {
            int restante = ancho - valor.length();
            for (int i = 1; i <= restante; i++) {
                valorTmp += complemento;
            }
        }
        return valor + valorTmp;
    }
}
