package innobile.itrace.data;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Dev1 on 6/04/15.
 */
public final class DateOperations {

    private DateOperations() {
    }

    /**
     * Devuelve un string con la fecha actual
     *
     * @return fecha en formato DDMMAAAA
     */
    public static String getDate() {
        Calendar fecha = Calendar.getInstance();
        return String.valueOf(fecha.get(Calendar.DAY_OF_MONTH)) + String.valueOf(fecha.get(Calendar.MONTH) + 1) + String.valueOf(fecha.get(Calendar.YEAR));
    }

    /**
     * Devuelve un objeto tipo calendar con la fecha actual
     *
     * @return fecha actual
     */
    public static Calendar getCalendar() {
        Calendar fecha = Calendar.getInstance();
        return fecha;
    }

    /**
     * Devuelve un string con la fecha actual
     *
     * @return fecha en formato AAAAMMDD
     */
    public static String getDateAAAAMMDD() {
        Calendar fecha = Calendar.getInstance();
        return String.valueOf(String.valueOf(fecha.get(Calendar.YEAR) + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.MONTH) + 1), 2, "0") + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.DATE)), 2, "0")));
    }

    /**
     * Devuelve un string con la fecha enviada como parámetro
     *
     * @return fecha en formato AAAAMMDD
     */
    public static String getDateAAAAMMDD(Date date) {
        Calendar fecha = Calendar.getInstance();
        fecha.setTime(date);
        return String.valueOf(String.valueOf(fecha.get(Calendar.YEAR) + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.MONTH) + 1), 2, "0") + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.DATE)), 2, "0")));
    }

    /**
     * Devuelve un string con la fecha actual
     *
     * @param separator
     * @return fecha en formato AAAAMMDD
     */
    public static String getDateAAAAMMDD(String separator) {
        Calendar fecha = Calendar.getInstance();
        return String.valueOf(String.valueOf(fecha.get(Calendar.YEAR) + separator + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.MONTH) + 1), 2, "0") + separator + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.DATE)), 2, "0")));
    }

    /**
     * Devuelve un string con la fecha actual
     *
     * @return fecha en formato AAAAMMDD
     */
    public static String getSlashedDateAAAAMMDD() {
        Calendar fecha = Calendar.getInstance();
        return String.valueOf(String.valueOf(fecha.get(Calendar.YEAR) + "/" + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.MONTH) + 1), 2, "0") + "/" + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.DATE)), 2, "0")));
    }

    /**
     * Devuelve un string con la fecha actual en formato DD/MM/AAAA
     *
     * @return fecha
     */
    public static String getSlashedDate() {
        Calendar fecha = Calendar.getInstance();
        return ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.DATE)), 2, "0") + "/" + ValidationSet.fixFieldToLengthLeft(String.valueOf(fecha.get(Calendar.MONTH) + 1), 2, "0") + "/" + String.valueOf(fecha.get(Calendar.YEAR));
    }

    /**
     * Returns a copy of the date passed as a parameter with the
     * <code>Calendar.MILLISECOND</code> field set to 0.
     */
    public final static Calendar getDateWithoutMilliSeconds(Calendar date) {
        /*
         * ***WARNING***
         *
         * An alternative implementation of this method could be:
         *
         * Calendar dateWithoutMilliSeconds = date.clone();
         * dateWithoutMilliSeconds.set(Calendar.MILLISECOND, 0); return
         * dateWithoutMilliSeconds;
         *
         * However, if the date passed as a parameter comes from a remote
         * invocation (RMI, for example), the clone gets corrupted after the
         * invocation of the "set" method. Incredibly, but true !!!
         */

        Calendar dateWithoutMilliSeconds = Calendar.getInstance();

        dateWithoutMilliSeconds.setTime(date.getTime());
        dateWithoutMilliSeconds.set(Calendar.MILLISECOND, 0);

        return dateWithoutMilliSeconds;

    }

    /**
     * Devulve los dias en el mes
     *
     * @param mes Mes
     * @param anio Año
     * @return dias del mes, 0 si el mes o año es inválido
     */
    public static int getDaysInMonth(int mes, int anio) {

        Calendar calendar = Calendar.getInstance();

        if (mes > 0 && mes < 13) {
            calendar.set(Calendar.YEAR, anio);
            calendar.set(Calendar.MONTH, mes - 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            // Obtener la cantidad máxima de dias
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        } else {
            return 0;
        }

    }

    /**
     * Verifica que los parámetros correspondan a una fecha
     *
     * @param dia Dia
     * @param mes Mes
     * @param anio Año
     * @return Verdadero si es una fecha correcta, falso de lo contrario
     */
    public static boolean isDate(int dia, int mes, int anio) {
        Calendar calendar = Calendar.getInstance();

        if (mes > 0 && mes < 13) {
            // Obtener la cantidad máxima de dias

            if (dia > 0 && dia <= getDaysInMonth(mes, anio)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Devulve el calendario dado los parámetros
     *
     * @param dia Dia
     * @param mes Mes
     * @param anio Año
     * @param startDate Falso si es al final del dia
     * @return Objeto calendario, nulo si no es una fecha válida
     */
    public static Calendar getCalendar(int dia, int mes, int anio,
                                       boolean startDate) {

        Calendar calendar = Calendar.getInstance();

        if (isDate(dia, mes + 1, anio)) {

            calendar.clear();
            // Configura el dia del mes
            calendar.set(Calendar.DAY_OF_MONTH, dia);
            // Configura el mes
            calendar.set(Calendar.MONTH, mes);
            // Configurar el año
            calendar.set(Calendar.YEAR, anio);

            // Si es al fial del dia.
            if (!startDate) {
                calendar.set(Calendar.HOUR, 11);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                calendar.set(Calendar.AM_PM, Calendar.PM);
            }
            return calendar;

        } else {
            return null;
        }
    }

    /**
     * Obtiene la hora del sistema en formato HH:MM:SS
     *
     * @return hora del sistema
     */
    public static String getHora() {
        Calendar calendar = Calendar.getInstance();
        int hora = calendar.get(Calendar.HOUR_OF_DAY);
        int minutos = calendar.get(Calendar.MINUTE);
        int segundos = calendar.get(Calendar.SECOND);
        return String.valueOf(hora + ":" + minutos + ":" + segundos);

    }
}
