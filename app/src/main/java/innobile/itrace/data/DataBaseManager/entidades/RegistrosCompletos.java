package innobile.itrace.data.DataBaseManager.entidades;

public class RegistrosCompletos {
    private String json;

    public RegistrosCompletos(String json) {
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
