package innobile.itrace.data.DataBaseManager.entidades;

public class SwitchEstado {
    private int Estado;

    public SwitchEstado(int estado) {
        Estado = estado;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int estado) {
        Estado = estado;
    }
}
