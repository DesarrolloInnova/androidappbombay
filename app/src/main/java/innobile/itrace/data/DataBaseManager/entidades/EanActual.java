package innobile.itrace.data.DataBaseManager.entidades;

public class EanActual {
    private int id;
    private String EanLeido;

    public EanActual(int id, String eanLeido) {
        this.id = id;
        EanLeido = eanLeido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEanLeido() {
        return EanLeido;
    }

    public void setEanLeido(String eanLeido) {
        EanLeido = eanLeido;
    }
}
