package innobile.itrace.data.DataBaseManager.entidades;

public class Login {
    String Username;
    String password;
    String Datetime;

    public Login(String username, String password, String datetime) {
        Username = username;
        this.password = password;
        Datetime = datetime;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }
}
