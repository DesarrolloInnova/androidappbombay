package innobile.itrace.data.DataBaseManager.entidades;

import java.util.Date;

public class Registros {
    private Integer _id;
    private String id;
    private String referencia;
    private String descripcion;
    private int cantidad;
    private String Datetime;

    public Registros(Integer _id, String id, String referencia, String descripcion, int cantidad, String Datetime) {
        this._id = _id;
        this.id = id;
        this.referencia = referencia;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.Datetime = Datetime;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
