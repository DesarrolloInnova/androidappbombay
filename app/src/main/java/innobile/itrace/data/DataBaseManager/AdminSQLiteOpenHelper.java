package innobile.itrace.data.DataBaseManager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    public AdminSQLiteOpenHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, nombre, factory, version);

    }

    @Override

    public void onCreate(SQLiteDatabase db) {

        //aquí creamos la tabla de usuario (dni, nombre, ciudad, numero)
        db.execSQL("create table usuario(ean integer primary key, referencia text, descripcion text)");
        db.execSQL("create table contador(id integer primary key, contador integer)");

    }

    @Override

    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

        db.execSQL("drop table if exists usuario");
        db.execSQL("drop table if exists contador");

        db.execSQL("create table usuario(ean integer primary key, referencia text, descripcion text, contador integer)");
        db.execSQL("create table contador(id integer primary key, contador integer)");

    }

}
