package innobile.itrace.data.DataBaseManager.utilidades;

public class Utilidades {
    //Primera tabla
    public static String TABLA_REGISTRO = "registros";
    public static String CAMPO_PRIMARYID = "_id";
    public static String CAMPO_ID = "id";
    public static String CAMPO_REFERENCIA = "referencia";
    public static String CAMPO_DESCRIPCION = "descripcion";
    public static String CAMPO_CANTIDAD = "Cantidad";
    public static String CAMPO_FECHA = "Datetime";

    //segunda tabla
    public static String TABLA_OBTENERDATOS = "registroscompletos";
    public static String JSON = "json";


    public static String TABLA_ESTADO = "estado";
    public static String ESTADO_ID = "Estado_id";
    public static String CAMPO_ESTADO = "estadoactual";
    //Para evitar datos duplicados establecer id como clave primaria


    //Tabla  de usuarios
    public static String TABLA_USUARIO = "Usuarios";
    public static String CAMPO_USERNAME = "Username";
    public static String CAMPO_PASSWORD = "password";
    public static String CAMPO_DATATIME = "Datetime";


    //Tabla codigo desde Scanner
    public static String TABLA_LEIDO_SCANNER = "Tabla_Scanner";
    public static String CAMPO_SCAMMER_ID = "id";
    public static String CAMPO_EAN_SCANNER = "eanLeido";


    //Tabla RequisicionData
    public static String REQUISICION_TABLA = "RequisicionData";
    public static String REQUISICION_DATA_EAN = "ean";
    public static String REQUISICION_DATA_CANTIDAD = "cantidad";
    public static String REQUISICION_DATA_OBSERVACIONES = "observaciones";
    public static String REQUISICION_DATA_DOCUMENTO = "nr_documento";
    public static String REQUISICION_DATA_FECHA = "fecha";

    //Tabla encabezado_Requisiciones
    public static String TABLA_ENCABEZADO = "tabla_encabezado";
    public static String NUMERO_DOCUMENTO = "Numero_documento";
    public static String TIPO_DOCUMENTO = "codigo_documento";
    public static String PRIORIDAD = "prioridad";
    public static String CENTRAL_COSTO = "central_costo";
    public static String RESPONSABLE = "responsable";
    public static String BODEGAORIGEN = "bodega_Origen";
    public static String BODEGADESTINO = "bodega_Destino";


    //Tabla donde se guardan los productos obtenidos desde mongodb
    public static String TABLA_PRODUCTOS = "tabla_productos";
    public static String CODIGO_PRODUCTO = "Codigo";
    public static String DESCRIPCION_PRODUCTO = "Descripcion";


    //Tabla de movimientos donde se van a guardar los productos leidos
    public static String TABLA_MOVIMIENTO = "tabla_movimientos";
    public static String mCODIGO_PRODUCTO = "Codigo";
    public static String mDESCRIPCION_PRODUCTO = "Descripcion";
    public static String mCANTIDAD_PRODUCTO = "Cantidad";
    public static String mResponsable = "Responsable";


    //Crear tabla datos traslados
    public static String TABLA_ENCABEZADO_TRASLADOS = "guardar_traslados";
    public static String tNUMERO_DOCUMENTO = "n_documento";
    public static String tBODEGA_ORIGEN = "bodega_origen";
    public static String tBODEGA_DESTINO = "bodega_destino";
    public static String tRESPONSABLE = "Responsable";
    public static String tPLACA = "tplaca";


    //Crear tabla que se encarga de guardar el inventario de Activos fijos
    public static String TABLA_INVENTARIOAF = "tabla_inventarioAF";
    public static String afCODIGO_PRODUCTO = "codigo_producto";
    public static String afNUMERO_DOCUMENTO = "numero_documento";
    public static String afCODIGO_BODEGA = "codigo_bodega";
    public static String afDESCRIPCION = "descripcion";
    public static String afMARCA = "marca";
    public static String afGRUPO = "grupo";
    public static String afCANTIDAD_LEIDA = "cantidad_leida";
    public static String afFECHA = "fecha";


    //Crear tabla que va a contener las bodegas
    public static String TABLA_VBODEGAS = "bodegas";
    public static String CODIGO_BODEGA = "codigo_bodega";
    public static String NOMBRE_BODEGA = "nombre_bodega";
    public static String ESTADO = "estado_datos";


    //Crear tabla de productos
    public static String TABLA_VPRODUCTOS = "vproductos_tabla";
    public static String CODIGO_SKU = "sku";
    public static String VCODIGO_BARRAS = "v_codigob";
    public static String VDESCRIPCION_PRODUCTO = "v_descripcion";


    //Crear tabla leidos inventario
    public static String vTABLA_INVENTARIO = "v_inventario";
    public static String vNUMERO_DOCUMENTO = "numero_documento";
    public static String vCODIGO_BARRAS = "codigo_barras";
    public static String vCODIGO_SKU_LEIDO = "codigo_sku";
    public static String vFECHA_LECTURA = "fecha_lectura";
    public static String vCANTIDAD = "cantidad";
    public static String vTIPO_LECTURA = "tipo_lectura";
    public static String vNOMBRE_BODEGA = "bodega";
    public static String vCodigo_Bodega = "codigo_bodega";
    public static String vEstado = "estado";
    public static String vDESCRIPCION = "v_descripcion";
    public static String vID = "id";
    public static String vESTANTE_INVENTARIO = "estante_Inventario";

    //Crear tabla maestros
    public static String TABLA_MAESTRO = "tabla_maestros";
    public static String maestroCODIGO_BODEGA = "mCodigobodega";
    public static String maestroSkuInventario = "sku_inventario";
    public static String maestroCantidadInventario = "cantidad_inventario";
    public static String maestroBODEGA_ASOCIADA = "bodega_asociada";

    //Crear tabla para el detalle y el còdigo de la maquinaria
    public static String TABLA_DETALLES = "tabla_detalle";
    public static String DETALLE_MAQUINAS = "detalle_maquinas";

    //Crear tabla para guardar los datos de Retina
    public static String TABLA_RETINA  = "tabla_retina";
    public static String SqUBICACIONES = "ubicaciones";
    public static String SqBODEGA = "bodega";
    public static String SqREFERENCIA = "referencnia";
    public static String SqEXTENSION = "extension";
    public static String SqSERIAL = "serial";
    public static String SqVENCIMIENTO = "vencimiento";
    public static String SqCANTIDAD = "cantidad";
    public static String SqLOTE = "lote";

    //Crear tabla bodegas
    public static String TABLA_BODEGAS_ID = "tabla_bodegas_id";
    public static String bodegaNombre = "nombre_bodega";
    public static String  bodegaID = "id_bodega";

    //Crear tabla pedidos
    public static String NUMERO_PEDIDO = "Num_Pedido";
    public static String NIT_CLIENTE = "nit_cliente";
    public static String COD_PRODUCTO = "cod_producto";


    //Estructura de la tabla de retina para guardar el detalle
    public static String TABLARETINADETALLES = "retDetalles";
    public static String retinaUBICACIONES = "ubicaciones";
    public static String retinaBODEGA = "bodega";
    public static String retinaREFERENCIA = "referencnia";
    public static String retinaEXTENSION = "extension";
    public static String retinaSERIAL = "serial";
    public static String retinaVENCIMIENTO = "vencimiento";
    public static String retinaCANTIDAD = "cantidad";
    public static String retinaLOTE = "lote";
    public static String retinaId = "_id";

    //Estructura de la tabla para guardar los datos de la cedula
    public static String TABLA_CEDULAS= "Cedulas_leidas";
    public static String NUMERO_CEDULA ="numero_cedula";
    public static String NOMBRE_COMPLETO = "nombre_completo";
    public static String FECHA_NACIMIENTO = "fecha_nacimiento";
    public static String CORREO_ELECTRONICO = "correo_electronico";
    public static String NUMERO_CELULAR = "numero_celular";
    public static String FECHA_LECTURA = "fecha_lectura";
    public static String USUARIO_INGRESO = "usuario_ingreso";
    public static String id = "id_diamante";


    //Estructura para guardar la ubicacion de las bodegas
    public static String TABLA_UBICACIONES= "tabla_ubicaciones";
    public static String UBICACIONES_RETINA= "ubicacion_retina";




    public static final String CREAR_TABLA_REGISTROS = "CREATE TABLE " + TABLA_REGISTRO + " (" + CAMPO_PRIMARYID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CAMPO_ID + " TEXT," + CAMPO_REFERENCIA + " TEXT, " + CAMPO_DESCRIPCION + " TEXT," + CAMPO_CANTIDAD + "," + CAMPO_FECHA + " DATETIME)";
    public static final String CREAR_TABLA_REGISTROSCOMPLETOS = "CREATE TABLE " + TABLA_OBTENERDATOS + "(" + JSON + " TEXT)";
    public static final String CREAR_TABLA_ESTADO = "CREATE TABLE " + TABLA_ESTADO + "(" + CAMPO_ESTADO + " TEXT)";
    public static final String CREAR_TABLA_USUARIOS = "CREATE TABLE " + TABLA_USUARIO + "(" + CAMPO_USERNAME + " TEXT," + CAMPO_PASSWORD + " TEXT" + "," + CAMPO_DATATIME + " DATETIME" + ")";
    public static final String CREAR_TABLA_SCANNER = "CREATE TABLE " + TABLA_LEIDO_SCANNER + "(" + CAMPO_SCAMMER_ID + " INTEGER," + CAMPO_EAN_SCANNER + " TEXT)";
    public static final String CREAR_TABLA_REQUISICION = "CREATE TABLE " + REQUISICION_TABLA + "(" + REQUISICION_DATA_EAN + " TEXT, " + REQUISICION_DATA_CANTIDAD + " INTEGER, " + REQUISICION_DATA_OBSERVACIONES + " TEXT, " + REQUISICION_DATA_DOCUMENTO + " INTEGER, " + REQUISICION_DATA_FECHA + " DATETIME)";
    public static final String CREAR_TABLA_ENCABEZADO = "CREATE TABLE " + TABLA_ENCABEZADO + "(" + TIPO_DOCUMENTO + " TEXT, " + NUMERO_DOCUMENTO + " INTEGER, " + PRIORIDAD + " TEXT," + CENTRAL_COSTO + " TEXT," + RESPONSABLE + " TEXT," + BODEGAORIGEN + " TEXT," + BODEGADESTINO + " TEXT)";
    public static final String CREAR_TABLA_PRODUCTO = "CREATE TABLE " + TABLA_PRODUCTOS + "(" + CODIGO_PRODUCTO + " TEXT, " + DESCRIPCION_PRODUCTO + " TEXT)";
    public static final String CREAR_TABLA_MOVIMIENTOS = "CREATE TABLE " + TABLA_MOVIMIENTO + "(" + mCODIGO_PRODUCTO + " TEXT, " + mDESCRIPCION_PRODUCTO + " TEXT, " + mCANTIDAD_PRODUCTO + " TEXT," + mResponsable + " TEXT, DATETIME DEFAULT CURRENT_TIMESTAMP)";
    public static final String CREAR_TABLA_TRASLADOS = "CREATE TABLE " + TABLA_ENCABEZADO_TRASLADOS + "(" + tNUMERO_DOCUMENTO + " TEXT, " + tBODEGA_ORIGEN + " TEXT, " + tBODEGA_DESTINO + " TEXT," + tRESPONSABLE + " TEXT, " + tPLACA + " TEXT)";
    public static final String CREAR_TABLA_INVENTARIAF = "CREATE TABLE " + TABLA_INVENTARIOAF + "(" + afCODIGO_PRODUCTO + " TEXT," + afNUMERO_DOCUMENTO + " TEXT, " + afCODIGO_BODEGA + " TEXT, " + afDESCRIPCION + " TEXT," + afMARCA + " TEXT," + afGRUPO + " TEXT," + afCANTIDAD_LEIDA + " TEXT," + afFECHA + " TEXT)";
    public static final String CREAR_TABLA_BODEGAS = "CREATE TABLE " + TABLA_VBODEGAS + "(" + CODIGO_BODEGA + " TEXT," + NOMBRE_BODEGA + " TEXT)";
    public static final String CREAR_TABLA_VPRODUCTOS = "CREATE TABLE " + TABLA_VPRODUCTOS + "(" + CODIGO_SKU + " TEXT," + VCODIGO_BARRAS + " TEXT, " + VDESCRIPCION_PRODUCTO + " TEXT);";
    public static final String CREAR_TABLA_VINVENTARIO = "CREATE TABLE " + vTABLA_INVENTARIO + "(" + vNUMERO_DOCUMENTO + " INTEGER," + vCODIGO_BARRAS + " TEXT," + vCODIGO_SKU_LEIDO + " TEXT," + vFECHA_LECTURA + " TEXT," + vCANTIDAD + " INTEGER," + vTIPO_LECTURA + " TEXT," + vNOMBRE_BODEGA + " TEXT," + vCodigo_Bodega + " TEXT," + vEstado + " TEXT DEFAULT 'IN', " + vDESCRIPCION + " TEXT, " + vID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + vESTANTE_INVENTARIO + ")";
    public static final String CREAR_TABLA_MAESTRO = "CREATE TABLE " + TABLA_MAESTRO + "(" + maestroCODIGO_BODEGA + " TEXT," + maestroSkuInventario + " TEXT," + maestroCantidadInventario + " INTEGER," + maestroBODEGA_ASOCIADA + " TEXT)";
    public static final String CREAR_TABLA_DETALLES_MAQUINAS = "CREATE TABLE " + TABLA_DETALLES + "(" + DETALLE_MAQUINAS + " TEXT)";
    public static final String CREAR_TABLA_BODEGA_ID = "CREATE TABLE " + TABLA_BODEGAS_ID + "(" + bodegaNombre + " TEXT, "  + bodegaID + " TEXT)";
    public static final String CREAR_TABLA_CEDULAS = "CREATE TABLE " + TABLA_CEDULAS + "(" + NUMERO_CEDULA + " TEXT," + NOMBRE_COMPLETO + " TEXT," + FECHA_NACIMIENTO + " TEXT," + CORREO_ELECTRONICO + " TEXT," + NUMERO_CELULAR + " TEXT," + FECHA_LECTURA +  " TEXT," + USUARIO_INGRESO + " TEXT," + id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL)";
    public static final String CREAR_TABLA_UBICACIONES = "CREATE TABLE " + TABLA_UBICACIONES + "(" + UBICACIONES_RETINA + " TEXT)";
    public static final String CREAR_TABLA_RETINA = "CREATE TABLE " + TABLA_RETINA + "(" + SqUBICACIONES  + " TEXT," +SqBODEGA + " TEXT," + SqREFERENCIA + " TEXT,"+ SqEXTENSION + " TEXT," + SqSERIAL +  " TEXT," + SqVENCIMIENTO + " TEXT," + SqCANTIDAD + " TEXT, " +  SqLOTE + " TEXT)" ;
    public static final String CREAR_TABLA_DETALLE_RETINA = "CREATE TABLE " + TABLARETINADETALLES + "(" + retinaUBICACIONES + " TEXT," + retinaBODEGA + " TEXT," + retinaREFERENCIA + " TEXT," + retinaEXTENSION + " TEXT," +  retinaSERIAL + " TEXT, " + retinaVENCIMIENTO + " TEXT," + retinaCANTIDAD + " TEXT, " + retinaLOTE + " TEXT," +  retinaId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL)";
}

