package innobile.itrace.data.DataBaseManager.utilidades;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;

public class SQLite_User extends SQLiteOpenHelper {

    String consulta4  = Utilidades.CREAR_TABLA_USUARIOS;
    private static final int DATABASE_VERSION = +3;


    public SQLite_User(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(consulta4);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+Utilidades.TABLA_USUARIO);
        db.execSQL(consulta4);
        onCreate(db);
    }





}
