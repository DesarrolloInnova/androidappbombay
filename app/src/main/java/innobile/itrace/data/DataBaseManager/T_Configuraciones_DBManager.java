package innobile.itrace.data.DataBaseManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import innobile.itrace.data.DbHelper;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;

import java.util.ArrayList;

/**
 * Created by Dev1 on 7/04/15.
 */
public class T_Configuraciones_DBManager {

    //Nombre de la tabla
    public static final String TABLE_T_CONFIGURACIONES = "T_Configuraciones";

    //Nombre de los campos
    public static final String con_consecutivo = "con_consecutivo";
    public static final String con_url = "con_url";
    public static final String con_url_Mongo = "con_url_Mongo";
    public static final String con_TipoImpresion = "con_TipoInpresion";
    public static final String con_TipoConexion = "con_TipoConexion";
    public static final String con_Usuario = "con_Usuario";

    //Sentencia SQL para crear una tabla
    public static final String CREATE_TABLE_T_CONFIGURACIONES = "create table " + TABLE_T_CONFIGURACIONES + " ("
            + con_consecutivo + " integer primary key,"
            + con_TipoImpresion + " integer  not null,"
            + con_TipoConexion + " text  null,"
            + con_url + " text not null,"
            + con_url_Mongo + " text  null,"
            + con_Usuario + " text  null);";


    private DbHelper helper;
    private SQLiteDatabase db;

    public T_Configuraciones_DBManager(Context context) {
        //Se crea una instancia de la clase DbHelper y se le pasa como argumento el contexto.
        helper = new DbHelper(context, CREATE_TABLE_T_CONFIGURACIONES);
        //Si la base de datos no existe getWritableDatabase crea la base de datos y la devuelve en modo de escritura,
        //si ya existe solamente la devuelve.
        db = helper.getWritableDatabase();
    }

    /**
     * Consulta una configuración filtrada por el consecutivo en una base de datos SQLite
     *
     * @param consecutivo
     * @return
     */
    public T_Configuraciones ConsultarConfiguracion(int consecutivo) {
        T_Configuraciones t_configuraciones = new T_Configuraciones();
        try {
            //Se configura un vector con los parámetros de las columnas que va a mostrar de la tabla SQLite
            String columnas[] = {con_consecutivo, con_TipoImpresion,con_TipoConexion,con_url,con_url_Mongo,con_Usuario};
            //Los datos de la consulta se devuelven de un Cursor
            Cursor c = db.query(TABLE_T_CONFIGURACIONES, columnas, con_consecutivo + "=?", new String[]{String.valueOf(consecutivo)}, null, null, null);
            if (c.moveToFirst()) {//Valida que el cursor tenga al menos un dato

                do {
                    t_configuraciones.setCon_consecutivo(c.getInt(0));
                    t_configuraciones.setCon_TipoImpresion(Integer.parseInt(c.getString(1)));
                    t_configuraciones.setCon_TipoConexion(c.getString(2));
                    //t_configuraciones.setCon_TipoConexion(Boolean.parseBoolean(c.getString(2)));
                     // t_configuraciones.setCon_TipoInpresion(c.getString(1));
                     // t_configuraciones.setCon_TipoConexion(c.getString(2));
                    t_configuraciones.setCon_URL(c.getString(3));
                    t_configuraciones.setCon_URLMongo(c.getString(4));
                        t_configuraciones.setCon_Usuario(c.getString(5));
                }
                while (c.moveToNext());
            }
        } catch (Exception ex) {
            throw ex;
        }
        return t_configuraciones;
    }

    /**
     * Consulta un listado de configuraciones en una base de datos SQLite
     *
     * @return
     */
    public ArrayList<T_Configuraciones> ConsultarConfiguraciones() {
        ArrayList<T_Configuraciones> arrayListT_Configuraciones = new ArrayList<>();
        try {
            //Se configura un vector con los parámetros de las columnas que va a mostrar de la tabla SQLite
            String columnas[] =  {con_consecutivo, con_TipoImpresion,con_TipoConexion,con_url,con_url_Mongo,con_Usuario};
            //Los datos de la consulta se devuelven de un Cursor
            Cursor c = db.query(TABLE_T_CONFIGURACIONES, columnas, null, null, null, null, null);
            if (c.moveToFirst()) {//Valida que el cursor tenga al menos un dato

                do {
                    T_Configuraciones t_configuraciones = new T_Configuraciones();
                    t_configuraciones.setCon_consecutivo(c.getInt(0));
                    t_configuraciones.setCon_TipoImpresion(Integer.parseInt(c.getString(1)));
                    t_configuraciones.setCon_TipoConexion(c.getString(2));
                    //t_configuraciones.setCon_TipoConexion(Boolean.parseBoolean(c.getString(2)));
                    // t_configuraciones.setCon_TipoInpresion(c.getString(1));
                    // t_configuraciones.setCon_TipoConexion(c.getString(2));
                    t_configuraciones.setCon_URL(c.getString(3));
                    t_configuraciones.setCon_URLMongo(c.getString(4));
                    t_configuraciones.setCon_Usuario(c.getString(5));

                    arrayListT_Configuraciones.add(t_configuraciones);
                }
                while (c.moveToNext());

            }
        } catch (Exception ex) {
            throw ex;
        }
        return arrayListT_Configuraciones;
    }

    /**
     * Contenedor de valores
     *
     * @param t_Configuraciones
     * @return
     */
    private ContentValues GenerarContentValues(T_Configuraciones t_Configuraciones) {
        //Contenedor de valores
        ContentValues valores = new ContentValues();
        try {
            valores.put(con_consecutivo, t_Configuraciones.getCon_consecutivo());
            valores.put(con_TipoImpresion, t_Configuraciones.getCon_TipoImpresion());
            valores.put(con_TipoConexion, t_Configuraciones.getCon_TipoConexion());
            valores.put(con_url, t_Configuraciones.getCon_URL());
            valores.put(con_url_Mongo, t_Configuraciones.getCon_URLMongo());
            valores.put(con_Usuario, t_Configuraciones.getCon_Usuario());

        } catch (Exception ex) {
            throw ex;
        }

        return valores;
    }

    /**
     * Inserta un contenedor de valores en una base de datos SQLite.(el insert es proporcionado por android)
     *
     * @param t_Configuraciones
     */
    public void ActualizarConfiguraciones(T_Configuraciones t_Configuraciones) {
        try {
            ArrayList<T_Configuraciones> arrayListT_Configuraciones = ConsultarConfiguraciones();
            if (arrayListT_Configuraciones.isEmpty()) {
                db.insert(TABLE_T_CONFIGURACIONES, null, GenerarContentValues(t_Configuraciones));

            } else {
                int resultado = db.update(TABLE_T_CONFIGURACIONES, GenerarContentValues(t_Configuraciones), con_consecutivo + "=?", new String[]{String.valueOf(Cons.CONFIGURACION_URL)});
                int x = 0;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }



}
