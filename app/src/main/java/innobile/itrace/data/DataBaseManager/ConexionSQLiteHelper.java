package innobile.itrace.data.DataBaseManager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {

    String consulta = Utilidades.CREAR_TABLA_REGISTROS;
    String consulta2 = Utilidades.CREAR_TABLA_REGISTROSCOMPLETOS;
    String consulta3 = Utilidades.CREAR_TABLA_ESTADO;
    String consulta5 = Utilidades.CREAR_TABLA_SCANNER;
    String consulta6 = Utilidades.CREAR_TABLA_REQUISICION;
    String consulta7 = Utilidades.CREAR_TABLA_ENCABEZADO;
    String crear_tabla_productos = Utilidades.CREAR_TABLA_PRODUCTO;
    String crear_tabla_movimientos = Utilidades.CREAR_TABLA_MOVIMIENTOS;
    String crear_tabla_traslados = Utilidades.CREAR_TABLA_TRASLADOS;
    String crear_tabla_inventarioaf = Utilidades.CREAR_TABLA_INVENTARIAF;
    String crear_tabla_bodegas = Utilidades.CREAR_TABLA_BODEGAS;
    String crear_tabla_vproductos = Utilidades.CREAR_TABLA_VPRODUCTOS;
    String crear_tabla_vinventario = Utilidades.CREAR_TABLA_VINVENTARIO;
    String crear_tabla_maestros = Utilidades.CREAR_TABLA_MAESTRO;
    String crear_tabla_detalles = Utilidades.CREAR_TABLA_DETALLES_MAQUINAS;
    String crear_tabla_retina = Utilidades.CREAR_TABLA_RETINA;
    String crear_tabla_bodega_id = Utilidades.CREAR_TABLA_BODEGA_ID;
     String retina_detalles = Utilidades.CREAR_TABLA_DETALLE_RETINA;
     String crear_tabla_cedulas = Utilidades.CREAR_TABLA_CEDULAS;
     String crear_tabla_ubicaciones = Utilidades.CREAR_TABLA_UBICACIONES;


    private static final int DATABASE_VERSION = +3;


    public ConexionSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(consulta);
        db.execSQL(consulta2);
        db.execSQL(consulta3);
        db.execSQL(consulta5);
        db.execSQL(consulta6);
        db.execSQL(consulta7);
        db.execSQL(crear_tabla_productos);
        db.execSQL(crear_tabla_movimientos);
        db.execSQL(crear_tabla_traslados);
        db.execSQL(crear_tabla_inventarioaf);
        db.execSQL(crear_tabla_bodegas);
        db.execSQL(crear_tabla_vproductos);
        db.execSQL(crear_tabla_vinventario);
        db.execSQL(crear_tabla_maestros);
        db.execSQL(crear_tabla_detalles);
        db.execSQL(crear_tabla_retina);
        db.execSQL(crear_tabla_bodega_id);
        db.execSQL(retina_detalles);
        db.execSQL(crear_tabla_cedulas);
        db.execSQL(crear_tabla_ubicaciones);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_REGISTRO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_OBTENERDATOS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_ESTADO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_LEIDO_SCANNER);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.REQUISICION_TABLA);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_ENCABEZADO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_PRODUCTOS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_MOVIMIENTO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_ENCABEZADO_TRASLADOS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_INVENTARIOAF);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_VBODEGAS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_VPRODUCTOS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.vTABLA_INVENTARIO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_MAESTRO);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_DETALLES);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_RETINA);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_BODEGAS_ID);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLARETINADETALLES);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_CEDULAS);
        db.execSQL("DROP TABLE IF EXISTS " + Utilidades.TABLA_UBICACIONES);

        db.execSQL(consulta);
        db.execSQL(consulta2);
        db.execSQL(consulta3);
        db.execSQL(consulta5);
        db.execSQL(consulta6);
        db.execSQL(consulta7);
        db.execSQL(crear_tabla_productos);
        db.execSQL(crear_tabla_movimientos);
        db.execSQL(crear_tabla_traslados);
        db.execSQL(crear_tabla_inventarioaf);
        db.execSQL(crear_tabla_bodegas);
        db.execSQL(crear_tabla_vproductos);
        db.execSQL(crear_tabla_vinventario);
        db.execSQL(crear_tabla_maestros);
        db.execSQL(crear_tabla_detalles);
        db.execSQL(crear_tabla_retina);
        db.execSQL(crear_tabla_bodega_id);
        db.execSQL(retina_detalles);
        db.execSQL(crear_tabla_cedulas);
        db.execSQL(crear_tabla_ubicaciones);
        onCreate(db);
    }
}
