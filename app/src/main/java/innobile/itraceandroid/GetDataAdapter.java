package innobile.itraceandroid;

import java.util.ArrayList;

/**
 * Created by JUNED on 6/16/2016.
 */
public class GetDataAdapter {

    public GetDataAdapter(String codigo, String fecha) {
        this.codigo = codigo;
        this.fecha = fecha;
    }

    String codigo;

    public GetDataAdapter() {

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    String fecha;


}