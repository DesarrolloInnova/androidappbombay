package innobile.itraceandroid.TrasladosTM;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.CrearEstructuraMovimiento;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.RetrofitAdapter.Bodega;
import innobile.itraceandroid.RetrofitAdapter.Detalle;
import innobile.itraceandroid.RetrofitAdapter.EquipoSten;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.Maquinas;
import innobile.itraceandroid.RetrofitAdapter.Producto_Existencia;
import innobile.itraceandroid.RetrofitAdapter.SuccessResponse;
import innobile.itraceandroid.RetrofitAdapter.restar_cantidad;
import innobile.itraceandroid.Variables_Globales;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;

public class Crear_TrasladoAC extends AppCompatActivity {

    public static Retrofit retrofit;
    private static Cursor c;
    public String BASE_URL = "https://webapi.innova-id.com/";
    public JsonPlaceHolderApi ourDataSet;
    private String V_URL_MON = "", Ruta = "";
    private TextView tDescripcion, tMarca, tGrupo, mNro_Documento;
    private AutoCompleteTextView tCodigoLeido;
    private ImageButton ibtnBuscar;
    private Button btnDetalle, btnProcesar, btnSincronizarDetalle;
    private EditText tObservaciones, tCantidad;
    private int comprobarEnvioDatos = 0;
    private RequestQueue requestQueue;
    private ContentValues contentValues;
    private ConexionSQLiteHelper conn;
    private SQLiteDatabase db;
    private Long cn = 0L;
    private String[] lstMaquinasDescripcion = new String[]{};
    private List<String> datos = new ArrayList<String>();
    private String Cantidad = "1";
    private CheckBox check_Cantidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear__traslado_ac);

        //Se conectan los elementos con la parte de la vista
        tDescripcion = findViewById(R.id.tNombre);
        tMarca = findViewById(R.id.tCiudad);
        tGrupo = findViewById(R.id.tGrupo);
        tCodigoLeido = findViewById(R.id.eCodigoIngresado);
        ibtnBuscar = findViewById(R.id.ibtnBuscar);
        btnDetalle = findViewById(R.id.btnDetalle);
        btnProcesar = findViewById(R.id.btnGuardar);
        btnSincronizarDetalle = findViewById(R.id.btnSincronizarDetalles);
        mNro_Documento = findViewById(R.id.mNro_documento);
        tObservaciones = findViewById(R.id.tObservaciones);
        tCantidad = findViewById(R.id.eCantidad);
        check_Cantidad = findViewById(R.id.check_Cantidad);

        //Cargo un dato vacio en el código leido para evitar problemas con null
        tCodigoLeido.setText("");

        //Enviando el número del documento
        mNro_Documento.setText(Global.CODIGO_DOCUMENTO);

        //Definiendo las varaibles de conexión del API
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(new NoCache(), network);
        // Start the queue
        requestQueue.start();


        btnSincronizarDetalle.setOnClickListener(view -> {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
            db = conn.getWritableDatabase();
            db.delete(Utilidades.TABLA_DETALLES, null, null);
            db.close();
            sendNetworkRequest();
        });

        sendNetworkRequest();


        //Recupero el id de la bodega origen y de la bodega destino
        codigoBodegaOrigen();

        if (cantidaRegistrosMaquinas() > 0) {
            cargarSqliteDetalles();
            ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(Crear_TrasladoAC.this, android.R.layout.simple_spinner_dropdown_item, datos);
            tCodigoLeido.setAdapter(adapterBodegas);
        }


        //Este botón se encarga de buscar la información y traer los datos desde el API
        ibtnBuscar.setOnClickListener(v -> {
            //Se encarga de consultar los datos del producto, recibe la URL y el código del producto a buscar
            String[] separandoCodigo = tCodigoLeido.getText().toString().split(" ; ");
            String codigo = separandoCodigo[0];
            if (separandoCodigo[0] == null) {
                codigo = tCodigoLeido.getText().toString();
            }
            consultarProducto(V_URL_MON, codigo);
        });


        MostrarMaquinaSten();
        ActualizarBodegasCantidad();

        //Se encarga de buscar la información y traer los datos desde el API automáticamente
        tCodigoLeido.setOnKeyListener((view, i, keyEvent) -> {
            //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
            if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                //Este contador se encarga de enviar los datos solo una vez
                comprobarEnvioDatos++;
                if (comprobarEnvioDatos == 2) {

                    if (tCodigoLeido.getText().toString().length() > 0) {
                        //Se obtiene solo el primer elemento leido desde el código QR
                        String[] separandoCodigo = tCodigoLeido.getText().toString().split(" ");
                        String codigo = separandoCodigo[0];
                        //Se encarga de consultar los datos del producto, recibe la URL y el código del producto a buscar
                        consultarProducto(V_URL_MON, codigo);
                    } else {
                        //Controla cuando no se ha ingresado ningún código
                        Toast.makeText(getApplicationContext(), "Es necesario ingresar un código", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            }
            return false;
        });


        //Botón que se encarga de cargar la vista de detalle
        btnDetalle.setOnClickListener(view -> {
            //Este intent se encarga de carga la vista de detalles al presionar el botón detalle
            Intent intent = new Intent(Crear_TrasladoAC.this, Detalle_Traslados.class);
            intent.putExtra("codigo", mNro_Documento.getText().toString());
            startActivity(intent);
        });


        //Botón que se encarga de procesar el traslado y actualizar la bodega en el API
        btnProcesar.setOnClickListener(view -> {
            try {
                cambiarEstado();
                actualizarObservaciones();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        });
    }


    /**
     * Este método se encarga de consultar los datos del código ingresado
     * Autor: CarlosDnl
     * Fecha: 24/01/2020
     *
     * @param V_URL_MON    Url a la que se va a realizar la petición de los datos (Se modifica desde configuraciones)
     * @param codigoBuscar Es el código leido por la terminar o ingresado por el usuario
     */


    public void consultarProducto(String V_URL_MON, final String codigoBuscar) {

        //Se crea la URL a la cuál se va a realizar la consulta
        Ruta = V_URL_MON + "/api/ActivosFijos/list?valor=" + codigoBuscar;

        //Compronar si se guarda con cantidad
        if (check_Cantidad.isChecked()) {
            Cantidad = tCantidad.getText().toString();

        } else if (!check_Cantidad.isChecked()) {
            Cantidad = "1";
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                jsonArray -> {

                    JSONObject jsonObject = null;
                    try {

                        //Se recorre el json y se almacenan los datos en variables globales
                        jsonObject = jsonArray.getJSONObject(0);
                        DatosProductos.acRESERVADO = jsonObject.getString("Reservado");
                        DatosProductos.acID_PRODUCTO = jsonObject.getString("_id");
                        DatosProductos.acID_EMPRESA = jsonObject.getJSONObject("empresas").getString("_id");
                        DatosProductos.acCODIGO = jsonObject.getString("Codigo");
                        DatosProductos.acDESCRIPCION = jsonObject.getString("Descripcion");
                        if (jsonObject.has("Marca")) {
                            DatosProductos.acMARCA = jsonObject.getString("Marca");
                        } else {
                            DatosProductos.acMARCA = "Sin marca";
                        }
                        DatosProductos.acGRUPO = jsonObject.getString("Grupo");
                        DatosProductos.acBODEGAS = jsonObject.getJSONObject("bodegas").getString("_id");

                        if (!Boolean.parseBoolean(DatosProductos.acRESERVADO)) {
                            //Guardando los datos del activo fijo que fue leido
                            if (DatosProductos.ID_ORIGEN.equals(DatosProductos.acBODEGAS)) {
                                try {
                                    guardarProducto();
                                    actualizarReserva();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //Se cargan los datos en la vista
                                cargarDatos();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                                alertDialog.setTitle("Mensaje");
                                alertDialog.setMessage(getString(R.string.bodegaDatoAlert));
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        (dialog, which) -> dialog.dismiss());
                                alertDialog.show();
                            }


                        } else {
                            AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                            alertDialog.setTitle("Mensaje");
                            alertDialog.setMessage(getString(R.string.trasladoDatoAlert));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    (dialog, which) -> dialog.dismiss());
                            alertDialog.show();
                        }


                    } catch (JSONException e) {
                        AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                        alertDialog.setTitle("Mensaje");
                        alertDialog.setMessage(e.toString());
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                (dialog, which) -> dialog.dismiss());
                        alertDialog.show();
                    }
                },
                volleyError -> Toast.makeText(Crear_TrasladoAC.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
    }


    /**
     * Este método se encarga de enviar los datos obtenidos desde el API a los TextView
     * Autor: CarlosDnl
     * Fecha: 24/01/2020
     */

    public void cargarDatos() {
        tDescripcion.setText(DatosProductos.acDESCRIPCION);
        tMarca.setText(DatosProductos.acMARCA);
        //tGrupo.setText( DatosProductos.acGRUPO);
        tGrupo.setText(DatosProductos.acGRUPO);
        comprobarEnvioDatos = 0;
        tCodigoLeido.setText("");
    }


    public String codigoBodegaOrigen() {

        String Ruta = null;
        Ruta = V_URL_MON + "/api/bodega/list?valor=" + URLEncoder.encode(Variables_Globales.BODEGA_ORIGEN);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                jsonArray -> {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            DatosProductos.ID_ORIGEN = jsonObject.getString("_id");
                        } catch (JSONException e) {
                            Toast.makeText(Crear_TrasladoAC.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    codigoBodegaDestino();
                },
                volleyError -> Toast.makeText(Crear_TrasladoAC.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
        return DatosProductos.ID_ORIGEN;
    }


    public void codigoBodegaDestino() {

        String Ruta = null;

        //URL para realizar la peticion
        Ruta = V_URL_MON + "/api/bodega/list?valor=" + URLEncoder.encode(Variables_Globales.BODEGA_DESTINO);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                jsonArray -> {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            DatosProductos.ID_DESTINO = jsonObject.getString("_id");
                        } catch (JSONException e) {
                            Toast.makeText(Crear_TrasladoAC.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    try {
                        if (CrearEstructuraMovimiento.CREAR_ESTRUCTURA) {
                            crearEstructuraMovimientos();
                            CrearEstructuraMovimiento.CREAR_ESTRUCTURA = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                volleyError -> Toast.makeText(Crear_TrasladoAC.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);

    }


    private void crearEstructuraMovimientos() throws JSONException {

        String Ruta = null;

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        JSONObject movimiento = new JSONObject();

        movimiento.put("usuarios", Cons.id_Usuario);
        movimiento.put("empresas", Cons.Empresa);
        movimiento.put("Tipo_Documento", Cons.Tipo_Producto);
        movimiento.put("Numero_Documento", mNro_Documento.getText().toString());
        movimiento.put("Bodega_Origen", DatosProductos.ID_ORIGEN);
        movimiento.put("Bodega_Destino", DatosProductos.ID_DESTINO);
        movimiento.put("Responsable", Variables_Globales.RESPONSABLE);
        movimiento.put("Placa_Vehiculo", Variables_Globales.PLACA_VEHICULO);
        movimiento.put("Estado_Documento", "IN");
        movimiento.put("Observaciones", tObservaciones.getText().toString());
        movimiento.put("Fecha_Creacion", formatter.format(date));
        Ruta = V_URL_MON + "/api/movimiento/guardar";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());
        requestQueue.add(request);


    }


    public void sendNetworkRequest() {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
        db = conn.getWritableDatabase();
        if (cantidaRegistrosMaquinas() == 0) {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
            Call<List<Maquinas>> call = jsonPlaceHolderApi.getMaquinas();

            call.enqueue(new Callback<List<Maquinas>>() {
                @Override
                public void onResponse(Call<List<Maquinas>> call, retrofit2.Response<List<Maquinas>> response) {
                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                        return;
                    }

                    //Se limpian los datos para evitar registros duplicados
                    datos.clear();
                    List<Maquinas> maquina = response.body();
                    //Reinicio de la base de datos

                    db.beginTransactionNonExclusive();
                    try {
                        for (Maquinas maquinas : maquina) {
                            contentValues = new ContentValues();
                            contentValues.put(Utilidades.DETALLE_MAQUINAS, maquinas.getCodigo() + " ; " + maquinas.getDescripcion());
                            db.insert(Utilidades.TABLA_DETALLES, null, contentValues);
                        }
                        db.setTransactionSuccessful();
                    } finally {
                        db.endTransaction();
                    }
                    db.close();
                    cargarSqliteDetalles();
                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(Crear_TrasladoAC.this, android.R.layout.simple_spinner_dropdown_item, datos);
                    tCodigoLeido.setAdapter(adapterBodegas);

                    Toasty.success(getApplicationContext(), "Se insertaron " + cantidaRegistrosMaquinas() + " registros", Toasty.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<List<Maquinas>> call, Throwable t) {

                }
            });
        }

    }


    public void guardarProducto() throws JSONException {


        String Ruta = null;

        JSONObject nuevoProducto = new JSONObject();
        nuevoProducto.put("Numero_Documento", mNro_Documento.getText().toString());
        nuevoProducto.put("productos", DatosProductos.acID_PRODUCTO);
        nuevoProducto.put("Cantidad", Cantidad);


/*
        JSONObject nuevoProducto = new JSONObject();
        nuevoProducto.put("Numero_Documento_Afectado", "2348");
        nuevoProducto.put("Numero_Documento",Global.CODIGO_DOCUMENTO);
        nuevo.put("productos",DatosProductos.pID);
        Detalles.put("Cantidad", txtCantidad.getText().toString());
        // Detalles.put("Precio",  Variables_Globales_Movimientos.Precio);
        // Detalles.put("Unidad_Medida", Variables_Globales_Movimientos.Unidad_Medida);*/

        Ruta = V_URL_MON + "/api/Movimiento/actualizarMovimiento";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, nuevoProducto, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
    }

    public void actualizarReserva() throws JSONException {

        String Ruta = null;


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", DatosProductos.acID_PRODUCTO);
        jsonObject.put("V_Reservado", "true");

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/ActivosFijos/actualizaReserva";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);

    }


    private void datosProcesar() {

        String Ruta = null;
        Ruta = V_URL_MON + "/api/movimiento/mostrarTraslados?valor=" + mNro_Documento.getText().toString();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, Ruta, null,
                jsonArray -> {

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jsonArray.getJSONObject(0);
                        JSONArray array = jsonObject.getJSONArray("Detalles");
                        for (int j = 0; j < array.length(); ++j) {
                            JSONObject jsonDetalle = array.getJSONObject(j);
                            String id_producto = jsonDetalle.getJSONObject("productos").getString("_id");
                            procesar(id_producto);
                        }

                        AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                        alertDialog.setTitle("Mensaje");
                        alertDialog.setMessage(getString(R.string.procesarDatoAlert));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                volleyError -> Toast.makeText(Crear_TrasladoAC.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);

    }

    private void procesar(String producto_Id) throws JSONException {
        String Ruta = null;

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", producto_Id);
        jsonObject.put("nueva_bodega", DatosProductos.ID_DESTINO);

        //Si el checkbox está seleccionado guarda el dato con cantidad
        if (check_Cantidad.isChecked()) {
            Cantidad = tCantidad.getText().toString();
            //restarCantidadSalidaBodega(producto_Id, Cantidad, DatosProductos.ID_ORIGEN, DatosProductos.ID_DESTINO);
            //ActualizarBodegasCantidad(producto_Id, DatosProductos.ID_ORIGEN, Cantidad);
        } else if (!check_Cantidad.isChecked()) {


        }


        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/ActivosFijos/procesar";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());
        requestQueue.add(request);
    }

    private void restarCantidadSalidaBodega(String _id, String cantidad, String BodegaOrigen, String bodegaDestino) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://webapi.innova-id.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<restar_cantidad> call = jsonPlaceHolderApi.postRestarCantidad(_id, cantidad);

        call.enqueue(new Callback<restar_cantidad>() {
            @Override
            public void onResponse(Call<restar_cantidad> call, retrofit2.Response<restar_cantidad> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }
                restar_cantidad restar = response.body();
                Toasty.success(getApplicationContext(), restar.getCorrecto(), Toasty.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<restar_cantidad> call, Throwable t) {
                Toast.makeText(getApplicationContext(), String.valueOf(t), Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void MostrarMaquinaSten() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.1.83:4000/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<EquipoSten>> call = jsonPlaceHolderApi.getEquipoEten();

        call.enqueue(new Callback<List<EquipoSten>>() {
            @Override
            public void onResponse(Call<List<EquipoSten>> call, retrofit2.Response<List<EquipoSten>> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }

                for (EquipoSten equipoSten: response.body()) {
                    for(Detalle detalle : equipoSten.getDetalles()){
                      Toast.makeText(getApplicationContext(),detalle.getBodega().getNombre(), Toast.LENGTH_SHORT ).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<EquipoSten>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), String.valueOf(t) , Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void ActualizarBodegasCantidad() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.1.83:4000/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<SuccessResponse> call = jsonPlaceHolderApi.updateBodegas("5ef37f8beabd680b3c48715f", "5ed100e316d9fd0e0548bc81", "19000000");

        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, retrofit2.Response<SuccessResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }
                SuccessResponse sResponse = response.body();
                Toasty.success(getApplicationContext(), sResponse.getResponse(), Toasty.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), String.valueOf(t) , Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cambiarEstado() throws JSONException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String Ruta = null;
        JSONObject movimiento = new JSONObject();

        movimiento.put("Numero_Documento", mNro_Documento.getText().toString());
        movimiento.put("Fecha_Procesado", formatter.format(date));

        Ruta = V_URL_MON + "/api/movimiento/cambiarEstado";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_TrasladoAC.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            (dialog, which) -> dialog.dismiss());
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());
        datosProcesar();
        requestQueue.add(request);

    }

    /**
     * Se encarga de actualizar las observaciones en la estructura del movimiento
     */

    public void actualizarObservaciones() throws JSONException {

        String Ruta = null;

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Numero_Documento", mNro_Documento.getText().toString());
        jsonObject.put("Observaciones", tObservaciones.getText().toString());

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/movimiento/actualizarObservaciones";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray

            }
        }, error -> Toast.makeText(Crear_TrasladoAC.this, error.toString(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);

    }

    private Long cantidaRegistrosMaquinas() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_DETALLES);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    private String[] cargarSqliteDetalles() {
        //Se limpian los datos para evitar registros duplicados
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_DETALLES;//Set name of your table
        c = db.rawQuery("SELECT * FROM  " + myTable, null);
        if (c.moveToFirst()) {
            do {
                String detalleMaquina = c.getString(0);
                datos.add(detalleMaquina);
            } while (c.moveToNext());
        }
        //cargarBodegasLista();
        return lstMaquinasDescripcion;
    }


}
