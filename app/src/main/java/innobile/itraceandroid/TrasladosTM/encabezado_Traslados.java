package innobile.itraceandroid.TrasladosTM;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.CrearEstructuraMovimiento;
import innobile.itraceandroid.DefinirRuta;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.TipoConexion;
import innobile.itraceandroid.Variables_Globales;

public class encabezado_Traslados extends AppCompatActivity {

    private AutoCompleteTextView CentroCostos, Responsable, BodegaOrigen, BodegaDestino;
    private ImageView ImageCentralCosto, ImagenResponsable, ImagenBodegaOrigen, ImagenBodegaDestino;
    private String[] lstCentroCostos = new String[]{"Sin datos"};
    private String[] lstResponsable = new String[]{"Sin datos"};
    private String[] lstBodegaOrigen = new String[]{"Sin datos"};
    private String[] lstBodegaDestino = new String[]{"Sin datos"};
    private Button btnDetalle;
    private TextView Nro_Docto;
    private String Cod_Empresa = "01";
    private String Tipo_Docto = "";
    private Boolean band = true, ComprobarData;
    private TextView fecha;
    Date date = new Date();
    private String V_URL, V_URL_MON = null;
    private String comprobarConsulta = "";
    private String V_TipoConexion = null;
    private RequestQueue requestQueue;
    private Button probando;
    private Boolean running = true;
    private int cont = 0;
    private EditText placa_Vehiculo;


    private String descripcionBodega = "", nombreResponsable = "", nombreCentralCosto = "";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_traslados);
        BodegaOrigen = findViewById(R.id.autoCompleteBodegaOrigen);
        BodegaDestino = findViewById(R.id.autoCompleteBodegaDestino);
        CentroCostos = findViewById(R.id.autoCompleteCentroCosto);
        Responsable = findViewById(R.id.autoCompleteResponsable);
        ImagenBodegaOrigen = findViewById(R.id.ImagenBodegaOrigen);
        ImagenBodegaDestino = findViewById(R.id.ImagenBodegaDestino);
        ImageCentralCosto = findViewById(R.id.ImagenCentralCosto);
        ImagenResponsable = findViewById(R.id.ImagenResponsable);
        Nro_Docto = findViewById(R.id.txtNroDocto);
        btnDetalle = findViewById(R.id.Buttondetalle);
        //  CentroCostos.setThreshold(2);
        //   CentroCostos.setThreshold(1);
        fecha = findViewById(R.id.MostrarFecha);
        placa_Vehiculo = findViewById(R.id.placa_vehiculo);


        //Se encarga de validar la creación de la estructura de la tabla movimientos
        CrearEstructuraMovimiento.CREAR_ESTRUCTURA = true;


        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024); // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(new NoCache(), network);
        // Start the queue
        requestQueue.start();


        //Asignando valor a placa vehiculo para evitar datos null
        placa_Vehiculo.setText("");

        //Definiendo el valor del docuumento por defecto cuando no se tienen datos creados
        Nro_Docto.setText("1");
        Global.CODIGO_DOCUMENTO = "1";


        //Compronbando el tipo de conexiòn


        //Configurando el tipo de documento
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Configurando la fecha
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        fecha.setText(formatter.format(date));

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Se encarga de obtener el código del documento actual
        numeroDocumento();

        /*

        try {
            ConsultarNuevoNroDcto();
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/


        try {
            CargarResponsables("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            CargarBodegaOrigen("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ImagenBodegaOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BodegaOrigen.showDropDown();
            }
        });

        ImagenBodegaDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BodegaDestino.showDropDown();
            }
        });


        ImagenResponsable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Responsable.showDropDown();
            }
        });


        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ComprobarData = ComprobarCamposVacios();
                //COMPROBAR QUE TODOS LOS CAMPOS FUERON SELECCIONADOS:  BODEGA ORIGEN Y BODEGA GENERAL, CENTRO DE COSTOS Y RESPONSABLE
                if (ComprobarData == true) {


                    GuardarEncabezadoLocalmente(Nro_Docto.getText().toString(), BodegaOrigen.getText().toString(), BodegaDestino.getText().toString(), Responsable.getText().toString(), placa_Vehiculo.getText().toString());
                    //LLAMA FORMULARION Y MANDA PARAMETROS
                    if (Tipo_Docto.equals("TR")) {
                        Intent intent = new Intent(encabezado_Traslados.this, Crear_Traslado.class);
                        Variables_Globales.BODEGA_ORIGEN = BodegaOrigen.getText().toString();
                        Variables_Globales.BODEGA_DESTINO = BodegaDestino.getText().toString();
                        Variables_Globales.RESPONSABLE = Responsable.getText().toString();
                        Variables_Globales.PLACA_VEHICULO = placa_Vehiculo.getText().toString();
                        startActivity(intent);

                    } else if (Tipo_Docto.equals("AF")) {
                        Intent intent = new Intent(encabezado_Traslados.this, Crear_TrasladoAC.class);
                        Variables_Globales.BODEGA_ORIGEN = BodegaOrigen.getText().toString();
                        Variables_Globales.BODEGA_DESTINO = BodegaDestino.getText().toString();
                        Variables_Globales.RESPONSABLE = Responsable.getText().toString();
                        Variables_Globales.PLACA_VEHICULO = placa_Vehiculo.getText().toString();
                        startActivity(intent);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Es necesario ingresar todos los datos", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void CargarCentroCostos(String centralCostoDato) throws JSONException {

        String Ruta = null;
        JSONObject jsonBuscarCentroCostos = null;
        jsonBuscarCentroCostos = new JSONObject();

        if (TipoConexion.TIPO_CONEXION.equals(Cons.SERVER)) {

            jsonBuscarCentroCostos.put("Valor", centralCostoDato);


            // String url =   V_URL + "/CentralCosto";


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarCentroCostos", jsonBuscarCentroCostos, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstCentroCostos = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            nombreCentralCosto = mJsonObject.getString("NOMBRE");
                            lstCentroCostos[i] = nombreCentralCosto.trim();

                        }
                        ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstCentroCostos);
                        CentroCostos.setAdapter(adapterCentralCosto);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(request);

        } else if (TipoConexion.TIPO_CONEXION.equals(Cons.NUBE)) {

            /*
             * URL para realizar la peticion
             * */

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", Cons.Empresa);

            Ruta = V_URL_MON + "/api/centroCosto/BuscarCentroCostosxEmpresa";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray preguntasJA = response.getJSONArray("recordset");
                        lstCentroCostos = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {

                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            nombreCentralCosto = mJsonObject.getString("Nombre");
                            lstCentroCostos[i] = nombreCentralCosto.trim();

                        }

                        ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstCentroCostos);
                        CentroCostos.setAdapter(adapterCentralCosto);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(encabezado_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


            requestQueue.add(request);
        }

    }

    public void CargarResponsables(String ResponsableDato) throws JSONException {

        String Ruta = null;
        JSONObject jsonBuscarResponsables = null;
        jsonBuscarResponsables = new JSONObject();

        if (TipoConexion.TIPO_CONEXION.equals(Cons.SERVER)) {

            jsonBuscarResponsables.put("Valor", ResponsableDato);
            Ruta = V_URL + "/BuscarBodegas";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarResponsables, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstResponsable = new String[preguntasJA.length()];

                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            nombreResponsable = mJsonObject.getString("NOMBRE");
                            lstResponsable[i] = nombreResponsable.trim();
                        }

                        ArrayAdapter<String> adapterResponsables = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstResponsable);
                        Responsable.setAdapter(adapterResponsables);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });


            requestQueue.add(request);
        } else if (TipoConexion.TIPO_CONEXION.equals(Cons.NUBE)) {

            /*
             * URL para realizar la peticion
             * */

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", Cons.Empresa);

            Ruta = V_URL_MON + "/api/Responsable/BuscarResponsablexEmpresa";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray preguntasJA = response.getJSONArray("recordset");
                        lstResponsable = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {

                            JSONObject jsonObject = preguntasJA.getJSONObject(i);
                            nombreResponsable = jsonObject.getString("Nombre");
                            lstResponsable[i] = nombreResponsable.trim();

                        }

                        ArrayAdapter<String> adapterResponsables = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstResponsable);
                        Responsable.setAdapter(adapterResponsables);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            },
                    volleyError -> Toast.makeText(encabezado_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());


            requestQueue.add(request);
        }

    }

    /**
     * @ Tipo de conexiones definidas en la variable global
     * @ TipoConexion.TIPO_CONEXION  Guarda un nùmero segùn el tipo de conexiòn
     * Los valores son los siguientes
     * -  TipoConexion.TIPO_CONEXION = "1" es una conexion de tipo Rb_Local
     * -  TipoConexion.TIPO_CONEXION = "2" es una conexion de tipo Rb_Nube
     * -  TipoConexion.TIPO_CONEXION = "3" es una conexion de tipo Rb_Server
     */

    public void CargarBodegaOrigen(String ValorBodega) throws JSONException {

        String Ruta = null;
        JSONObject jsonBuscarBodegas = null;
        jsonBuscarBodegas = new JSONObject();

        if (TipoConexion.TIPO_CONEXION.equals(Cons.SERVER)) {

            jsonBuscarBodegas.put("Valor", ValorBodega);
            Ruta = V_URL + "/BuscarBodegas";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarBodegas, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstBodegaOrigen = new String[preguntasJA.length()];
                        lstBodegaDestino = new String[preguntasJA.length()];

                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);

                            descripcionBodega = mJsonObject.getString("NOMBRE");
                            lstBodegaOrigen[i] = descripcionBodega.trim();
                            lstBodegaDestino[i] = descripcionBodega.trim();


                        }
                        ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                        BodegaOrigen.setAdapter(adapterBodegas);

                        ArrayAdapter<String> adapterBodegaDestino = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                        BodegaDestino.setAdapter(adapterBodegaDestino);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });


            requestQueue.add(request);
        } else if (TipoConexion.TIPO_CONEXION.equals(Cons.NUBE)) {

            /*
             * URL para realizar la peticion
             *
             *
             * */

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", Cons.Empresa);

            Ruta = V_URL_MON + "/api/bodega/BuscarBodegaxEmpresa";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray preguntasJA = response.getJSONArray("recordset");
                        lstBodegaOrigen = new String[preguntasJA.length()];
                        lstBodegaDestino = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);

                            descripcionBodega = mJsonObject.getString("Descripcion");
                            lstBodegaOrigen[i] = descripcionBodega.trim();

                        }

                        ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                        BodegaOrigen.setAdapter(adapterBodegas);


                        ArrayAdapter<String> adapterBodegaDestino = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                        BodegaDestino.setAdapter(adapterBodegaDestino);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            },
                    volleyError -> Toast.makeText(encabezado_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

            requestQueue.add(request);
        }

    }

    public void CargarBodegaDestino(String ValorBodega) throws JSONException {


        String Ruta = null;
        JSONObject jsonBuscarBodegas = null;
        jsonBuscarBodegas = new JSONObject();

        if (TipoConexion.TIPO_CONEXION.equals("3")) {
            jsonBuscarBodegas.put("Valor", ValorBodega);
            Ruta = V_URL + "/BuscarBodegas";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarBodegas, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                        JSONArray preguntasJA = response.getJSONArray("recordset");


                        lstBodegaDestino = new String[preguntasJA.length()];

                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);

                            descripcionBodega = mJsonObject.getString("NOMBRE");
                            lstBodegaDestino[i] = descripcionBodega.trim();

                        }
                        ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                        BodegaDestino.setAdapter(adapterBodegas);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, error -> Toast.makeText(encabezado_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show());

            requestQueue.add(request);
        } else if (TipoConexion.TIPO_CONEXION.equals("2")) {

            /*
             * URL para realizar la peticion
             * */

            Ruta = V_URL_MON + "/api/bodega/list";

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                    jsonArray -> {


                        lstBodegaDestino = new String[jsonArray.length()];


                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                descripcionBodega = jsonObject.getString("Descripcion");
                                lstBodegaDestino[i] = descripcionBodega.trim();
                            } catch (JSONException e) {

                            }
                        }

                        ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);

                        BodegaDestino.setAdapter(adapterBodegas);


                    },
                    volleyError -> Toast.makeText(encabezado_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

            requestQueue.add(request);
        }

    }


    /***/


    public void GuardarEncabezadoLocalmente(String numeroDocumento, String bodegaOrigen, String bodegaDestino, String responsable, String placa) {
        try {
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_ENCABEZADO_TRASLADOS, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.tNUMERO_DOCUMENTO, numeroDocumento);
            values.put(Utilidades.tBODEGA_ORIGEN, bodegaOrigen);
            values.put(Utilidades.tBODEGA_DESTINO, bodegaDestino);
            values.put(Utilidades.tRESPONSABLE, responsable);
            values.put(Utilidades.tPLACA, placa);


            db.insert(Utilidades.TABLA_ENCABEZADO_TRASLADOS, null, values);
            db.close();
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Este método se encarga de mostrar el código actual a mostrar en un tipo de movimiento RQ o RC
     * Autor: CarlosDnl
     * Fecha: 05/12/2019
     */

    public void ConsultarNuevoNroDcto() throws JSONException {


        //Creando el json con los parametros que va a recibir el API
        JSONObject jsonBuscarConsecutivoMov = null;
        jsonBuscarConsecutivoMov = new JSONObject();

        jsonBuscarConsecutivoMov.put("Cod_Empresa", Cod_Empresa);
        jsonBuscarConsecutivoMov.put("Tipo_Documento", Tipo_Docto);

        //URL a la que será realizada la petición
        // String url =   V_URL + "/selectCode";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarConsecutivoMov", jsonBuscarConsecutivoMov, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray mJsonArray = response.getJSONArray("recordset");

                    // for(int i=0; i < mJsonArray.length(); i++){
                    if (mJsonArray.length() >= 1) {
                        JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                        String referencia = mJsonObject.getString("Consecutivo");
                        int nuevo_valor = 1;
                        nuevo_valor = nuevo_valor + Integer.parseInt(referencia.trim());

                        Nro_Docto.setText(String.valueOf(nuevo_valor));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show());


        requestQueue.add(request);
    }

    public Boolean ComprobarCamposVacios() {
        band = true;

        if (Responsable.getText().toString().equals("") || BodegaDestino.getText().toString().equals("") || BodegaOrigen.getText().toString().equals("")) {
            band = false;
        }
        return band;
    }


    public void numeroDocumento() {

        String Ruta = null;

        Ruta = V_URL_MON + "/api/movimiento/recuperarUltimoRegistro";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                jsonArray -> {

                    JSONObject jsonObject = null;

                    try {
                        jsonObject = jsonArray.getJSONObject(0);
                        Nro_Docto.setText(String.valueOf(Integer.parseInt(jsonObject.getString("Numero_Documento")) + 1));
                        Global.CODIGO_DOCUMENTO = Nro_Docto.getText().toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                },
                volleyError -> Toast.makeText(encabezado_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
    }

    private void prueba_datos() {
        //global var

        Thread MyThread = new Thread() {//create thread
            @Override
            public void run() {


                String url = "https://reqres.in/api/users?page=2"; // my URL


                JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                        jsonArray -> Nro_Docto.setText(String.valueOf(cont++)),
                        volleyError -> Nro_Docto.setText(String.valueOf(cont++)));
                int i = 0;
                while (running) {

                    System.out.println("counter: " + i);
                    i++;

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                        System.out.println("Sleep interrupted");
                    }

                    // Add the request to the RequestQueue.
                    requestQueue.add(request);
                }
                System.out.println("onEnd Thread");
            }
        };
        MyThread.start(); // start thread
    }

}

/*
 jsonBuscarBodegas.put("Cod_Empresa", Cons.Empresa);
 Ruta = V_URL_MON + "/api/Bodega/BuscarBodegasxEmpresa";


 //Mostrando la ruta para comprobar que la URL estè correcta
 Toast.makeText(getApplicationContext(), Ruta, Toast.LENGTH_SHORT).show();

 JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarBodegas, new Response.Listener<JSONObject>() {
@Override
public void onResponse(JSONObject response) {

String Bodegas;  //Esta varaible va a quitar los espacions de la respuesta del json
String Respuesta = null;
String Mensaje = null;
//creo Objeto Json para entrar al Output
JSONObject JOutput = null;
//creo Objeto Array Json para entrar al Recordset
JSONArray mJsonArray = null;

try {

JOutput  = response.getJSONObject("output");
Respuesta = JOutput.optString("Respuesta");
Mensaje = JOutput.optString("Mensaje");

if (Respuesta.equals("1")) {

mJsonArray = response.getJSONArray("recordset");
lstBodegaOrigen = new String[mJsonArray.length()];
lstBodegaDestino = new String[mJsonArray.length()];

for (int i = 0; i < mJsonArray.length(); i++) {

JSONObject mJsonObject = mJsonArray.getJSONObject(i);
Bodegas = mJsonObject.getString("Descripcion");
lstBodegaOrigen[i] = Bodegas.trim();
lstBodegaDestino[i] = Bodegas.trim();
}
ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(encabezado_Traslados.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
BodegaOrigen.setAdapter(adapterBodegas);
BodegaDestino.setAdapter(adapterBodegas);

}else
{
Toast.makeText(getApplicationContext(), Mensaje.toString() ,Toast.LENGTH_SHORT).show();
}

} catch (JSONException e) {
e.printStackTrace();
}
}
}, new Response.ErrorListener() {
@Override
public void onErrorResponse(VolleyError error) {
Toast.makeText(encabezado_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
}
});

 queue.add(request);
 */



