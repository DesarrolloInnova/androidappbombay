package innobile.itraceandroid.TrasladosTM;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.modelo.ModeloDeleteData;



class DeleteTrasladosAdapter extends RecyclerView.Adapter<DeleteTrasladosAdapter.PlayerViewHolder> {
    public List<ModeloDeleteData> players;



    public class PlayerViewHolder extends RecyclerView.ViewHolder {
        private TextView name, nationality, club, rating, age;

        public PlayerViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.codigo);
            nationality = (TextView) view.findViewById(R.id.maquina);
            club = (TextView) view.findViewById(R.id.descripcion);
        }
    }

    public DeleteTrasladosAdapter(List<ModeloDeleteData> players) {
        this.players = players;
    }

    @Override
    public DeleteTrasladosAdapter.PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delete_traslado, parent, false);

        return new DeleteTrasladosAdapter.PlayerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeleteTrasladosAdapter.PlayerViewHolder holder, int position) {
        ModeloDeleteData player = players.get(position);
        holder.name.setText(player.getCodigo());
        holder.nationality.setText(player.getpDescripcion());
        holder.club.setText(player.getUnidad_Medida());
    }

    @Override
    public int getItemCount() {
        return players.size();
    }
}


