package innobile.itraceandroid.TrasladosTM;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.ActivosFijo;
import innobile.Adapter.Recycler_view_Bodegas;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.ModeloDeleteData;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.Maquinas;
import innobile.itraceandroid.RetrofitAdapter.activos_bodegas;
import innobile.itraceandroid.RetrofitAdapter.bodegas_id;
import innobile.itraceandroid.Variables_Globales;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Reportes_Traslados extends AppCompatActivity {

    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private List<ActivosFijo> lstBodegas;
    private RecyclerView recyclerView;
    private String V_URL_MON = "", Ruta = "";
    private Button btnBuscar;
    private AutoCompleteTextView bodega;
    public static Retrofit retrofit;
    private ConexionSQLiteHelper conn;
    private SQLiteDatabase db;
    private ContentValues contentValues;
    private Long cn = 0L;
    private static FileWriter writer;
    private static Cursor c;
    private List<String> datos = new ArrayList<String>();
    public FileWriter myWriter;
    public String nombre_Archivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes__traslados);

        btnBuscar = findViewById(R.id.btnBuscar);
        bodega = findViewById(R.id.bodega);

        //Definiendo las varaibles de conexión del API
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();


        lstBodegas = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_bodegas);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(V_URL_MON + '/')
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }


        btnBuscar.setOnClickListener(view -> {
            if(!bodega.getText().toString().trim().isEmpty()){
                activosFijosBodega();
            }
        });


        traerBodegasId();

    }

    public void traerBodegasId() {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_BODEGAS_ID, null, 1);
        db = conn.getWritableDatabase();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<bodegas_id>> call = jsonPlaceHolderApi.getBodegas();

        call.enqueue(new Callback<List<bodegas_id>>() {
            @Override
            public void onResponse(Call<List<bodegas_id>> call, Response<List<bodegas_id>> response) {

                List<bodegas_id> bodegas = response.body();
                db.beginTransactionNonExclusive();
                try {
                    for (bodegas_id bodegas_id : bodegas) {
                        contentValues = new ContentValues();
                        contentValues.put(Utilidades.bodegaNombre, bodegas_id.getDescripcion().trim());
                        contentValues.put(Utilidades.bodegaID, bodegas_id.get_id().trim());
                        db.insert(Utilidades.TABLA_BODEGAS_ID, null, contentValues);
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                db.close();

                cargarSqliteDetalles();
                ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(Reportes_Traslados.this, android.R.layout.simple_spinner_dropdown_item, datos);
                bodega.setAdapter(adapterBodegas);
            }

            @Override
            public void onFailure(Call<List<bodegas_id>> call, Throwable t) {

            }
        });
    }

    public void activosFijosBodega() {

        lstBodegas = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_bodegas);


        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<activos_bodegas>> call = jsonPlaceHolderApi.getActivos_Bodegas(obtenerIdBodega(bodega.getText().toString()));

        call.enqueue(new Callback<List<activos_bodegas>>() {
            @Override
            public void onResponse(Call<List<activos_bodegas>> call, Response<List<activos_bodegas>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    nombre_Archivo = DateFormat.format("MM-dd-yyyyy-h-mmssaa", System.currentTimeMillis()).toString();
                    File root = new File(Environment.getExternalStorageDirectory(), "Reportes");
                    if (!root.exists()) {
                        root.mkdirs();
                    }
                    File filepath = new File(root, nombre_Archivo.trim() + ".txt");  // file path to save
                    myWriter = new FileWriter(filepath);

                    List<activos_bodegas> activos_fijos = response.body();

                    for (activos_bodegas activos : activos_fijos) {
                        ActivosFijo listActivos = new ActivosFijo();
                        listActivos.setDescripcion(activos.getDescripcion());
                        listActivos.setCodigo(activos.getCodigo());
                        listActivos.setMarca(activos.getMarca());
                        listActivos.setGrupo(activos.getGrupo());
                        myWriter.append(listActivos.getDescripcion() + ";" + listActivos.getCodigo() + ";" + listActivos.getMarca() + ";" + listActivos.getGrupo() + "\n");
                        lstBodegas.add(listActivos);
                    }
                    myWriter.close();
                    Toasty.success(getApplicationContext(), "El reporte " + nombre_Archivo.trim() + " fue generado correctamente", Toasty.LENGTH_SHORT).show();
                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
                setuprecyclerview(lstBodegas);
            }

            @Override
            public void onFailure(Call<List<activos_bodegas>> call, Throwable t) {

            }
        });


    }


    private void setuprecyclerview(List<ActivosFijo> lstBodega) {
        Recycler_view_Bodegas myadapter = new Recycler_view_Bodegas(this, lstBodega);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myadapter);

    }

    private void cargarSqliteDetalles() {
        //Se limpian los datos para evitar registros duplicados
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_BODEGAS_ID, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_BODEGAS_ID;//Set name of your table
        c = db.rawQuery("SELECT * FROM  " + myTable, null);
        if (c.moveToFirst()) {
            do {
                datos.add(c.getString(0));
                datos.add(c.getString(1));
            } while (c.moveToNext());
        }
    }

    private String obtenerIdBodega(String nombre_Bodega) {
        String id = "";
        //Se limpian los datos para evitar registros duplicados
        datos.clear();
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_BODEGAS_ID, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_BODEGAS_ID;//Set name of your table
        c = db.rawQuery("SELECT " + Utilidades.bodegaID + " FROM  " + myTable + " WHERE " + Utilidades.bodegaNombre + " = '" + nombre_Bodega.trim() + "'", null);
        if (c.moveToFirst()) {
            do {
                id = c.getString(0);
            } while (c.moveToNext());
        }

        return id;
    }

    private Long cantidaRegistrosBodegas() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_BODEGAS_ID, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_BODEGAS_ID);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }


}
