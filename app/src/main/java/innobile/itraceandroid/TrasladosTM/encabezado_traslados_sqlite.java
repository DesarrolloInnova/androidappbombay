package innobile.itraceandroid.TrasladosTM;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.DefinirRuta;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.Variables_Globales;

public class encabezado_traslados_sqlite extends AppCompatActivity {

    private AutoCompleteTextView placa, Responsable, BodegaOrigen, BodegaDestino;
    private ImageView ImageCentralCosto, ImagenResponsable, ImagenBodegaOrigen,ImagenBodegaDestino ;
    private String[] lstCentroCostos = new String[] {"Sin datos"};
    private String[] lstResponsable = new String[] {"Sin datos"};
    private String[] lstBodegaOrigen = new String[] {"Sin datos"};
    private String[] lstBodegaDestino = new String[] {"Sin datos"};
    private Button btnDetalle;
    private TextView Nro_Docto;
    private String Cod_Empresa = "01";
    private String Tipo_Docto = "";
    private Boolean band = true, ComprobarData;
    private TextView fecha;
    Date date = new Date();
    private String V_URL, V_URL_MON = null;
    private String comprobarConsulta = "";
    private String V_TipoConexion  = null;
    private  ConexionSQLiteHelper conn;

    private String descripcionBodega = "", nombreResponsable = "", nombreCentralCosto = "";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_traslados_sqlite);
        BodegaOrigen = findViewById(R.id.autoCompleteBodegaOrigen);
        BodegaDestino = findViewById(R.id.autoCompleteBodegaDestino);
        placa = findViewById(R.id.t_placa);
        Responsable = findViewById(R.id.autoCompleteResponsable);
        ImagenBodegaOrigen = findViewById(R.id.ImagenBodegaOrigen);
        ImagenBodegaDestino = findViewById(R.id.ImagenBodegaDestino);
        ImageCentralCosto = findViewById(R.id.ImagenCentralCosto);
        ImagenResponsable = findViewById(R.id.ImagenResponsable);
        Nro_Docto = findViewById(R.id.txtNroDocto);
        btnDetalle = findViewById(R.id.Buttondetalle);
        placa.setThreshold(2);
        placa.setThreshold(1);
        fecha = findViewById(R.id.MostrarFecha);


        //Definiendo el valor del docuumento por defecto cuando no se tienen datos creados
        Nro_Docto.setText(getIntent().getStringExtra("Codigo"));
        fecha.setText(getIntent().getStringExtra("Fecha"));

        recuperarRegistros();

        getIntent().getStringExtra("prioridad");


        //Compronbando el tipo de conexiòn


        //Configurando el tipo de documento
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Configurando la fecha
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        fecha.setText(  formatter.format(date));

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL=  t_configuraciones.getCon_URL();
        V_URL_MON =  t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();





        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ComprobarData = ComprobarCamposVacios();
                //COMPROBAR QUE TODOS LOS CAMPOS FUERON SELECCIONADOS:  BODEGA ORIGEN Y BODEGA GENERAL, CENTRO DE COSTOS Y RESPONSABLE
                if(ComprobarData == true){


                    //LLAMA FORMULARION Y MANDA PARAMETROS
                    if(Tipo_Docto.equals("AF")){
                        Intent intent = new Intent(encabezado_traslados_sqlite.this, Crear_TrasladoAC.class);
                        Variables_Globales.BODEGA_ORIGEN = BodegaOrigen.getText().toString();
                        Variables_Globales.BODEGA_DESTINO = BodegaDestino.getText().toString();
                        Variables_Globales.RESPONSABLE = Responsable.getText().toString();
                        Variables_Globales.PLACA_VEHICULO = placa.getText().toString();
                        Global.CODIGO_DOCUMENTO = getIntent().getStringExtra("Codigo");
                        startActivity(intent);
                    }



                }else{
                    Toast.makeText(getApplicationContext(),"Es necesario ingresar todos los datos", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public Boolean ComprobarCamposVacios(){
        band = true;

        if( placa.getText().toString().equals("") || Responsable.getText().toString().equals("") || BodegaDestino.getText().toString().equals("")|| BodegaOrigen.getText().toString().equals("")){
            band = false;
        }
        return band;
    }


    public void recuperarRegistros(){
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_ENCABEZADO_TRASLADOS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_ENCABEZADO_TRASLADOS;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM  '" + myTable + "' where n_documento = " + Nro_Docto.getText().toString(), null);


        if (c.moveToFirst()) {
            do {
                String bodega_origen = c.getString(1);
                String bodega_destino = c.getString(2);
                String placa_vehiculo = c.getString(4);
                String responsable = c.getString(3);

                BodegaOrigen.setText(bodega_origen);
                BodegaDestino.setText(bodega_destino);
                placa.setText(placa_vehiculo);
                Responsable.setText(responsable);



            } while (c.moveToNext());
        }
    }





}
