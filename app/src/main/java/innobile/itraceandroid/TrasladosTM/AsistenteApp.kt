package innobile.itraceandroid.TrasladosTM

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Network
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.NoCache
import es.dmoral.toasty.Toasty
import innobile.itrace.ITraceApplication.getContext
import innobile.itrace.R
import innobile.itrace.data.Cons
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager
import org.json.JSONObject


class AsistenteApp : AppCompatActivity() {
    var tCodigo: TextView? = null
    var tMaquinas: TextView? = null
    var tFecha: TextView? = null
    var requestQueue: RequestQueue? = null
    var V_URL_MON = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asistente_app)
        tCodigo = findViewById(R.id.t_codigo)

        tCodigo!!.setText(getIntent().getStringExtra("codigo"))
        consultar_Maquinas()
    }

    fun consultar_Maquinas() {
        //Se crea el objeto de las peticiones a volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            val network: Network = BasicNetwork(HurlStack())
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = RequestQueue(NoCache(), network)
            // Start the queue
            requestQueue!!.start()
        }

        var jsonBody: JSONObject? = null
        jsonBody = JSONObject()
        jsonBody.put("_id", getString(R.string.id_empresa))
        jsonBody.put("Numero_Documento", tCodigo!!.text.trim())

        //Se codifica la URL para no recibir espacios en blanco
        val url = V_URL_MON + "https://webapi.innova-id.com/api/movimiento/buscarXNumeroDocumento"

        // Request a string response from the provided URL.
        val req = JsonObjectRequest(Request.Method.POST, url, jsonBody,
                Response.Listener { response ->

                    //Se obtiene el jsonArray
                    val jsonArray = response.getJSONArray("movimientos")
                    val mJsonObject: JSONObject = jsonArray.getJSONObject(0)
                    val jsonDetalles = mJsonObject.getJSONArray("Detalles")
                    val array = arrayOfNulls<String>(jsonDetalles.length())
                    for (i in 0 until jsonDetalles.length()) {
                        val dJsonObject: JSONObject = jsonDetalles.getJSONObject(i)
                        val maquinas = dJsonObject.getJSONObject("productos").getString("Descripcion")
                        tMaquinas!!.text = tMaquinas!!.text.toString() + "maquinas" + "\n"
                    }
                    tFecha!!.text = mJsonObject.getString(" Fecha_Creacion")
                }, Response.ErrorListener { error ->
            Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_LONG).show()
        }
        )
        requestQueue!!.add(req)
    }
}
