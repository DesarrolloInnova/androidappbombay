package innobile.itraceandroid.TrasladosTM;

import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import innobile.itrace.R;
import innobile.itraceandroid.TrasladosTM.Controlador.PageControllerMovimientos;

public class InformacionMovimientos extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    TabItem tab1, tab2, tab3;
    PageControllerMovimientos pagerControllerMovimientos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_movimientos);

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewpager);
        tab1 = findViewById(R.id.tab_listar);
        tab2 = findViewById(R.id.tab_buscar);
        tab3 = findViewById(R.id.tab_filtrar);

        pagerControllerMovimientos = new PageControllerMovimientos(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerControllerMovimientos);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    pagerControllerMovimientos.notifyDataSetChanged();
                }
                if (tab.getPosition() == 1) {
                    pagerControllerMovimientos.notifyDataSetChanged();
                }
                if (tab.getPosition() == 2) {
                    pagerControllerMovimientos.notifyDataSetChanged();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }
}

