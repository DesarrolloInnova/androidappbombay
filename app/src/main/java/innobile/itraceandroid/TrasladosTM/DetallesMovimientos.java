package innobile.itraceandroid.TrasladosTM;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.movimientos;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;

public class DetallesMovimientos extends AppCompatActivity {
    private TextView tCodigo, tMaquina, tFecha;
    private RequestQueue requestQueue = null;
    private String V_URL_MON = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_movimientos);
        tCodigo = findViewById(R.id.t_codigo);
        tMaquina = findViewById(R.id.t_maquinas);
        tFecha = findViewById(R.id.t_fecha);

        //Enviando el código actual a la vista
        tCodigo.setText(getIntent().getStringExtra("codigo"));

        //Definiendo las varaibles de conexión del API
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();

        try {
            mostrarDatosCodigo();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void mostrarDatosCodigo() throws JSONException {
        cliente();
        JSONObject movimiento = new JSONObject();
        movimiento.put("_id", "5e2f4c3ef80d8246f0e545d8");
        movimiento.put("Numero_Documento", tCodigo.getText().toString().trim());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL_MON + "/api/movimiento/buscarXNumeroDocumento", movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray movimiento = response.getJSONArray("movimientos");

                    if (movimiento.length() > 0) {
                        JSONObject mJsonObject = movimiento.getJSONObject(0);
                        JSONArray mJsonDetalles = mJsonObject.getJSONArray("Detalles");

                        for (int i = 0; i < mJsonDetalles.length(); i++) {
                            JSONObject objectDetalles = mJsonDetalles.getJSONObject(i);
                            String maquinas = objectDetalles.getJSONObject("productos").getString("Descripcion");
                            tMaquina.setText(tMaquina.getText().toString() + maquinas + "\n");
                        }

                        if (mJsonObject.has("Fecha_Creacion")) {
                            tFecha.setText(mJsonObject.getString("Fecha_Creacion"));
                        } else {
                            tFecha.setText("Sin fecha");
                        }

                    } else {
                        Toasty.info(getApplicationContext(), "No se encontraron movimientos", Toasty.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, error -> Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show());
        requestQueue.add(request);
    }

    public void cliente() {
        //Se crea el objeto de las peticiones a volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }
    }
}
