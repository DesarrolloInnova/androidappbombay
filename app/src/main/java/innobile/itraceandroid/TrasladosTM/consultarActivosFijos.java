package innobile.itraceandroid.TrasladosTM;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.Maquinas;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class consultarActivosFijos extends AppCompatActivity {

    private AutoCompleteTextView codigoLeido = null;
    private TextView tDescripcion = null, tMarca = null, tGrupo, tCodigo = null, tBodega = null;
    private ImageButton ibtnBuscar = null;
    private String V_URL = null, V_URL_MON = null, V_TipoConexion = null;
    private int comprobarTAB = 0;
    private RequestQueue requestQueue;
    public static Retrofit retrofit;
    private ContentValues contentValues;
    private ConexionSQLiteHelper conn;
    private SQLiteDatabase db;
    private Long cn = 0L;
    private String[] ltsMaquinas = new String[]{};
    private static Cursor c;
    private List<String> datos = new ArrayList<String>();


    private Button probando;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_activos_fijos);

        codigoLeido = findViewById(R.id.eCodigoIngresado);
        tDescripcion = findViewById(R.id.tNombre);
        tMarca = findViewById(R.id.tCiudad);
        tGrupo = findViewById(R.id.txtGrupo);
        ibtnBuscar = findViewById(R.id.ibtnBuscar);
        tCodigo = findViewById(R.id.tCodigo);
        tBodega = findViewById(R.id.tBodega);


        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(new NoCache(), network);
        // Start the queue
        requestQueue.start();

        //Configurando la URL de acceso para realizar la petición
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();

        sendNetworkRequest();

        if (cantidaRegistrosMaquinas() > 0) {
            cargarSqliteDetalles();
            ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(consultarActivosFijos.this, android.R.layout.simple_spinner_dropdown_item, datos);
            codigoLeido.setAdapter(adapterBodegas);
        }

        //Configurando la busqueda al presionar el botón
        ibtnBuscar.setOnClickListener(view -> {
            //Se encarga de consultar los datos del producto, recibe la URL y el código del producto a buscar
            String[] separandoCodigo = codigoLeido.getText().toString().split(";");
            String codigo = separandoCodigo[0];
            if (separandoCodigo[0] == null) {
                codigo = codigoLeido.getText().toString();
            }
            consultarProducto(V_URL_MON, codigo);
        });


        //Configurando la busque automática por TAB
        //Se encarga de ejecutar el método de buscar producto al percibir un enter
        codigoLeido.setOnKeyListener((view, i, keyEvent) -> {
            //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
            if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                //Este contador se encarga de enviar los datos solo una vez
                comprobarTAB++;
                if (comprobarTAB == 2) {
                    //Se obtiene el primer elemento del codigo leido desde el QR
                    String[] separandoCodigo = codigoLeido.getText().toString().split(" ");
                    String codigo = separandoCodigo[0];
                    //Se encarga de traer todos los datos de un producto
                    consultarProducto(V_URL_MON, codigo);
                }
                return true;
            }
            return false;
        });


    }

    public void consultarProducto(String V_URL_MON, final String codigoBuscar) {

        //Configurando la URL para realizar la petición en el API

        String Ruta;
        Ruta = V_URL_MON + "/api/ActivosFijos/list?valor=" + codigoBuscar;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {

                        //Se obtienen los datos y se cargar en las vistas
                        JSONObject jsonObject = null;
                        try {

                            //Se obtiene el código que fue leido
                            tCodigo.setText(codigoBuscar);

                            jsonObject = jsonArray.getJSONObject(0);
                            tDescripcion.setText(jsonObject.getString("Descripcion"));

                            //Se encarga de comprobar si el dato viene vacio, algunos productos tiene el campo marca y otros no
                            if (jsonObject.has("Marca")) {
                                tMarca.setText(jsonObject.getString("Marca"));
                            } else {
                                tMarca.setText("sin marca");
                            }
                            tGrupo.setText(jsonObject.getString("Grupo"));

                            tBodega.setText(jsonObject.getJSONObject("bodegas").getString("Descripcion"));


                            //Reinicio el código para no tener que borrarlo manualmente
                            codigoLeido.setText("");
                        } catch (JSONException e) {
                            AlertDialog alertDialog = new AlertDialog.Builder(consultarActivosFijos.this).create();
                            alertDialog.setTitle("Mensaje");
                            alertDialog.setMessage("No se encontro el código");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }

                        comprobarTAB = 0;
                    }

                },
                volleyError -> Toast.makeText(consultarActivosFijos.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
    }


    public void sendNetworkRequest() {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
        db = conn.getWritableDatabase();
        if (cantidaRegistrosMaquinas() == 0) {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(V_URL_MON)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
            Call<List<Maquinas>> call = jsonPlaceHolderApi.getMaquinas();

            call.enqueue(new Callback<List<Maquinas>>() {
                @Override
                public void onResponse(Call<List<Maquinas>> call, retrofit2.Response<List<Maquinas>> response) {
                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                        return;
                    }
                    //Se limpian los datos para evitar registros duplicados
                    datos.clear();
                    List<Maquinas> maquina = response.body();
                    //Reinicio de la base de datos

                    db.beginTransactionNonExclusive();
                    try {
                        for (Maquinas maquinas : maquina) {
                            contentValues = new ContentValues();
                            contentValues.put(Utilidades.DETALLE_MAQUINAS, maquinas.getCodigo() + ";" + maquinas.getDescripcion());
                            db.insert(Utilidades.TABLA_DETALLES, null, contentValues);
                        }
                        db.setTransactionSuccessful();
                    } finally {
                        db.endTransaction();
                    }
                    db.close();
                    cargarSqliteDetalles();
                    ArrayAdapter<String> detallesActivos = new ArrayAdapter<String>(consultarActivosFijos.this, android.R.layout.simple_spinner_dropdown_item, datos);
                    codigoLeido.setAdapter(detallesActivos);

                    Toasty.success(getApplicationContext(), "Se insertaron " + cantidaRegistrosMaquinas() + " registros", Toasty.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<List<Maquinas>> call, Throwable t) {

                }
            });
        }

    }

    private Long cantidaRegistrosMaquinas() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_DETALLES);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    private String[] cargarSqliteDetalles() {
        //Se limpian los datos para evitar registros duplicados
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_DETALLES, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_DETALLES;//Set name of your table
        c = db.rawQuery("SELECT * FROM  " + myTable, null);
        if (c.moveToFirst()) {
            do {
                String detalleMaquina = c.getString(0);
                datos.add(detalleMaquina);
            } while (c.moveToNext());
        }
        //cargarBodegasLista();
        return ltsMaquinas;
    }

}
