package innobile.itraceandroid.TrasladosTM;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import innobile.itrace.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.ModeloDeleteData;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.DeleteRequisicionData;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.Global_Detalles;
import innobile.itraceandroid.SwipeController;
import innobile.itraceandroid.SwipeControllerActions;
import innobile.itraceandroid.TipoConexion;
import innobile.itraceandroid.Variables_Globales;
import innobile.itraceandroid.Ver_Detalle_Movimientos;


public class Detalle_Traslados extends AppCompatActivity {
    public DeleteTrasladosAdapter mAdapter;
    public SwipeController swipeController = null;
    private String Codigo, pDescripcion, Unidad_Medida;
    private List<ModeloDeleteData> players = new ArrayList<>();
    private ModeloDeleteData player = new ModeloDeleteData();
    private String Tipo_Docto;
    private String V_URL = null;
    private String V_TipoConexion = null;
    private String V_URL_MON = null, Bodega_Origen;
    private String codigo_Producto = "";
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verdetallerq);

        //Guardando el tipo de movimiento que se está ejecutando
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(cache, network);
        // Start the queue
        requestQueue.start();


        //Pasando la estructura de la lista
        mAdapter = new DeleteTrasladosAdapter(players);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);


        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Comprobando el tipo de movimiento para ejecutar la carga de datos en la lista

        buscarDetalleMongodb();

        /*
         * Se encarga de comprobar la cantidad reservada actualmente
         * */

/*


        try {
            getReferenceData();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        setupRecyclerView();
    }


    public void getReferenceData() throws JSONException {


        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarMovimientosDetalles", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray mJsonArray = response.getJSONArray("recordset");


                    for (int i = 0; i < mJsonArray.length(); i++) {
                        ModeloDeleteData player = new ModeloDeleteData();
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        player.setCodigo(mJsonObject.getString("Codigo"));
                        player.setpDescripcion(mJsonObject.getString("Descripcion"));
                        player.setUnidad_Medida(mJsonObject.getString("cantidad"));
                        players.add(player);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Notifica los cambios al adaptador
                mAdapter.notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(request);
    }


    private void setupRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        final TextView name = (TextView) findViewById(R.id.codigo);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                try {
                    if (TipoConexion.TIPO_CONEXION.equals("1")) {
                        deleteDataApi(mAdapter.players.remove(position).getCodigo());
                    } else if (TipoConexion.TIPO_CONEXION.equals("2")) {
                        //actualizarCantidadReservada(mAdapter.players.remove(position).getUnidad_Medida());
                        actualizarReserva(mAdapter.players.remove(position).getCodigo());


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),  mAdapter.players.remove(position).getCodigo(), Toast.LENGTH_SHORT).show();
                // mAdapter.players.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }


    public void deleteDataApi(String Codigo_Eliminar) throws JSONException {


        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Codigo", Codigo_Eliminar);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/EliminarProductoMovimientos", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    /***
     * Se encarga de obtener los datos para detalle desde Mongo
     * */

    public void buscarDetalleMongodb() {

        String Ruta = null;

        Ruta = V_URL_MON + "/api/Movimiento/BuscarMovimientosDetalles?valor=" + getIntent().getStringExtra("codigo");
        //Ruta = V_URL_MON + "/api/Movimiento/BuscarMovimientosDetalles?valor=1";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = jsonArray.getJSONObject(0);
                            Global.ID_MOVIMIENTO = jsonObject.getString("_id");
                            JSONArray array = jsonObject.getJSONArray("Detalles");
                            for (int j = 0; j < array.length(); ++j) {
                                ModeloDeleteData player = new ModeloDeleteData();
                                JSONObject jsonDetalle = array.getJSONObject(j);
                                Global_Detalles.ID_BORRAR = jsonDetalle.getString("_id");
                                Global_Detalles.CODIGO_PRODUCTO = jsonDetalle.getJSONObject("productos").getString("_id");
                                player.setCodigo(Global_Detalles.CODIGO_PRODUCTO);
                                player.setpDescripcion(jsonDetalle.getJSONObject("productos").getString("Descripcion"));
                                player.setUnidad_Medida(jsonDetalle.getJSONObject("productos").getString("Marca"));
                                players.add(player);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(Detalle_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        requestQueue.add(request);
    }

    /**
     * Se encarga de borrar un detalle en Mongo
     */

    public void borrarDetalleMongo(String id_Producto) throws JSONException {


        //consultarDatosProductoBorrar();


        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Documento", Global.ID_MOVIMIENTO);
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("id_producto", id_Producto);

        String url = V_URL_MON + "/api/Movimiento/EliminarMovimientos";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    public void actualizarReserva(final String id_Actualizar) throws JSONException {

        String Ruta = null;


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", id_Actualizar);
        jsonObject.put("V_Reservado", "false");

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/ActivosFijos/actualizaReserva";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");


                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Detalle_Traslados.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detalle_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        borrarDetalleMongo(id_Actualizar);
        requestQueue.add(request);

    }


    public void actualizarCantidadReservada(String cantidadRestar) throws JSONException {

        String Ruta = null;


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", Global_Detalles.CODIGO_PRODUCTO);
        jsonObject.put("id_bodega", Variables_Globales.BODEGA_ORIGEN);
        jsonObject.put("Cantidad_Reservada", Integer.parseInt(cantidadRestar) * -1);

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/producto/ActualizarCantRerservada";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Detalle_Traslados.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detalle_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(request);


    }
}






/*
public class Ver_Detalle_Movimientos extends AppCompatActivity {

    public DeleteDataAdapter mAdapter ;
    SwipeController swipeController = null;
    private String Codigo, pDescripcion, Unidad_Medida;
    private List<ModeloDeleteData> players = new ArrayList<>();
    private ModeloDeleteData player = new ModeloDeleteData();
    private String Tipo_Docto;
    private String V_URL = null;
    private String V_TipoConexion  = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verdetallerq);

        //Guardando el tipo de movimiento que se está ejecutando
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Pasando la estructura de la lista
        mAdapter = new DeleteDataAdapter(players);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL=  t_configuraciones.getCon_URL();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Comprobando el tipo de movimiento para ejecutar la carga de datos en la lista


        try {
            getReferenceData();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setupRecyclerView();
    }

    public void getReferenceData() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));




        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarMovimientosDetalles", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray mJsonArray = response.getJSONArray("recordset");


                    for (int i = 0; i < mJsonArray.length(); i++) {
                        ModeloDeleteData player = new ModeloDeleteData();
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        player.setCodigo( mJsonObject.getString("Codigo"));
                        player.setpDescripcion(mJsonObject.getString("Descripcion"));
                        player.setUnidad_Medida( mJsonObject.getString("cantidad"));
                        players.add(player);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Notifica los cambios al adaptador
                mAdapter.notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
    }



    private void setupRecyclerView() {
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        final TextView name = (TextView) findViewById(R.id.name);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                try {
                    deleteDataApi( mAdapter.players.remove(position).getCodigo());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),  mAdapter.players.remove(position).getCodigo(), Toast.LENGTH_SHORT).show();
                // mAdapter.players.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }


    public void deleteDataApi(String Codigo_Eliminar) throws JSONException {

        RequestQueue queue = Volley.newRequestQueue(this);

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Codigo", Codigo_Eliminar);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/EliminarProductoMovimientos", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(request);
    }


}

*/


