package innobile.itraceandroid.TrasladosTM.Controlador;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.movimientos;
import innobile.Adapter.movimientos_adapter;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.Fajate.configuracion_inventario;


public class filtrar_fragment extends Fragment {

    private EditText eFechaInicial, eFecha_Final;
    private RecyclerView recyclerMovimientos;
    private InputStream inputStream = null;
    private Button btnFiltrar;
    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private List<movimientos> lstMovimientos;
    private String V_URL_MON = "";
    private String nMes = "", nDia = "";

    public filtrar_fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_filtrar_fragment, container, false);

        eFechaInicial = rootView.findViewById(R.id.efecha_inicial);
        eFecha_Final = rootView.findViewById(R.id.efecha_final);
        recyclerMovimientos = rootView.findViewById(R.id.recycler_movimientos);
        btnFiltrar = rootView.findViewById(R.id.btn_filtrar);
        lstMovimientos = new ArrayList<>();

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();


        btnFiltrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    obtenerDatosFiltrados();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        return rootView;
    }


    public void obtenerDatosFiltrados() throws JSONException {
        cliente();
        JSONObject movimiento = new JSONObject();
        movimiento.put("FECHA_INICIAL", eFechaInicial.getText().toString().trim() + "T05:00:00.000+00:00");
        movimiento.put("FECHA_FINAL",  eFecha_Final.getText().toString().trim()  + "T05:00:00.000+00:00");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL_MON + "/api/movimiento/filtrarfecha", movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {
                    JSONArray movimiento = response.getJSONArray("movimientos");
                    if (movimiento.length() > 0) {
                        for (int i = 0; i < movimiento.length(); i++) {
                            JSONObject mJSONOBject = movimiento.getJSONObject(i);
                            movimientos mJson = new movimientos();
                            mJson.setCodigo(mJSONOBject.getString("Numero_Documento"));
                            mJson.setBodega_Origen(mJSONOBject.getJSONObject("Bodega_Origen").getString("Descripcion"));
                            mJson.setBodega_Destion(mJSONOBject.getJSONObject("Bodega_Destino").getString("Descripcion"));
                            mJson.setResponsable(mJSONOBject.getString("Responsable"));
                            mJson.setObservaciones(mJSONOBject.getString("Observaciones"));
                            lstMovimientos.add(mJson);
                        }
                    } else {
                        Toasty.info(getContext(), "No se encontraron datos para el filtro", Toasty.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setuprecyclerview(lstMovimientos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    public void cliente() {
        //Se crea el objeto de las peticiones a volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }
    }

    private void setuprecyclerview(List<movimientos> lstMovimientos) {
        movimientos_adapter myadapter = new movimientos_adapter(getContext(), lstMovimientos);
        recyclerMovimientos.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerMovimientos.setAdapter(myadapter);
    }
}
