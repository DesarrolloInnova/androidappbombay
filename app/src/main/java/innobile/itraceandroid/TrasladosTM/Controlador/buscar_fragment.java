package innobile.itraceandroid.TrasladosTM.Controlador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Vector;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.TrasladosTM.Crear_TrasladoAC;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class buscar_fragment extends Fragment {
    private EditText tnumero_documento;
    private TextView tbodega_origen;
    private TextView tbodega_destino;
    private TextView tresponsable;
    private ArrayList<String> maquinasArrayList = new ArrayList<String>();
    private TextView listView;
    private String V_URL, V_URL_MON, V_TipoConexion;
    private ImageView btn_buscar;
    private RequestQueue requestQueue = null;


    public buscar_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_buscar_fragment, container, false);

        tnumero_documento = rootView.findViewById(R.id.tnumero_documento);
        tbodega_origen = rootView.findViewById(R.id.tbodega_origen);
        tbodega_destino = rootView.findViewById(R.id.tbodega_destino);
        tresponsable = rootView.findViewById(R.id.tresponsable);
        btn_buscar = rootView.findViewById(R.id.ibtnBuscar);
        listView = rootView.findViewById(R.id.ltsmaquinas);


        //Definiendo las varaibles de conexión del API
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();


        btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tnumero_documento != null) {
                    try {
                        //Limpiando los datos de la lista
                        listView.setText("");

                        consultarMovimiento(tnumero_documento.getText().toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return rootView;
    }

    private void consultarMovimiento(String codigo) throws JSONException {
        cliente();
        JSONObject movimiento = new JSONObject();
        movimiento.put("_id", "5e2f4c3ef80d8246f0e545d8");
        movimiento.put("Numero_Documento", codigo);


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL_MON + "/api/movimiento/buscarXNumeroDocumento", movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray movimiento = response.getJSONArray("movimientos");

                    if (movimiento.length() > 0) {
                        JSONObject mJsonObject = movimiento.getJSONObject(0);
                        tbodega_origen.setText(mJsonObject.getJSONObject("Bodega_Origen").getString("Descripcion"));
                        tbodega_destino.setText(mJsonObject.getJSONObject("Bodega_Destino").getString("Descripcion"));
                        tresponsable.setText(mJsonObject.getString("Responsable"));
                        JSONArray mJsonDetalles = mJsonObject.getJSONArray("Detalles");

                        for (int i = 0; i < mJsonDetalles.length(); i++) {
                            JSONObject objectDetalles = mJsonDetalles.getJSONObject(i);
                            String maquinas = objectDetalles.getJSONObject("productos").getString("Descripcion");
                            listView.setText(listView.getText().toString() + maquinas + "\n");
                        }

                    } else {
                        Toasty.info(getContext(), "No se encontraron movimientos", Toasty.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    public void cliente() {
        //Se crea el objeto de las peticiones a volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }
    }
}
