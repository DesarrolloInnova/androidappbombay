package innobile.itraceandroid.TrasladosTM.Controlador;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PageControllerMovimientos  extends FragmentPagerAdapter {
    int numoftabs;

    public PageControllerMovimientos(FragmentManager fm, int numoftabs) {
        super(fm);
        this.numoftabs = numoftabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new filtrar_fragment();
            case 1:
                return new buscar_fragment();
            case 2:
                return new  listar_fragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numoftabs;
    }
}
