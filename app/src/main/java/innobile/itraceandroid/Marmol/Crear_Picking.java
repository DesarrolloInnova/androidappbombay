package innobile.itraceandroid.Marmol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.MyResponse;
import innobile.itraceandroid.RetrofitAdapter.recordsets;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Crear_Picking extends AppCompatActivity {
    private Button btnDetalle;
    private TextView numeroPedido;
    public static Retrofit retrofit;
    private String V_URL_MON = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear__picking);
        btnDetalle = findViewById(R.id.btnDetalle);
        numeroPedido = findViewById(R.id.tnumero_pedido);

        //Definiendo las varaibles de conexión del API
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URLMongo();

        //Enviando el código del pedido
        numeroPedido.setText(getIntent().getExtras().getString("numero_pedido"));

        //Traer información del pedido
        consultarInformacionPedido(getIntent().getExtras().getString("numero_pedido"));

        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Crear_Picking.this, Detalle_Picking.class);
                startActivity(intent);
            }
        });
    }

    private void consultarInformacionPedido(String numero_pedido) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(V_URL_MON + "/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<recordsets>> call = jsonPlaceHolderApi.postPedidos(numero_pedido);

        call.enqueue(new Callback<List<recordsets>>() {
            @Override
            public void onResponse(Call<List<recordsets>> call, Response<List<recordsets>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }


                List<recordsets> pedido = response.body();

              for (recordsets pedidos : pedido) {
                    Toast.makeText(getApplicationContext(), pedidos.getNum_Pedido(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<recordsets>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });


        /*
        call.enqueue(new Callback<List<recordsets>>() {
            @Override
            public void onResponse(Call<List<recordsets>> call, Response<List<recordsets>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }

                List<recordsets> pedido = response.body();
                //Reinicio de la base de datos


                for (recordsets pedidos : pedido) {
                    Toast.makeText(getApplicationContext(), pedidos.getNum_Pedido(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<recordsets>> call, Throwable t) {

            }
        });*/
    }
}
