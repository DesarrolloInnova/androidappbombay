package innobile.itraceandroid.Marmol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.github.clans.fab.FloatingActionMenu;

import innobile.itrace.R;

public class ConsultarPicking extends AppCompatActivity {
    private FloatingActionMenu nuevoPicking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_picking);

        nuevoPicking = findViewById(R.id.nuevo_picking);

        nuevoPicking.findViewById(R.id.material_design_floating_action_menu_item1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConsultarPicking.this, Encabezado_Picking.class);
                startActivity(intent);
            }
        });
    }
}
