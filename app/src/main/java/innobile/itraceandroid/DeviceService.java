package innobile.itraceandroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DeviceService extends Service {

    private static final int NOTIFICATION_ID = 1;

    private static final String ACTION_SET_FOREGROUND_ACTIVITY = "avd.api.action_set_foreground";

    private static final String BUNDLE_KEY_FOREGROUND_ACTIVITY_CLASS = "avd.api.foreground_activity_class";

    private static final Logger logger = LoggerFactory.getLogger(DeviceService.class.getSimpleName());

    public static final String WAKE_LOCK_NAME = "avd.api.sampleapp.wakelock";

    private PowerManager.WakeLock wakeLock = null;

    public static void setForegroundActivity(Activity activity) {
        Intent intent = new Intent(activity, DeviceService.class);
        intent.setAction(ACTION_SET_FOREGROUND_ACTIVITY);
        intent.putExtra(BUNDLE_KEY_FOREGROUND_ACTIVITY_CLASS, activity.getClass());
        activity.startService(intent);
    }

    public static void removeForegroundActivity(Activity activity) {
        Intent intent = new Intent(activity, DeviceService.class);
        intent.setAction(ACTION_SET_FOREGROUND_ACTIVITY);
        activity.startService(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            logger.debug("Release wake lock");
        }
    }

    @SuppressLint("InvalidWakeLockTag")
    @SuppressWarnings("unchecked")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Class<? extends Activity> clazz = Connection.class;


        if (intent.getAction() == ACTION_SET_FOREGROUND_ACTIVITY) {

            if (intent.hasExtra(BUNDLE_KEY_FOREGROUND_ACTIVITY_CLASS)) {
                clazz = (Class<? extends Activity>) intent.getSerializableExtra(BUNDLE_KEY_FOREGROUND_ACTIVITY_CLASS);
            }
        }

        if (wakeLock == null) {

            logger.debug("Obtain wake lock for DeviceService");

            PowerManager mgr = (PowerManager) getSystemService(POWER_SERVICE);
            wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_NAME);
            wakeLock.acquire();
        }

        registerForegorund(clazz);

        return START_REDELIVER_INTENT;
    }

    private void registerForegorund(Class<? extends Activity> foregroundActivityClass) {

        Intent intent = new Intent(this, foregroundActivityClass);

        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getApplicationContext());

        builder.setTicker("Sample App")
                .setContentTitle("Sample App")
                // .setSmallIcon(R.drawable.sample_app_active)
                .setContentText("Active")
                .setContentIntent(pi);

        startForeground(NOTIFICATION_ID, builder.build());
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


}

