package innobile.itraceandroid.Bombay;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import avd.api.core.exceptions.ApiScannerException;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.ConsultarPorReferencia;
import innobile.itraceandroid.GlobalDatoDetalle;
import innobile.itraceandroid.SampleAppActivity;
import innobile.itraceandroid.SampleApplication;

public class ConsultaProductos  extends SampleAppActivity {


    private EditText edtBuscarProducto;
    private String V_Codigo = "";
    private String cantidadPedida = "";
    private String cantidadFaltante = "";
    private String codigoPlu, descripcion, ubicacion, undEmpaque, unidadMedida, volumen, cantEmpaque, minimo, maximo, codigoBarras;
    private SampleApplication application = null;
    private ToggleButton tbScanEngine = null;
    private RequestQueue requestQueue;
    private String datoDetalles ="";
    private ImageButton btnBuscarProducto;
    private TextView lblCodigoDeBarras, lblCodigoPlus, lblUbicacion;
    private TextView txtRecuperarCodigo, txtCodigo,txtCodigoDeBarras, txtCodigoPlus,txtDescripcion,txtUbicacion,txtUnidadMedida,  txtVolumen,txtCantidadEmpaque, txtMaximo, txtMinimo,txtUnidadEmpaque, txtPrecioxUnd, txtPrecioxMayor, txtSaldoInventario, txtCantidadPedida, txtCantidadPendiente;
    private Button btnPrecios;
    private String V_URL = null;
    private String comprobarDetalle;
    private String V_TipoConexion  = null;
    private int NVecesComprobando = 0;
    private int MY_TIMEOUT = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_productos);



        //Referenciando los elementos
        txtCodigo = findViewById(R.id.mCodigo);
        txtCodigoDeBarras = findViewById(R.id.txtCodigoDeBarras);
        txtCodigoPlus = findViewById(R.id.txtCodigoPlus);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtUbicacion = findViewById(R.id.txtUbicacion);
        txtUnidadMedida = findViewById(R.id.txtUnidadMedida);
        txtVolumen = findViewById(R.id.txtVolumen);
        txtCantidadEmpaque = findViewById(R.id.txtCantidadEmpaque);
        txtMinimo = findViewById(R.id.txtMinimo);
        txtMaximo = findViewById(R.id.txtMaximo);
        txtUnidadEmpaque = findViewById(R.id.txtUnidadEmpaque);
        btnBuscarProducto = findViewById(R.id.ibtnBuscarProducto);
        edtBuscarProducto = findViewById(R.id.edtBuscarProducto);
        txtPrecioxMayor = findViewById(R.id.txtPrecioxMayor);
        txtSaldoInventario = findViewById(R.id.txtSaldoInventario);
        txtCantidadPedida = findViewById(R.id.txtCantidadPedida);
        txtCantidadPendiente = findViewById(R.id.txtCantidadPendiente);
        txtRecuperarCodigo = findViewById(R.id.txtRecuperarCodigo);
        lblCodigoDeBarras = findViewById(R.id.lblCodigoDeBarras);
        lblCodigoPlus = findViewById(R.id.lblCodigoPlu);
        lblUbicacion= findViewById(R.id.lblUbicacion);
        btnPrecios = findViewById(R.id.btnMostrarPrecios);
        // Ocultando algunas funcionalidades
        txtCodigoDeBarras.setVisibility(View.GONE);
        txtCodigoPlus.setVisibility(View.GONE);
        txtUbicacion.setVisibility(View.GONE);
        lblCodigoDeBarras.setVisibility(View.GONE);
        lblCodigoPlus.setVisibility(View.GONE);
        lblUbicacion.setVisibility(View.GONE);

        //Estos campos faltan por traer de ka base de datos
        txtPrecioxMayor = findViewById(R.id.txtPrecioxMayor);
        txtSaldoInventario = findViewById(R.id.txtSaldoInventario);
        txtCantidadPedida = findViewById(R.id.txtCantidadPedida);
        txtCantidadPendiente = findViewById(R.id.txtCantidadPendiente);


        //Configuración de la conexión
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL=  t_configuraciones.getCon_URL();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Controla que solamente se generé una vez el objeto de la petición
        if (requestQueue == null) {
            // Instantiate the cache
            Cache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024); // 1MB cap
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(cache, network);
            // Start the queue
            requestQueue.start();
        }


        //Recupero el código que se va a buscar
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            datoDetalles = getIntent().getExtras().getString("codigo_buscar");
            edtBuscarProducto.setText(datoDetalles);
        }







        //Comprobando si se trajo un dato desde el detalle
        comprobarDetalle = GlobalDatoDetalle.comprobarTraerDetalle;

        if(comprobarDetalle.equals("1")){
            try {
                getProductData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        //Este campo busca los datos y los muestra en un RecyclerView
        btnBuscarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ConsultaProductos.this, ConsultarPorReferencia.class);
                intent.putExtra("data_buscar", edtBuscarProducto.getText().toString());
                startActivity(intent);
                finish();
            }
        });

        //Se detectan los datos ingresados en el EditText automáticamente
       edtBuscarProducto.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                        //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
                        if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB)  || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                            //Este contador se encarga de enviar los datos solo una vez
                            NVecesComprobando++;
                            try {
                                if(NVecesComprobando == 2){
                                //Se encarga de traer todos los datos de un producto
                                     getProductData();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return true;
                    }
                return false;
            }
        });

       //Botón que se activa al momento de consultar los precios
        btnPrecios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    mostrarPrecios();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    /**
     * Autor: CarlosDnl
     * Consulta todos los productos relacionado con un registro desde la BD
     * Fecha creación: 22/11/2019
     * */


    public void getProductData() throws JSONException {
        try{
            //Se crea el JSON a enviar al POST
            JSONObject jsonBody;
            jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", "01");
            jsonBody.put("Cod_Bodega", "BODGENERAL");
            jsonBody.put("Valor",edtBuscarProducto.getText().toString());

            //Se envía el código al método getCantidadPedida para obtener la cantidad pedida y la cantidad restante


            //Se trae el saldo que está configurado en un SP


            //Se configura la URL a la que se va a realizar la petición
            String url = V_URL +"/BuscarProducto";

            // private String codigoPlu, descripcion, ubicacion, undEmpaque, unidadMedida, volumen, cantEmpaque, minimo, maximo;

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se almacena la erspuesta obtenida
                        JSONArray mJsonArray = response.getJSONArray("recordset");


                        //Se recorre el JSON

                            JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                            V_Codigo  = mJsonObject.getString("CODIGO").trim();
                            codigoBarras = mJsonObject.getString("CODBARRAS");
                            codigoPlu = mJsonObject.getString("CODIGOPLU");
                            descripcion = mJsonObject.getString("DESCRIPCION");
                            ubicacion = mJsonObject.getString("UBICACION");
                            undEmpaque = mJsonObject.getString("UNDEMPAQUE");
                            unidadMedida = mJsonObject.getString("UNIDADMED");
                            volumen = mJsonObject.getString("VOLUMEN");
                            cantEmpaque = mJsonObject.getString("CANTEMPAQUE");
                            minimo = mJsonObject.getString("MINIMO");
                            maximo = mJsonObject.getString("MAXIMO");


                            //Se enviían los valores a los campos correspondientes
                            txtCodigo.setText(V_Codigo);
                            txtCodigoDeBarras.setText(codigoBarras);
                            txtCodigoPlus.setText(codigoPlu);
                            txtDescripcion.setText(descripcion);
                            txtUbicacion.setText(ubicacion);
                            txtUnidadEmpaque.setText(undEmpaque);
                            txtUnidadMedida.setText(unidadMedida);
                            txtVolumen.setText(volumen);
                            txtCantidadEmpaque.setText(cantEmpaque);
                            txtMinimo.setText(minimo);
                            txtMaximo.setText(maximo);
                            txtRecuperarCodigo.setText(V_Codigo);
                            //Se envía un campo en blanco al EditText edtBuscarProducto para borrar el código actual
                            edtBuscarProducto.setText("");
                            txtCantidadPedida.setText("0");
                            txtCantidadPendiente.setText("0");

                            //Reiniciando el contador que se encarga de enviar solo una petición
                            NVecesComprobando = 0;
                            GlobalDatoDetalle.comprobarTraerDetalle = "0";



                        getCantidadPedida( txtCodigo.getText().toString());
                        getInventoryBalance(txtCodigo.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Autor: CarlosDnl
     * Se configuran las funcionalides para recibir los datos  onResume()  onPause()
     * Fecha creación: 22/11/2019
     * */


    public void getInventoryBalance(String Codigo) throws JSONException {
        try{
            //Se configura el JSON a enviar
            JSONObject jsonBody;
            jsonBody = new JSONObject();
            jsonBody.put("Codigo", Codigo);
            jsonBody.put("Cod_Bodega", "BODEGAGENERALN");

            //La URL que se encarga de procesar la petición en el servicion REST
            String url = V_URL + "/BuscarSaldoProducto";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se obtiene el JSON
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            String saldoProducto = mJsonObject.getString("SaldoProducto");

                            //Se envía el saldo a txtSaldoInventario para mostrarlo en la pantalla
                            txtSaldoInventario.setText(saldoProducto);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar el saldo", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Autor: CarlosDnl
     * Se configuran las funcionalides para recibir los datos  onResume()  onPause()
     * Fecha creación: 22/11/2019
     * */

    @Override
    protected void onResume() {
        super.onResume();

        application = (SampleApplication)getApplication();
        application.requiredDevice = application.ANY_DEVICE;
        application.lastActivityForAnyDevice = ConsultaProductos.class;

        this.setupDefaults();
    }

    @Override
    public void onPause()
    {
        unregisterReceiver(mDeviceSelectedReceiver);
        if (isFinishing())
        {
            try { application.removeScannerActivity(); } catch (ApiScannerException e) { }
        }

        super.onPause();
    }

    /**
     * Autor: CarlosDnl
     * Se comprueba si algún dispositivo conectado
     * Fecha creación: 22/11/2019
     * */

    private final BroadcastReceiver mDeviceSelectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (application.allDevicesSelected())
                tbScanEngine.setEnabled(false);
            else
            {
            }
        }
    };

    /**
     * Autor: CarlosDnl
     * Se realiza la configuración de la PathFinder para conectarse y recibir el código leido
     * Fecha creación: 22/11/2019
     * */

    private void setupDefaults() {

        IntentFilter filter = new IntentFilter(SampleApplication.INTENT_ACTION_UPDATE_DEVICE_SELECTION);
        registerReceiver(mDeviceSelectedReceiver, filter);
        sendBroadcast(new Intent(SampleApplication.INTENT_ACTION_UPDATE_DEVICE_SELECTION));


        try {
            application.addScannerActivity();
        } catch (ApiScannerException e) {
            showMessageBox("Adding listener failed",
                    String.format("Scanner listener has not been added. Error code is %d. Scan results processing is inactive.", e.getErrorCode()),
                    "Ok", null, null, null, application.getDevice());
        }
    }

    /**
     * Autor: CarlosDnl
     * Realiza la consulta de la cantidad pedida y la cantidad restante
     * Fecha creación: 22/11/2019
     * */

    public void getCantidadPedida(String codigoProducto) throws JSONException {

        try{

            //Creando el cuerpo del JSON a enviar al POST
            JSONObject jsonBody;
            jsonBody = new JSONObject();
            jsonBody.put("Codigo", codigoProducto);

            //Consigurando la URL
            String url = V_URL + "/BuscarCantidadPedida";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se almacena la respuesat del JSON
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        //Se recorre el JSOn y se parsean los datos
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            cantidadPedida = mJsonObject.getString("CANTPEDIDA");
                            cantidadFaltante = mJsonObject.getString("CANTFALTANTE");

                            //Se envian los datos obtenidos a los TXT correspondientes
                            txtCantidadPedida.setText(cantidadPedida);
                            txtCantidadPendiente.setText(cantidadFaltante);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        }catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar cantidades", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Se encarga de mostrar los precios de los productos en una lista dentro de un AlertDialog
     * Autor: CarlosDnl
     * Fecha: 17-02-2020
     * */


    public void mostrarPrecios() throws JSONException {

        try {
            //Se crea un json que serà enviado para que se guarde en la BD remota
            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Codigo", txtRecuperarCodigo.getText().toString());


            //Creando la URL a la cual se va a realizar la peticiòn
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
            String url = t_configuraciones.getCon_URL() + "/BuscarPrecioProducto";

            //Se infla la pantalla en la que se va a mostrar la informaciòn de la bodega y la cantidad
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View myScrollView = inflater.inflate(R.layout.scroll_text, null, false);

            //Se datosCantidadBodega que va a mostrar los datos de la lista en pantalla
            final TextView datosCantidadBodega = myScrollView
                    .findViewById(R.id.textViewWithScroll);

            //Se le envia un campo vacio para evitar errores de null


            //Se realiza la peticiòn
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Se obtiene el json y se guarda en un JSONArray
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        //Se obtiene el json y se guarda en un JSONArray
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            String saldo = mJsonObject.getString("CODPRECIO");
                            String Bodega = mJsonObject.getString("PRECIO");
                            datosCantidadBodega.append("   " + saldo + " : " + Bodega + "\n");
                        }

                        //Se crear el mensaje de alerta que va a mostrar los datos de la lista cargada con los precios
                        new AlertDialog.Builder(ConsultaProductos.this).setView(myScrollView)
                                .setTitle("Lista de precios")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @TargetApi(11)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }

                                }).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar el precio", Toast.LENGTH_SHORT).show();
        }
    }

}
