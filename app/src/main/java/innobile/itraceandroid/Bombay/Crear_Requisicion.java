package innobile.itraceandroid.Bombay;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itrace.view.LoginActivity;
import innobile.itraceandroid.Consultar_Recepcion;
import innobile.itraceandroid.Global;
import innobile.itraceandroid.Ver_Detalle_Movimientos;

public class Crear_Requisicion extends AppCompatActivity {


    private int prioridadCodigo = 0;
    private TextView codigo_Encargado, tDescripcion, txtUnidades, txtcodigo, tCodigoBarras, unidades;
    private EditText tCantidad, tObservaciones, tCodigo;
    private Button procesar, Mostrar, btnRegresarMenu, btnRegresarLogin;
    private int MY_TIMEOUT = 5000;
    private String V_URL, Ruta = null, V_URL_MON = null;
    private RequestQueue requestQueue = null;
    private int contadorIngresado = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear__requisicion);
        final TextView Codigo_documento = findViewById(R.id.Codigo_requisicion);
        ImageButton ibtnBuscar = findViewById(R.id.ibtnBuscar);
        Button btnDetalle = findViewById(R.id.btnDetalle);

        tCodigo = findViewById(R.id.edtBuscarProducto);
        codigo_Encargado = findViewById(R.id.Codigo_requisicion);
        tDescripcion = findViewById(R.id.txtDescripcion);
        tCantidad = findViewById(R.id.txtCantidad);
        tObservaciones = findViewById(R.id.txtObservaciones);
        txtUnidades = findViewById(R.id.txtUnidades);
        txtcodigo = findViewById(R.id.EnviarCodigo);
        tCodigoBarras = findViewById(R.id.codigobarras);
        unidades = findViewById(R.id.txtUnidades);
        //Botones
        Mostrar = findViewById(R.id.Mostrar);
        procesar = findViewById(R.id.btnGuardar);
        btnRegresarLogin = findViewById(R.id.RegresarLogin);
        btnRegresarMenu = findViewById(R.id.RegresarMenu);

        //Se encarga de generar la fecha

        //Configurando la URL local para realizar las peticiones y concatenar con cada uno de las direcciones en la nube
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL = t_configuraciones.getCon_URL();  //Url que se encarga de la conexión desde SqlServer
        V_URL_MON = t_configuraciones.getCon_URLMongo();  //Url que se encarga de la conexión desde la nube

        //Se carga el numero del documento
        Codigo_documento.setText(getIntent().getStringExtra("codigo"));

        //Se crea el objeto de las peticiones a volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }


        //Regresa la actividad al login de la aplicación
        btnRegresarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Crear_Requisicion.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        //Encontrando el código de la prioridad seleccionada
        String valor = getIntent().getStringExtra("prioridad");
        if (valor.equals("Urgente")) {
            prioridadCodigo = 1;
        }


        btnRegresarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Crear_Requisicion.this, Consultar_Recepcion.class);
                startActivity(i);
            }
        });


        Mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    cantidadProducto();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        //Se ejecuta que método asíncrono que se encarga de repuerar el código del responsable
        try {
            nitResponsable();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        procesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    new SweetAlertDialog(Crear_Requisicion.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("¿Estas seguro?")
                            .setContentText("Si procesar este documento no podras revertir los cambios")
                            .setConfirmText("Confirmar")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    try {
                                        if (codigo_Encargado.getText().toString() != null) {
                                            procesarRequisicion(codigo_Encargado.getText().toString());
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setCancelButton("Salir", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Toasty.info(getApplicationContext(), "El proceso fue cancelado", Toast.LENGTH_SHORT, true).show();
                                }
                            })
                            .show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Crear_Requisicion.this, Ver_Detalle_Movimientos.class);
                intent.putExtra("codigo", codigo_Encargado.getText().toString());
                intent.putExtra("documento", txtcodigo.getText().toString());
                startActivity(intent);
            }
        });

        ibtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    consultarProducto();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        tCodigo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
                if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    //Este contador se encarga de enviar los datos solo una vez
                    contadorIngresado++;
                    try {
                        if (contadorIngresado == 2) {
                            consultarProducto();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Autor: CarlosDnl
     * Realiza la busqueda de los productos y los muestra en la pantalla
     * Fecha creación: 20/11/2019
     */

    public void consultarProducto() throws JSONException {
        try {
            //Comprobando que el codigo ingresado no sea un campo vacio
            if (!tCodigo.getText().toString().trim().isEmpty()) {

                JSONObject jsonBody = null;
                jsonBody = new JSONObject();

                jsonBody.put("Cod_Empresa", "01");
                jsonBody.put("Cod_Bodega", "BODGENERAL");
                jsonBody.put("Valor", tCodigo.getText().toString().trim());

                //Se configura la URL para realizar la petición
                Ruta = V_URL + "/BuscarProducto";

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (response.length() > 1) {
                                //Se guarda la respuesta el REST
                                JSONArray mJsonArray = response.getJSONArray("recordset");
                                if (mJsonArray.length() > 0) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                                    if (mJsonObject.has("CODIGO") && mJsonObject.has("CODBARRAS") && mJsonObject.has("UNIDADMED") && mJsonObject.has("DESCRIPCION")) {
                                        txtcodigo.setText(String.valueOf(mJsonObject.getString("CODIGO")).trim());
                                        tCodigoBarras.setText(String.valueOf(mJsonObject.getString("CODBARRAS")).trim());
                                        unidades.setText(String.valueOf(mJsonObject.getString("UNIDADMED")).trim());
                                        tDescripcion.setText(String.valueOf(mJsonObject.getString("DESCRIPCION")).trim());
                                        Global.CODIGO_PRODUCTO = txtcodigo.getText().toString();
                                        //Se reinicia el editext al traer un producto
                                        tCodigo.setText("");
                                        datosInsertarMovimientos();
                                        contadorIngresado = 0;
                                    } else {
                                        AlertDialog alertDialog = new AlertDialog.Builder(Crear_Requisicion.this).create();
                                        alertDialog.setTitle("Mensaje");
                                        alertDialog.setMessage("Es necesario validar este producto en el sistema");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();

                                    }


                                } else {
                                    new SweetAlertDialog(Crear_Requisicion.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Oops...")
                                            .setContentText("No pude encontrar el codigo ingresado")
                                            .show();


                                    tCodigo.setText("");
                                    contadorIngresado = 0;
                                }
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(Crear_Requisicion.this).create();
                                alertDialog.setTitle("Mensaje");
                                alertDialog.setMessage("El servidor regresó muchos datos");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.show();

                                tCodigo.setText("");
                                contadorIngresado = 0;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy(
                        MY_TIMEOUT * 5,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(request);
            } else {
                new SweetAlertDialog(Crear_Requisicion.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Houston, tenemos un problema...")
                        .setContentText("Es necesario ingresar un codigo")
                        .show();

            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de motrar los productos", Toast.LENGTH_SHORT).show();
        } catch (OutOfMemoryError e) {
            Toast.makeText(getApplicationContext(), "Error de memoria", Toast.LENGTH_SHORT).show();
        } finally {
            // get ready to be fired by your boss
        }

    }


    /**
     * Se encarga de obtener la cantidad en existencias que se encuentran en la bodega de un determinado producto
     * Fecha creación: 20/11/2019
     */


    public void cantidadProducto() throws JSONException {
        try {

            //Se crea un json que serà enviado para que se guarde en la BD remota
            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Codigo", Global.CODIGO_PRODUCTO);
            jsonBody.put("Cod_Bodega", "");


            //Creando la URL a la cual se va a realizar la peticiòn
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
            String url = t_configuraciones.getCon_URL() + "/BuscarSaldoProducto";

            //Se infla la pantalla en la que se va a mostrar la informaciòn de la bodega y la cantidad
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View myScrollView = inflater.inflate(R.layout.scroll_text, null, false);

            //Se datosCantidadBodega que va a mostrar los datos de la lista en pantalla
            final TextView datosCantidadBodega = myScrollView
                    .findViewById(R.id.textViewWithScroll);

            //Se le envia un campo vacio para evitar errores de null
            datosCantidadBodega.setText("");
            //Se realiza la peticiòn
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Se obtiene el json y se guarda en un JSONArray
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        //Se obtiene el kson y se guarda en un JSONArray
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            if (mJsonObject.has("SaldoProducto") && mJsonObject.has("BODEGA")) {
                                String saldo = mJsonObject.getString("SaldoProducto");
                                String Bodega = mJsonObject.getString("BODEGA");
                                TextView txtTotalItems = findViewById(R.id.txtTotalItems);
                                txtTotalItems.setText(saldo);
                                datosCantidadBodega.append("   " + Bodega + " : " + saldo + "\n");
                            }
                        }

                        //Se crear el mensaje de alerta que va a mostrar los datos de la lista
                        new AlertDialog.Builder(Crear_Requisicion.this).setView(myScrollView)
                                .setTitle("Cantidad")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @TargetApi(11)
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }

                                }).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Se encarga de insertar los datos en la tabla movimientos
     * Fecha creación: 20/11/2019
     */

    public void enviarDatosApi() {
        try {

            //Creando el objeto para realizar la petición a la URL usando Volley
            Ruta = V_URL + "/InsertarTablaMovimientos";
            //Se crea un json que serà enviado para que se guarde en la BD remota
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", Global.CODIGO_EMPRESA);
            jsonBody.put("Numero_Documento", Global.CODIGO_REQUISICION);
            jsonBody.put("Tipo_Documento", Global.TIPO_MOVIMIENTO);
            jsonBody.put("Codigo_Barras", Global.CODIGO_BARRAS);
            jsonBody.put("Codigo", Global.CODIGO_LEIDO);
            jsonBody.put("Descripcion", Global.DESCRIPCION);
            jsonBody.put("Cod_Bodega", "BODEGAGENERALN");
            jsonBody.put("Cantidad", Global.mCANTIDAD);
            jsonBody.put("Unidad_Medida", Global.UNIDAD_MEDIDA);
            jsonBody.put("Unidad_Empaque", Global.UNIDAD_EMPAQUE);
            jsonBody.put("Observaciones", Global.OBSERVACIONES);
            jsonBody.put("Prioridad", Global.PRIORIDAD);
            jsonBody.put("Nit_Responsable", Global.NIT_RESPONSABLE);
            jsonBody.put("Codigo_Usuario", Cons.Usuario);
            jsonBody.put("Centro_Costos", Global.CODIGO_CENTRAL);
            jsonBody.put("Nro_Dcto_Afectado", "0");
            jsonBody.put("Nro_Dcto_Tercero", "");
            jsonBody.put("Nombre_Usuario", Cons.Usuario);
            jsonBody.put("Nit_Tercero", "43645438-7");

            //Este if se encarga de controlar que el esquema solo sea creado una vez
            if (Global.CREAR_REQUISICON_MONGO) {
                //Se ejecuta que método asíncrono que se encarga de crear el esquema en mongo
                encabezadoRequisicion();
                //Cambio el estado de la variable para que no ingrese de nuevo
                Global.CREAR_REQUISICON_MONGO = false;

            }

            detallesRequisicion();


            //Se guarda el JSON creado para enviarlo al servicio REST
            final String requestBody = jsonBody.toString();


            //Se realiza la peticiòn y se controlan las respuestas y los errore
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            stringRequest.setShouldCache(false);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /**
     * Método para procesar la requisiciòn, se ejecuta un procedimiento que se encarga de procesar los datos en la BD
     * Fecha creación: 20/11/2019
     */

    public void datosInsertarMovimientos() {
        try {
            //Inicializando la prioridad en 0


            //Se rellenan las variables con los datos que van a enviar a la BD
            Global.CODIGO_EMPRESA = "01";
            Global.CODIGO_REQUISICION = codigo_Encargado.getText().toString();
            Global.TIPO_MOVIMIENTO = Global.TIPO_DOCUMENTO;
            Global.CODIGO_BARRAS = tCodigoBarras.getText().toString();
            Global.CODIGO_LEIDO = txtcodigo.getText().toString();
            Global.DESCRIPCION = tDescripcion.getText().toString();
            Global.BODEGA = Global.CENTRAL_COSTO;
            Global.mCANTIDAD = tCantidad.getText().toString();
            Global.UNIDAD_MEDIDA = unidades.getText().toString();
            Global.UNIDAD_EMPAQUE = unidades.getText().toString();
            Global.OBSERVACIONES = tObservaciones.getText().toString();
            Global.PRIORIDAD = String.valueOf(prioridadCodigo);
            Global.NIT_RESPONSABLE = Global.NIT_ENCARGADO;
            Global.CODIGO_USUARIOS = "10123";

            enviarDatosApi();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }


    }


    /**
     * Método para procesar la requisiciòn, se ejecuta un procedimiento que se encarga de procesar los datos en la BD
     * Fecha creación: 20/11/2019
     */

    public void procesarRequisicion(String codigoDocumento) throws JSONException {
        try {
            //Creando el objeto para realizar la petición a la URL usando Volley
            Ruta = V_URL + "/InsertarMovimientosERP";
            //Se crea un objetoObject para realizar el POST al servicio Rest
            JSONObject jsonBody = new JSONObject();


            jsonBody.put("Cod_Empresa", "01");
            jsonBody.put("Numero_Documento", codigoDocumento);
            jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
            jsonBody.put("Origen", "COM");

            //Se realiza la peticiòn y se controlan las respuesta y los posibles errores
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String Respuesta = null;
                    String Mensaje = null;
                    //creo Objeto Json para entrar al Output
                    JSONObject JOutput = null;
                    JSONArray mJsonArray = null;

                    try {
                        JOutput = response.getJSONObject("output");
                        Respuesta = JOutput.optString("Respuesta");
                        Mensaje = JOutput.optString("Mensaje");
                        if (Respuesta.equals("1")) {


                            new SweetAlertDialog(Crear_Requisicion.this)
                                    .setTitleText(Mensaje)
                                    .show();


                            String[] separandoCodigo = Mensaje.split(" ");
                            String codigo = separandoCodigo[2];
                            procesarMongoDB(codigo);

                        } else {
                            new SweetAlertDialog(Crear_Requisicion.this)
                                    .setTitleText(Mensaje)
                                    .show();
                        }

                        //Se cambia el estado del procesado en MongoDB

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al procesar la requsicion", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Método para consultar el código de la central
     * Autor: CarlosDnl
     * Fecha creación: 20/11/2019
     */

    public void codigoCentral() throws JSONException {
        try {
            //Creando el objeto para realizar la petición a la URL usando Volley
            Ruta = V_URL + "/BuscarCentroCostos";

            ///Creando el json que se enviara al Rest
            final JSONObject jsonBody = new JSONObject("{ \"Valor\":\"" + getIntent().getStringExtra("central_costo") + "\"}");

            //Se realizar la peticiòn a la URL especificando el Mètodo como un POST
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Guardando la respuesta como un JSONArray
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        //Recorriendo la respuesta para obtener los valores

                        //Se extrae la respuesta del objeto
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            //Enviando el còdigo del encargado a un textView oculto en la Activity para conservar su valor
                            Global.CODIGO_CENTRAL = mJsonObject.getString("CODIGO").trim();
                        }

                        /**
                         * Métodos que se usan para guardar los datos en mongoDB
                         * */
/*
                        try {
                            encabezadoRequisicion();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
*/


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Método para consultar el código del responsable
     * Autor: CarlosDnl
     * Fecha creación: 20/11/2019
     */

    public void nitResponsable() throws JSONException {
        try {
            //Creando la URL a la cual se va a realizar la peticiòn
            Ruta = V_URL + "/BuscarResponsables";


            ///Creando el json que se enviara al Rest
            final JSONObject jsonBody = new JSONObject("{ \"Valor\":\"" + getIntent().getStringExtra("responsable") + "\"}");

            //Se realizar la peticiòn a la URL especificando el Mètodo como un POST
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Guardando la respuesta como un JSONArray
                        JSONArray preguntasJA = response.getJSONArray("recordset");
                        //Recorriendo la respuesta para obtener los valores
                        JSONObject mJsonObject = preguntasJA.getJSONObject(0);
                        if (mJsonObject.has("NIT")) {
                            Global.NIT_ENCARGADO = mJsonObject.getString("NIT");
                        } else {
                            Global.NIT_ENCARGADO = "123132";
                        }


                        //Se ejecuta que método asíncrono que se encarga de repuerar el código de la central
                        codigoCentral();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al obtener el nit del responsable", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Se configuran los métodos para guardar los datos en MongoDB
     * encabezadoRequisicion() --> Se encarga de crear la estructura del movimientos
     * detallesRequisicion() --> Se encarga de insertar los productos dentro de la estructura del movimientos
     * Autor:CarlosDnl
     * Fecha: 12-02-2020
     */


    public void encabezadoRequisicion() throws JSONException {
        try {
            String Ruta = null;

            JSONObject movimiento = new JSONObject();


            movimiento.put("Cod_Empresa", "01");
            movimiento.put("Numero_Documento", codigo_Encargado.getText().toString().trim());
            movimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO.trim());
            movimiento.put("Prioridad", String.valueOf(prioridadCodigo).trim());
            movimiento.put("Nit_Responsable", Global.NIT_ENCARGADO.trim());
            movimiento.put("Codigo_Usuario", "10123");
            movimiento.put("Centro_Costos", Global.CODIGO_CENTRAL);
            movimiento.put("Nro_Dcto_Afectado", "0000");
            movimiento.put("Nombre_Usuario", Cons.Usuario.trim());

            Ruta = V_URL_MON + "/api/Movimiento/guardar";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, movimiento, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {


                    //Guardando la respuesta como un JSONArray
                    JSONArray mJsonArray = null;
                    try {
                        mJsonArray = response.getJSONArray("output");
                        //Recorriendo la respuesta para obtener los valores
                        JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                        String mensaje = mJsonObject.getString("Mensaje");
                        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    public void detallesRequisicion() throws JSONException {

        String Ruta = null;

        JSONObject detalles = new JSONObject();

        detalles.put("Numero_Documento", Global.CODIGO_REQUISICION);
        detalles.put("Codigo_Barras", Global.CODIGO_BARRAS);
        detalles.put("Codigo", Global.CODIGO_LEIDO);
        detalles.put("Descripcion", Global.DESCRIPCION);
        detalles.put("Cod_Bodega", "BODEGAGENERALN");
        detalles.put("Cantidad", Global.mCANTIDAD);
        detalles.put("Unidad_Medida", Global.UNIDAD_MEDIDA);
        detalles.put("Unidad_Empaque", Global.UNIDAD_EMPAQUE);
        detalles.put("Observaciones", Global.OBSERVACIONES);


        Ruta = V_URL_MON + "/api/Movimiento/actualizarMovimiento";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, detalles, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");
                    //Recorriendo la respuesta para obtener los valores
                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Crear_Requisicion.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    private void procesarMongoDB(String Nro_Dcto_Afectado) {
        try {

            //Se crea el objeto que se encarga de generar la fecha

            //Configurando la fecha
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String Ruta = null;

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();

            jsonBody.put("Cod_Empresa", "01");
            jsonBody.put("Numero_Documento", Global.CODIGO_REQUISICION);
            jsonBody.put("Tipo_Documento", Global.TIPO_MOVIMIENTO);
            jsonBody.put("Nro_Dcto_Afectado", Nro_Dcto_Afectado);
            jsonBody.put("Fecha_Procesado", formatter.format(date));

            Ruta = V_URL_MON + "/api/Movimiento/cambiarEstado";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Crear_Requisicion.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}