package innobile.itraceandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.Bombay.Crear_Requisicion;

public class encabezado_Requisiciones extends AppCompatActivity {

    private AutoCompleteTextView CentralCosto, Responsable, Prioridad;
    private ImageView ImageCentralCosto, ImagenResponsable, ImagenPrioridad;
    private String[] lstCentroCostos = new String[]{"Sin datos"};
    private String[] lstResponsable = new String[]{"Sin datos"};
    private String[] lstPrioridad = new String[]{"Normal", "Urgente"};
    private Button btnDetalle;
    private TextView Nro_Docto;
    private String Cod_Empresa = "01";
    private String Tipo_Docto = "";
    private Boolean band = true, ComprobarData = true;
    private TextView fecha;
    Date date = new Date();
    private String V_URL = null;
    private String comprobarConsulta = "";
    private String V_TipoConexion = null;
    private Button btnNavegacion;
    int MY_TIMEOUT = 2000;
    private RequestQueue requestQueue;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_requisicion);
        Prioridad = findViewById(R.id.autoCompletePrioridad);
        CentralCosto = findViewById(R.id.autoCompleteCentroCosto);
        Responsable = findViewById(R.id.autoCompleteResponsable);
        ImagenPrioridad = findViewById(R.id.ImagenPrioridad);
        ImageCentralCosto = findViewById(R.id.ImagenCentralCosto);
        ImagenResponsable = findViewById(R.id.ImagenResponsable);
        Nro_Docto = findViewById(R.id.txtNroDocto);
        btnDetalle = findViewById(R.id.Buttondetalle);
        CentralCosto.setThreshold(2);
        CentralCosto.setThreshold(1);
        fecha = findViewById(R.id.MostrarFecha);
        btnNavegacion = findViewById(R.id.btnNavegacion);

        try {

            //Activando la opciones para navegar desde el encabezado
            btnNavegacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(encabezado_Requisiciones.this, DefinirRuta.class);
                    startActivity(intent);
                    finish();
                }
            });

            //Configurando volley para enviar las peticiones
            if (requestQueue == null) {
                //Configurando la petición por Volley
                // Instantiate the cache
                Cache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024); // 1MB cap
                // Set up the network to use HttpURLConnection as the HTTP client.
                Network network = new BasicNetwork(new HurlStack());
                // Instantiate the RequestQueue with the cache and network.
                requestQueue = new RequestQueue(new NoCache(), network);
                // Start the queue
                requestQueue.start();
            }


            //Configurando el tipo de documento
            Tipo_Docto = Global.TIPO_DOCUMENTO;

            //Configurando la fecha
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            fecha.setText(formatter.format(date));

            //URL a la que se le va a realizar la peticion
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
            V_URL = t_configuraciones.getCon_URL();
            V_TipoConexion = t_configuraciones.getCon_TipoConexion();


            try {
                ConsultarNuevoNroDcto();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(encabezado_Requisiciones.this, android.R.layout.simple_spinner_dropdown_item, lstPrioridad);
            Prioridad.setAdapter(adapterCentralCosto);


            try {
                CargarCentroCostos("*");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                CargarResponsables("*");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            ImagenPrioridad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Prioridad.showDropDown();
                }
            });

            ImageCentralCosto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CentralCosto.showDropDown();
                }
            });

            ImagenResponsable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Responsable.showDropDown();
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de cargar el encabezado", Toast.LENGTH_SHORT).show();
        }


        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //COMPROBAR QUE TODOS LOS CAMPOS FUERON SELECCIONADOS:  PRIORIDAD, CENTRO DE COSTOS Y RESPONSABLE
                    if (ComprobarData == true) {


                        GuardarEncabezadoLocalmente(Tipo_Docto.toString(), Integer.parseInt(Nro_Docto.getText().toString()), Prioridad.getText().toString(), CentralCosto.getText().toString(), Responsable.getText().toString());
                        //LLAMA FORMULARION Y MANDA PARAMETROS

                        if (Tipo_Docto.equals("RQ")) {
                            Intent intent = new Intent(encabezado_Requisiciones.this, Crear_Requisicion.class);
                            intent.putExtra("codigo", Nro_Docto.getText().toString());
                            intent.putExtra("prioridad", Prioridad.getText().toString());
                            intent.putExtra("central_costo", CentralCosto.getText().toString());
                            intent.putExtra("responsable", Responsable.getText().toString());
                            intent.putExtra("comprobarConsulta", comprobarConsulta);
                            startActivity(intent);
                        } else if (Tipo_Docto.equals("RM")) {
                            Intent intent = new Intent(encabezado_Requisiciones.this, OrdeCompra.class);
                            intent.putExtra("codigo", Nro_Docto.getText().toString());
                            intent.putExtra("prioridad", Prioridad.getText().toString());
                            intent.putExtra("central_costo", CentralCosto.getText().toString());
                            intent.putExtra("responsable", Responsable.getText().toString());
                            intent.putExtra("comprobarConsulta", comprobarConsulta);
                            startActivity(intent);
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Comprueba los datos ingresados", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Se controló un error, pulsa de nuevo el botón", Toast.LENGTH_SHORT).show();
                }

                ComprobarData = ComprobarCamposVacios();
            }
        });


    }

    public void CargarCentroCostos(String centralCostoDato) throws JSONException {
        try {

            JSONObject jsonBuscarCentroCostos = null;
            jsonBuscarCentroCostos = new JSONObject();
            jsonBuscarCentroCostos.put("Valor", centralCostoDato);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarCentroCostos", jsonBuscarCentroCostos, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                        String centralCosto;  //Esta varaible va a quitar los espacions de la respuesta del json


                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstCentroCostos = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            centralCosto = mJsonObject.getString("NOMBRE");
                            lstCentroCostos[i] = centralCosto.trim();

                        }
                        ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(encabezado_Requisiciones.this, android.R.layout.simple_spinner_dropdown_item, lstCentroCostos);
                        CentralCosto.setAdapter(adapterCentralCosto);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_Requisiciones.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }


    }

    public void CargarResponsables(String ResponsableDato) throws JSONException {

        try {

            JSONObject jsonBuscarResponsables = null;
            jsonBuscarResponsables = new JSONObject();
            jsonBuscarResponsables.put("Valor", ResponsableDato);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarResponsables", jsonBuscarResponsables, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String texto;

                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstResponsable = new String[preguntasJA.length()];

                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            texto = mJsonObject.getString("NOMBRE");
                            lstResponsable[i] = texto.trim();
                        }

                        ArrayAdapter<String> adapterResponsables = new ArrayAdapter<String>(encabezado_Requisiciones.this, android.R.layout.simple_spinner_dropdown_item, lstResponsable);
                        Responsable.setAdapter(adapterResponsables);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_Requisiciones.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    public void GuardarEncabezadoLocalmente(String Tipo_Dcto, int Nro_Docto, String prioridad, String central_costo, String responsable) {
        try {
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_ENCABEZADO, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.TIPO_DOCUMENTO, Tipo_Dcto);
            values.put(Utilidades.NUMERO_DOCUMENTO, Nro_Docto);
            values.put(Utilidades.PRIORIDAD, prioridad);
            values.put(Utilidades.CENTRAL_COSTO, central_costo);
            values.put(Utilidades.RESPONSABLE, responsable);


            db.insert(Utilidades.TABLA_ENCABEZADO, null, values);
            db.close();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Este método se encarga de mostrar el código actual a mostrar en un tipo de movimiento RQ o RC
     * Autor: CarlosDnl
     * Fecha: 05/12/2019
     */


    public void ConsultarNuevoNroDcto() throws JSONException {
        try {
            //Creando el json con los parametros que va a recibir el API
            JSONObject jsonBuscarConsecutivoMov = null;
            jsonBuscarConsecutivoMov = new JSONObject();

            jsonBuscarConsecutivoMov.put("Cod_Empresa", Cod_Empresa);
            jsonBuscarConsecutivoMov.put("Tipo_Documento", Tipo_Docto);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarConsecutivoMov", jsonBuscarConsecutivoMov, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        if (mJsonArray.length() >= 1) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                            String referencia = mJsonObject.getString("Consecutivo");
                            int nuevo_valor = 1;
                            nuevo_valor = nuevo_valor + Integer.parseInt(referencia.trim());

                            Nro_Docto.setText(String.valueOf(nuevo_valor));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Se encarga de comprobar que los campos ingresados en los Spinners sean correctos
     * Autor: CarlosDnl
     * Fecha: 17-02-2020
     */

    public Boolean ComprobarCamposVacios() {
        try {
            band = true;
            if (Prioridad.getText().toString().equals("") || CentralCosto.getText().toString().equals("") || Responsable.getText().toString().equals("")) {
                band = false;
                Toast.makeText(getApplicationContext(), "Es necesario ingresar todos los datos", Toast.LENGTH_SHORT).show();
            }
            if (Prioridad.getText().toString().equals("Normal") || Prioridad.getText().toString().equals("Urgente")) {
                band = true;
            } else {
                band = false;
                Toast.makeText(getApplicationContext(), "La prioridad debe ser Normal o Urgente", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al obtener el nit del responsable", Toast.LENGTH_SHORT).show();
        }

        return band;
    }

}





