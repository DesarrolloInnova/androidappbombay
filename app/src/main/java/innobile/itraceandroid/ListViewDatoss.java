package innobile.itraceandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;

import static android.location.Location.convert;

public class ListViewDatoss extends AppCompatActivity {
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_datoss);
        try {
            getDescriptionData();
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public void getDescriptionData() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        TextView txtDescripcionSeach = findViewById(R.id.txtDescripcionSeach);

        String dato = getIntent().getStringExtra("nombre");

        if(dato.equals("")) {
            dato = "000000";
        }


        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Cod_Bodega", "BODGENERAL");
        jsonBody.put("Valor", dato);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        String url =  t_configuraciones.getCon_URL() + "/BuscarProducto";

        final TextView textView6 = findViewById(R.id.textView6);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {




                    lv =  findViewById(R.id.ListDataView);
                    JSONArray mJsonArray = response.getJSONArray("recordset");

                    List<String> listadata = new ArrayList<String>();

                    if (mJsonArray  != null) {
                        for (int i=0;i<mJsonArray.length();i++){
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            String ean = mJsonObject.getString("CODIGO");
                            String descripcion = mJsonObject.getString("DESCRIPCION");
                            listadata.add(ean + '\n' + descripcion);
                        }

                        String[] stockArr = new String[listadata.size()];
                        stockArr = listadata.toArray(stockArr);

                        lv.setAdapter(new ArrayAdapter<String>(ListViewDatoss.this,android.R.layout.simple_list_item_1 ,  stockArr));


                        // This is the array adapter, it takes the context of the activity as a
                        // first parameter, the type of list view as a second parameter and your
                        // array as a third parameter.

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show());

        queue.add(request);

    }
}
