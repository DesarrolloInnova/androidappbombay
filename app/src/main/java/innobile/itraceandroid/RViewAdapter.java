package innobile.itraceandroid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.GetDetalleAdapter;
import innobile.itrace.transport.T_Configuraciones;

public class RViewAdapter extends RecyclerView.Adapter<RViewAdapter.DataObjectHolder> {

    private Context context;
    private ArrayList<GetDetalleAdapter> listaCoches;

    public RViewAdapter(Context context, ArrayList<GetDetalleAdapter> listaCoches) {
        this.context = context;
        this.listaCoches = listaCoches;
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {

        private ImageView img;
        private TextView txtNombre;
        private TextView txtDescripcion;
        public RelativeLayout layoutABorrar;


        public DataObjectHolder(@NonNull View itemView) {
            super(itemView);
            this.img = itemView.findViewById(R.id.img);
            this.txtNombre = itemView.findViewById(R.id.txtNombre);
            this.txtDescripcion = itemView.findViewById(R.id.lblDescripcion);
            this.layoutABorrar = itemView.findViewById(R.id.layoutABorrar);
        }
    }

    public void removeItem(int position) {
        notifyItemRemoved(position);
        Toast.makeText(context, "El dato se borro correctamente", Toast.LENGTH_SHORT).show();
    }


    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.requisicion_row_item_detalle, viewGroup, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {

        holder.txtNombre.setText(listaCoches.get(position).getCodigo());
        holder.txtDescripcion.setText(listaCoches.get(position).getDescripcion());

        //Glide.with(context).load(listaCoches.get(position).getImg()).into(holder.img);

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Position: " +
                        holder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaCoches.size();
    }


    public void BorrarElemento(String Numero_Documento, String codigo) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(context);
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
            String URL = t_configuraciones.getCon_URL() + "/Eliminar";

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("Numero_Documento", Numero_Documento);
            jsonBody.put("codigo", codigo);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

