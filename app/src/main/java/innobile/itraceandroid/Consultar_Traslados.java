package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.TrasladosTM.encabezado_Traslados;


public class Consultar_Traslados extends AppCompatActivity {

    public DeleteRequisicionData mAdapter;
    private SwipeController swipeController = null;
    private JSONObject jsonBody = null;
    private FloatingActionMenu floatingActionButton;
    private RecyclerView recyclerView;
    private String V_URL, V_URL_MON = null;
    private String V_TipoConexion = null;
    private JSONArray mJsonArray = null;
    private String comprobarConsulta = "", tipoMovimiento = "", tipo_Movimiento = "";
    private List<GetDataAdapter> mlstRequisicionData = new ArrayList<>();
    private TextView nombreMovimiento;
    private String total_acumulado = "0";
    private RequestQueue requestQueue;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_traslados);

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(cache, network);

        // Start the queue
        requestQueue.start();


        //Almacenando variable para comprobar el tipo de movimiento
        tipo_Movimiento = Global.TIPO_DOCUMENTO;

        //Instancionando el objeto para realizar las peticiones

        //Creando el recyclerView
        recyclerView = findViewById(R.id.recyclerviewid);
        nombreMovimiento = findViewById(R.id.nombreMovimiento);


        nombreMovimiento.setText("TRASLADOS");

        floatingActionButton = findViewById(R.id.material_design_android_floating_action_menu);
        assert floatingActionButton != null;

        //Pasando referencia de la lista global
        mAdapter = new DeleteRequisicionData(getApplicationContext(), mlstRequisicionData);


        //Configurando la URL local para realizar las peticiones y concatenar con cada uno de las direcciones en la nube
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Cargando la nueva actividad usando el FloatingMenu
        floatingActionButton.findViewById(R.id.material_design_floating_action_menu_item1).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //Comprobando el tipo de movimiento para cargar el encabezado correspondiente
                Intent intent = new Intent(Consultar_Traslados.this, encabezado_Traslados.class);
                startActivity(intent);
                finish();

            }
        });

        swipeRefreshLayout = findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        BuscarMovimientos();
                    }
                }
        );

        //Llamado el JSON para mostrar las requisiciones
        BuscarMovimientos();

        //Cargando la vista del RecyclerView

        //Creando la vista de RecyclerView
        setupRecyclerView();


    }

    /**
     * Se encarga de traer las requisiciones y los datos al momento de recibir la mercancia
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    private void BuscarMovimientos() {
        try {
            swipeRefreshLayout.setRefreshing(false);
            String Ruta = null;
            JSONObject jsonBuscarMovimiento = null;
            jsonBuscarMovimiento = new JSONObject();

            if (TipoConexion.TIPO_CONEXION.equals("1")) {
                jsonBuscarMovimiento.put("Cod_Empresa", "01");
                jsonBuscarMovimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
                jsonBuscarMovimiento.put("Estado", "IN");
                Ruta = V_URL + "/BuscarMovimientos";

                final JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarMovimiento,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Esta es la lista global
                                mlstRequisicionData.clear();
                                String Respuesta = null;
                                String Mensaje = null;
                                //creo Objeto Json para entrar al Output
                                JSONObject JOutput = null;
                                JSONArray mJsonArray = null;


                                try {

                                    JOutput = response.getJSONObject("output");
                                    Respuesta = JOutput.optString("Respuesta");
                                    Mensaje = JOutput.optString("Mensaje");

                                    if (Respuesta.equals("1")) {

                                        mJsonArray = response.getJSONArray("recordset");
                                        for (int i = 0; i < mJsonArray.length(); i++) {

                                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                            GetDataAdapter requisicionData = new GetDataAdapter();
                                            requisicionData.setCodigo(mJsonObject.getString("Nro_Dcto"));
                                            requisicionData.setFecha(mJsonObject.getString("Fecha"));
                                            mlstRequisicionData.add(requisicionData);
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), Mensaje, Toast.LENGTH_SHORT).show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                // Notifica los cambios al adaptador
                                mAdapter.notifyDataSetChanged();


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                );
                requestQueue.add(getRequest);
            }


            if (TipoConexion.TIPO_CONEXION.equals("2")) {

                jsonBuscarMovimiento.put("Cod_Empresa", Cons.Empresa);
                jsonBuscarMovimiento.put("Tipo_Documento", "AF");
                jsonBuscarMovimiento.put("Estado_Doc", "IN");

                Ruta = V_URL_MON + "/api/Movimiento/BuscarMovimientos";

                final JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBuscarMovimiento,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Esta es la lista global
                                mlstRequisicionData.clear();
                                String Respuesta = null;
                                String Mensaje = null;
                                //creo Objeto Json para entrar al Output
                                JSONObject JOutput = null;
                                //creo Objeto Array Json para entrar al Recordset
                                JSONArray mJsonArray = null;

                                try {


                                    JOutput = response.getJSONObject("output");
                                    Respuesta = JOutput.optString("Respuesta");
                                    Mensaje = JOutput.optString("Mensaje");

                                    if (Respuesta.equals("1")) {

                                        mJsonArray = response.getJSONArray("recordset");
                                        for (int i = 0; i < mJsonArray.length(); i++) {

                                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                            GetDataAdapter requisicionData = new GetDataAdapter();
                                            requisicionData.setCodigo(mJsonObject.getString("Numero_Documento"));
                                            requisicionData.setFecha(mJsonObject.getString("Fecha_Creacion"));
                                            mlstRequisicionData.add(requisicionData);
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), Mensaje.toString(), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                // Notifica los cambios al adaptador
                                mAdapter.notifyDataSetChanged();


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                );

                requestQueue.add(getRequest);
            }


        } catch (Error e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Se añaden las funciones a los items del RecyclerView y permite borrar los datos
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    private void setupRecyclerView() {
        try {
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(mAdapter);

            swipeController = new SwipeController(new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {


                    cantidadDescontar(mAdapter.players.remove(position).getCodigo());

/*
                    try {
                        BorrarRegistro( mAdapter.players.remove(position).getCodigo());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
*/

                    //Contiene la posicición del Item que fue eliminaod
                    mAdapter.notifyItemRemoved(position);
                    mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

                }


            });

            //Se crear el touch para añadir funcionalidad al presionar cada uno de los items
            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
            itemTouchhelper.attachToRecyclerView(recyclerView);

            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                    swipeController.onDraw(c);
                }
            });
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Se encarga de borrar los datos en al API al deslizar el item y presionar el botón ELIMINAR
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    public void BorrarRegistro(String Codigo_Eliminar) throws JSONException {

        String Ruta = null;
        //Se crea el JSON que contiene el Codigo a eliminar
        JSONObject jsonEliminarMovimiento = null;
        jsonEliminarMovimiento = new JSONObject();

        if (V_TipoConexion.trim().equals(Cons.SERVER)) {

            jsonEliminarMovimiento.put("Cod_Empresa", "01");
            jsonEliminarMovimiento.put("Numero_Documento", Codigo_Eliminar);
            jsonEliminarMovimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO);

            Ruta = V_URL + "/EliminarMovimientos";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonEliminarMovimiento, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
        }

        if (TipoConexion.TIPO_CONEXION.equals("2")) {


            jsonEliminarMovimiento.put("Numero_Documento", Codigo_Eliminar);
            Ruta = V_URL_MON + "/api/Movimiento/eliminarXDocumento";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonEliminarMovimiento, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(request);
        }
    }


    private void cantidadDescontar(final String codigo) {
        String Ruta = null;
        Ruta = V_URL_MON + "/api/movimiento/mostrarTraslados?valor=" + codigo;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, Ruta, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = jsonArray.getJSONObject(0);
                            JSONArray array = jsonObject.getJSONArray("Detalles");
                            for (int j = 0; j < array.length(); ++j) {
                                JSONObject jsonDetalle = array.getJSONObject(j);
                                String id_producto = jsonDetalle.getJSONObject("productos").getString("_id");
                                actualizarReserva(id_producto);
                            }


                            BorrarRegistro(codigo);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        mAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(Consultar_Traslados.this, "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        requestQueue.add(request);

    }


    public void actualizarCantidadReservada(String cantidadRestar) throws JSONException {

        String Ruta = null;


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", Global_Detalles.CODIGO_PRODUCTO);
        jsonObject.put("id_bodega", Variables_Globales.BODEGA_ORIGEN);
        jsonObject.put("Cantidad_Reservada", Integer.parseInt(cantidadRestar) * -1);

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/producto/ActualizarCantRerservada";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Consultar_Traslados.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Consultar_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });


        requestQueue.add(request);


    }

    public void actualizarReserva(String id_producto) throws JSONException {

        String Ruta = null;


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id", id_producto);
        jsonObject.put("V_Reservado", "false");

        //Toast.makeText(Ver_Detalle_Movimientos.this, "Dato en la nube" + String.valueOf(Integer.parseInt(Global_Detalles.CANTIDAD_PRODUCTO) - Integer.parseInt(cantidadRestar)), Toast.LENGTH_SHORT).show();

        Ruta = V_URL_MON + "/api/ActivosFijos/actualizaReserva";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Ruta, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray
                JSONArray mJsonArray = null;
                try {
                    mJsonArray = response.getJSONArray("output");

                    //Recorriendo la respuesta para obtener los valores

                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    String mensaje = mJsonObject.getString("Mensaje");

                    AlertDialog alertDialog = new AlertDialog.Builder(Consultar_Traslados.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage(mensaje);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Consultar_Traslados.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });


        requestQueue.add(request);

    }


}