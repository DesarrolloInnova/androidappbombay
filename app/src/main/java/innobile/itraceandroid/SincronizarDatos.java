package innobile.itraceandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.DAOInventario;
import innobile.itrace.transport.T_Configuraciones;

public class SincronizarDatos extends AppCompatActivity {
    String vdocumento, nombreArchivo = "prueba";
    private RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sincronizar_datos);
        queue =  new Volley().newRequestQueue(this);
        Button Generar = findViewById(R.id.btnGenerar);
        Button btnDescargar = findViewById(R.id.btnDescargarDatos);
        Button button = findViewById(R.id.button);

        Generar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    nombreArchivo = vdocumento + "-" + simpleDateFormat.format(new Date());
                    GenerarArchivo(nombreArchivo);
                } catch (Exception ex) {

                }
            }
        });

        Boolean band = ComprobarSwitch();

        //Sincronizar datos con la nube
        try{
            if(band){
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            SincronizarDatos(0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }else if(band == false){

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            SincronizarDatos(1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }else{
                Toast.makeText(getApplicationContext(), "La opciòn elegida es incorrecta", Toast.LENGTH_SHORT).show();
            }


        }catch (Error e){
            Toast.makeText(getApplicationContext(),"Error al sincronziar", Toast.LENGTH_SHORT).show();
        }





        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DownloadAllData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void GenerarArchivo(String NArchivo) throws Exception {
        try {

            ArrayList<Registros> lista = DAOInventario.getListaxID(getApplicationContext());

            if (lista.size() == 0) {
                Toast t = Toast.makeText(this, "No hay datos por exportar",
                        Toast.LENGTH_SHORT);
                t.show();
            } else {

                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                NArchivo += ".txt";


                if (!path.exists()) {

                    path.mkdirs();
                }


                final File file = new File(path, NArchivo);



                String contenido = "";
                for (Registros ModInv : lista) {


                    contenido +=  ";" + ModInv.get_id() + ";" + ModInv.getId() + ";" + ModInv.getReferencia() + ";" + ModInv.getDescripcion() + ";" + "\r\n";

                }

                FileOutputStream fos = new FileOutputStream(file, false);
                OutputStreamWriter archivo = new OutputStreamWriter(fos);

                archivo.write(contenido);
                archivo.flush();
                archivo.close();

                Toast t = Toast.makeText(this, "Los datos fueron grabados",
                        Toast.LENGTH_SHORT);
                t.show();
            }
        } catch (IOException e) {
            Toast t = Toast.makeText(this, "Error" + e,
                    Toast.LENGTH_SHORT);
            t.show();
            throw e;
        } catch (Exception e) {
            Toast t = Toast.makeText(this, "Error" + e,
                    Toast.LENGTH_SHORT);
            t.show();
            throw e;
        }

    }

    private void DownloadAllData() throws JSONException {

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String JSONObject = gson.toJson(getResults());
        final org.json.JSONObject jsonBody = new JSONObject(JSONObject);
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        String url = t_configuraciones.getCon_URL() + "/GetDataEan";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                InsertarDatos(response);
                Toast.makeText(getApplicationContext(),"Los datos fueron descargados correctamente", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        Toast.makeText(getApplicationContext(),"Los datos fueron descargados correctamente", Toast.LENGTH_SHORT).show();
        queue.add(request);
    }

    private JSONArray getResults()
    {
        Context context = this;

        String myPath = String.valueOf(context.getDatabasePath("registros"));// Set path to your database

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table


        SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        String searchQuery = "SELECT  * FROM " + myTable;
        Cursor cursor = myDataBase.rawQuery(searchQuery, null );

        JSONArray resultSet     = new JSONArray();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for( int i=0 ;  i< totalColumn ; i++ )
            {
                if( cursor.getColumnName(i) != null )
                {
                    try
                    {
                        if( cursor.getString(i) != null )
                        {
                            Log.d("TAG_NAME", cursor.getString(i) );
                            rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
                        }
                        else
                        {
                            rowObject.put( cursor.getColumnName(i) ,  "" );
                        }
                    }
                    catch( Exception e )
                    {
                        Log.d("TAG_NAME", e.getMessage()  );
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString() );

        return resultSet;
    }

    public void InsertarDatos(JSONObject json)  {

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, Utilidades.TABLA_OBTENERDATOS, null, 1);

        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.JSON, json.toString());
        db.insert(Utilidades.TABLA_OBTENERDATOS, null, values);

    }

    public Boolean ComprobarSwitch() {
        Boolean band = false;
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado", null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_ESTADO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);

        String json = "";

        if (c.moveToFirst()) {
            do {
                json = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();

        if(json.equals("0")){
            band = true;
        }


        return band;
    }

    private void SincronizarDatos(int Online) throws JSONException {

        if(Online == 1){


            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            String JSONObject = gson.toJson(getResults());
            final JSONObject jsonBody = new JSONObject(JSONObject);
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            String url = t_configuraciones.getCon_URL() + "/SincronizarDatos";


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            Toast.makeText(getApplicationContext(), "Sincronización realizada en la nube", Toast.LENGTH_SHORT).show();
            queue.add(request);
        }
        else{
            Toast t = Toast.makeText(this, "Sincronización realizada localmente",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }
}
