
package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.CustomAdapter;
import innobile.itrace.modelo.Model;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itrace.view.LoginActivity;

public class OrdeCompra extends AppCompatActivity {

    private ListView lv;
    private ArrayList<Model> modelArrayList;
    private CustomAdapter customAdapter;
    private Button Guardar;
    private String[] pedidosList;
    private EditText codigoBuscar;
    private ImageButton ibtnBuscarProducto;
    public static String[] datos;
    private String Ruta = "";
    public static EditText cantidad;
    public static TextView tDiferencia = null;
    private int controllSendData = 0, prioridadCodigo;
    private String codigoEmpresa, codigoDocumento, tipoDocumento, codigoBarras, codigoScan, descripcion, bodega, unidadMedida, unidadEmpaque, observaciones, prioridad, nit_responsable, codigoUsuario, centralCosto;
    private TextView Codigo_requisicion, tDescripcion, txtUnidades, tCodigo, tCodigoBarras, tCodigoCentral, tCodigoEncargado, SaldoBodega, unidades;
    private EditText tCodigoLeido, txtCantidad, txtObservaciones, CodigoEan;
    private Button procesar, Mostrar, btnRegresarMenu, RegresarLogin;
    private int contador = 0;
    private String nitEncargado, codigoCentral, resultado, V_Nro_Dcto_Tercero, V_Nombre_Usuario, V_Nit_Tercero;
    private String V_URL, V_URL_MON = null;
    private String V_TipoConexion = null;
    private RequestQueue requestQueue;
    private int contadorIngresado = 0;
    private Date date = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orde_compra);


        //Instanciando elementos desde la vista
        codigoBuscar = findViewById(R.id.edtBuscarProducto);
        ibtnBuscarProducto = findViewById(R.id.ibtnBuscar);
        cantidad = findViewById(R.id.txtCantidad);
        tDiferencia = findViewById(R.id.txtDiferencia);
        tCodigoLeido = findViewById(R.id.edtBuscarProducto);
        Codigo_requisicion = findViewById(R.id.Codigo_requisicion);
        tDescripcion = findViewById(R.id.txtDescripcion);
        txtCantidad = findViewById(R.id.txtCantidad);
        txtObservaciones = findViewById(R.id.txtObservaciones);
        txtUnidades = findViewById(R.id.txtUnidades);
        tCodigo = findViewById(R.id.EnviarCodigo);
        tCodigoBarras = findViewById(R.id.codigobarras);
        procesar = findViewById(R.id.btnGuardar);
        tCodigoCentral = findViewById(R.id.codigocentral);
        tCodigoEncargado = findViewById(R.id.codigoresponsable);
        CodigoEan = findViewById(R.id.txtBuscarProducto);
        Mostrar = findViewById(R.id.Mostrar);
        SaldoBodega = findViewById(R.id.SaldoBodega);
        btnRegresarMenu = findViewById(R.id.RegresarMenu);
        unidades = findViewById(R.id.txtUnidades);
        RegresarLogin = findViewById(R.id.RegresarLogin);
        final TextView Codigo_documento = findViewById(R.id.Codigo_requisicion);
        ImageButton ibtnBuscar = findViewById(R.id.ibtnBuscar);
        Button btnDetalle = findViewById(R.id.btnDetalle);
        lv = findViewById(R.id.lv);
        Guardar = findViewById(R.id.next);

        //Creo el objeto para generar la fecha
        date = new Date();
        SimpleDateFormat fecha_generada = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        //Asginando un valor de inicio  a la variable cantidad
        cantidad.setText("0");

        //Creando el objeto para realizar las conexiones con volley
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }

        //Se encarga de buscar el còdigo manualmente
        ibtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Se llama el metodo que se encarga de traer los datos del producto
                    obtenerDatoProducto();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        //Configurando la URL local para realizar las peticiones y concatenar con cada uno de las direcciones en la nube
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        tCodigoLeido.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
                if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    //Este contador se encarga de enviar los datos solo una vez
                    contadorIngresado++;
                    try {
                        if (contadorIngresado == 2) {
                            obtenerDatoProducto();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });


        //Cargando el codigo del documento
        Codigo_documento.setText(getIntent().getStringExtra("codigo"));

        //Regresa la actividad al login de la aplicación
        RegresarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OrdeCompra.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnRegresarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OrdeCompra.this, Consultar_Recepcion.class);
                startActivity(i);
            }
        });

        //Se encarga de abrir la actividad de detalle y pasa los datos necesarios para consultar las remisiones relacionadas con el documento
        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(OrdeCompra.this, Ver_Detalle_Movimientos.class);
                    intent.putExtra("codigo", Codigo_requisicion.getText().toString());
                    intent.putExtra("documento", tCodigo.getText().toString());
                    intent.putExtra("codigoEan", codigoBuscar.getText().toString());
                    intent.putExtra("cantidadDescontar", txtCantidad.getText().toString());
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error al tratar de ingresar a detalles", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //Botón para procesar el pedido de mercancia
        procesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(OrdeCompra.this);
                builder.setTitle("Confirmar");
                builder.setMessage("¿Estas seguro?");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            procesarRebiboMercancia();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "El proceso fue cancelado", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });


        //Se obtiene el código del proveedor
        try {
            obtenerCodigoProveedor();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Se encarga de recorrer la lista y guardar todos los elementos que fueron modificados
        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    for (int i = 0; i < CustomAdapter.modelArrayList.size(); i++) {
                        if (CustomAdapter.modelArrayList.get(i).getSelected()) {
                            //Toast.makeText(getApplicationContext(),  CustomAdapter.modelArrayList.get(i).getAnimal(), Toast.LENGTH_SHORT).show();
                            datos = CustomAdapter.modelArrayList.get(i).getAnimal().split(" ");
                            // Toast.makeText(getApplicationContext(), CustomAdapter.mensajePrueba(getApplicationContext(),datos[0]) , Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),"Còdigo: " + datos[0] + " Cantidad a restar: " +    Integer.parseInt(CustomAdapter.mensajePrueba(getApplicationContext(),datos[0])) , Toast.LENGTH_SHORT).show();
                            insertarDatoMovimiento(datos[0], CustomAdapter.mensajePrueba(getApplicationContext(), datos[0]));
                        }
                    }

                    AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage("Los datos se guardaron correctamente");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } catch (Exception e) {
                    AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                    alertDialog.setTitle("Mensaje");
                    alertDialog.setMessage("Alguno de los datos que intenta ingresar està vacio");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }


            }
        });

        //ConsultarRequisicionActual();
        try {
            obtenerCodigoResponsable();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            obtenerCodigoCentral();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Modelo que se encarga de guardar los datos de los pedidos en una lista
    private ArrayList<Model> getModel(boolean isSelect, String[] pedidosList) {
        ArrayList<Model> list = new ArrayList<>();
        for (int i = 0; i < pedidosList.length; i++) {
            Model model = new Model();
            model.setSelected(isSelect);
            model.setAnimal(pedidosList[i]);
            list.add(model);
        }
        return list;
    }

    /**
     * Este método se encarga de cargar la lista con los datos del API
     * Autor: CarlosDnl
     * Fecha: 10/12/2019
     */

    public void mostrarRemisionDatos() throws JSONException {
        try {
            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Valor", tCodigo.getText().toString());

            Ruta = V_URL + "/BuscarOCPendientesxProd";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Se guarda la respuesta el REST
                        JSONArray mJsonArray = response.getJSONArray("recordset");
                        pedidosList = new String[mJsonArray.length()];

                        //Se recorren y almacenan los datos
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            String NRODCTO = mJsonObject.getString("NRODCTO");
                            String CANTIDAD = mJsonObject.getString("CANTIDAD");
                            String UNDVENTA = mJsonObject.getString("UNDVENTA");
                            pedidosList[i] = NRODCTO.trim() + "    " + CANTIDAD.trim() + "    " + UNDVENTA.trim();
                        }

                        if (pedidosList.length > 0) {
                            modelArrayList = getModel(false, pedidosList);
                            customAdapter = new CustomAdapter(OrdeCompra.this, modelArrayList, Integer.parseInt(cantidad.getText().toString()));
                            lv.setAdapter(customAdapter);

                            //Reiniciando el còdigo a buscar

                        } else if (pedidosList.length == 0) {

                            pedidosList = new String[]{"Sin    registro"};
                            modelArrayList = getModel(false, pedidosList);
                            customAdapter = new CustomAdapter(OrdeCompra.this, modelArrayList, 0);
                            lv.setAdapter(customAdapter);

                            //Reiniciando el valor del còdigo
                            codigoBuscar.setText("");

                            AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                            alertDialog.setTitle("Mmmm");
                            alertDialog.setMessage("No encontramos ningùn registro");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }
    }


    public static void actualizarCantidad(int cantidad) {
        tDiferencia.setText(String.valueOf(cantidad));
    }

    public static int obtenerCantidad() {
        return Integer.parseInt(tDiferencia.getText().toString());
    }


    public static String recuperarNuevaCantidad(Context context, String codigoBuscar, ArrayList<String> nombreArrayList) {

        String resultadoBusqueda = "0";
        for (int i = 0; i < nombreArrayList.size(); i++) {
            if (codigoBuscar.equals(nombreArrayList.get(i))) {
                resultadoBusqueda = nombreArrayList.get(i + 1);
                // Toast.makeText(context, "Resultado: " + resultadoBusqueda, Toast.LENGTH_SHORT).show();
            }
        }
        return resultadoBusqueda;
    }


    public void insertarDatoMovimiento(String codigoPedido, String nuevaCantidad) {

        //Inicializando la prioridad en 0

        //Se obtiene la prioridad enviada desde la Activity encabezado_requisicion
        try {
            //Se rellenan las variables con los datos que van a enviar a la BD
            codigoEmpresa = "01";
            codigoDocumento = Codigo_requisicion.getText().toString();
            tipoDocumento = Global.TIPO_DOCUMENTO;
            codigoBarras = tCodigoBarras.getText().toString();
            codigoScan = tCodigo.getText().toString();
            descripcion = tDescripcion.getText().toString();
            bodega = "BODEGAGENERALN";
            cantidad = cantidad;
            unidadMedida = unidades.getText().toString();
            unidadEmpaque = unidades.getText().toString();
            observaciones = txtObservaciones.getText().toString();
            prioridad = String.valueOf(prioridadCodigo);
            nit_responsable = tCodigoEncargado.getText().toString();
            codigoUsuario = Cons.Usuario;
            centralCosto = tCodigoCentral.getText().toString();
            V_Nro_Dcto_Tercero = getIntent().getStringExtra("V_Nro_Dcto_Tercero");
            V_Nombre_Usuario = Cons.Usuario;
            V_Nit_Tercero = tCodigoEncargado.getText().toString();
            //Este mètodo se encarga de recibir todos los campos y enviarlos al servicio REST
            insertMovementesData(codigoEmpresa, codigoDocumento, tipoDocumento, codigoBarras, codigoScan, descripcion, bodega, nuevaCantidad, unidadMedida, unidadEmpaque, observaciones, prioridad, nit_responsable, codigoUsuario, centralCosto, codigoPedido, V_Nro_Dcto_Tercero, V_Nombre_Usuario, V_Nit_Tercero);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de obtener los datos a insertar", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Envia los datos hasta el API para almacenar los recibos de mercancia
     * Autor:CarlosDnl
     * Fecha: 12/12/2019
     **/

    public void insertMovementesData(String codigoEmpresa, String codigoDocumento, String tipoDocumento, String codigoBarras, String codigoScan, String descripcion, String bodega, String cantidad, String unidadMedida, String unidadEmpaque, String observaciones, String prioridad, String nitResponsable, String codigoUsuario, String centralCosto, String codigoDocumentoModificar, String V_Nro_Dcto_Tercero, String V_Nombre_Usuario, String V_Nit_Tercero) {
        try {


            Ruta = V_URL + "/InsertarTablaMovimientos";

            //Se crea un json que serà enviado para que se guarde en la BD remota
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", codigoEmpresa);
            jsonBody.put("Numero_Documento", codigoDocumento);
            jsonBody.put("Tipo_Documento", tipoDocumento);
            jsonBody.put("Codigo_Barras", codigoBarras);
            jsonBody.put("Codigo", codigoScan);
            jsonBody.put("Descripcion", descripcion);
            jsonBody.put("Cod_Bodega", bodega);
            jsonBody.put("Cantidad", cantidad);
            jsonBody.put("Unidad_Medida", unidades.getText().toString());
            jsonBody.put("Unidad_Empaque", unidades.getText().toString());
            jsonBody.put("Observaciones", observaciones);
            jsonBody.put("Prioridad", prioridad);
            jsonBody.put("Nit_Responsable", "1128446305");
            jsonBody.put("Codigo_Usuario", Cons.Usuario);
            jsonBody.put("Centro_Costos", centralCosto);
            jsonBody.put("Nro_Dcto_Afectado", codigoDocumentoModificar);
            jsonBody.put("Nro_Dcto_Tercero", V_Nro_Dcto_Tercero);
            jsonBody.put("Nombre_Usuario", V_Nombre_Usuario);
            jsonBody.put("Nit_Tercero", V_Nit_Tercero);


            //Se guarda el JSON creado para enviarlo al servicio REST
            final String requestBody = jsonBody.toString();


            //Se realiza la peticiòn y se controlan las respuestas y los errore
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            stringRequest.setShouldCache(false);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /**
     * Autor: CarlosDnl
     * Realiza la busqueda de los productos y los muestra en la pantalla
     * Fecha creación: 20/11/2019
     */

    public void obtenerDatoProducto() throws JSONException {

        try {
            if (!tCodigoLeido.getText().toString().trim().isEmpty()) {
                JSONObject jsonBody = null;
                jsonBody = new JSONObject();

                jsonBody.put("Cod_Empresa", "01");
                jsonBody.put("Cod_Bodega", "BODGENERAL");
                jsonBody.put("Valor", tCodigoLeido.getText().toString().trim());


                //Se configura la URL para realizar la petición

                Ruta = V_URL + "/BuscarProducto";

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            JSONArray mJsonArray = response.getJSONArray("recordset");

                            if (mJsonArray.length() > 0) {
                                //Se guarda la respuesta el REST
                                JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                                //Obtendo los datos del Api y los muestro en los campos correspondientes
                                tCodigo.setText(mJsonObject.getString("CODIGO").trim());
                                tCodigoBarras.setText(mJsonObject.getString("CODBARRAS").trim());
                                unidades.setText(mJsonObject.getString("UNIDADMED").trim());
                                tDescripcion.setText(mJsonObject.getString("DESCRIPCION").trim());
                                Global.RECUPERAR_CODIGO_REMISION = tCodigo.getText().toString();
                                //Reinicio el valor del Editext
                                tCodigoLeido.setText("");
                                //Muestro los datos de la remisiòn
                                mostrarRemisionDatos();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                                alertDialog.setTitle("Mensaje");
                                alertDialog.setMessage("No se puedo encontrar el producto ingresado");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.show();
                            }

                            //Reinicio el contator para poder ingresar en la siguiente lectura
                            contadorIngresado = 0;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                request.setShouldCache(false);
                request.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(request);

            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                alertDialog.setTitle("Mensaje");
                alertDialog.setMessage("Es necesario ingresar un codigo");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }


        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de obtener el código de referencia", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Método para consultar el código de la central
     * Fecha creación: 20/11/2019
     * Autor: CarlosDnl
     */

    public void obtenerCodigoCentral() throws JSONException {

        //Creando la URL a la cual se va a realizar la peticiòn

        Ruta = V_URL + "/BuscarCentroCostos";

        ///Creando el json que se enviara al Rest
        final JSONObject jsonBody = new JSONObject("{ \"Valor\":\"" + getIntent().getStringExtra("central_costo") + "\"}");

        //Se realizar la peticiòn a la URL especificando el Mètodo como un POST
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    //Guardando la respuesta como un JSONArray
                    JSONArray mJsonArray = response.getJSONArray("recordset");


                    //Se extrae la respuesta del objeto
                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                    //Enviando el còdigo del encargado a un textView oculto en la Activity para conservar su valor
                    codigoCentral = mJsonObject.getString("CODIGO");
                    //Enviando el codigo de la central a un textView oculto en la Activity para conservar su valor
                    tCodigoCentral.setText(codigoCentral);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrdeCompra.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


    /**
     * Método para consultar el código del responsable
     * Fecha creación: 20/11/2019
     * Autor: CarlosDnl
     */

    public void obtenerCodigoResponsable() throws JSONException {
        try {
            Ruta = V_URL + "/BuscarResponsables";

            ///Creando el json que se enviara al Rest
            final JSONObject jsonBody = new JSONObject("{ \"Valor\":\"" + getIntent().getStringExtra("responsable") + "\"}");

            //Se realizar la peticiòn a la URL especificando el Mètodo como un POST
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        //Guardando la respuesta como un JSONArray
                        JSONArray responsables = response.getJSONArray("recordset");
                        //Se extrae la respuesta del objeto
                        JSONObject mJsonObject = responsables.getJSONObject(0);
                        nitEncargado = mJsonObject.getString("NIT");
                        //Enviando el còdigo del encargado a un textView oculto en la Activity para conservar su valor
                        tCodigoEncargado.setText(nitEncargado);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al obtener el código del responsable", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Se encarga de procesar el recibo de mercancia
     * Fecha: 16/12/2019
     * Autor: CarlosDnl
     **/

    public void procesarRebiboMercancia() throws JSONException {


        try {
            Ruta = V_URL + "/InsertarMovimientosERP";

            //Se crea un objetoObject para realizar el POST al servicio Rest
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("Cod_Empresa", "01");
            jsonBody.put("Numero_Documento", Codigo_requisicion.getText().toString());
            jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
            jsonBody.put("Origen", "COM");


            //Se realiza la peticiòn y se controlan las respuesta y los posibles errores
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    String Respuesta = null;
                    String Mensaje = null;
                    //creo Objeto Json para entrar al Output
                    JSONObject JOutput = null;

                    try {

                        JOutput = response.getJSONObject("output");
                        Respuesta = JOutput.optString("Respuesta");
                        Mensaje = JOutput.optString("Mensaje");

                        if (Respuesta.equals("1")) {
                            AlertDialog alertDialog = new AlertDialog.Builder(OrdeCompra.this).create();
                            alertDialog.setTitle("Mensaje");
                            alertDialog.setMessage(Mensaje);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();

                        } else {
                            Toast.makeText(getApplicationContext(), Mensaje, Toast.LENGTH_SHORT).show();
                        }
/*
                        if(resultado.equals("1")){
                            Toast.makeText(getApplicationContext(), "El proceso se realizó correctamente", Toast.LENGTH_SHORT).show();
                        }else if (resultado.equals("0")){
                            Toast.makeText(getApplicationContext(), "El necesario ingresar datos para realizar el proceso", Toast.LENGTH_SHORT).show();
                        }
   */
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(OrdeCompra.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de procesar", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Se encarga de consultar el código para el proveedor seleccionado
     * Fecha: 19/12/2019
     * Autor: CarlosDnl
     **/

    public void obtenerCodigoProveedor() throws JSONException {
        try {
            JSONObject jsonBody;
            jsonBody = new JSONObject();
            jsonBody.put("Valor", getIntent().getStringExtra("V_Nit_Tercero"));

            //Creando la URL a la cual se va a realizar la peticiòn

            Ruta = V_URL + "/BuscarTerceros";
            //Se realizar la peticiòn a la URL especificando el Mètodo como un POST
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Guardando la respuesta como un JSONArray
                        JSONArray terceros = response.getJSONArray("recordset");

                        //Se extrae la respuesta del objeto
                        JSONObject mJsonObject = terceros.getJSONObject(0);
                        nitEncargado = mJsonObject.getString("NIT");
                        //Enviando el còdigo del encargado a un textView oculto en la Activity para conservar su valor
                        tCodigoEncargado.setText(nitEncargado);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de obtener el código del proveedor", Toast.LENGTH_SHORT).show();
        }
    }



}
