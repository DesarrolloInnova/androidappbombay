package innobile.itraceandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import innobile.itrace.R;

public class MenuRecepcion extends AppCompatActivity {
    CardView ibtnrequision = null, ibtnRecibirMercancia = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_recepcion);

        ibtnrequision = findViewById(R.id.ibtnRequsicion);
        ibtnRecibirMercancia = findViewById(R.id.ibtnRecibirMercancia);

        //Se carga cuando el tipo de movimiento es una Requisicion
        ibtnrequision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuRecepcion.this, Consultar_Recepcion.class);
                //Envia 1 si el tipo de movimiento es RQ
                Global.TIPO_DOCUMENTO = "RQ";
                startActivity(intent);
                finish();
            }
        });

        //Se carga cuando el tipo de movimiento es una Remision
        ibtnRecibirMercancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuRecepcion.this, Consultar_Recepcion.class);
                //Envia 1 si el tipo de movimiento es RC
                Global.TIPO_DOCUMENTO = "RM";
                startActivity(intent);
                finish();
            }
        });
    }
}
