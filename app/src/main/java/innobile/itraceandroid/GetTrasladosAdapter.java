package innobile.itraceandroid;

public class GetTrasladosAdapter {
    String codigoTralado;
    String fechaTraslado;

    public GetTrasladosAdapter(String codigoTralado, String fechaTraslado) {
        this.codigoTralado = codigoTralado;
        this.fechaTraslado = fechaTraslado;
    }

    public GetTrasladosAdapter() {

    }


    public String getCodigoTralado() {
        return codigoTralado;
    }

    public void setCodigoTralado(String codigoTralado) {
        this.codigoTralado = codigoTralado;
    }

    public String getFechaTraslado() {
        return fechaTraslado;
    }

    public void setFechaTraslado(String fechaTraslado) {
        this.fechaTraslado = fechaTraslado;
    }
}
