package innobile.itraceandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;

public class Ver_requisicion_actual extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_requisicion_actual);

       TextView txtCodigo = findViewById(R.id.ValorRequisicion);
        final String RequisicionCode = getIntent().getStringExtra("anime_name");
        txtCodigo.setText(RequisicionCode);

        final TextView Codigo_documento = findViewById(R.id.ValorRequisicion);

        final ImageButton itbBuscar = findViewById(R.id.ibtnBuscar);

        //Elementos para insertar
        final EditText ean = findViewById(R.id.txtBuscarProducto);
        final EditText txtCantidad = findViewById(R.id.txtCantidad);
        final EditText txtObservaciones = findViewById(R.id.txtObservaciones);
        final TextView codigo_requisicion = findViewById(R.id.Codigo_requisicion);



        itbBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    getReferenceData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });



            Button btnDetalle = findViewById(R.id.btnDetalle);
            btnDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                //.    GuardarRequisicionSQLITE(ean.getText().toString(),Integer.parseInt(txtCantidad.getText().toString()),txtObservaciones.getText().toString(),Integer.parseInt(RequisicionCode),"2013-10-07 08:23:19.120");
                    Intent intent = new Intent(Ver_requisicion_actual.this, DetalleRequisicion.class);
                    intent.putExtra("codigo", Codigo_documento.getText());
                    startActivity(intent);
                }
            });
    }




    public void getReferenceData() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);

        TextView txtCodigo = findViewById(R.id.txtBuscarProducto);

        final JSONObject jsonBody = new JSONObject("{ \"ean\":\"" + txtCodigo.getText().toString() + "\"}" );

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        String url =  t_configuraciones.getCon_URL() + "/GetReferenceTabla";

        final TextView textView6 = findViewById(R.id.textView6);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray mJsonArray = response.getJSONArray("recordset");
                    TextView Unidades = findViewById(R.id.txtUnidades);
                    TextView descripcion = findViewById(R.id.lblDescripcion);


                    for(int i=0; i < mJsonArray.length(); i++){
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        String Unidad = mJsonObject.getString("Unidad");
                        String Descripcion = mJsonObject.getString("descripcion");

                        Unidades.setText(Unidad);
                        descripcion.setText(Descripcion);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
    }





    public void GuardarRequisicionSQLITE(String ean, int cantidad, String observaciones, int codigo_documento, String fecha){

        try{
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.REQUISICION_TABLA, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.REQUISICION_DATA_EAN, ean);
            values.put(Utilidades.REQUISICION_DATA_CANTIDAD, cantidad);
            values.put(Utilidades.REQUISICION_DATA_OBSERVACIONES, observaciones);
            values.put(Utilidades.REQUISICION_DATA_DOCUMENTO, codigo_documento);
            values.put(Utilidades.REQUISICION_DATA_FECHA,  fecha);

            db.insert(Utilidades.REQUISICION_TABLA, null, values);
            db.close();
        }catch (Error e){
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }



    }
}
