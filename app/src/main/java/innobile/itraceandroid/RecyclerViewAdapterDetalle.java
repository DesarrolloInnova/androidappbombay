package innobile.itraceandroid;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.modelo.GetDetalleAdapter;

/**
 * Created by JUNED on 6/16/2016.
 */
public class RecyclerViewAdapterDetalle extends RecyclerView.Adapter<RecyclerViewAdapterDetalle.MyViewHolder> {

    private Context mContext;
    private List<GetDetalleAdapter> mData;
    RequestOptions option;
    private String dato;
    private AlertDialog.Builder alertDialog;
    private Context myContext = null;
    private Button Eliminar = null;


    public RecyclerViewAdapterDetalle(Context mContext, List<GetDetalleAdapter> mData, String valor) {
        this.mContext = mContext;
        this.mData = mData;
        dato = valor;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.requisicion_row_item_detalle, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Eliminar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "Funciona", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return viewHolder;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_name.setText(mData.get(position).getCodigo());
        holder.tv_rating.setText(mData.get(position).getDescripcion());

        // Load Image from the internet and set it into Imageview using Glide
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;
        TextView tv_rating;
        LinearLayout view_container;

        public MyViewHolder(View itemView) {
            super(itemView);

            view_container = itemView.findViewById(R.id.container);
            tv_name = itemView.findViewById(R.id.anime_name);
            tv_rating = itemView.findViewById(R.id.rating);
        }
    }


}
