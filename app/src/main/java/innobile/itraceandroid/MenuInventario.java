package innobile.itraceandroid;

import android.content.Intent;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

import cn.pedant.SweetAlert.SweetAlertDialog;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.Fajate.Informacion_Inventario;
import innobile.itraceandroid.Fajate.configuracion_inventario;

public class MenuInventario extends AppCompatActivity {
    public CardView btnInventario, btnConfiguracion, btnReporte, btn_verInformacion;
    private ConexionSQLiteHelper conn;
    private long cn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_inventario);

        //Referenciando los elementos de la vista
        btnInventario = findViewById(R.id.btn_inventario);
        btnConfiguracion = findViewById(R.id.btn_configuracion);
        btnReporte = findViewById(R.id.btn_reporte);
        btn_verInformacion  = findViewById(R.id.btn_verInformacion);

        //Configurando las funciones de los CardView al ser presionados
        btnInventario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cantidadRegistrosBodega() == 0 && cantidaRegistrosProductos() == 0) {
                    new SweetAlertDialog(MenuInventario.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Se deben cargar los productos y las bodegas")
                            .setContentText("Lo puedes hacer desde Configuración inventario")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })

                            .show();
                } else if (cantidaRegistrosProductos() == 0) {
                    new SweetAlertDialog(MenuInventario.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Es necesario ingresar las productos")
                            .setContentText("Lo puedes hacer desde Configuración inventario")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })

                            .show();
                } else if (cantidadRegistrosBodega() > 0 && cantidaRegistrosProductos() > 0) {
                    Intent intent = new Intent(MenuInventario.this, ConsultarInventario.class);
                    startActivity(intent);
                } else if (cantidadRegistrosBodega() == 0) {
                    new SweetAlertDialog(MenuInventario.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Es necesario ingresar las bodegas")
                            .setContentText("Lo puedes hacer desde Configuración inventario")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                }
            }
        });
        btnConfiguracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuInventario.this, configuracion_inventario.class);
                startActivity(intent);
            }
        });
        btnReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.COMPROBAR_REPORTE = true;
                Intent intent = new Intent(MenuInventario.this, Reporte_Inventario.class);
                startActivity(intent);
            }
        });

        btn_verInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuInventario.this, Informacion_Inventario.class);
                startActivity(intent);
            }
        });

    }

    private Long cantidadRegistrosBodega() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_VBODEGAS);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    private Long cantidaRegistrosProductos() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VPRODUCTOS, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_VPRODUCTOS);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }
}
