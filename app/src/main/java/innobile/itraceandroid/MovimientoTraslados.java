package innobile.itraceandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import innobile.itrace.R;

public class MovimientoTraslados extends AppCompatActivity {
    private Button btnDetalle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimiento_traslados);
        btnDetalle = findViewById(R.id.btnDetalle);
        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MovimientoTraslados.this, DetalleTraslado.class);
                startActivity(intent);
            }
        });

        TextView tiendaOrigen = findViewById(R.id.txtTiendaOrigen);
        TextView tiendaDestino = findViewById(R.id.txtTiendaDestino);




        tiendaOrigen.setText(getIntent().getStringExtra("tiendaOrigen"));
        tiendaDestino.setText(getIntent().getStringExtra("tiendaDestino"));
    }
}
