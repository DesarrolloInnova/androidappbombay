package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;

public class EncabezadoActivosFijos extends AppCompatActivity {
    private Button btnDetalle = null;
    private String[] lstBodegaOrigen = new String[]{"Sin datos"};
    private AutoCompleteTextView BodegaOrigen;
    private ImageView ImagenBodegaOrigen;
    private String V_URL, V_URL_MON = null;
    private String V_TipoConexion = null;
    private TextView numero_documento = null, fecha_documento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_activos_fijos);

        //Referenciando los elementos
        btnDetalle = findViewById(R.id.btnDetalle);
        BodegaOrigen = findViewById(R.id.autoCompleteBodegaOrigen);
        ImagenBodegaOrigen = findViewById(R.id.ImagenBodegaOrigen);
        numero_documento = findViewById(R.id.numero_documento);
        fecha_documento = findViewById(R.id.fecha_documento);


        //Generando el código del documento
        UUID id_documento = UUID.randomUUID();
        String[] separandoCodigo = id_documento.toString().split("-");
        String codigo = separandoCodigo[0];
        numero_documento.setText(codigo);

        //Creamos un objeto de la clase Calendar.
        fecha_documento.setText(getFechaActual());


        //Configurando URL de la conexión
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();

        //Agregando la función para navegar a la otra ventana al presionar el botón
        btnDetalle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Se encarga de consultar los datos del producto

                //Comprobamos si la bodega tiene datos
                if (!BodegaOrigen.getText().toString().equals("")) {
                    Intent startActivity = new Intent(EncabezadoActivosFijos.this, InventarioActivosFijos.class);
                    startActivity.putExtra("Bodega_origen", BodegaOrigen.getText().toString());
                    startActivity.putExtra("numero_documento", numero_documento.getText().toString());
                    startActivity.putExtra("fecha", fecha_documento.getText().toString());
                    startActivity(startActivity);
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(EncabezadoActivosFijos.this).create();
                    alertDialog.setTitle("Atención");
                    alertDialog.setMessage(getString(R.string.bodegaOrigenAlert));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

            }
        });

        try {
            CargarBodegaOrigen("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Mostrando los datos de la bodega origen
        ImagenBodegaOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BodegaOrigen.showDropDown();
            }
        });
    }

    private void CargarBodegaOrigen(String s) throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        String Ruta = null;
        JSONObject jsonBody = null;
        jsonBody = new JSONObject();
        jsonBody.put("Cod_Empresa", Cons.Empresa);

        Ruta = V_URL_MON + "/api/bodega/BuscarBodegaxEmpresa";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    //Obtengo el objeto de bodega y lo recorro
                    JSONArray preguntasJA = response.getJSONArray("recordset");
                    lstBodegaOrigen = new String[preguntasJA.length()];

                    for (int i = 0; i < preguntasJA.length(); i++) {
                        JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                        String datos_bodegas = mJsonObject.getString("Descripcion");
                        lstBodegaOrigen[i] = datos_bodegas.trim();
                    }

                    //Cargando los datos obtenidos desde el API en la lista
                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(EncabezadoActivosFijos.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                    BodegaOrigen.setAdapter(adapterBodegas);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(EncabezadoActivosFijos.this, "Sin internet, si tienes alguna duda contacta soporte" + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        queue.add(request);
    }

    /**
     * Se encarga de obtener la fecha actual
     */

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(ahora);
    }
}
