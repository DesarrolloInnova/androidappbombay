
package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.GetInventarioAdapter;
import innobile.Adapter.InventarioModelData;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;

public class ConsultarInventario extends AppCompatActivity {


    private FloatingActionMenu nuevoInventario;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AutoCompleteTextView autoCompleteConteo, autoCompleteBodega;
    private String[] lstConteo = new String[]{"1", "2", "3"}, lstBodegas = new String[]{};
    private ConexionSQLiteHelper conn;
    private long cn = 0;
    private String codigo = "0";
    List<String> datos = new ArrayList<String>();
    public InventarioModelData mAdapter;
    private SwipeController swipeController = null;
    private List<GetInventarioAdapter> mlstRequisicionData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_inventario);

        recyclerView = findViewById(R.id.recyclerviewid);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        nuevoInventario = findViewById(R.id.nuevo_inventario);
        assert nuevoInventario != null;

        //Pasando referencia de la lista global
        mAdapter = new InventarioModelData(getApplicationContext(), mlstRequisicionData);
        buscarInventarioSQlite();
        setupRecyclerView();


        //Refrescando la vista
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                buscarInventarioSQlite();
            }
        });


        //Cargando la nueva actividad usando el FloatingMenu
        nuevoInventario.findViewById(R.id.material_design_floating_action_menu_item1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    //Generando el còdigo del documento
                    conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
                    SQLiteDatabase db = conn.getReadableDatabase();
                    String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
                    Cursor c = db.rawQuery("SELECT " + Utilidades.vNUMERO_DOCUMENTO + " FROM " + myTable + ";", null);
                    if (c.moveToFirst()) {
                        do {
                            codigo = c.getString(0);
                        } while (c.moveToNext());
                    }
                    db.close();


                    //Generando la fecha
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    //Creando la vista del alertDialog
                    final AlertDialog.Builder mBuilder = new AlertDialog.Builder(ConsultarInventario.this);
                    View mView = getLayoutInflater().inflate(R.layout.dialog_encabezado, null);

                    //Instanciando los elementos del componente dialog_encabezado
                    final TextView tNumero_Documento = mView.findViewById(R.id.tnumero_documento);
                    final TextView tFecha_Documento = mView.findViewById(R.id.tfecha_documento);
                    final ImageView imageViewConteo = mView.findViewById(R.id.ImageViewConteo);
                    autoCompleteConteo = mView.findViewById(R.id.autoCompleteConteo);
                    autoCompleteBodega = mView.findViewById(R.id.autoCompleteBodega);
                    final ImageView imageViewBodega = mView.findViewById(R.id.ImagenBodega);

                    //botones
                    Button mDetalle = (Button) mView.findViewById(R.id.btnDetalle);
                    Button mSalir = (Button) mView.findViewById(R.id.btnSalir);

                    //Se envia el codigo de documento y la fecha a los campo
                    tNumero_Documento.setText(String.valueOf(Integer.parseInt(codigo) + 1));
                    tFecha_Documento.setText(formatter.format(date));


                    //Agregando funcionalidad al boton detalle

                    try {
                        mDetalle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(ConsultarInventario.this, CrearInventario.class);
                                intent.putExtra("tCodigoDocumento", tNumero_Documento.getText().toString());
                                intent.putExtra("tConteo", autoCompleteConteo.getText().toString());
                                intent.putExtra("Fecha_Lectura", tFecha_Documento.getText().toString());
                                intent.putExtra("Nombre_Bodega", autoCompleteBodega.getText().toString());
                                startActivity(intent);
                            }
                        });
                    } catch (Exception e) {
                        Toasty.error(getApplicationContext(), "Uno de los campos no tiene información.", Toast.LENGTH_SHORT, true).show();
                    }


                    //Cargando los datos en el ListView de autocomplete
                    ArrayAdapter<String> adapterConteo = new ArrayAdapter<String>(ConsultarInventario.this, android.R.layout.simple_spinner_dropdown_item, lstConteo);
                    autoCompleteConteo.setAdapter(adapterConteo);

                    //Agregando funcionalidad al boton que muestra los datos del spinner
                    imageViewConteo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            autoCompleteConteo.showDropDown();
                        }
                    });


                    cargandoBodegas();


                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(ConsultarInventario.this, android.R.layout.simple_spinner_dropdown_item, lstBodegas);
                    autoCompleteBodega.setAdapter(adapterBodegas);

                    //Agregando funcionalidad al boton que muestra los datos del spinner
                    imageViewBodega.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            autoCompleteBodega.showDropDown();
                        }
                    });


                    //Se carga la vista dentro de la actividad actual
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();


                    //Agregando funcionalidad al boton salir
                    mSalir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                            Toasty.info(getApplicationContext(), "Presionaste el para salir.", Toast.LENGTH_SHORT, true).show();
                        }

                    });

                } catch (Exception ex) {
                    Toasty.error(getApplicationContext(), "Error con un dato nulo.", Toast.LENGTH_SHORT, true).show();
                    ex.printStackTrace();
                } catch (Error e) {
                    Toasty.error(getApplicationContext(), "Error dentro de la aplicacion", Toast.LENGTH_SHORT, true).show();
                }

            }


        });


    }

    private String[] cargandoBodegas() {
        int i = 0;
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VBODEGAS;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM  " + myTable, null);


        if (c.moveToFirst()) {
            do {
                String nombre_Bodega = c.getString(1);
                datos.add(nombre_Bodega);
            } while (c.moveToNext());
        }

        //Definiendo el tamaño del arreglo según los datos en Sqlite
        lstBodegas = new String[datos.size()];

        //Guardando los elementos del array en una lista
        for (int j = 0; j < datos.size(); j++) {
            lstBodegas[j] = datos.get(j);
        }
        return lstBodegas;
    }

    private Long cantidadRegistrosBodega() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_VBODEGAS);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    /**
     * Se encarga de motrar los inventarios abiertos
     */

    private void buscarInventarioSQlite() {
        //Se limpia la vista para evitar cargar los mismos datos
        mlstRequisicionData.clear();

        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " WHERE " + Utilidades.vEstado + " = 'IN' GROUP BY " + Utilidades.vNUMERO_DOCUMENTO + ";", null);
        if (c.moveToFirst()) {
            do {
                GetInventarioAdapter requisicionData = new GetInventarioAdapter();
                requisicionData.setCodigo_Documento(c.getString(0));
                requisicionData.setFecha(c.getString(3));
                requisicionData.setTipo_Conteo(c.getString(5));
                requisicionData.setBodega_Nombre(c.getString(6));
                mlstRequisicionData.add(requisicionData);
            } while (c.moveToNext());
        }

        db.close();

        //Se deja de mostrar el icono de refrescar
        swipeRefreshLayout.setRefreshing(false);
        // Notifica los cambios al adaptador
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);


    }


    /**
     * Se añaden las funciones a los items del RecyclerView y permite borrar los datos
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    private void setupRecyclerView() {
        try {
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(mAdapter);

            swipeController = new SwipeController(new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {

                    borrarDatoSqlite(mAdapter.inventario.remove(position).getCodigo_Documento());

                    //Contiene la posicición del Item que fue eliminaod
                    mAdapter.notifyItemRemoved(position);
                    mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

                }
            });

            //Se crear el touch para añadir funcionalidad al presionar cada uno de los items
            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
            itemTouchhelper.attachToRecyclerView(recyclerView);

            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                    swipeController.onDraw(c);
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void borrarDatoSqlite(String codigo_Borrar) {
        //DELETE FROM tabla1 WHERE valor = 2018
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        Cursor c = db.rawQuery("DELETE   FROM " + myTable + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = '" + codigo_Borrar + "';", null);
        c.moveToFirst();
        db.close();
    }

}
