package innobile.itraceandroid;

import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.ModeloDeleteData;
import innobile.itrace.transport.T_Configuraciones;

public class Ver_Detalle_Movimientos extends AppCompatActivity {

    public DeleteDataAdapter mAdapter;
    SwipeController swipeController = null;
    private String Codigo, pDescripcion, Unidad_Medida;
    private List<ModeloDeleteData> detalles = new ArrayList<>();
    private ModeloDeleteData player = new ModeloDeleteData();
    private String Tipo_Docto;
    private String V_URL = null;
    private String V_TipoConexion = null, V_URL_MON = null;
    private RequestQueue requestQueue;
    private  RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verdetallerq);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //Guardando el tipo de movimiento que se está ejecutando
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Pasando la estructura de la lista
        mAdapter = new DeleteDataAdapter(detalles);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();  //Url que se encarga de la conexión desde la nube
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }


        //Comprobando el tipo de movimiento para ejecutar la carga de datos en la lista
        try {
            getReferenceData();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setupRecyclerView();
    }

    public void getReferenceData() throws JSONException {


        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarMovimientosDetalles", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray mJsonArray = response.getJSONArray("recordset");


                    for (int i = 0; i < mJsonArray.length(); i++) {
                        ModeloDeleteData detalle = new ModeloDeleteData();
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        detalle.setCodigo(mJsonObject.getString("Codigo"));
                        detalle.setpDescripcion(mJsonObject.getString("Descripcion"));
                        detalle.setUnidad_Medida(mJsonObject.getString("cantidad"));
                        detalles.add(detalle);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Notifica los cambios al adaptador
                mAdapter.notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                try {
                    deleteDataApi(mAdapter.productos.remove(position).getCodigo());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),  mAdapter.players.remove(position).getCodigo(), Toast.LENGTH_SHORT).show();
                // mAdapter.players.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    /**
     * Se encarga de borrar los datos desde SQLServer
     * Autor:CarlosDnl
     * Fecha: 20-02-2020
     */
    public void deleteDataApi(final String Codigo_Eliminar) throws JSONException {

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo"));
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Codigo", Codigo_Eliminar);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/EliminarProductoMovimientos", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);

        try {
            borrarMongoDBDato(Codigo_Eliminar);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Se encargar de borrar el dato en MongoDB
     */

    public void borrarMongoDBDato(String Codigo_Eliminar) throws JSONException {

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Numero_Documento", getIntent().getStringExtra("codigo").trim());
        jsonBody.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
        jsonBody.put("Codigo", Codigo_Eliminar.trim());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL_MON + "/api/Movimiento/remove", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


}



