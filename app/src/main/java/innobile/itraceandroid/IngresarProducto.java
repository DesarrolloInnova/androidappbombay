package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.MetodosGlobales.bodega;
import innobile.itraceandroid.TrasladosTM.DatosProductos;

public class IngresarProducto extends AppCompatActivity {
    private ImageButton btnspeak1 = null, btnspeak2 = null, btnspeak3 = null, btnspeak4 = null;
    private int comprobarBoton = 0;
    private static EditText tnuevo_codigo = null, tDescripcion = null, tMarca = null, tGrupo = null, tcantidad = null;
    private ImageView ImagenBodegaOrigen;
    private String[] lstBodegaOrigen = new String[]{"Sin datos"};
    private AutoCompleteTextView BodegaOrigen;
    private String V_URL, V_URL_MON = null;
    private String V_TipoConexion = null;
    private innobile.itraceandroid.MetodosGlobales.bodega bodega = new bodega();
    private Button btnGuardar = null;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_producto);

        //Referenciando los elementos
        btnspeak1 = findViewById(R.id.btnspeak1);
        btnspeak2 = findViewById(R.id.btnspeak2);
        btnspeak3 = findViewById(R.id.btnspeak3);
        btnspeak4 = findViewById(R.id.btnspeak4);

        tnuevo_codigo = findViewById(R.id.tnuevo_codigo);
        tDescripcion = findViewById(R.id.tNombre);
        tMarca = findViewById(R.id.tCiudad);
        tGrupo = findViewById(R.id.tGrupo);
        tcantidad = findViewById(R.id.tcantidad);


        BodegaOrigen = findViewById(R.id.autoCompleteBodegaOrigen);
        ImagenBodegaOrigen = findViewById(R.id.ImagenBodegaOrigen);
        btnGuardar = findViewById(R.id.btnGuardar);


        //Rellenando campos para evitar datos vacios
        tnuevo_codigo.setText("");
        tDescripcion.setText("");
        tMarca.setText("");
        tGrupo.setText("");
        tcantidad.setText("");

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(new NoCache(), network);
        // Start the queue
        requestQueue.start();


        //Configurando URL de la conexión
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        try {
            CargarBodegaOrigen("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Mostrando los datos de la bodega origen
        ImagenBodegaOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BodegaOrigen.showDropDown();
            }
        });


        //Agregando funcionalidad a los botones
        btnspeak1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                comprobarBoton = 1;
                comandosVoz();
            }
        });

        btnspeak2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 2;
                comandosVoz();
            }
        });

        btnspeak3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 3;
                comandosVoz();
            }
        });

        btnspeak4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 4;
                comandosVoz();
            }
        });


        //Agregando funcionanlidad al botón que se encarga de guardar
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                comprobarDatosVacios();


            }


        });

    }


    /**
     * Se encarga de tomar acciones con el asistente de Google
     */

    private void comandosVoz() {
        Intent voice = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());
        voice.putExtra(RecognizerIntent.EXTRA_PROMPT, "Asistente Innova");
        startActivityForResult(voice, 1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            ArrayList<String> arrayList = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);


            if (comprobarBoton == 1) {
                tnuevo_codigo.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 2) {
                tDescripcion.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 3) {
                tMarca.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 4) {
                tGrupo.setText(arrayList.get(0).toString());
            } else {
                Toast.makeText(getApplicationContext(), "Ocurrió un error", Toast.LENGTH_LONG).show();
            }

        }

    }


    private void CargarBodegaOrigen(String s) throws JSONException {

        String Ruta = null;
        JSONObject jsonBody = null;
        jsonBody = new JSONObject();
        jsonBody.put("Cod_Empresa", Cons.Empresa);

        Ruta = V_URL_MON + "/api/bodega/BuscarBodegaxEmpresa";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    //Obtengo el objeto de bodega y lo recorro
                    JSONArray preguntasJA = response.getJSONArray("recordset");
                    lstBodegaOrigen = new String[preguntasJA.length()];

                    for (int i = 0; i < preguntasJA.length(); i++) {
                        JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                        String datos_bodegas = mJsonObject.getString("Descripcion");
                        lstBodegaOrigen[i] = datos_bodegas.trim();
                    }

                    //Cargando los datos obtenidos desde el API en la lista
                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(IngresarProducto.this, android.R.layout.simple_spinner_dropdown_item, lstBodegaOrigen);
                    BodegaOrigen.setAdapter(adapterBodegas);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(IngresarProducto.this, "Sin internet, si tienes alguna duda contacta soporte" + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    public void cargarIdBodega() {
        bodega.codigoBodega(getApplicationContext(), V_URL_MON, BodegaOrigen.getText().toString(), getString(R.string.ingresar_producto));

    }


    public static void guardarDatos(final Context context, String V_URL_MON) throws JSONException {

        RequestQueue queue = Volley.newRequestQueue(context);
        String Ruta = null;

        JSONObject movimiento = new JSONObject();
        movimiento.put("Codigo", tnuevo_codigo.getText().toString());
        movimiento.put("Descripcion", tDescripcion.getText().toString());
        movimiento.put("Marca", tMarca.getText().toString());
        movimiento.put("Grupo", tGrupo.getText().toString());
        movimiento.put("bodegas", DatosProductos.ID_ORIGEN);
        if(!tcantidad.getText().toString().trim().isEmpty()){
            movimiento.put("Cantidad", tcantidad.getText().toString());
            Toasty.success(context, "El producto fue registrado con cantidad", Toasty.LENGTH_SHORT).show();
        }

        Ruta = V_URL_MON + "/api/activosfijos/guardar";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Toasty.success(context, "EL producto fue registrado correctamente", Toasty.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        queue.add(request);

    }

    private void comprobarCodigo() throws JSONException {

        String Ruta = null;

        JSONObject movimiento = new JSONObject();
        movimiento.put("Codigo", tnuevo_codigo.getText().toString());


        Ruta = V_URL_MON + "/api/activosfijos/comprobarCodigo";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, movimiento, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {

                    if (response.has("Respuesta")) {
                        String Respuesta = response.getString("Respuesta");

                        if (Respuesta.equals("0")) {
                            cargarIdBodega();
                        } else {
                            AlertDialog alertDialog = new AlertDialog.Builder(IngresarProducto.this).create();
                            alertDialog.setTitle("No se puede registrar el producto");
                            alertDialog.setMessage("Al parecer el código ya está registrado en la base de datos");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(IngresarProducto.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    private void comprobarDatosVacios() {

        if (tnuevo_codigo.getText().toString().equals("") || tDescripcion.getText().toString().equals("") || tMarca.getText().toString().equals("") || tGrupo.getText().toString().equals("") || BodegaOrigen.getText().toString().equals("")) {

            AlertDialog alertDialog = new AlertDialog.Builder(IngresarProducto.this).create();
            alertDialog.setTitle("Mensaje");
            alertDialog.setMessage("Es necesario ingresar todos los campos");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            //Rellenamos automáticamente el producto de marca si el producto no la requiere
            tMarca.setText("Sin marca");

        } else {
            try {
                comprobarCodigo();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}
