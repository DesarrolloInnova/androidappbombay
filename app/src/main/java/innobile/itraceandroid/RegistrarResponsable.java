package innobile.itraceandroid;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.transport.T_Configuraciones;

public class RegistrarResponsable extends AppCompatActivity {

    private ImageButton btnspeak2 = null, btnspeak3 = null, btnspeak4 = null, btnspeak5 = null, btnspeak6 = null;
    private EditText Codigo = null, Nombre = null, Ciudad = null, Placa_Vehiculo = null, Tipo_Vehiculo = null, Direccion = null;
    private int comprobarBoton = 0;
    private Button btnGuardar = null;
    private RequestQueue requestQueue;
    private String bodegaOrigen = "", bodegaDestino = "", V_URL = null, V_URL_MON = null, V_TipoConexion = null, id_Bodega_Destino = "", id_bodega_Origen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_responsable);


        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(new NoCache(), network);
        // Start the queue
        requestQueue.start();

        //Referenciando los elementos desde las vistas
        Codigo = findViewById(R.id.tnuevo_codigo);
        Nombre = findViewById(R.id.tNombre);
        Ciudad = findViewById(R.id.tCiudad);
        Placa_Vehiculo = findViewById(R.id.tplaca_vehiculo);
        Tipo_Vehiculo = findViewById(R.id.tTipo_Vehiculo);
        Direccion = findViewById(R.id.tDireccion);


        btnspeak2 = findViewById(R.id.btnspeak2);
        btnspeak3 = findViewById(R.id.btnspeak3);
        btnspeak4 = findViewById(R.id.btnspeak4);
        btnspeak5 = findViewById(R.id.btnspeak5);
        btnspeak6 = findViewById(R.id.btnspeak6);

        btnGuardar = findViewById(R.id.btnGuardar);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Generando el código del documento
        UUID id_documento = UUID.randomUUID();
        String[] separandoCodigo = id_documento.toString().split("-");
        String codigo = separandoCodigo[0];

        Codigo.setText(codigo);


        //Evitar campos nulos
        Nombre.setText("");
        Ciudad.setText("");
        Placa_Vehiculo.setText("");
        Tipo_Vehiculo.setText("");
        Direccion.setText("");


        //Agregando funcionalidad a los botones


        btnspeak2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 1;
                comandosVoz();
            }
        });

        btnspeak3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 2;
                comandosVoz();
            }
        });

        btnspeak4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 3;
                comandosVoz();
            }
        });

        btnspeak5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 4;
                comandosVoz();
            }
        });

        btnspeak6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarBoton = 5;
                comandosVoz();
            }
        });


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    guardarDatos();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    private void comandosVoz() {
        Intent voice = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());
        voice.putExtra(RecognizerIntent.EXTRA_PROMPT, "Asistente Innova");
        startActivityForResult(voice, 1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            ArrayList<String> arrayList = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);


            if (comprobarBoton == 1) {
                Nombre.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 2) {
                Ciudad.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 3) {
                Placa_Vehiculo.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 4) {
                Tipo_Vehiculo.setText(arrayList.get(0).toString());
            } else if (comprobarBoton == 5) {
                Direccion.setText(arrayList.get(0).toString());
            } else {
                Toast.makeText(getApplicationContext(), "Ocurrió un error", Toast.LENGTH_LONG).show();
            }

        }

    }

    public void guardarDatos() throws JSONException {


        String Ruta = null;


        /***
         *   Codigo = findViewById(R.id.tnuevo_codigo);
         *         Nombre = findViewById(R.id.tNombre);
         *         Ciudad = findViewById(R.id.tCiudad);
         *         Placa_Vehiculo = findViewById(R.id.tplaca_vehiculo);
         *         Tipo_Vehiculo = findViewById(R.id.tTipo_Vehiculo);
         *         Direccion = findViewById(R.id.tDireccion);
         *
         *
         */

        JSONObject responsable = new JSONObject();
        responsable.put("Codigo", Codigo.getText().toString());
        responsable.put("Nombre", Nombre.getText().toString());
        responsable.put("Ciudad", Ciudad.getText().toString());
        responsable.put("Placa_Vehiculo", Placa_Vehiculo.getText().toString());
        responsable.put("Tipo_Vehiculo", Tipo_Vehiculo.getText().toString());
        responsable.put("Direccion", Direccion.getText().toString());

        Ruta = V_URL_MON + "/api/responsable/guardar";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, responsable, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Toast.makeText(RegistrarResponsable.this, "EL responsable fue registrado correctamente", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegistrarResponsable.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }
}

//Codigo, Nombre, Ciudad, Placa_Vehiculo, Tipo_Vehiculo, Direccion

