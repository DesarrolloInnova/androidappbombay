package innobile.itraceandroid.Retina;

public class ModeloDeleteRetina {
    String descripcion;
    String extension;
    String serial;
    String fecha_vencimiento;
    String lote;
    String id;

    public ModeloDeleteRetina() {
    }

    public ModeloDeleteRetina(String descripcion, String extension, String serial, String fecha_vencimiento, String lote, String id) {
        this.descripcion = descripcion;
        this.extension = extension;
        this.serial = serial;
        this.fecha_vencimiento = fecha_vencimiento;
        this.lote = lote;
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
