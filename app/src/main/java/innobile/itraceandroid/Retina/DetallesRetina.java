package innobile.itraceandroid.Retina;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import java.util.ArrayList;
import java.util.List;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.SwipeController;
import innobile.itraceandroid.SwipeControllerActions;

public class DetallesRetina extends AppCompatActivity {


    private static ConexionSQLiteHelper conn;
    public DeleteRetinaAdapter mAdapter;
    SwipeController swipeController = null;
    private List<ModeloDeleteRetina> detalles = new ArrayList<>();
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_retina);
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new DeleteRetinaAdapter(detalles);
        recuperarDatosLeidos();
        setupRecyclerView();
    }


    public void recuperarDatosLeidos(){
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLARETINADETALLES, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLARETINADETALLES; //Ingresa el nombre de tu tabla
        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " ORDER BY " + Utilidades.retinaId + " DESC;", null);
        if (c.moveToFirst()) {
            do {
                ModeloDeleteRetina detalle = new ModeloDeleteRetina();
                detalle.setDescripcion(c.getString(2));
                detalle.setExtension( c.getString(0));
                detalle.setSerial( c.getString(1));
                detalle.setFecha_vencimiento(c.getString(3));
                detalle.setLote(c.getString(4));
                detalle.setId(String.valueOf(c.getInt(8)));
                detalles.add(detalle);
            } while (c.moveToNext());
        }
        db.close();
        mAdapter.notifyDataSetChanged();
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                try{
                    borrarDatosSqlite(mAdapter.productos.remove(position).getId());
                }catch (Exception e){
                    Toasty.warning(getApplicationContext(), "No se puedo eliminar el registro", Toasty.LENGTH_LONG).show();
                }
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    /**
     * Se encarga de borrar un registro de la base de datos
     * */

    public void borrarDatosSqlite(String id){
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLARETINADETALLES, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        String myTable = Utilidades.TABLARETINADETALLES;//Set name of your table
        Cursor c = db.rawQuery("DELETE FROM " + myTable + " WHERE " + Utilidades.retinaId + " = '" + id + "';", null);
        c.moveToFirst();
        db.close();
    }
}
