package innobile.itraceandroid.Retina;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import innobile.itrace.R;


class DeleteRetinaAdapter extends RecyclerView.Adapter<DeleteRetinaAdapter.PlayerViewHolder> {
    public List<ModeloDeleteRetina> productos;



    public class PlayerViewHolder extends RecyclerView.ViewHolder {
        private TextView descripcion, extension, serial, fecha_vencimiento, lote, t_id;

        public PlayerViewHolder(View view) {
            super(view);
            descripcion = (TextView) view.findViewById(R.id.t_descripcion);
            extension = (TextView) view.findViewById(R.id.t_extension);
            serial = (TextView) view.findViewById(R.id.t_serial);
            fecha_vencimiento = (TextView) view.findViewById(R.id.t_fecha);
            lote = (TextView) view.findViewById(R.id.t_lote);
            t_id = view.findViewById(R.id.t_id);

        }
    }

    public DeleteRetinaAdapter(List<ModeloDeleteRetina> productos) {
        this.productos = productos;
    }

    @Override
    public DeleteRetinaAdapter.PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delete_retina_row, parent, false);

        return new DeleteRetinaAdapter.PlayerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeleteRetinaAdapter.PlayerViewHolder holder, int position) {
        ModeloDeleteRetina productos = this.productos.get(position);
        holder.descripcion.setText(productos.getDescripcion());
        holder.extension.setText(productos.getExtension());
        holder.serial.setText(productos.getSerial());
        holder.fecha_vencimiento.setText(productos.getFecha_vencimiento());
        holder.lote.setText(productos.getLote());
        holder.t_id.setText(productos.getId());
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}

