package innobile.itraceandroid.Retina;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.ContextCompat;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.Producto_Existencia;
import innobile.itraceandroid.RetrofitAdapter.UbicacionRetina;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class InventarioRetina extends AppCompatActivity {

    public static Retrofit retrofit;
    private static ConexionSQLiteHelper conn;
    private EditText codeRead;
    private AutoCompleteTextView autocompleteCellar;
    private int checkIncome = 0;
    private String LOTE = "000000", PATH = "", V_URL_MON = "", bodega_seleccionada = "";
    private Button btnNube, btnLocal, btnCargarDatos, btnSalir, btnDetalle;
    private Intent buscarArchivo;
    private InputStream inputStream = null;
    private List<String> lista_ubicaciones = new ArrayList<String>();
    private long cn = 0;
    private TextView  t_Ubicaciones, tBodega, t_Referencia, t_Extension, t_Serial, t_Vencimiento, t_Cantidad, t_Lote, tConteo, bodega_Header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario_retina);

        codeRead = findViewById(R.id.codigo_leido);
        btnCargarDatos = findViewById(R.id.btnCargarDatos);
        btnDetalle = findViewById(R.id.btnDetalle);
        tConteo = findViewById(R.id.t_conteo);


        t_Ubicaciones = findViewById(R.id.t_ubicacion);
        tBodega = findViewById(R.id.t_bodega);
        t_Referencia = findViewById(R.id.t_referencia);
        t_Extension = findViewById(R.id.t_extension);
        t_Serial = findViewById(R.id.t_serial);
        t_Vencimiento = findViewById(R.id.t_vencimiento);
        t_Cantidad = findViewById(R.id.t_cantidad);
        t_Lote = findViewById(R.id.t_lote);
        bodega_Header = findViewById(R.id.t_bodega_header);

        //URL a la que se le va a realizar la peticion -->  Ejemplo: http://192.168.1.78:3000/api
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL_MON = t_configuraciones.getCon_URL();

        //Se detectan los datos ingresados en el EditText automáticamente
        codeRead.setOnKeyListener((view, i, keyEvent) -> {
            //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
            if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                //Este contador se encarga de enviar los datos solo una vez

                if (!codeRead.getText().toString().trim().isEmpty()) {

                    try {
                        String[] split = codeRead.getText().toString().trim().split(".:");
                        String p3 = split[2];
                        String[] second_split = p3.trim().split(" ");
                        String codigo = second_split[0];
                        buscarCodigo(codigo);
                        codeRead.setText("");

                    } catch (ArrayIndexOutOfBoundsException e) {
                        extraerCodigoLote(codeRead.getText().toString().trim());
                    }

                }
                return true;
            }
            return false;
        });
        comprobarDatosTablaUbicaciones();

        btnCargarDatos.setOnClickListener(view -> cargarDatosInventario());
        btnDetalle.setOnClickListener(view -> {
            Intent intent = new Intent(InventarioRetina.this, DetallesRetina.class);
            startActivity(intent);
        });
    }


    public void comprobarDatosTablaUbicaciones() {
        if (cantidadRegistrosInventario() == 0) {
            cargarDatosInventario();
        }
    }




    public void extraerCodigoLote(String codigo) {
        try {
            if (codigo.substring(0, 2).equals("01")) {
                if (codigo.substring(16, 18).equals("10")) {
                    LOTE = codigo.substring(18, 25);
                } else if (codigo.substring(16, 18).equals("17")) {
                    if (codigo.substring(24, 26).equals("10")) {
                        if (codigo.length() == 33) {
                            LOTE = codigo.substring(26, 33);
                        } else {
                            LOTE = codigo.substring(26, 31);
                        }
                    } else if (codigo.substring(24, 26).equals("21")) {
                        if (codigo.length() == 34) {
                            LOTE = codigo.substring(26, 34);
                        } else {
                            LOTE = codigo.substring(26, 32);
                        }
                    }
                }
            } else {
                LOTE = codigo;
            }

            buscarCodigo(LOTE);
        } catch (StringIndexOutOfBoundsException e) {
            Toasty.warning(getApplicationContext(), "Error al tratar de leer el código", Toasty.LENGTH_SHORT).show();
        }

    }

    /**
     * .TXT
     */
    public void cargarDatosInventario() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(InventarioRetina.this);
        View mView = getLayoutInflater().inflate(R.layout.cargarinventairoretina, null);
        btnNube = mView.findViewById(R.id.btn_nube);
        btnLocal = mView.findViewById(R.id.btn_local);
        btnSalir = mView.findViewById(R.id.btn_salir);
        autocompleteCellar = mView.findViewById(R.id.complete_bodegas);


        if (cantidadRegistrosUbicaciones() == 0) {

            autocompleteCellar.setText("Espere... ");

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(V_URL_MON + "/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
            Call<List<UbicacionRetina>> call = jsonPlaceHolderApi.getUbicaciones();


            call.enqueue(new Callback<List<UbicacionRetina>>() {
                @Override
                public void onResponse(Call<List<UbicacionRetina>> call, Response<List<UbicacionRetina>> response) {

                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                        return;
                    }
                    for (UbicacionRetina ubicaciones_response : response.body()) {
                        CargarUbicaciones(ubicaciones_response.getUBICACION());
                    }

                    autocompleteCellar.setText("");
                }

                @Override
                public void onFailure(Call<List<UbicacionRetina>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), String.valueOf(t), Toast.LENGTH_SHORT).show();
                }
            });

        } else if (cantidadRegistrosUbicaciones() > 0) {
            mostrarUbicaciones();
        }

        btnLocal.setOnClickListener(view -> {
            buscarArchivo = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            buscarArchivo.addCategory(Intent.CATEGORY_OPENABLE);
            buscarArchivo.setType("text/*");
            startActivityForResult(buscarArchivo, 10);
        });

        btnNube.setOnClickListener(view -> {
            Toasty.warning(getApplicationContext(), "Espere...", Toasty.LENGTH_LONG).show();
            traerInformacionRetrofit(autocompleteCellar.getText().toString().trim(), "*");
        });

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnSalir.setOnClickListener(view -> {
            if (!autocompleteCellar.getText().toString().trim().isEmpty()) {
                bodega_seleccionada = autocompleteCellar.getText().toString();
                bodega_Header.setText(bodega_seleccionada);
                dialog.dismiss();
            } else {
                Toasty.warning(getApplicationContext(), "Es necesario ingresar una bodega", Toasty.LENGTH_SHORT).show();
            }

        });
    }


    public void CargarUbicaciones(String ubicacion) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_UBICACIONES, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();

        db.beginTransactionNonExclusive();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Utilidades.UBICACIONES_RETINA, ubicacion.trim());
            db.insert(Utilidades.TABLA_UBICACIONES, null, contentValues);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        db.close();
        mostrarUbicaciones();
    }

    private void mostrarUbicaciones() {


        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_UBICACIONES, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_UBICACIONES; //Ingresa el nombre de tu tabla
        Cursor c = db.rawQuery("SELECT * FROM " + myTable, null);
        if (c.moveToFirst()) {
            do {
                lista_ubicaciones.add(c.getString(0));
            } while (c.moveToNext());
        }
        db.close();

        ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(InventarioRetina.this, android.R.layout.simple_spinner_dropdown_item, lista_ubicaciones);
        autocompleteCellar.setAdapter(adapterCentralCosto);


        autocompleteCellar.setOnClickListener(view -> autocompleteCellar.showDropDown());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        try {
                            generarUbicacionArchivo(data);
                            new AsyncCaller().execute();
                        } catch (RuntimeException e) {
                            new SweetAlertDialog(InventarioRetina.this)
                                    .setTitleText("El archivo no tiene el formato correcto")
                                    .show();
                        }
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    }
                }
                break;
        }
    }

    public void generarUbicacionArchivo(@Nullable Intent data) {
        Uri uri = data.getData();
        String path = uri.getPath();
        PATH = path.substring(path.indexOf(":") + 1);
    }

    /**
     * Se encarga de guardar la información de las bodegas desde .txt a Sqlite
     * Autor:CarlosDnl
     * Fecha: 25-02-2020
     */

    public void cargarInformacionInventario(String[] texto) {
        try {
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_RETINA, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            //Reinicio de la base de datos
            db.delete(Utilidades.TABLA_RETINA, null, null);

            db.beginTransactionNonExclusive();
            try {
                for (int i = 0; i < texto.length; i++) {
                    String[] linea = texto[i].split(";");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Utilidades.SqUBICACIONES,linea[0]);
                    contentValues.put(Utilidades.SqBODEGA, linea[1]);
                    contentValues.put(Utilidades.SqREFERENCIA, linea[2]);
                    contentValues.put(Utilidades.SqEXTENSION,linea[3]);
                    contentValues.put(Utilidades.SqSERIAL, linea[4]);
                    contentValues.put(Utilidades.SqVENCIMIENTO, linea[5]);
                    contentValues.put(Utilidades.SqCANTIDAD, linea[6]);
                    contentValues.put(Utilidades.SqLOTE,linea[7]);
                    db.insert(Utilidades.TABLA_RETINA, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

            db.close();
            new SweetAlertDialog(InventarioRetina.this)
                    .setTitleText("Se insertarón " + cantidadRegistrosInventario() + " registros para realizar el inventario")
                    .show();


        } catch (RuntimeException e) {
            new SweetAlertDialog(InventarioRetina.this)
                    .setTitleText("El archivo no tiene el formato correcto")
                    .show();
        }
    }

    public void traerInformacionRetrofit(String ubicacion, String valor) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(V_URL_MON + "/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<Producto_Existencia>> call = jsonPlaceHolderApi.postBuscarProducto(ubicacion, valor);

        call.enqueue(new Callback<List<Producto_Existencia>>() {
            @Override
            public void onResponse(Call<List<Producto_Existencia>> call, Response<List<Producto_Existencia>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }

                ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_RETINA, null, 1);
                SQLiteDatabase db = conn.getWritableDatabase();
                //Reinicio de la base de datos
                db.delete(Utilidades.TABLA_RETINA, null, null);
                db.close();


                for (Producto_Existencia producto : response.body()) {
                    cargarInformacionNube(producto.getUBICACION().trim(), producto.getBODEGA().trim(), producto.getREFERENCIA().trim(), producto.getEXTENSION().trim(), producto.getSERIAL().trim(), producto.getVENCIMIENTO().trim(),String.valueOf(producto.getCANTIDAD()).trim(), producto.getLOTE().trim());
                }

                if(cantidadRegistrosInventario() > 0){
                    Toasty.success(getApplicationContext(), "Los datos fueron cargados correctamente", Toasty.LENGTH_SHORT).show();
                }else if(cantidadRegistrosInventario() == 0){
                    Toasty.error(getApplicationContext(), "La bodega seleccionada no tiene registros", Toasty.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<List<Producto_Existencia>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), String.valueOf(t), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void cargarInformacionNube(String UBICACION, String BODEGA, String REFERENCIA, String EXTENSION, String SERIAL, String VENCIMIENTO, String CANTIDAD, String LOTE) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_RETINA, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        //Reinicio de la base de datos




        db.beginTransactionNonExclusive();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Utilidades.SqUBICACIONES, UBICACION);
            contentValues.put(Utilidades.SqBODEGA, BODEGA);
            contentValues.put(Utilidades.SqREFERENCIA, REFERENCIA);
            contentValues.put(Utilidades.SqEXTENSION, EXTENSION);
            contentValues.put(Utilidades.SqSERIAL, SERIAL);
            contentValues.put(Utilidades.SqVENCIMIENTO, VENCIMIENTO);
            contentValues.put(Utilidades.SqCANTIDAD, CANTIDAD);
            contentValues.put(Utilidades.SqLOTE, LOTE);
            db.insert(Utilidades.TABLA_RETINA, null, contentValues);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    private Long cantidadRegistrosInventario() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_RETINA, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_RETINA);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    private Long cantidadRegistrosUbicaciones() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_UBICACIONES, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_UBICACIONES);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    private Long cantidadRegistrosDetalles() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLARETINADETALLES, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLARETINADETALLES);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn;
    }

    public void buscarCodigo(String codigo) {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_RETINA, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_RETINA;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " WHERE " + Utilidades.SqSERIAL + " = '" + codigo + "' OR " + Utilidades.SqLOTE + " ='" + codigo + "';", null);
        if (c.moveToFirst()) {

            //t_Ubicaciones, tBodega, t_Referencia, t_Extension, t_Serial, t_Vencimiento, t_Cantidad, t_Lote,
            do {
                t_Ubicaciones.setText(c.getString(0));
                tBodega.setText(c.getString(1));
                t_Referencia.setText(c.getString(2));
                t_Extension.setText(c.getString(3));
                t_Serial.setText(c.getString(4));
                t_Vencimiento.setText(c.getString(5));
                t_Cantidad.setText(c.getString(6));
                t_Lote.setText(c.getString(7));

            } while (c.moveToNext());
            guardarDatoLeido();
        } else {
            Toasty.warning(getApplicationContext(), "No se encontro el código ");
            t_Ubicaciones.setText("Sin registros");
            tBodega.setText("");
            t_Referencia.setText("");
            t_Extension.setText("");
            t_Serial.setText("");
            t_Vencimiento.setText("");
            t_Cantidad.setText("");
            t_Lote.setText("");
        }
        db.close();
        codeRead.setText("");
       
    }


    //t_Ubicaciones, tBodega, t_Referencia, t_Extension, t_Serial, t_Vencimiento, t_Cantidad, t_Lote,

    public void guardarDatoLeido() {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLARETINADETALLES, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        db.beginTransactionNonExclusive();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Utilidades.retinaUBICACIONES, t_Ubicaciones.getText().toString());
            contentValues.put(Utilidades.retinaBODEGA, tBodega.getText().toString());
            contentValues.put(Utilidades.retinaREFERENCIA, t_Referencia.getText().toString());
            contentValues.put(Utilidades.retinaEXTENSION, t_Extension.getText().toString());
            contentValues.put(Utilidades.retinaSERIAL, t_Serial.getText().toString());
            contentValues.put(Utilidades.retinaVENCIMIENTO, t_Vencimiento.getText().toString());
            contentValues.put(Utilidades.retinaCANTIDAD, t_Cantidad.getText().toString());
            contentValues.put(Utilidades.retinaLOTE, t_Lote.getText().toString());
            db.insert(Utilidades.TABLARETINADETALLES, null, contentValues);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
        tConteo.setText(String.valueOf(cantidadRegistrosDetalles()));
    }

    private class AsyncCaller extends AsyncTask<Void, Void, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Void... params) {
            try {
                inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/" + PATH);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                int i = inputStream.read();
                while (i != -1) {
                    byteArrayOutputStream.write(i);
                    i = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return byteArrayOutputStream.toString().trim().split("\n");
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            cargarInformacionInventario(result);
        }
    }
}
