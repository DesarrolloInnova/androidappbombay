package innobile.itraceandroid.ProyectoAnterior;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import innobile.itrace.Activity.InventarioActivity;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;

public class ActividadGeneral extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_general);
        Button btnGuardar = findViewById(R.id.btnGuardar);
        Switch save = findViewById(R.id.Save);
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado",null,1);

        NumeroDeRegistros();

        //Enviando el estado desde la base de datos
        save.setChecked(ComprobarSwitch());


        //Comprobando si el switch es TRUE o FALSE
        save.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    GuardarEstado("0");
                    Toast.makeText(getApplicationContext(), "Trabajando en modo offline",Toast.LENGTH_SHORT).show();
                } else {
                    GuardarEstado("1");
                    Toast.makeText(getApplicationContext(), "Trabajando en modo Online",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void GuardarEstado(String Estado) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado",null,1);

        SQLiteDatabase db = conn.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_ESTADO,  Estado);
        db.insert(Utilidades.TABLA_ESTADO, null, values);
        db.close();
    }



    public void ActualizarEstado(String Estado) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado",null,1);

        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Utilidades.CAMPO_ESTADO,  Estado);
        db.update(Utilidades.TABLA_ESTADO,  values,null, null);
        db.close();
    }

    public Boolean ComprobarSwitch() {
        Boolean band = false;
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado", null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_ESTADO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM '" + myTable + "'", null);

        String json = "";

        if (c.moveToFirst()) {
            do {
                json = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();

       if(json.equals("0")){
           band = true;
       }


       return band;
    }

    public void NumeroDeRegistros(){
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, Utilidades.TABLA_REGISTRO, null,1);
        SQLiteDatabase db = conn.getWritableDatabase();
        Long numRows = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_REGISTRO);
        EditText textView6 = findViewById(R.id.txtConteo);
        textView6.setText(numRows.toString());
    }



}
