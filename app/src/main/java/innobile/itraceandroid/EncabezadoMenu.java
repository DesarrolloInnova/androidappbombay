package innobile.itraceandroid;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.Bombay.Crear_Requisicion;

public class EncabezadoMenu extends AppCompatActivity {

    private MaterialBetterSpinner spinner1;
    private MaterialBetterSpinner spinner2;
    private MaterialBetterSpinner spinner3;
    private String tipoMovimiento = "";
    private List<GetDataAdapter> lstAnime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_menu);

        //Enviar dato del código
        final TextView txtMostrarCodigo = findViewById(R.id.txtMostrarCodigo);
        txtMostrarCodigo.setText(getIntent().getStringExtra("Codigo"));
        TextView txtMostrarFecha = findViewById(R.id.txtMostrarFecha);
        txtMostrarFecha.setText(getIntent().getStringExtra("Fecha"));

        spinner1 = (MaterialBetterSpinner) findViewById(R.id.material_spinner1);
        spinner2 = (MaterialBetterSpinner) findViewById(R.id.material_spinner2);
        spinner3 = (MaterialBetterSpinner) findViewById(R.id.material_spinner3);


        //Definiendo el tipo de documento
        tipoMovimiento = Global.TIPO_DOCUMENTO;

        spinner1.setText("Esperando datos");
        spinner2.setText("Esperando datos");
        spinner3.setText("Esperando datos");

        //Rellenando los spinners con los datos bajados desde la nube
        try {
            Spinner1();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Spinner2();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Spinner3();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String[] data = {" ", " "};
        RellenarSpinner3(data);

        RellenarSpinner(data);

        String[] send = {" ", " "};
        RellenarSpinner2(send);

        Button buttondesalle = findViewById(R.id.Buttondetalle);


        if (tipoMovimiento.equals("RQ")) {
            buttondesalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(EncabezadoMenu.this, Crear_Requisicion.class);
                    intent.putExtra("codigo", txtMostrarCodigo.getText().toString());
                    intent.putExtra("prioridad", spinner1.getText().toString());
                    intent.putExtra("central_costo", spinner2.getText().toString());
                    intent.putExtra("responsable", spinner3.getText().toString());
                    startActivity(intent);
                }
            });

        } else if (tipoMovimiento.equals("RM")) {
            buttondesalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(EncabezadoMenu.this, OrdeCompra.class);
                    intent.putExtra("codigo", txtMostrarCodigo.getText().toString());
                    intent.putExtra("prioridad", spinner1.getText().toString());
                    intent.putExtra("central_costo", spinner2.getText().toString());
                    intent.putExtra("responsable", spinner3.getText().toString());
                    startActivity(intent);
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "No se encontro el tipo de movimiento", Toast.LENGTH_SHORT).show();
        }


        //Traer los datos

        CapturasListaRequisicion(getApplicationContext(), Integer.parseInt(txtMostrarCodigo.getText().toString()));


    }


    public void Spinner1() throws JSONException {

    }


    public void Spinner2() throws JSONException {

    }

    public void Spinner3() throws JSONException {

    }


    public void RellenarSpinner(String[] SPINNER_DATA) {


        spinner1 = (MaterialBetterSpinner) findViewById(R.id.material_spinner1);

        ArrayAdapter<String> adapterPrioridad = new ArrayAdapter<String>(EncabezadoMenu.this, android.R.layout.simple_dropdown_item_1line, SPINNER_DATA);

        spinner1.setAdapter(adapterPrioridad);

    }


    public void RellenarSpinner2(String[] SPINNER_COSTO) {


        spinner2 = (MaterialBetterSpinner) findViewById(R.id.material_spinner2);

        ArrayAdapter<String> adapterCosto = new ArrayAdapter<String>(EncabezadoMenu.this, android.R.layout.simple_dropdown_item_1line, SPINNER_COSTO);

        spinner2.setAdapter(adapterCosto);

    }

    public void RellenarSpinner3(String[] SPINNER_RESPONSABLE) {
        //Tercer spinner

        spinner3 = (MaterialBetterSpinner) findViewById(R.id.material_spinner3);

        ArrayAdapter<String> adapterResponsable = new ArrayAdapter<String>(EncabezadoMenu.this, android.R.layout.simple_dropdown_item_1line, SPINNER_RESPONSABLE);

        spinner3.setAdapter(adapterResponsable);

        // jsonObjectRequest();

    }


    public void CapturasListaRequisicion(Context context, int CodigoActual) {
        String prioridad = "";
        String central = "";
        String responsable = "";

        spinner1 = (MaterialBetterSpinner) findViewById(R.id.material_spinner1);
        spinner2 = (MaterialBetterSpinner) findViewById(R.id.material_spinner2);
        spinner3 = (MaterialBetterSpinner) findViewById(R.id.material_spinner3);


        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_ENCABEZADO, null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_ENCABEZADO;//Set name of your table

        Cursor c = db.rawQuery("SELECT * FROM  '" + myTable + "' where Numero_documento = " + CodigoActual, null);


        if (c.moveToFirst()) {
            do {
                prioridad = c.getString(2);
                central = c.getString(3);
                responsable = c.getString(4);
            } while (c.moveToNext());
        }
        db.close();

        spinner1.setText(prioridad);
        spinner2.setText(central);
        spinner3.setText(responsable);
    }

    public void ImprimirDatos(ArrayList Lista, Context context) {

        for (int i = 0; i < Lista.size(); i++) {
            Toast.makeText(context, "probando", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, Lista.get(i).toString(), Toast.LENGTH_SHORT).show();
        }
    }

}


