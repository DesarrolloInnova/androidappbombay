package innobile.itraceandroid;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import innobile.itrace.Activity.RecyclerItemTouchHelper;
import innobile.itrace.Activity.RecyclerItemTouchHelperRequisicion;
import innobile.itrace.Adapter.AdapterInv;
import innobile.itrace.Adapter.AdapterRequisicion;
import innobile.itrace.DAO.DAOInventario;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.entidades.Registros;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.RequisicionData;

public class DetalleRequisicion extends AppCompatActivity implements RecyclerItemTouchHelperRequisicion.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private TextView txtConteo;
    int valor = 0;
    private ListView MovimientosListaView;
    ArrayList<RequisicionData> ListInventario;
    private static AdapterRequisicion adapterInv;
    private DAOInventario daoInventario;
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_requisicion);

        txtConteo = findViewById(R.id.txtConteo);
        recyclerView = (RecyclerView) findViewById(R.id.list);

        daoInventario = new DAOInventario();
        Button buttonSugerencia = findViewById(R.id.botonSugerencia);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        NumeroDeRegistros();


        swipeRefreshLayout.setOnRefreshListener(() -> {
            cargarListado();
            NumeroDeRegistros();
            swipeRefreshLayout.setRefreshing(false);
        });


        buttonSugerencia.setOnClickListener(view -> Toast.makeText(getApplicationContext(), "Ingrese la cantidad que desea borrar y deslice el datos al que desea aplicar el borrado", Toast.LENGTH_LONG).show());

    }


    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AdapterRequisicion.ViewHolder) {

            // get the removed item code
            //  int id = ListInventario.get(viewHolder.getAdapterPosition()).getNr_documento();

            // backup of removed item for undo purpose
            //  final RequisicionData deletedItem = ListInventario.get(viewHolder.getAdapterPosition());
            //  final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapterInv.removeItem(viewHolder.getAdapterPosition());
            EditText cantidad = findViewById(R.id.plain_text_input);


        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        cargarListado();
    }

    //CARGAR LA TABLA DE INVENTARIO EN UNA LISTA.
    public void cargarListado() {

        ListInventario = DAOInventario.CapturasListaRequisicion(getApplicationContext(), Integer.parseInt(getIntent().getStringExtra("codigo")));
        adapterInv = new AdapterRequisicion(ListInventario);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapterInv);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelperRequisicion(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


    }

    public void NumeroDeRegistros() {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, Utilidades.TABLA_REGISTRO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Long numRows = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_REGISTRO);
        TextView textView6 = findViewById(R.id.txtConteo);
        textView6.setText(String.valueOf(numRows));
    }


}

