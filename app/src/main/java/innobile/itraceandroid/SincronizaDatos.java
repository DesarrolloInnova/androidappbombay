package innobile.itraceandroid;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import innobile.itrace.Activity.InventarioActivity;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;

public class SincronizaDatos extends AppCompatActivity {


    private String V_URL = null, V_URL_MON = null;
    private String V_TipoConexion = null;
    private ConexionSQLiteHelper conn;
    private SQLiteDatabase db;
    private LinearLayout sincronizarLocalmente, sincronizarNube;
    private ProgressBar pgsBar;
    private Button btnSincronizar;
    private static String DB_PATH = "/data/data/innobile.itraceandroid/databases/";
    private Animation fade;
    private int MY_TIMEOUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sincroniza_datos);


        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        sincronizarLocalmente = findViewById(R.id.sincronizarLocalmente);
        sincronizarNube = findViewById(R.id.sincronizarNube);
        btnSincronizar = findViewById(R.id.btnSincronizar);

        pgsBar.setVisibility(View.GONE);

        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_PRODUCTOS, null, 1);
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();

        fade = AnimationUtils.loadAnimation(this, R.anim.fade);
        btnSincronizar.setAlpha(0);


        sincronizarLocalmente.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                sincronizarLocalmente.setBackground(getDrawable(R.drawable.bg_item_selected));
                btnSincronizar.startAnimation(fade);
                btnSincronizar.setAlpha(1);
                btnSincronizar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        traerDatosProductos();
                    }
                });
            }
        });


        sincronizarNube.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                sincronizarNube.setBackground(getDrawable(R.drawable.bg_item_selected));
                btnSincronizar.startAnimation(fade);
                btnSincronizar.setAlpha(1);
                btnSincronizar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        convertirSQLiteaJson();
                    }
                });
            }
        });
    }

    public void traerDatosProductos() {

        RequestQueue queue = Volley.newRequestQueue(this);
        String Ruta = null;


        pgsBar.setVisibility(View.VISIBLE);

        db = conn.getWritableDatabase();

        //URL para realizar la peticion
        Ruta = V_URL_MON + "/api/producto/list";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Ruta, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {


                        try {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                //Se recorre cada uno de los elementos del json y se insertan en la base de datos SQLite
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ContentValues values = new ContentValues();
                                values.put(Utilidades.CODIGO_PRODUCTO, jsonObject.getString("Codigo").trim());
                                values.put(Utilidades.DESCRIPCION_PRODUCTO, jsonObject.getString("Descripcion").trim());
                                db.insert(Utilidades.TABLA_PRODUCTOS, null, values);
                            }

                            pgsBar.setVisibility(View.GONE);
                            //Al terminar se cierra la conexión con la base de datos

                            AlertDialog alertDialog = new AlertDialog.Builder(SincronizaDatos.this).create();
                            alertDialog.setTitle("Datos descargados");
                            alertDialog.setMessage("Los datos fueron guardados correctamente");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {


                    @Override

                    public void onErrorResponse(VolleyError volleyError) {
                        AlertDialog alertDialog = new AlertDialog.Builder(SincronizaDatos.this).create();
                        alertDialog.setTitle("Mmm");
                        alertDialog.setMessage("Error al tratar de sincronizar los datos");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                        pgsBar.setVisibility(View.GONE);
                    }


                });

        queue.add(request);
    }


    public void enviarDatosNube(JSONObject data) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String Ruta = null;


        Ruta = V_URL_MON + "/api/movimiento/guardar";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Ruta, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Guardando la respuesta como un JSONArray

            }
        }, error -> {
            AlertDialog alertDialog = new AlertDialog.Builder(SincronizaDatos.this).create();
            alertDialog.setTitle("Error al tratar de sincronizar");
            alertDialog.setMessage("Los datos no se pudieron sincronizar");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        });


        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT * 5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    private JSONObject convertirSQLiteaJson() {

        String myPath = DB_PATH + Utilidades.TABLA_MOVIMIENTO;// Set path to your database

        String myTable = Utilidades.TABLA_MOVIMIENTO;//Set name of your table

        //or you can use `context.getDatabasePath("my_db_test.db")`

        SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        String searchQuery = "SELECT  * FROM " + myTable;
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);

        JSONObject rowObject = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();


            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            enviarDatosNube(rowObject);
            cursor.moveToNext();
        }
        cursor.close();

        Log.d("TAG_NAME", rowObject.toString());
        return rowObject;
    }


}
