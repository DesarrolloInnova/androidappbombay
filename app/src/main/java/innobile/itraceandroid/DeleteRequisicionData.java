package innobile.itraceandroid;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import innobile.itrace.R;
import innobile.itraceandroid.TrasladosTM.encabezado_traslados_sqlite;


public class DeleteRequisicionData extends RecyclerView.Adapter<DeleteRequisicionData.PlayerViewHolder> {
    public List<GetDataAdapter> players;

    private Context mContext;


    public DeleteRequisicionData(Context mContext, List<GetDataAdapter> players) {
        this.players = players;
        this.mContext = mContext;

    }

    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.requisicion_row_item, parent, false);
        final DeleteRequisicionData.PlayerViewHolder viewHolder = new DeleteRequisicionData.PlayerViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Global.TIPO_DOCUMENTO.equals("AF")) {
                    Intent i = new Intent().setClass(mContext, encabezado_traslados_sqlite.class);
                    Global.CREAR_REQUISICON_MONGO = false;
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    i.putExtra("Codigo", players.get(viewHolder.getAdapterPosition()).getCodigo());
                    i.putExtra("Fecha", players.get(viewHolder.getAdapterPosition()).getFecha());
                    mContext.startActivity(i);
                } else {
                    Intent i = new Intent().setClass(mContext, EncabezadoMenu.class);
                    Global.CREAR_REQUISICON_MONGO = false;
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    i.putExtra("Codigo", players.get(viewHolder.getAdapterPosition()).getCodigo());
                    i.putExtra("Fecha", players.get(viewHolder.getAdapterPosition()).getFecha());
                    mContext.startActivity(i);
                }

            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        GetDataAdapter player = players.get(position);
        holder.tv_name.setText(player.getCodigo());
        holder.tv_rating.setText(player.getFecha());

    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;
        TextView tv_rating;
        LinearLayout view_container;

        public PlayerViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            tv_name = itemView.findViewById(R.id.anime_name);
            tv_rating = itemView.findViewById(R.id.rating);
        }
    }

}


