package innobile.itraceandroid.Fajate;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.ContextCompat;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.bodegas_id;
import innobile.itraceandroid.RetrofitAdapter.insertar_datos;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class configuracion_inventario extends AppCompatActivity {

    private static ConexionSQLiteHelper conn;
    private static Cursor c;
    private static FileWriter writer;
    private CardView sincronizarBodega, sincronizarProducto, subirMaestro, actualizarInventario;
    private LinearLayout colorBodega, colorProducto, colorMaestro, colorInventario;
    private Intent buscarArchivo;
    private RippleBackground rippleBackground;
    private long cn = 0;
    private int boton_Presionado = 0;
    private String PATH = "", bodega_Asociada = "";
    private TextView codigos;
    private RequestQueue requestQueue;
    private String[] lstBodegas = new String[]{}, lstDocumento = new String[]{};
    private List<String> datos = new ArrayList<String>();
    private List<String> documentos = new ArrayList<String>();
    private static String nombre_Archivo, myTable;
    private static SQLiteDatabase db;
    private int MY_TIMEOUT = 60000;
    private EditText fecha;
    public static Retrofit retrofit;
    private String V_URL = null, V_TipoConexion = null;
    private static AutoCompleteTextView autoCompleteBodegas, nombreDocumento, numeroDocumento;
    private Calendar calendar;
    private InputStream inputStream = null;
    private DatePickerDialog dpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_inventario2);

        sincronizarBodega = findViewById(R.id.ibtSincronizarBodega);
        sincronizarProducto = findViewById(R.id.btnSincronizarProducto);
        subirMaestro = findViewById(R.id.btnSubirMaestro);
        actualizarInventario = findViewById(R.id.btnAcctualizarInventario);
        codigos = findViewById(R.id.codigos);
        colorBodega = findViewById(R.id.cambiarColorBodega);
        colorProducto = findViewById(R.id.cambiarColorProducto);
        colorMaestro = findViewById(R.id.cambiarColorMaestro);
        colorInventario = findViewById(R.id.cambiarColorInventario);


        try {
            comprobarColorActual();
        } catch (NumberFormatException e) {
            Log.i("NumberFormatException", e.toString());
        } catch (RuntimeException e) {
            Log.i("RuntimeException", e.toString());
        }


        //Controla que solamente se generé una vez el objeto de la petición
        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }


        //Configuración de la conexión
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);
        V_URL = t_configuraciones.getCon_URL();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        /* Codigo para sincronizar la bodega */

        sincronizarBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambiarBotonPresionado(0);


                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(configuracion_inventario.this);

                View mView = getLayoutInflater().inflate(R.layout.sincronizar_bodegas, null);
                final Button nube = mView.findViewById(R.id.btnSincronizarNube);
                final Button Local = mView.findViewById(R.id.btnSincronizarLocalmente);
                rippleBackground = mView.findViewById(R.id.content);
                final Button mSalir = (Button) mView.findViewById(R.id.btnSalir);

                nube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rippleBackground.startRippleAnimation();
                        sincronizarDatosBodegas();
                    }
                });

                Local.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rippleBackground.startRippleAnimation();
                        buscarArchivoVentana();
                    }
                });


                //Se carga la vista dentro de la actividad actual
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                mSalir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                        if (cantidadRegistrosBodega() == 0) {
                            Toasty.warning(getApplicationContext(), "La tabla bodega no tiene registros", Toast.LENGTH_SHORT, true).show();
                        } else if (cantidadRegistrosBodega() > 0) {
                            Toasty.success(getApplicationContext(), "La tabla de bodega tiene datos :D", Toast.LENGTH_SHORT, true).show();
                        }

                    }
                });

            }
        });


        /*Código para sincronizar los productos*/

        sincronizarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambiarBotonPresionado(1);
                //Creando la vista del alertDialog
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(configuracion_inventario.this);
                View mView = getLayoutInflater().inflate(R.layout.sincronizar_productos, null);
                //Instanciando los elementos del componente dialog_encabezado
                final Button nube = mView.findViewById(R.id.btnSincronizarNube);
                final Button Local = mView.findViewById(R.id.btnSincronizarLocalmente);
                rippleBackground = mView.findViewById(R.id.content);
                Button mSalir = (Button) mView.findViewById(R.id.btnSalir);


                nube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rippleBackground.startRippleAnimation();
                        sincronizarDatosProductos();
                    }
                });

                Local.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rippleBackground.startRippleAnimation();
                        buscarArchivoVentana();
                    }
                });
                //Se carga la vista dentro de la actividad actual
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                mSalir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                        if (cantidaRegistrosProductos() == 0) {
                            Toasty.warning(getApplicationContext(), "La tabla productos no tiene registros", Toast.LENGTH_SHORT, true).show();
                        } else if (cantidaRegistrosProductos() > 0) {
                            Toasty.success(getApplicationContext(), "La tabla de productos tiene datos :D", Toast.LENGTH_SHORT, true).show();
                        }
                    }
                });
            }
        });

        /*Sincronizar tabla maestro*/
        subirMaestro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cantidadRegistrosBodega() > 1) {
                    cambiarBotonPresionado(2);
                    //Creando la vista del alertDialog
                    final AlertDialog.Builder mBuilder = new AlertDialog.Builder(configuracion_inventario.this);
                    View mView = getLayoutInflater().inflate(R.layout.sincronizar_maestro, null);
                    //Instanciando los elementos del componente dialog_encabezado
                    final Button nube = mView.findViewById(R.id.btnSincronizarNube);
                    final Button Local = mView.findViewById(R.id.btnSincronizarLocalmente);
                    rippleBackground = mView.findViewById(R.id.content);
                    Button mSalir = (Button) mView.findViewById(R.id.btnSalir);
                    fecha = mView.findViewById(R.id.Fecha_Documento);
                    autoCompleteBodegas = mView.findViewById(R.id.autoCompleteBodega);

                    cargarSqliteBodegas();

                    fecha.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            calendar = Calendar.getInstance();

                            int dia = calendar.get(Calendar.DAY_OF_MONTH);
                            int mes = calendar.get(Calendar.MONTH);
                            int año = calendar.get(Calendar.YEAR);

                            dpd = new DatePickerDialog(configuracion_inventario.this, new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int año, int mes, int dia) {
                                    fecha.setText((año + 1) + "-" + mes + "-" + dia);
                                }
                            }, año, mes, dia);


                            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
                            dpd.show();
                        }
                    });


                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(configuracion_inventario.this, android.R.layout.simple_spinner_dropdown_item, lstBodegas);
                    autoCompleteBodegas.setAdapter(adapterBodegas);

                    nube.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            rippleBackground.startRippleAnimation();
                            sincronizarDatosMaestro();
                        }
                    });

                    Local.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (autoCompleteBodegas.getText().toString().length() > 0) {
                                bodega_Asociada = autoCompleteBodegas.getText().toString();
                                rippleBackground.startRippleAnimation();
                                buscarArchivoVentana();
                            } else {
                                Toasty.warning(getApplicationContext(), "Es necesario ingresar una bodega", Toasty.LENGTH_SHORT).show();
                            }

                        }
                    });


                    //Se carga la vista dentro de la actividad actual
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    mSalir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                            if (cantidaRegistrosProductos() == 0) {
                                Toasty.warning(getApplicationContext(), "La tabla productos no tiene registros", Toast.LENGTH_SHORT, true).show();
                            } else if (cantidaRegistrosProductos() > 0) {
                                Toasty.success(getApplicationContext(), "La tabla de productos tiene datos :D", Toast.LENGTH_SHORT, true).show();
                            }
                        }
                    });
                } else {
                    Toasty.warning(getApplicationContext(), "Es necesario subir los datos de las bodegas", Toasty.LENGTH_LONG).show();
                }


            }
        });


        /*Actualizar inventario*/
        actualizarInventario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cantidadRegistrosBodega() > 1) {
                    cambiarBotonPresionado(3);

                    final AlertDialog.Builder mBuilder = new AlertDialog.Builder(configuracion_inventario.this);
                    View mView = getLayoutInflater().inflate(R.layout.actualizar_inventario, null);
                    Button btnGenerarInventario = mView.findViewById(R.id.btnGenerarInventario);
                    numeroDocumento = mView.findViewById(R.id.autoCompleteNumero);
                    autoCompleteBodegas = mView.findViewById(R.id.autoCompleteBodega);
                    final ImageView documentos = mView.findViewById(R.id.ImageViewDocumento);
                    final Button btnSiguienteNombre = mView.findViewById(R.id.btnSiguienteNombre);
                    Button mSalir = (Button) mView.findViewById(R.id.btnSalir);

                    numeroDocumento.setEnabled(false);
                    cargarSqliteBodegas();

                    ArrayAdapter<String> adapterBodegas = new ArrayAdapter<String>(configuracion_inventario.this, android.R.layout.simple_spinner_dropdown_item, lstBodegas);
                    autoCompleteBodegas.setAdapter(adapterBodegas);

                    btnSiguienteNombre.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            numeroDocumento.setEnabled(true);
                            cargarSqliteDocumentos();
                            ArrayAdapter<String> adapterDocumento = new ArrayAdapter<String>(configuracion_inventario.this, android.R.layout.simple_spinner_dropdown_item, lstDocumento);
                            numeroDocumento.setAdapter(adapterDocumento);
                            documentos.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    numeroDocumento.showDropDown();
                                }
                            });
                        }
                    });


                    btnGenerarInventario.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            grabandoInventarioArchivo();
                        }
                    });

                    //Se carga la vista dentro de la actividad actual
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    mSalir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });
                } else {
                    Toasty.warning(getApplicationContext(), "Es necesario subir los datos de las bodegas", Toasty.LENGTH_LONG).show();
                }
            }
        });


    }

    private String[] cargarSqliteDocumentos() {
        try {
            documentos.clear();
            abrirConexionDocumentos(autoCompleteBodegas.getText().toString());
            if (c.moveToFirst()) {
                do {
                    String nombre_Bodega = c.getString(0);
                    documentos.add(nombre_Bodega);
                } while (c.moveToNext());
            }
            cargarDocumentoLista();
        } catch (SQLiteException e) {
            Toasty.error(getApplicationContext(), "Error al tratar de cargar los documentos", Toasty.LENGTH_SHORT).show();
        }

        return lstDocumento;

    }

    private void cargarDocumentoLista() {

        lstDocumento = new String[documentos.size()];
        for (int j = 0; j < documentos.size(); j++) {
            lstDocumento[j] = documentos.get(j);
        }
    }

    private void abrirConexionDocumentos(String nombre_Bodega) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        c = db.rawQuery("SELECT " + Utilidades.vNUMERO_DOCUMENTO + " FROM  " + myTable + " WHERE " + Utilidades.vNOMBRE_BODEGA + " = '" + nombre_Bodega + "' GROUP BY " + Utilidades.vNUMERO_DOCUMENTO + ";", null);
    }

    private void buscarArchivoVentana() {
        buscarArchivo = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        buscarArchivo.addCategory(Intent.CATEGORY_OPENABLE);
        buscarArchivo.setType("text/*");
        startActivityForResult(buscarArchivo, 10);
    }


    private void comprobarColorActual() {
        if (cantidadRegistrosBodega() > 1) {
            colorBodega.setBackgroundColor(Color.parseColor("#D5F5E3"));
        }
        if (cantidaRegistrosMaestro() > 1) {
            colorMaestro.setBackgroundColor(Color.parseColor("#D5F5E3"));
        }
        if (cantidaRegistrosProductos() > 1) {
            colorProducto.setBackgroundColor(Color.parseColor("#D5F5E3"));
        }
    }


    private void cambiarBotonPresionado(int i) {
        boton_Presionado = i;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        try {
                            generarUbicacionArchivo(data);
                            validarBotonPresionado();
                        } catch (RuntimeException e) {
                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("El archivo no tiene el formato correcto")
                                    .show();
                        }
                    } else {
                        // Solicitando permisos al usuario
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    }
                }
                break;
        }
    }

    public void generarUbicacionArchivo(@Nullable Intent data) {
        Uri uri = data.getData();
        String path = uri.getPath();
        PATH = path.substring(path.indexOf(":") + 1);
    }

    public void validarBotonPresionado() {
        new AsyncCaller().execute();
    }

    private Long cantidadRegistrosBodega() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_VBODEGAS);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn + 1;
    }

    private Long cantidaRegistrosProductos() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VPRODUCTOS, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_VPRODUCTOS);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn + 1;
    }

    private Long cantidaRegistrosMaestro() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_MAESTRO, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            cn = DatabaseUtils.queryNumEntries(db, Utilidades.TABLA_MAESTRO);
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cn + 1;
    }


    /**
     * Se encarga de guardar la información de las bodegas desde .txt a Sqlite
     * Autor:CarlosDnl
     * Fecha: 25-02-2020
     */
    public void cargarInformacionBodegas(String[] texto) {
        try {
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            //Reinicio de la base de datos
            db.delete(Utilidades.TABLA_VBODEGAS, null, null);

            db.beginTransactionNonExclusive();
            try {
                for (int i = 0; i < texto.length; i++) {
                    String[] linea = texto[i].split(";");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Utilidades.CODIGO_BODEGA, linea[0]);
                    contentValues.put(Utilidades.NOMBRE_BODEGA, linea[1]);
                    db.insert(Utilidades.TABLA_VBODEGAS, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

            db.close();
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("Se insertarón " + cantidadRegistrosBodega() + " bodegas")
                    .show();
            colorBodega.setBackgroundColor(Color.parseColor("#D5F5E3"));
            rippleBackground.stopRippleAnimation();

        } catch (RuntimeException e) {
            rippleBackground.stopRippleAnimation();
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("El archivo no tiene el formato correcto")
                    .show();

        }


    }

    /**
     * Se encarga de almacenar la información desde .txt a sqlite
     * Autor:CarlosDnl
     * Fecha: 25-02-2020
     */

    private void cargarInformacionProductos(String[] texto) {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VPRODUCTOS, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            //Reinicio de la base de datos
            db.delete(Utilidades.TABLA_VPRODUCTOS, null, null);

            db.beginTransactionNonExclusive();
            try {
                for (int i = 0; i < texto.length; i++) {
                    String[] linea = texto[i].split(";");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Utilidades.CODIGO_SKU, linea[0].trim());
                    contentValues.put(Utilidades.VCODIGO_BARRAS, linea[1].trim());
                    contentValues.put(Utilidades.VDESCRIPCION_PRODUCTO, linea[2].trim());
                    db.insert(Utilidades.TABLA_VPRODUCTOS, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            //Se cierra la conexión con la base de datos
            db.close();
            //Mostrando mensaje de confirmación
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("Se insertarón " + cantidaRegistrosProductos() + " productos")
                    .show();

            //Cambiando el color del LinearLayout a verde
            colorProducto.setBackgroundColor(Color.parseColor("#D5F5E3"));
            //Se detiene la animación
            rippleBackground.stopRippleAnimation();
        } catch (RuntimeException e) {
            rippleBackground.stopRippleAnimation();
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("El archivo no tiene el formato correcto")
                    .show();

        }
    }

    /**
     * Se encarga de guardar la informaciòn de la tabla maestro en Sqlite
     * Autor:CarlosDnl
     * Fecha:27-02-2020
     */


    public void guardarDatosMaestro(String[] texto) {
        try {

            abrirMaestroSqlite();
            reiniciarSqliteMaestro();

            db.beginTransactionNonExclusive();
            try {
                for (int i = 0; i < texto.length; i++) {
                    String[] linea = texto[i].split(";");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Utilidades.maestroCODIGO_BODEGA, linea[0].trim());
                    contentValues.put(Utilidades.maestroSkuInventario, linea[2].trim());
                    contentValues.put(Utilidades.maestroCantidadInventario, linea[3].trim());
                    contentValues.put(Utilidades.maestroBODEGA_ASOCIADA, linea[1].trim());
                    db.insert(Utilidades.TABLA_MAESTRO, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            db.close();

            //Mostrando mensaje de confirmación
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("Se insertarón " + cantidaRegistrosMaestro() + " datos en la tabla Maestro")
                    .show();

            cambiarColorMaestro();
            detenerAnimacionMaestro();
        } catch (RuntimeException e) {
            rippleBackground.stopRippleAnimation();
            new SweetAlertDialog(configuracion_inventario.this)
                    .setTitleText("El archivo no tiene el formato correcto")
                    .show();
        }

    }

    public void abrirMaestroSqlite() {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_MAESTRO, null, 1);
        db = conn.getWritableDatabase();
    }

    public void reiniciarSqliteMaestro() {
        db.delete(Utilidades.TABLA_MAESTRO, null, null);
    }

    public void detenerAnimacionMaestro() {
        //Se detiene la animación
        rippleBackground.stopRippleAnimation();
    }

    public void cambiarColorMaestro() {
        //Cambiando el color del LinearLayout a verde
        colorMaestro.setBackgroundColor(Color.parseColor("#D5F5E3"));
    }

    private class AsyncCaller extends AsyncTask<Void, Void, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Void... params) {
            try {
                inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/" + PATH);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            //Grabando los datos del archivo en un Array
            try {
                int i = inputStream.read();
                while (i != -1) {
                    byteArrayOutputStream.write(i);
                    i = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return byteArrayOutputStream.toString().trim().split("\n");
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            comprobarBotonPresionado(result);
        }
    }


    private void comprobarBotonPresionado(String[] result) {
        if (boton_Presionado == 0) {
            cargarInformacionBodegas(result);
        } else if (boton_Presionado == 1) {
            cargarInformacionProductos(result);
        } else if (boton_Presionado == 2) {
            guardarDatosMaestro(result);
        }
    }


    private String[] cargarSqliteBodegas() {
        //Se limpian los datos para evitar registros duplicados
        datos.clear();
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VBODEGAS;//Set name of your table
        c = db.rawQuery("SELECT * FROM  " + myTable, null);
        if (c.moveToFirst()) {
            do {
                String nombre_Bodega = c.getString(1);
                datos.add(nombre_Bodega);
            } while (c.moveToNext());
        }
        cargarBodegasLista();
        return lstBodegas;
    }


    public void cargarBodegasLista() {
        lstBodegas = new String[datos.size()];
        for (int j = 0; j < datos.size(); j++) {
            lstBodegas[j] = datos.get(j);
        }
    }

    public void grabandoInventarioArchivo() {
        try {
            crearArchivoIntentarios();
            abrirConexionInventario(getApplicationContext());
            Cursor c = db.rawQuery("SELECT * FROM " + myTable + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = " + numeroDocumento.getText().toString(), null);
            if (c.moveToFirst()) {
                do {
                    writer.append("Numero documento: " + c.getInt(0) + ", Codigo barras: " + c.getString(1) + ", Codigo sku: " + c.getString(2) + ", Fecha lectura: " + c.getString(3) + ", Cantidad: " + c.getInt(4) + ", Tipo lectura: " + c.getString(5) + ", Bodega: " + c.getString(6) + ", Codigo bodega: " + c.getString(7) + ", Estado: " + c.getString(8) + "\n");
                    almacenarConteoNube(c.getInt(0), c.getString(1), c.getString(2), c.getString(3), c.getInt(4), c.getString(5), c.getString(6), c.getString(7));
                } while (c.moveToNext());
            }
            writer.flush();
            writer.close();
            String m = "Inventario " + nombre_Archivo.trim() + ".txt";
            Toasty.success(getApplicationContext(), m, Toasty.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void almacenarConteoNube(int numero_Documento, String codigo_Barras, String codigo_sku, String fecha_Lectura, int cantidad, String tipo_Lectura, String bodega, String codigo_Bodega) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(V_URL + '/')
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<insertar_datos> call = jsonPlaceHolderApi.postConteoData(String.valueOf(numero_Documento), codigo_Barras, codigo_sku, fecha_Lectura, String.valueOf(cantidad), tipo_Lectura, bodega, codigo_Bodega);

        call.enqueue(new Callback<insertar_datos>() {
            @Override
            public void onResponse(Call<insertar_datos> call, retrofit2.Response<insertar_datos> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<insertar_datos> call, Throwable t) {

            }
        });
    }

    public static void crearArchivoIntentarios() throws IOException {
        nombre_Archivo = DateFormat.format("MM-dd-yyyyy-h-mmssaa", System.currentTimeMillis()).toString();
        File root = new File(Environment.getExternalStorageDirectory(), "Inventarios");
        if (!root.exists()) {
            root.mkdirs();
        }
        File filepath = new File(root, nombre_Archivo.trim() + ".txt");  // file path to save
        writer = new FileWriter(filepath);
    }

    public static void abrirConexionInventario(Context context) {
        conn = new ConexionSQLiteHelper(context, Utilidades.vTABLA_INVENTARIO, null, 1);
        db = conn.getReadableDatabase();
        myTable = Utilidades.vTABLA_INVENTARIO;//Nombre de la tabla
    }

    public void sincronizarDatosBodegas() {
        try {

            //Se configura la URL a la que se va a realizar la petición
            String url = V_URL + "/tablaBodegas";

            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
            final SQLiteDatabase db = conn.getWritableDatabase();

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se almacena la erspuesta obtenida
                        JSONArray mJsonArray = response.getJSONArray("recordset");
                        if (mJsonArray.length() > 0) {
                            //Reinicio de la base de datos
                            db.delete(Utilidades.TABLA_VBODEGAS, null, null);
                            db.beginTransactionNonExclusive();

                            try {
                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(Utilidades.CODIGO_BODEGA, mJsonObject.getString("CodBodega").trim());
                                    contentValues.put(Utilidades.NOMBRE_BODEGA, mJsonObject.getString("bodega").trim());
                                    db.insert(Utilidades.TABLA_VBODEGAS, null, contentValues);
                                }
                                db.setTransactionSuccessful();
                            } finally {
                                db.endTransaction();
                            }

                            db.close();
                            Long cantidad_Bodega = cantidadRegistrosBodega() - 1;
                            Toasty.success(getApplicationContext(), "Se insertarón " + cantidad_Bodega + " bodegas", Toasty.LENGTH_SHORT).show();

                            colorBodega.setBackgroundColor(Color.parseColor("#D5F5E3"));
                            rippleBackground.stopRippleAnimation();
                        } else {
                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("Sin registros")
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }
    }


    public void sincronizarDatosProductos() {
        try {
            //Se configura la URL a la que se va a realizar la petición
            String url = V_URL + "/tablaProductos";
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VPRODUCTOS, null, 1);
            final SQLiteDatabase db = conn.getWritableDatabase();


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se almacena la erspuesta obtenida
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        if (mJsonArray.length() > 0) {
                            //Reinicio de la base de datos
                            db.delete(Utilidades.TABLA_VPRODUCTOS, null, null);
                            db.beginTransactionNonExclusive();


                            try {
                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(Utilidades.CODIGO_SKU, mJsonObject.getString("Sku").trim());
                                    contentValues.put(Utilidades.VCODIGO_BARRAS, mJsonObject.getString("CodBarras").trim());
                                    contentValues.put(Utilidades.VDESCRIPCION_PRODUCTO, mJsonObject.getString("Descripcion").trim());
                                    db.insert(Utilidades.TABLA_VPRODUCTOS, null, contentValues);

                                    db.insert(Utilidades.TABLA_VBODEGAS, null, contentValues);
                                }
                                db.setTransactionSuccessful();
                            } finally {
                                db.endTransaction();
                            }

                            db.close();
                            Long cantidad_Bodega = cantidaRegistrosProductos() - 1;
                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("Se insertarón " + cantidad_Bodega + " bodegas")
                                    .show();


                            colorProducto.setBackgroundColor(Color.parseColor("#D5F5E3"));
                            rippleBackground.stopRippleAnimation();
                        } else {
                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("Sin registros")
                                    .show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (SQLiteException e) {
                        Toast.makeText(getApplicationContext(), "El formato del archivo es incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }
    }


    public void sincronizarDatosMaestro() {
        try {
            //Se crea un json que serà enviado para que se guarde en la BD remota
            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("V_Fecha", fecha.getText().toString().trim());
            jsonBody.put("V_Cod_Bodega", codigoBodega(autoCompleteBodegas.getText().toString().trim()));

            //Toast.makeText(getApplicationContext(), fecha.getText().toString() +  " " + codigoBodega(autoCompleteBodegas.getText().toString()), Toast.LENGTH_SHORT).show();

            //Se configura la URL a la que se va a realizar la petición
            String url = V_URL + "/tablaMaestro";
            abrirMaestroSqlite();

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //Se almacena la erspuesta obtenida
                        JSONArray mJsonArray = response.getJSONArray("recordset");
                        if (mJsonArray.length() > 0) {
                            //Reinicio de la base de datos
                            reiniciarSqliteMaestro();

                            db.beginTransactionNonExclusive();
                            try {
                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(Utilidades.maestroCODIGO_BODEGA, mJsonObject.getString("Codbodega").trim());
                                    contentValues.put(Utilidades.maestroSkuInventario, mJsonObject.getString("SkuInventario").trim());
                                    contentValues.put(Utilidades.maestroCantidadInventario, mJsonObject.getString("CantidadInventario").trim());
                                    contentValues.put(Utilidades.maestroBODEGA_ASOCIADA, autoCompleteBodegas.getText().toString());
                                    db.insert(Utilidades.TABLA_MAESTRO, null, contentValues);
                                }
                                db.setTransactionSuccessful();
                            } finally {
                                db.endTransaction();
                            }
                            db.close();

                            //Mostrando mensaje de confirmación
                            Long cantidad = cantidaRegistrosMaestro() + 1;

                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("Se insertarón " + cantidad + " datos en la tabla Maestro")
                                    .show();

                            cambiarColorMaestro();
                            detenerAnimacionMaestro();
                        } else {
                            new SweetAlertDialog(configuracion_inventario.this)
                                    .setTitleText("Sin registros")
                                    .show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }
    }

    private String codigoBodega(String nombre_Bodega) {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VBODEGAS;//Set name of your table
        Cursor c = db.rawQuery("SELECT " + Utilidades.CODIGO_BODEGA + " FROM " + myTable + " WHERE " + Utilidades.NOMBRE_BODEGA + " = '" + nombre_Bodega + "';", null);
        if (c.moveToFirst()) {
            do {
                nombre_Bodega = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();
        //PI
        return nombre_Bodega;
    }
}

