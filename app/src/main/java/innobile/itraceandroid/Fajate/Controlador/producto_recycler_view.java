package innobile.itraceandroid.Fajate.Controlador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import innobile.Adapter.producto_data_adapter;
import innobile.itrace.R;

public class producto_recycler_view extends RecyclerView.Adapter<producto_recycler_view.MyViewHolder> {

    private Context mContext;
    private List<producto_data_adapter> mData;

    public producto_recycler_view(Context mContext, List<producto_data_adapter> mData) {
        this.mContext = mContext;
        this.mData = mData;

    }

    @Override
    public producto_recycler_view.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.producto_data_adapter, parent, false);
        final producto_recycler_view.MyViewHolder viewHolder = new producto_recycler_view.MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(producto_recycler_view.MyViewHolder holder, int position) {

        holder.tCodigoSku.setText(mData.get(position).getCODIGO_SKU());
        holder.tCodigoBarras.setText(mData.get(position).getVCODIGO_BARRAS());
        holder.tDescripcion.setText(mData.get(position).getDESCRIPCION_PRODUCTO());


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tCodigoSku;
        TextView tCodigoBarras;
        TextView tDescripcion;
        LinearLayout view_container;


        public MyViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            tCodigoSku = itemView.findViewById(R.id.tcodigo_sku);
            tCodigoBarras = itemView.findViewById(R.id.tcodigo_barras);
            tDescripcion = itemView.findViewById(R.id.tdescripcion);

        }
    }

}