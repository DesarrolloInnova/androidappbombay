package innobile.itraceandroid.Fajate.Controlador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import innobile.Adapter.maestro_data_adapter;
import innobile.itrace.R;

public class maestro_recycler_view extends RecyclerView.Adapter<maestro_recycler_view.MyViewHolder> {

    private Context mContext;
    private List<maestro_data_adapter> mData;

    public maestro_recycler_view(Context mContext, List<maestro_data_adapter> mData) {
        this.mContext = mContext;
        this.mData = mData;

    }

    @Override
    public maestro_recycler_view.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.maestro_row_data, parent, false);
        final maestro_recycler_view.MyViewHolder viewHolder = new maestro_recycler_view.MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(maestro_recycler_view.MyViewHolder holder, int position) {
        holder.tCodigoBodega.setText(mData.get(position).getMaestroCODIGO_BODEGA());
        holder.tCodigoSKU.setText(mData.get(position).getMaestroSkuInventario());
        holder.tCantidad.setText(mData.get(position).getMaestroCantidadInventario());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tCodigoBodega;
        TextView tCodigoSKU;
        TextView tCantidad;
        LinearLayout view_container;

        public MyViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            tCodigoBodega = itemView.findViewById(R.id.tcodigo_bodega);
            tCodigoSKU = itemView.findViewById(R.id.tcodigo_sku);
            tCantidad = itemView.findViewById(R.id.tcantidad);
        }
    }

}