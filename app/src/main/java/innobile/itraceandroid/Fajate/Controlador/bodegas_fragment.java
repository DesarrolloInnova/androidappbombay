package innobile.itraceandroid.Fajate.Controlador;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import innobile.Adapter.GetBodegaAdapter;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


/**
 * A simple {@link Fragment} subclass.
 */
public class bodegas_fragment extends Fragment {

    private ConexionSQLiteHelper conn;
    public RecyclerView.Recycler recycler;
    private  RecyclerView recyclerView;
    private List<GetBodegaAdapter> lstBodegas;

    public bodegas_fragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bodegas_fragment, container, false);
        //return inflater.inflate(R.layout.fragment_bodegas_fragment, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_bodegas);
        cargarDatosBodega();


        return rootView;
    }

    public void cargarDatosBodega(){
        lstBodegas = new ArrayList<>();

        conn = new ConexionSQLiteHelper(getActivity(), Utilidades.TABLA_VBODEGAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VBODEGAS;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM " + myTable, null);
        if (c.moveToFirst()) {
            do {
                GetBodegaAdapter bodegas = new GetBodegaAdapter();
                bodegas.setCODIGO_BODEGA(c.getString(0));
                bodegas.setNOMBRE_BODEGA(c.getString(1));
                lstBodegas.add(bodegas);
            } while (c.moveToNext());
        }

        db.close();

        setuprecyclerview(lstBodegas);
    }
    private void setuprecyclerview(List<GetBodegaAdapter> lstBodega) {
        bodega_recycler_view myadapter = new bodega_recycler_view(getActivity(), lstBodega);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(myadapter);

    }
}
