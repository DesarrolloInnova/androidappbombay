package innobile.itraceandroid.Fajate.Controlador;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import innobile.Adapter.GetBodegaAdapter;
import innobile.Adapter.producto_data_adapter;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


/**
 * A simple {@link Fragment} subclass.
 */
public class Productos_Fragment extends Fragment {
    private ConexionSQLiteHelper conn;
    public RecyclerView.Recycler recycler;
    private RecyclerView recyclerView;
    private List<producto_data_adapter> lstBodegas;

    public Productos_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_productos_2, container, false);
        View rootView = inflater.inflate(R.layout.fragment_productos_2, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_productos);
        cargarDatosProducto();


        return rootView;
    }


    public void cargarDatosProducto() {
        lstBodegas = new ArrayList<>();

        conn = new ConexionSQLiteHelper(getActivity(), Utilidades.TABLA_VPRODUCTOS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VPRODUCTOS;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM " + myTable, null);
        if (c.moveToFirst()) {
            do {
                producto_data_adapter bodegas = new producto_data_adapter();
                bodegas.setCODIGO_SKU(c.getString(0));
                bodegas.setVCODIGO_BARRAS(c.getString(1));
                bodegas.setDESCRIPCION_PRODUCTO(c.getString(2));
                lstBodegas.add(bodegas);
            } while (c.moveToNext());
        }
        db.close();
        setuprecyclerview(lstBodegas);
    }

    private void setuprecyclerview(List<producto_data_adapter> lstBodega) {
        producto_recycler_view myadapter = new producto_recycler_view(getActivity(), lstBodega);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(myadapter);
    }
}
