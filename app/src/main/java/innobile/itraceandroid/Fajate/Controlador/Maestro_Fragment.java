package innobile.itraceandroid.Fajate.Controlador;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.GetBodegaAdapter;
import innobile.Adapter.maestro_data_adapter;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;


/**
 * A simple {@link Fragment} subclass.
 */
public class Maestro_Fragment extends Fragment {
    private ConexionSQLiteHelper conn;
    private RecyclerView recyclerView;
    private List<maestro_data_adapter> lstMaestro;

    public Maestro_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maestro_, container, false);
        //return inflater.inflate(R.layout.fragment_bodegas_fragment, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_maestro);
        cargarDatosBodega();


        return rootView;
    }

    public void cargarDatosBodega() {
        lstMaestro = new ArrayList<>();

        conn = new ConexionSQLiteHelper(getActivity(), Utilidades.TABLA_MAESTRO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_MAESTRO;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM " + myTable, null);
        if (c.moveToFirst()) {
            do {
                maestro_data_adapter bodegas = new maestro_data_adapter();
                bodegas.setMaestroCODIGO_BODEGA(c.getString(0));
                bodegas.setMaestroSkuInventario(c.getString(1));
                bodegas.setMaestroCantidadInventario(c.getString(2));
                lstMaestro.add(bodegas);
            } while (c.moveToNext());
        }

        db.close();
        setuprecyclerview(lstMaestro);
    }

    private void setuprecyclerview(List<maestro_data_adapter> lstBodega) {
        maestro_recycler_view myadapter = new maestro_recycler_view(getActivity(), lstBodega);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(myadapter);
    }
}
