package innobile.itraceandroid.Fajate.Controlador;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PagerController extends FragmentPagerAdapter {
    int numoftabs;

    public PagerController(FragmentManager fm, int numoftabs) {
        super(fm);
        this.numoftabs = numoftabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new bodegas_fragment();
            case 1:
                return new Productos_Fragment();
            case 2:
                return new Maestro_Fragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numoftabs;
    }
}
