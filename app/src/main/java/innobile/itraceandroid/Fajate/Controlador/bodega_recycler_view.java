package innobile.itraceandroid.Fajate.Controlador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import innobile.Adapter.GetBodegaAdapter;
import innobile.itrace.R;

public class bodega_recycler_view extends RecyclerView.Adapter<bodega_recycler_view.MyViewHolder> {

    private Context mContext;
    private List<GetBodegaAdapter> mData;


    public bodega_recycler_view(Context mContext, List<GetBodegaAdapter> mData) {
        this.mContext = mContext;
        this.mData = mData;

    }

    @Override
    public bodega_recycler_view.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.bodegas_row_data, parent, false);
        final bodega_recycler_view.MyViewHolder viewHolder = new bodega_recycler_view.MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(bodega_recycler_view.MyViewHolder holder, int position) {

   holder.tCodigo.setText(mData.get(position).getCODIGO_BODEGA());
   holder.tNombre.setText(mData.get(position).getNOMBRE_BODEGA());



    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tNombre;
        TextView tCodigo;
        LinearLayout view_container;


        public MyViewHolder(View itemView) {
            super(itemView);

            view_container = itemView.findViewById(R.id.container);
            tCodigo = itemView.findViewById(R.id.tcodigo_bodega);
            tNombre = itemView.findViewById(R.id.tnombre_bodega);

        }
    }

}