package innobile.itraceandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import avd.api.core.BarcodeType;
import avd.api.core.ConnectionState;
import avd.api.core.IDevice;
import avd.api.core.IListenerError;
import avd.api.core.IListenerScan;
import avd.api.core.IListenerTriggerPress;
import avd.api.core.IPrinter;
import avd.api.core.exceptions.ApiCommunicationException;
import avd.api.core.exceptions.ApiDeviceException;
import avd.api.core.exceptions.ApiDeviceManagerException;
import avd.api.core.exceptions.ApiPrinterException;
import avd.api.core.exceptions.ApiScannerException;
import avd.api.core.imports.ButtonState;
import avd.api.core.imports.ButtonType;
import avd.api.core.imports.ResourceMediaType;
import avd.api.core.imports.TriggerMode;
import avd.api.devices.management.DeviceConnectionInfo;
import avd.api.devices.management.DeviceManager;
import avd.api.resources.ApiResourceException;
import avd.api.resources.ResourceManager;
import avd.sdk.CompanionAPIErrors;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.DAOInventario;
import innobile.itrace.transport.T_Configuraciones;

public class SampleApplication extends Application {
    private static final String LOG_FOLDER_ENV_VARIABLE = "av.sampleapp.logpath";
    private static final String LOG_FOLDER = "Download/ApiLog";
    private static final String LOG_CONFIG_FILE = "logconfig.xml";

    private IListenerError connectionErrorCallback;
    private IListenerError printerErrorCallback;
    private IListenerError systemErrorCallback;

    //Elementos
    private EditText tvScannerActivityText = null;


    private TextView txtTipoLectura;
    private TextView Descripcion = null;
    private TextView Referencia = null;
    private TextView Codigo_leido = null;
    private TextView Total_leidos = null;
    private TextView Leidos_codigo = null;
    private CheckBox comprobar = null;
    private EditText cantidad = null;
    private EditText buscar = null;

    private TextView txtViewCodigo = null;
    private TextView txtViewCodigodeBarras = null;
    private TextView txtPlus = null;
    private TextView txtViewDescripcion = null;
    private TextView txtViewUbicacion = null;
    private TextView txtUnidadMedida = null;
    private TextView txtVolumen = null;
    private TextView txtViewMinimo = null;
    private TextView textViewEmpaque = null;
    private TextView txtViewMaximo = null;


    private String codigo;
    private String codbarras;
    private String codigoplu;
    private String descripcion;
    private String undempaque;
    private String unidadmed;
    private String volumen;
    private String cantempaque;
    private String minimo;
    private String maximo;
    private IListenerScan scannerActivityCallback = null;
    private String scannerActivityText = "";
    public int cont = 0;


    private Button btnDeviceLeft = null;
    private Button btnDeviceRight = null;
    private File resourceDirs = null;
    private TextView tvDeviceName = null;
    private BroadcastReceiver mDeviceSelectedReceiver = null;

    private IListenerTriggerPress triggerPressCallback;

    private IListenerScan scanPrintScenarioCallback;

    public OnClickListener abortPrinterError;
    private OnClickListener clearPrinterError;
    private OnClickListener abortPrinterErrorAndResyncPrinter;
    private OnClickListener clearPrinterErrorAndResyncPrinter;
    private OnClickListener ignoreConnectionLoss;

    private DeviceManager deviceManager = new DeviceManager();
    public boolean areResourcesRegistered = false;

    public SampleAppActivity currentActivity = null;
    public boolean isButtonPressAllowed = true;
    private boolean isScanPrintScenarioActive = false;
    private boolean isScannerActive = false;
    private int numberOfUnresolvedConnectionLosses = 0;
    public String requiredDevice = NO_DEVICES;
    public Class<?> lastActivityForAnyDevice = null;

    public static final String INTENT_ACTION_UPDATE_SCREEN_CONTROLS = "Update screen controls";
    public static final String INTENT_ACTION_UPDATE_DEVICE_SELECTION = "Update device selection";
    public static final int INTENT_FILE_CHOOSER_ID = 1;
    public static String SDK_VERSION;
    public static String ANY_DEVICE;
    public static String ALL_DEVICES;
    public static String NO_DEVICES;
    private int printSampleIndex = 0;

    private RequestQueue queue;

    public int ContarRegistros = 0;

    public String resourcePath = null;

    private int counter = 0;

    private String V_URL = null;
    private int V_TipoImpresion = 0;
    private boolean V_TipoConexion = false;

    // We have to pass data into the listener that would attempt reconnection, so we implement it as a custom class that
    // contains the data that we need to pass.
    private class AttemptReconnectionListener implements OnClickListener {
        private DeviceConnectionInfo reconnectDeviceConnectionInfo = null;

        public AttemptReconnectionListener(DeviceConnectionInfo reconnectDeviceConnectionInfo) {
            this.reconnectDeviceConnectionInfo = reconnectDeviceConnectionInfo;
        }

        @Override
        public void onClick(DialogInterface arg0, int arg1) {

        }
    }

    // We have to pass data into the listener that would ignore reconnection, so we implement it as a custom class that
    // contains the data that we need to pass.
    private class IgnoreReconnectionListener implements OnClickListener {
        private Class<?> availableActivity;

        public IgnoreReconnectionListener(Class<?> availableActivity) {
            this.availableActivity = availableActivity;
        }

        @Override
        public void onClick(DialogInterface arg0, int arg1) {
            // Intent removes from the back stack all activities down to but not including availableActivity, effectively reverting application to this activity.
            Intent i = new Intent(currentActivity, availableActivity);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            currentActivity.startActivity(i);
        }
    }


    public class DeviceData {
        public IDevice device;
        public boolean isScannerEnabled;
        public boolean isSystemParametersLoaded;
        public boolean isPrinterParametersLoaded;
        public boolean isScannerParametersLoaded;
        public boolean isBarcodeParametersLoaded;
    }

    private List<String> connectedDevicesNames = new ArrayList<String>();
    public Map<String, DeviceData> connectedDevicesData = new HashMap<String, DeviceData>();
    public String currentDeviceName = null;

    @Override
    public void onCreate() {
        super.onCreate();
        queue = new Volley().newRequestQueue(this);
        registerResources();
        try {
            registerResource(ResourceMediaType.Lnt, "Sample Print LNT", "SamplePrint.LNT");
        } catch (ApiResourceException e) {
            e.printStackTrace();
        }
//        configureLogging();

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_TipoImpresion = t_configuraciones.getCon_TipoImpresion();
//      V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        try {
            SDK_VERSION = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        ALL_DEVICES = getResources().getText(R.string.all_devices).toString();
        ANY_DEVICE = getResources().getText(R.string.any_device).toString();
        NO_DEVICES = getResources().getText(R.string.no_devices).toString();
        currentDeviceName = NO_DEVICES;

        abortPrinterError = new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                IDevice device = currentActivity.findDeviceAssociatedWithAlert(arg0);
                if (device == null)
                    return;

                try {
                    device.getPrinter().abortError();
                } catch (ApiPrinterException e) {
                }

                currentActivity.dismissAllAlertsForDevice(device, false); // All error messages pertaining to this device are irrelevant now.
            }

        };

        clearPrinterError = new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                IDevice device = currentActivity.findDeviceAssociatedWithAlert(arg0);
                if (device == null)
                    return;

                try {
                    device.getPrinter().clearError();
                } catch (ApiPrinterException e) {
                }

                currentActivity.dismissAllAlertsForDevice(device, false); // All error messages pertaining to this device are irrelevant now.
            }

        };

        abortPrinterErrorAndResyncPrinter = new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                IDevice device = currentActivity.findDeviceAssociatedWithAlert(arg0);
                if (device == null)
                    return;

                try {
                    device.getPrinter().abortError();
                    device.getPrinter().resync();
                } catch (ApiPrinterException e) {
                }

                currentActivity.dismissAllAlertsForDevice(device, false); // All error messages pertaining to this device are irrelevant now.
            }
        };

        clearPrinterErrorAndResyncPrinter = new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                IDevice device = currentActivity.findDeviceAssociatedWithAlert(arg0);
                if (device == null)
                    return;

                try {
                    device.getPrinter().clearError();
                    //device.getPrinter().resync();
                } catch (ApiPrinterException e) {
                }

                currentActivity.dismissAllAlertsForDevice(device, false); // All error messages pertaining to this device are irrelevant now.
            }
        };

        ignoreConnectionLoss = new OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                --numberOfUnresolvedConnectionLosses;
                resolveConnectionLosses();
            }
        };

        connectionErrorCallback = new IListenerError() {

            @Override
            public void onErrorReceived(String errorText, int errorCode, IDevice device) {
                if (device == null)
                    return;

                ++numberOfUnresolvedConnectionLosses;

                eraseDevice(device.getSerial());

                AttemptReconnectionListener attemptReconnection = new AttemptReconnectionListener(new DeviceConnectionInfo(device.getSerial(), device.getConnection().getAddress(), device.getConnection().getType()));

                currentActivity.showMessageBox("Connection error", String.format("Connection error %d. %s", errorCode, errorText),
                        "Attempt reconnection", attemptReconnection,
                        "Ignore", ignoreConnectionLoss, device);

            }
        };

        systemErrorCallback = new IListenerError() {

            @Override
            public void onErrorReceived(String errorText, int errorCode, IDevice device) {
                if (device != null)
                    currentActivity.showMessageBox("Device error", String.format("Device error %d.", errorCode),
                            "Abort", abortPrinterError, "Ignore", null, device);
            }
        };

        printerErrorCallback = new IListenerError() {

            @Override
            public void onErrorReceived(String errorText, int errorCode, IDevice device) {
                if (device == null)
                    return;

                if ((errorCode == CompanionAPIErrors.PE_MISSING_BLACK_MARK) ||
                        (errorCode == CompanionAPIErrors.PE_UNEXPECTED_BLACK_MARK) ||
                        (errorCode == CompanionAPIErrors.PE_SENSE_MARK_IS_TOO_LONG)) {

                    currentActivity.createAndShowMessageBox("Printer Error", String.format("%s\nError code is %d.", errorText, errorCode),
                            "Abort Print Job & Resync", abortPrinterErrorAndResyncPrinter,
                            "Ignore", null,
                            "Clear Error", clearPrinterErrorAndResyncPrinter, device);
                } else {
                    currentActivity.createAndShowMessageBox("Printer Error", String.format("%s\nError code is %d.", errorText, errorCode),
                            "Abort Print Job", abortPrinterError,
                            "Ignore", null,
                            "Clear Error", clearPrinterError, device);
                }
            }
        };


        //Este método se escarga de cargar los datos

        scannerActivityCallback = new IListenerScan() {


            @Override
            public void onScanReceived(final String scanData, BarcodeType barcodeType, IDevice device) {
                if ((tvScannerActivityText == null) || (device == null))
                    return;


                String deviceName = device.getSerial();
                if (allDevicesSelected() || currentDeviceName.equals(deviceName)) {
                    cont++;
                    scannerActivityText = scanData;
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String c = currentActivity.toString();
                            String p = "ConsultaProductos";
                            String t = c.replace(".", " ").replace("@", " ");
                            Boolean r = t.contains(p);

                            if (r) {
                                tvScannerActivityText.setText(scannerActivityText);


                                try {
                                    MostrarConsultaProductos();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {

                                String cadenaDondeBuscar = currentActivity.toString();
                                String palabra = "InventarioActivity";
                                String texto = cadenaDondeBuscar.replace(".", " ").replace("@", " ");
                                boolean resultado = texto.contains(palabra);

                                if (resultado) {
                                    tvScannerActivityText.setText(scannerActivityText);
                                } else {


                                    int Tipo_Impresion = V_TipoImpresion;

                                    String tipoLecturaDato = txtTipoLectura.getText().toString();

                                    if (Tipo_Impresion == 0) {
                                        try {
                                            Scan_Print_Scan();
                                            Toast.makeText(getApplicationContext(), "Scan- print - Scan", Toast.LENGTH_SHORT).show();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (Tipo_Impresion == 1 || scannerActivityText == "NR") {
                                        try {
                                            Scan_Print_Scan();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Scan_Print_Print();
                                        Toast.makeText(getApplicationContext(), "Scan- print - print", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "No ha seleccionado ningún método de lectura", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                        }
                    });
                }
            }
        };


        scanPrintScenarioCallback = new IListenerScan() {
            @Override
            public void onScanReceived(final String scanData, BarcodeType barcodeType, IDevice device) {
                final String stringBarcodeType = apiBarcodeTypeToLntBarcodeType(barcodeType);
                final int indexBarcode = lntBarcodeTypeToLntFieldIndex(stringBarcodeType);
                final byte[][] printData = {scanData.getBytes()};
                final byte[][] barcodeData = new byte[lntBarcodeTypeFieldIndices.length][];
                for (int i = 0; i < barcodeData.length; ++i) {
                    if (i == indexBarcode)
                        barcodeData[i] = scanData.getBytes();
                    else
                        barcodeData[i] = "".getBytes();
                }

                if (device == null)
                    return;

                final String deviceName = device.getSerial();
                if (allDevicesSelected() || currentDeviceName.equals(deviceName)) {
                    final IDevice deviceLocal = device;
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                if (indexBarcode == -1)
                                    deviceLocal.getPrinter().print("Barcode Text Print LNT", 1, printData);
                                else
                                    deviceLocal.getPrinter().print("Barcode Print LNT", 1, barcodeData);
                            } catch (ApiPrinterException e) {
                                currentActivity.showErrorMessageBox("Scanned label print failed.", e, "Abort", abortPrinterError, "Ignore", null, deviceLocal);
                            }
                        }
                    });
                }
            }
        };

        triggerPressCallback = new IListenerTriggerPress() {

            @Override
            public void onTriggerPressReceived(ButtonType buttonType, ButtonState buttonState, IDevice device) {
                if (device == null)
                    return;

                String triggerPressData = buttonType == ButtonType.Trigger ? "Trigger button" : "Feed button";
                triggerPressData = triggerPressData + " " + ((buttonState == ButtonState.Pressed) ? "is pressed" : "is released");

                currentActivity.showMessageBox("Button press notice", triggerPressData, "Ok", null, null, null, device);
            }
        };

        mDeviceSelectedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                updateDeviceIndexPresentation();
            }
        };
        IntentFilter filter = new IntentFilter(SampleApplication.INTENT_ACTION_UPDATE_DEVICE_SELECTION);
        registerReceiver(mDeviceSelectedReceiver, filter);
    }

    public IDevice createDevice(DeviceConnectionInfo deviceConnectionInfo) throws ApiDeviceManagerException, ApiScannerException, ApiPrinterException, ApiDeviceException, ApiCommunicationException {
        IDevice device = deviceManager.createDevice(deviceConnectionInfo);

        if (device != null) {
            if (connectedDevicesData.isEmpty())
                startService(new Intent(this, DeviceService.class));

            DeviceData deviceData = new DeviceData();
            device.addListenerDeviceError(systemErrorCallback);
            device.getConnection().addListenerConnectionError(connectionErrorCallback);
            device.getPrinter().addListenerPrinterError(printerErrorCallback);
            device.getScanner().enableScan(true);

            deviceData.device = device;
            deviceData.isScannerEnabled = true;
            deviceData.isSystemParametersLoaded = false;
            deviceData.isPrinterParametersLoaded = false;
            deviceData.isScannerParametersLoaded = false;
            deviceData.isBarcodeParametersLoaded = false;

            connectedDevicesData.put(deviceConnectionInfo.getSerial(), deviceData);
            connectedDevicesNames.add(deviceConnectionInfo.getSerial());

            int status;
            status = device.getStatus();
            if (status != 0)
                currentActivity.showMessageBox("Bad Device Status", String.format("Device status indicates an error. Error code is %d.", status),
                        "Abort", abortPrinterError, "Ignore", null, null);
            else {
                status = device.getPrinter().getStatus();
                if (status != 0)
                    currentActivity.showMessageBox("Bad Printer Status", String.format("Printer status indicates an error. Error code is %d.", status),
                            "Abort", abortPrinterError, "Ignore", null, null);
            }
        }

        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_DEVICE_SELECTION));
        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_SCREEN_CONTROLS));

        return device;
    }

    public void eraseDevice(String deviceSerial) {
        DeviceData deviceData = connectedDevicesData.get(deviceSerial);
        if (deviceData == null)
            return;

        if (deviceData.device.getConnection().getState() == ConnectionState.Open) {
            try {
                deviceData.device.removeListenerDeviceError(systemErrorCallback);
                deviceData.device.getConnection().removeListenerConnectionError(connectionErrorCallback);
                deviceData.device.getPrinter().removeListenerPrinterError(printerErrorCallback);
            } catch (ApiDeviceException e) {
                currentActivity.showErrorMessageBox("Device error", e, "Ok", null, null, null, null);
            } catch (ApiCommunicationException e) {
                currentActivity.showErrorMessageBox("Communication error", e, "Ok", null, null, null, null);
            } catch (ApiPrinterException e) {
                currentActivity.showErrorMessageBox("Printer error", e, "Ok", null, null, null, null);
            }
        }
        deviceManager.removeDevice(deviceData.device.getConnection().getAddress());

        currentActivity.dismissAllAlertsForDevice(deviceData.device, false);

        connectedDevicesData.remove(deviceSerial);
        connectedDevicesNames.remove(deviceSerial);

        resolveConnectionLosses(); // Automatic attempt to resolve connection losses if device erasure has been caused by other reasons than connection loss.
    }

    // Erase all device instances and data on them from the application.
    public void eraseDevices() {
        // All alerts associated with devices should be permanently closed.
        currentActivity.dismissAllAlerts(false);
        // Caches of device instances and data on them are cleared. Also performing zeroing of the numberOfUnresolvedConnectionLosses.
        // This method is now invoked solely when bluetooth is turned off, but zeroing should be done in any case
        // as no devices means no unresolved connections and no messages and user will always be redirected to proper screen).
        numberOfUnresolvedConnectionLosses = 0;
        connectedDevicesNames.clear();
        connectedDevicesData.clear();
        currentDeviceName = NO_DEVICES;
    }

    // This method is invoked after all the connection losses have been resolved. It determines whether the user can
    // stay on current screen, and, if it is not possible, to which screen they should be directed.
    private void navigateToProperScreen() {
        Class<?> availableActivity = MainActivity.class; // Default screen to which user should be directed.
        String message = null;  // Message with which user would be directed to another screen.

        if (requiredDevice == NO_DEVICES) // Current screen requires no devices to be present at it. We will stay on it.
            return;

        if (requiredDevice == ANY_DEVICE) // Current screen requires at least one arbitrary device connected to the application.
        {
            if (currentDeviceName != NO_DEVICES) // At least one device is connected. We will stay on this screen.
                return;

            // No devices connected. We will redirect the user to the main screen.
            message = "You cannot stay on this screen as you have lost connection to all devices.You will be redirected to the main screen.";
        } else // Current screen requires a specific device connected to the application.
        {
            if (currentDeviceName == requiredDevice) // Device is still connected. We will stay on this screen.
                return;

            // We lost connection to the required device. We will redirect the user to the nearest available screen (main screen by default).
            message = "You cannot stay on this screen as you have lost connection to the device, which properties you are viewing. You will be redirected to the nearest available activity.";
            if (currentDeviceName != NO_DEVICES) // At least one device is connected. We will redirect the user to the last activity that does not require a specific device.
                availableActivity = lastActivityForAnyDevice;
        }

        // Show message box on proper activity, hence it needs to be shown after the intent is sent.
        currentActivity.showMessageBox("Connection error", message, "Ok", new IgnoreReconnectionListener(availableActivity), null, null, null);
    }

    // This method is invoked when a connection loss is handled. It performs necessary actions to clean up the application state
    // after all connection losses have been handled.
    private void resolveConnectionLosses() {
        if (numberOfUnresolvedConnectionLosses > 0) // Not all connection losses have been resolved yet. Do nothing.
            return;

        if (connectedDevicesData.isEmpty())  // Stop service that displays 6140 icon whenever at least one device is connected.
            stopService(new Intent(this, DeviceService.class));

        // If there are no more connected devices, set "No devices" as current selected device.
        // If only one device is connected, choose it as currently selected device.
        // If several devices are connected, there was a device selected (not "All Devices" option was selected),
        // and that device is no more connected, select the first device from the list of connected devices.
        if (connectedDevicesNames.isEmpty())
            currentDeviceName = NO_DEVICES;
        else if ((connectedDevicesNames.size() == 1) ||
                (!allDevicesSelected() && !connectedDevicesNames.contains(currentDeviceName)))
            currentDeviceName = connectedDevicesNames.listIterator().next();

        navigateToProperScreen();

        // Update the screen
        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_DEVICE_SELECTION));
        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_SCREEN_CONTROLS));
    }

    public boolean allDevicesSelected() {
        return (currentDeviceName == ALL_DEVICES);
    }

    private void scrollDeviceIndexLeft() {
        if (allDevicesSelected())
            currentDeviceName = connectedDevicesNames.get(connectedDevicesNames.size() - 1);
        else {
            int indexOfDevice = connectedDevicesNames.indexOf(currentDeviceName) - 1;
            currentDeviceName = connectedDevicesNames.get(indexOfDevice);
        }

        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_DEVICE_SELECTION));
    }

    private void scrollDeviceIndexRight() {
        int indexOfDevice = connectedDevicesNames.indexOf(currentDeviceName) + 1;
        if (indexOfDevice == connectedDevicesNames.size())
            currentDeviceName = ALL_DEVICES;
        else
            currentDeviceName = connectedDevicesNames.get(indexOfDevice);

        sendBroadcast(new Intent(INTENT_ACTION_UPDATE_DEVICE_SELECTION));
    }

    public void initializeDeviceIndexButtons(Activity currentActivity) {

        tvDeviceName = (TextView) currentActivity.findViewById(R.id.tvDeviceName);
        btnDeviceLeft = (Button) currentActivity.findViewById(R.id.btnScrollDeviceIndexLeft);

        btnDeviceLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isButtonPressAllowed)
                    return;

                scrollDeviceIndexLeft();
            }
        });

        btnDeviceRight = (Button) currentActivity.findViewById(R.id.btnScrollDeviceIndexRight);
        btnDeviceRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isButtonPressAllowed)
                    return;

                scrollDeviceIndexRight();
            }
        });

        updateDeviceIndexPresentation();
    }

    private void updateDeviceIndexPresentation() {
        if (connectedDevicesNames.size() < 2) {
//            btnDeviceLeft.setVisibility(View.INVISIBLE);
            btnDeviceRight.setVisibility(View.INVISIBLE);
        } else {
            if (connectedDevicesNames.indexOf(currentDeviceName) == 0) {
                btnDeviceLeft.setVisibility(View.INVISIBLE);
                btnDeviceRight.setVisibility(View.VISIBLE);
            } else if (allDevicesSelected()) {
                btnDeviceLeft.setVisibility(View.VISIBLE);
                btnDeviceRight.setVisibility(View.INVISIBLE);
            } else {
                btnDeviceLeft.setVisibility(View.VISIBLE);
                btnDeviceRight.setVisibility(View.VISIBLE);
            }
        }

        tvDeviceName.setText(currentDeviceName);
    }

    public IDevice getDevice() {
        if ((currentDeviceName == NO_DEVICES) || (currentDeviceName == ALL_DEVICES) || !connectedDevicesData.containsKey(currentDeviceName))
            return null;

        return connectedDevicesData.get(currentDeviceName).device;
    }

    public DeviceData getDeviceData() {
        if ((currentDeviceName == NO_DEVICES) || (currentDeviceName == ALL_DEVICES))
            return null;

        return connectedDevicesData.get(currentDeviceName);
    }

    public boolean isScannerEnabled() {
        DeviceData data = getDeviceData();
        if (data == null)
            return false;

        return data.isScannerEnabled;
    }

    public void setScanEngineEnabled(DeviceData deviceData, boolean newIsScannerEnabled) {
        if (deviceData == null)
            return;

        try {
            deviceData.device.getScanner().enableScan(newIsScannerEnabled);
            deviceData.isScannerEnabled = newIsScannerEnabled;
        } catch (ApiScannerException e) {
            currentActivity.showErrorMessageBox("Scanner enabling/disabling setting failed.", e, "Abort", abortPrinterError, "Ignore", null, deviceData.device);
        }
    }

    private void configureLogging() {

        File logDir = new File(Environment.getExternalStorageDirectory(), LOG_FOLDER);

        if (!logDir.exists() || !logDir.isDirectory()) {
            logDir.mkdirs();
        }

        System.setProperty(LOG_FOLDER_ENV_VARIABLE, logDir.getAbsolutePath());

        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        try {
            context.reset();
            configurator.doConfigure(getAssets().open(LOG_CONFIG_FILE));
        } catch (JoranException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addScannerActivity() throws ApiScannerException {

        String c = currentActivity.toString();
        String p = "ConsultaProductos";
        String t = c.replace(".", " ").replace("@", " ");
        boolean r = t.contains(p);

        if (r) {
            tvScannerActivityText = currentActivity.findViewById(R.id.txtBuscarProducto);

            txtViewCodigo = currentActivity.findViewById(R.id.mCodigo);
            txtViewCodigodeBarras = currentActivity.findViewById(R.id.txtCodigoDeBarras);
            txtPlus = currentActivity.findViewById(R.id.txtCodigoPlus);
            txtViewDescripcion = currentActivity.findViewById(R.id.txtDescripcion);
            txtViewUbicacion = currentActivity.findViewById(R.id.txtUbicacion);
            txtUnidadMedida = currentActivity.findViewById(R.id.txtUnidadMedida);
            txtVolumen = currentActivity.findViewById(R.id.txtVolumen);
            textViewEmpaque = currentActivity.findViewById(R.id.txtUnidadEmpaque);
            txtViewMinimo = currentActivity.findViewById(R.id.txtMinimo);
            txtViewMaximo = currentActivity.findViewById(R.id.txtMaximo);


        } else {
            String cadenaDondeBuscar = currentActivity.toString();
            String palabra = "InventarioActivity";
            String texto = cadenaDondeBuscar.replace(".", " ").replace("@", " ");
            boolean resultado = texto.contains(palabra);

            if (resultado) {
                tvScannerActivityText = currentActivity.findViewById(R.id.txtBuscarProducto); //Inventario
            } else {
                tvScannerActivityText = currentActivity.findViewById(R.id.Ean_Leido);
                Descripcion = currentActivity.findViewById(R.id.textView8);
                Referencia = currentActivity.findViewById(R.id.textView7);
                Codigo_leido = currentActivity.findViewById(R.id.Ean);
                Total_leidos = currentActivity.findViewById(R.id.textView6);
                Leidos_codigo = currentActivity.findViewById(R.id.Conteo);
                comprobar = currentActivity.findViewById(R.id.checkBox);
                cantidad = currentActivity.findViewById(R.id.Cantidad);
                txtTipoLectura = currentActivity.findViewById(R.id.MostrarMetodoLectura);
            }
        }
        if (isScannerActive)
            return;

        for (DeviceData deviceData : connectedDevicesData.values())
            deviceData.device.getScanner().addListenerScan(scannerActivityCallback);

        isScannerActive = true;
    }

    public void removeScannerActivity() throws ApiScannerException {
        tvScannerActivityText = null;

        if (!isScannerActive)
            return;

        for (DeviceData deviceData : connectedDevicesData.values())
            deviceData.device.getScanner().removeListenerScan(scannerActivityCallback);

        isScannerActive = false;
    }

    public void addScannerActivityLine(String line) {
        scannerActivityText += line;
        if (tvScannerActivityText != null)
            tvScannerActivityText.setText(scannerActivityText);
    }

    public void addScanPrintScenario() throws ApiScannerException, ApiDeviceException {
        if (isScanPrintScenarioActive)
            return;

        for (DeviceData deviceData : connectedDevicesData.values()) {
            //Setting trigger mode to scan, so we can use trigger for scan&print scenarios, and not refer to the button.
            deviceData.device.setTriggerMode(TriggerMode.Scan);

            //Enabling the scanner, so that it works, otherwise this scenario would be rendered useless.
            setScanEngineEnabled(deviceData, true);

            deviceData.device.getScanner().addListenerScan(scanPrintScenarioCallback);
        }

        isScanPrintScenarioActive = true;
    }

    public void removeScanPrintScenario() throws ApiScannerException {
        if (!isScanPrintScenarioActive)
            return;

        for (DeviceData deviceData : connectedDevicesData.values())
            deviceData.device.getScanner().removeListenerScan(scanPrintScenarioCallback);

        isScanPrintScenarioActive = false;
    }

    public void addSystemActivity() {
        // It is guaranteed that this methos is called only when getDevice() returns a correct device.
        IDevice device = getDevice();
        try {
            device.addListenerTriggerPress(triggerPressCallback);
        } catch (ApiDeviceException e) {
            currentActivity.showMessageBox("Adding listener failed",
                    String.format("Trigger press listener has not been added. Error code is %d. Listening to trigger press will be inactive.", e.getErrorCode()),
                    "Ok", null, null, null, device);
        }
    }

    public void removeSystemActivity() {
        IDevice currentDevice = getDevice();
        if (currentDevice != null) {
            try {
                currentDevice.removeListenerTriggerPress(triggerPressCallback);
            } catch (ApiDeviceException e) {
            }
        }
    }

    public void registerResource(ResourceMediaType resourceMediaType, String alias, String fileName) throws ApiResourceException {
        File file = new File(resourcePath, fileName);
        try {
            final InputStream is = getResources().getAssets().open(fileName);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = is.read(buffer, 0, buffer.length)) != -1) {
                fos.write(buffer, 0, read);
            }
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            currentActivity.showMessageBox("Resource file not found", "Unable to copy resource from application to device storage.", "OK", null, null, null, null);
            return;
        } catch (IOException e) {
            currentActivity.showMessageBox("Resource copying failed", "Unable to copy resource from application to device storage.", "OK", null, null, null, null);
            return;
        }

        if (!file.exists()) {
            currentActivity.showMessageBox("Resource copying failed", "Resource was properly copied, but is not detected at destination.", "OK", null, null, null, null);
            return;
        }

        ResourceManager.registerResource(resourceMediaType, alias, file.getAbsolutePath());
        if (!ResourceManager.checkResource(resourceMediaType, alias)) {
            currentActivity.showMessageBox("Resource copying failed", "Resource is registered, but its registration is not verified by the resource check.", "OK", null, null, null, null);
        }
    }

    private String apiBarcodeTypeToLntBarcodeType(BarcodeType barcodeType) {
        String result = "";
        switch (barcodeType) {
            case UPCA:
                result = "upca";
                break;
            case UPCA_2:
                result = "upca+2";
                break;
            case UPCA_5:
                result = "upca+5";
                break;
            case UPCE:
                result = "upce";
                break;
            case UPCE_2:
                result = "upce+2";
                break;
            case UPCE_5:
                result = "upce+5";
                break;
            case UPCE1:
                result = "upce1";
                break;
            case EAN_8:
                result = "ean8";
                break;
            case EAN_8_2:
                result = "ean8+2";
                break;
            case EAN_8_5:
                result = "ean8+5";
                break;
            case EAN_13:
                result = "ean13";
                break;
            case EAN_13_2:
                result = "ean13+2";
                break;
            case EAN_13_5:
                result = "ean13+5";
                break;
            case CodeD25:
                result = "code2of5";
                break;
            case CodeITF:
                result = "i2of5";
                break;
            case Code39:
            case Code39FullASCII:
                result = "code39";
                break;
            case Code93:
                result = "code93";
                break;
            case Codabar:
                result = "codabar";
                break;
            case Code128:
                result = "code128";
                break;
            case PDF_417:
            case MacroPDF_417:
                result = "pdf417";
                break;
            case MicroPDF:
            case MacroMicroPDF:
                result = "micropdf417";
                break;
            case MSI:
                result = "msi";
                break;
            case Maxicode:
                result = "maxicode";
                break;
            case DataMatrix:
                result = "data matrix";
                break;
            case QR:
                result = "qr";
                break;
            case PostnetUS:
                result = "postnet";
                break;
            case RSS_14:
            case RSSLimited:
                result = "rss";
                break;
            case RSSExpanded:
                result = "rssexpanded";
                break;
            case CompositeCC_A_EAN_128:
            case CompositeCC_A_EAN_13:
            case CompositeCC_A_EAN8:
            case CompositeCC_A_RSSExpanded:
            case CompositeCC_A_RSSLimited:
            case CompositeCC_A_RSS14:
            case CompositeCC_A_UPCA:
            case CompositeCC_A_UPCE:
            case CompositeCC_C_EAN_128:
            case CompositeCC_B_EAN_128:
            case CompositeCC_B_EAN_13:
            case CompositeCC_B_EAN_8:
            case CompositeCC_B_RSSExpanded:
            case CompositeCC_B_RSSLimited:
            case CompositeCC_B_RSS14:
            case CompositeCC_B_UPCA:
            case CompositeCC_B_UPCE:
                result = "gs1";
                break;
            case CodeIATA:
            case Code11:
            case EAN_128:
            case Trioptic:
            case Bookland:
            case ISBT_128:
            case PlanetUS:
            case Code32:
            case ISBT_128Concat:
            case PostalJapan:
            case PostalAustralia:
            case PostalDutch:
            case PostbarCA:
            case PostalUK:
            case ParameterFNC3:
            case ScanletWebcode:
            case CueCATCode:
            case UPCE1_2:
            case TLC_39:
            case UPCE1_5:
            case MultipacketFormat:
            case Unknown:
            default:
                result = "";
        }

        return result;
    }

    // This indices denote the usage of appropriate barcode type indicators in barcode fields of the LNT "BarcodePrint.LNT".
    // We need to determine under which index to print the barcode when printing this LNT and the function below
    // performs the search of the barcode type in this array and returns appropriate index.
    // IMPORTANT : If you alter barcode fields in BarcodePrint.LNT (by changing their order or adding new ones),
    // be sure to update this array as well!
    private static final String[] lntBarcodeTypeFieldIndices = {"upca", "upca+2", "upca+5", "upce", "upce1", "upce+2", "upce+5",
            "ean8", "ean8+2", "ean8+5", "ean13", "ean13+2", "ean13+5", "i2of5", "itf", "code39", "code93", "code2of5", "codabar",
            "nw7", "msi", "code128", "code128a", "code128b", "code128c", "pdf417", "micropdf417", "maxicode", "code16",
            "data matrix", "quick response", "qr", "postnet", "gs1databar", "gs1", "rss"};

    private int lntBarcodeTypeToLntFieldIndex(String lntBarcodeType) {
        for (int result = 0; result < lntBarcodeTypeFieldIndices.length; ++result) {
            if (lntBarcodeType.equals(lntBarcodeTypeFieldIndices[result]))
                return result;
        }

        return -1;
    }

    private void registerResources() {
        try {
            resourceDirs = new File(Environment.getExternalStorageDirectory().toString(), "Download");

            if (resourceDirs.canWrite() == false)
                resourceDirs = new File(getFilesDir().getAbsolutePath().toString());

            resourcePath = resourceDirs.getAbsolutePath() + "/";
            ResourceManager.initializeResourcePath(resourcePath);

            String[] listOfResources = ResourceManager.getResourceList(ResourceMediaType.Font);

            if (!Arrays.asList(listOfResources).contains("Arial"))
                registerResource(ResourceMediaType.Font, "Arial", "Arial.ttf");

            listOfResources = ResourceManager.getResourceList(ResourceMediaType.Graphic);
            if (!Arrays.asList(listOfResources).contains("Avery Logo"))
                registerResource(ResourceMediaType.Graphic, "Avery Logo", "AveryDennisonLogo.png");

            listOfResources = ResourceManager.getResourceList(ResourceMediaType.Lnt);
            if (!Arrays.asList(listOfResources).contains("Sample Print LNT"))
                registerResource(ResourceMediaType.Lnt, "Sample Print LNT", "SamplePrint.LNT");

            if (!Arrays.asList(listOfResources).contains("Quantity Test LNT"))
                registerResource(ResourceMediaType.Lnt, "Quantity Test LNT", "QuantityTest.LNT");

            if (!Arrays.asList(listOfResources).contains("Barcode Print LNT"))
                registerResource(ResourceMediaType.Lnt, "Barcode Print LNT", "BarcodePrint.LNT");

            if (!Arrays.asList(listOfResources).contains("Barcode Text Print LNT"))
                registerResource(ResourceMediaType.Lnt, "Barcode Text Print LNT", "BarcodePrintTextOnly.LNT");
            if (!Arrays.asList(listOfResources).contains("Bold Italic Underline LNT"))
                registerResource(ResourceMediaType.Lnt, "Bold Italic Underline LNT", "BoldItalicUnderline.LNT");
            if (!Arrays.asList(listOfResources).contains("Bold Italic Underline MIX LNT"))
                registerResource(ResourceMediaType.Lnt, "Bold Italic Underline MIX LNT", "BoldItalicUnderlineMixed.LNT");
            if (!Arrays.asList(listOfResources).contains("BoldItalicUnderline NewLine LNT"))
                registerResource(ResourceMediaType.Lnt, "BoldItalicUnderline NewLine LNT", "BoldItalicUnderlineNewLine.LNT");
        } catch (ApiResourceException e) {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
        }
    }


    public void GuardarEan(String EAN) {
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_LEIDO_SCANNER, null, 1);

        SQLiteDatabase dbr = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_LEIDO_SCANNER;//Set name of your table

        Cursor c = dbr.rawQuery("SELECT " + Utilidades.CAMPO_ID + " FROM '" + myTable + "' where id =" + 1, null);


        if (c.moveToNext()) {

            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.CAMPO_EAN_SCANNER, EAN);

            db.update(Utilidades.TABLA_LEIDO_SCANNER, values, Utilidades.CAMPO_ID + "=" + 1, null);

        } else {
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.CAMPO_SCAMMER_ID, 1);
            values.put(Utilidades.CAMPO_EAN_SCANNER, EAN);


            db.insert(Utilidades.TABLA_REGISTRO, Utilidades.CAMPO_SCAMMER_ID, values);
        }
        dbr.close();

    }

    private void printSample() {

        printSampleIndex++;

        byte[][] stringText = {String.format(Descripcion.getText().toString()).getBytes()};

        for (DeviceData deviceData : connectedDevicesData.values()) {
            IDevice device = deviceData.device;
            IPrinter printer = device.getPrinter();
            try {
                printer.print("Sample Print LNT", 1, stringText);
            } catch (ApiPrinterException e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void printSampleScanPrint() {

        printSampleIndex++;

        byte[][] stringText = {String.format(Descripcion.getText().toString()).getBytes()};

        for (DeviceData deviceData : connectedDevicesData.values()) {
            IDevice device = deviceData.device;
            IPrinter printer = device.getPrinter();
            try {
                printer.print("Sample Print LNT", 1, stringText);
            } catch (ApiPrinterException e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            }
        }

    }


    public void ConsultarDatos(Boolean Online) throws JSONException {

        if (!Online) {
            final String valorEditText = tvScannerActivityText.getText().toString();
            Codigo_leido.setText(valorEditText);

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();

            jsonBody.put("Cod_Empresa", "01");
            jsonBody.put("Cod_Bodega", "BODGENERAL");
            jsonBody.put("Valor", valorEditText);


            String url = V_URL + "/BuscarProducto";


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray mJsonArray = response.getJSONArray("recordset");


                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                            String referencia = mJsonObject.getString("Cod_Barras");
                            String desrefer = mJsonObject.getString("Descripcion");


                            Referencia.setText(referencia);
                            Descripcion.setText(desrefer);

                            if (referencia.length() >= 3) {
                                tvScannerActivityText.setText("");
                            }


                        }

                        if (Referencia.getText().toString().length() == 0) {
                            tvScannerActivityText.setText("");
                            Descripcion.setText("Sin registro");
                            new AlertDialog.Builder(SampleApplication.this)
                                    .setTitle("Oh, oh, algo salió mal")
                                    .setMessage("No se encontró ningún registro con ese EAN")
                                    .setCancelable(false)
                                    .setPositiveButton("ok", null).show();
                        } else {
                            if (comprobar.isChecked() == true) {
                                IngresarDatos(valorEditText, Referencia.getText().toString(), Descripcion.getText().toString(), Integer.parseInt(cantidad.getText().toString()) + Acumulador(valorEditText));
                                Toast.makeText(getApplicationContext(), "Guardado con cantidad", Toast.LENGTH_SHORT).show();
                            } else {
                                IngresarDatos(valorEditText, Referencia.getText().toString(), Descripcion.getText().toString(), Acumulador(valorEditText) + 1);
                            }
                            //NumeroDeRegistros();
                            Total_leidos.setText(String.valueOf(ContarRegistros));
                            Long total = DAOInventario.CapturarCantidadCodigosLeidos(getApplicationContext(), valorEditText);
                            Leidos_codigo.setText(String.valueOf(Acumulador(valorEditText)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SampleApplication.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            queue.add(request);
        } else {
            ContarRegistros++;

            String referencia = "", desrefer = "";


            String data = DAOInventario.GetJsonData(getApplicationContext());
            int ubicacion = 0;
            String valor_Ean = tvScannerActivityText.getText().toString();


            JSONObject jsonBody = new JSONObject(DAOInventario.GetJsonData(getApplicationContext()));


            JSONArray jsonArray = jsonBody.getJSONArray("recordset");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String clave = object.getString("ean").trim();
                if (clave.equals(tvScannerActivityText.getText().toString())) {
                    ubicacion = i;
                    JSONObject ObjectU = jsonArray.getJSONObject(ubicacion);
                    referencia = ObjectU.getString("referencia");
                    desrefer = ObjectU.getString("desrefer");
                    Referencia.setText(referencia);
                    Descripcion.setText(desrefer);
                    break;
                }
            }


            if (Descripcion.getText().toString().length() == 0) {
                tvScannerActivityText.setText("");
                Descripcion.setText("Sin registro");
                new AlertDialog.Builder(SampleApplication.this)
                        .setTitle("Oh, oh, algo salió mal")
                        .setMessage("No se encontró ningún registro con ese EAN o no ha descargado los datos desde Sincronizar - Descargar datos")
                        .setCancelable(false)
                        .setPositiveButton("ok", null).show();
                cont = 0;
            } else {
                if (comprobar.isChecked() == true) {
                    IngresarDatos(valor_Ean, Referencia.getText().toString(), Descripcion.getText().toString(), Integer.parseInt(cantidad.getText().toString()) + Acumulador(valor_Ean));
                    Toast.makeText(getApplicationContext(), "Guardado con cantidad", Toast.LENGTH_SHORT).show();
                } else {
                    IngresarDatos(valor_Ean, Referencia.getText().toString(), Descripcion.getText().toString(), 1 + Acumulador(valor_Ean));
                }


                Codigo_leido.setText(tvScannerActivityText.getText().toString());
                Long total = DAOInventario.CapturarCantidadCodigosLeidos(getApplicationContext(), valor_Ean);
                Leidos_codigo.setText(String.valueOf(Acumulador(valor_Ean)));
                Total_leidos.setText(String.valueOf(ContarRegistros));


                cont = 0;


            }


        }


    }

    public void IngresarDatos(String id, String referencia, String descripcion, int Cantidad) {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_REGISTRO, null, 1);

        SQLiteDatabase dbr = conn.getWritableDatabase();

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

        Cursor c = dbr.rawQuery("SELECT " + Utilidades.CAMPO_ID + " FROM '" + myTable + "' where id =" + id, null);


        if (c.moveToNext()) {

            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.CAMPO_ID, id);
            values.put(Utilidades.CAMPO_REFERENCIA, referencia);
            values.put(Utilidades.CAMPO_DESCRIPCION, descripcion);
            values.put(Utilidades.CAMPO_CANTIDAD, Cantidad);
            values.put(Utilidades.CAMPO_FECHA, hourdateFormat.format(date));

            db.update(Utilidades.TABLA_REGISTRO, values, Utilidades.CAMPO_ID + "=" + id, null);
            Toast.makeText(getApplicationContext(), "El dato fue actualizado correctamenete", Toast.LENGTH_SHORT).show();
        } else {
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.CAMPO_ID, id);
            values.put(Utilidades.CAMPO_REFERENCIA, referencia);
            values.put(Utilidades.CAMPO_DESCRIPCION, descripcion);
            values.put(Utilidades.CAMPO_CANTIDAD, Cantidad);

            db.insert(Utilidades.TABLA_REGISTRO, Utilidades.CAMPO_PRIMARYID, values);
            Toast.makeText(getApplicationContext(), "El dato fu ingresado corrrectamente", Toast.LENGTH_SHORT).show();
        }
        dbr.close();
    }


    public int Acumulador(String EAN) {
        int Cantidad = 0;


        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_REGISTRO, null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_REGISTRO;//Set name of your table

        Cursor c = db.rawQuery("SELECT Cantidad FROM '" + myTable + "' where id = " + EAN, null);


        if (c.moveToFirst()) {
            do {
                Cantidad = c.getInt(0);
            } while (c.moveToNext());
        }
        db.close();

        return Cantidad;
    }

    public String EanActual(Context context) {
        int Cantidad = 0;
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context, Utilidades.TABLA_LEIDO_SCANNER, null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_LEIDO_SCANNER;//Set name of your table

        Cursor c = db.rawQuery("SELECT eanLeido FROM '" + myTable + "' where id = " + 1, null);

        String EANScanner = "";

        if (c.moveToFirst()) {
            do {
                EANScanner = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();

        return EANScanner;
    }

    public Boolean ComprobarSwitch() {
        Boolean band = false;
        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "db_estado", null, 1);

        SQLiteDatabase db = conn.getReadableDatabase();

        String myTable = Utilidades.TABLA_ESTADO;//Set name of your table

        Cursor c = db.rawQuery("SELECT " + Utilidades.CAMPO_ESTADO + " FROM '" + myTable + "'", null);

        String json = "";

        if (c.moveToFirst()) {
            do {
                json = c.getString(0);

            } while (c.moveToNext());
        }
        db.close();


        if (json.equals("0")) {
            band = true;
        }

        return band;
    }

    public void Scan_Print_Scan() throws JSONException {
        /*
        tvScannerActivityText.setText(scannerActivityText);
        Descripcion.setText("Descripcion de prueba");
        Referencia.setText("Referencia de prueba");
        Codigo_leido.setText("Código leido de prueba");
        Total_leidos.setText("Total leidos de prueba");
        Leidos_codigo.setText("Total leidos x código de prueba");

        String comprobar = EanActual(getApplicationContext());

        if(!comprobar.equals(tvScannerActivityText) || cont == 1){
            //Método de imprimir desactivado para ahorrar papel
            printSample();
        }
         Layout layout = tvScannerActivityText.getLayout(); */

        RequestQueue queue = Volley.newRequestQueue(this);

        if (!scannerActivityText.equals("NR")) {
            tvScannerActivityText.setText(scannerActivityText);
        }

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Cod_Bodega", "BODGENERAL");
        jsonBody.put("Codigo", tvScannerActivityText.getText());


        //final JSONObject jsonBody = new JSONObject("{ \"codigo\":\"" +  tvScannerActivityText.getText() + "\"}" );
        String url = V_URL + "/ObtenerScanner";
        Referencia.setText("");
        Leidos_codigo.setText("");

        final String[] preguntas = {};
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray preguntasJA = response.getJSONArray("recordset");


                    for (int i = 0; i < preguntasJA.length(); i++) {
                        JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                        Descripcion.setText(mJsonObject.getString("Cod_Barras"));
                        Referencia.setText(mJsonObject.getString("Codigo"));
                        Codigo_leido.setText(mJsonObject.getString("Talla"));
                        Total_leidos.setText(mJsonObject.getString("Color"));
                        Leidos_codigo.setText(mJsonObject.getString("Descripcion"));


                        if (Descripcion.getText().toString().length() > 0 && !scannerActivityText.equals("NR")) {
                            tvScannerActivityText.setText("");
                            printSample();
                        }


                    }


                    if (Referencia.length() == 0 && Leidos_codigo.length() == 0) {
                        tvScannerActivityText.setText("");
                        Descripcion.setText("");
                        Referencia.setText("");
                        Codigo_leido.setText("");
                        Total_leidos.setText("");
                        Leidos_codigo.setText("");
                        Toast.makeText(getApplicationContext(), "No se encontró ningú dato relacionado con ese código", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SampleApplication.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
    }


    public void Scan_Print_Print() {
        printSampleScanPrint();
    }

    public void MostrarConsultaProductos() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonBody = null;
        jsonBody = new JSONObject();


        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Cod_Bodega", "BODGENERAL");
        jsonBody.put("Valor", scannerActivityText);


        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        String url = t_configuraciones.getCon_URL() + "/BuscarProducto";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray mJsonArray = response.getJSONArray("recordset");

                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        codigo = mJsonObject.getString("Codigo");
                        codbarras = mJsonObject.getString("Cod_Barras");
                        codigoplu = mJsonObject.getString("Descripcion");
                        descripcion = mJsonObject.getString("Cod_Ubicacion");
                        undempaque = mJsonObject.getString("Und_Medida");
                        unidadmed = mJsonObject.getString("Unidad_medida");
                        volumen = mJsonObject.getString("VOLUMEN");
                        cantempaque = mJsonObject.getString("CANTEMPAQUE");
                        minimo = mJsonObject.getString("Minimo");
                        maximo = mJsonObject.getString("Maximo");
                        Toast.makeText(getApplicationContext(), codigo, Toast.LENGTH_SHORT).show();
                        //Reiniciando el contador
                    }

                    txtViewCodigo.setText(codigo);
                    txtViewCodigodeBarras.setText(codbarras);
                    txtPlus.setText(codigoplu);
                    txtViewDescripcion.setText(descripcion);
                    txtViewUbicacion.setText(undempaque);
                    txtUnidadMedida.setText(unidadmed);
                    txtVolumen.setText(volumen);
                    textViewEmpaque.setText(cantempaque);
                    txtViewMinimo.setText(minimo);
                    txtViewMaximo.setText(maximo);
                    tvScannerActivityText.setText("");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);

    }

}
