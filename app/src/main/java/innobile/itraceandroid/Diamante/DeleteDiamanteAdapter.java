package innobile.itraceandroid.Diamante;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import innobile.itrace.R;
import innobile.itraceandroid.Retina.ModeloDeleteRetina;

class DeleteDiamanteAdapter extends RecyclerView.Adapter<DeleteDiamanteAdapter.PlayerViewHolder> {
    public List<ModeloDiamante> productos;



    public class PlayerViewHolder extends RecyclerView.ViewHolder {
        private TextView nombre, cedula, fecha_nacimiento, correo, celular, fecha_ingreso , responsable;

        public PlayerViewHolder(View view) {
            super(view);
            nombre = (TextView) view.findViewById(R.id.nombre);
            cedula = (TextView) view.findViewById(R.id.numero_cedula);
            fecha_nacimiento = (TextView) view.findViewById(R.id.fecha_nacimiento);
            correo = (TextView) view.findViewById(R.id.correo_electronico);
            celular = (TextView) view.findViewById(R.id.celular);
            fecha_ingreso = view.findViewById(R.id.t_fecha_ingreso);
            responsable = view.findViewById(R.id.t_responsable);

        }
    }

    public DeleteDiamanteAdapter(List<ModeloDiamante> productos) {
        this.productos = productos;
    }

    @Override
    public DeleteDiamanteAdapter.PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delete_diamante_row, parent, false);

        return new DeleteDiamanteAdapter.PlayerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeleteDiamanteAdapter.PlayerViewHolder holder, int position) {
        ModeloDiamante productos = this.productos.get(position);
        holder.cedula.setText(productos.getCedula());
        holder.nombre.setText(productos.getNombre());
        holder.fecha_nacimiento.setText(productos.getFechaNacimiento());
        holder.correo.setText(productos.getCorreo());
        holder.celular.setText(productos.getCelular());
        holder.fecha_ingreso.setText(productos.getFechaIngreso());
        holder.responsable.setText(productos.getUsuario());

    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}

