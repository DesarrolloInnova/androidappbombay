package innobile.itraceandroid.Diamante;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.Adapter.ActivosFijo;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.RetrofitAdapter.Cedulas;
import innobile.itraceandroid.RetrofitAdapter.JsonPlaceHolderApi;
import innobile.itraceandroid.RetrofitAdapter.bodegas_id;
import innobile.itraceandroid.TrasladosTM.Reportes_Traslados;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LecturaCedulas extends AppCompatActivity {
    private int comprobarIngreso = 0;
    private EditText codigoLeido, Correo_Electronico, Numero_Celular;
    private TextView Numero_cedula, Nombre, Fecha_Nacimiento;
    private String sNumero_Cedula, primerNombre = "", segundoNombre = "", primerApellido, segundoApellido, sFecha_Nacimiento;
    private Button btn_guardar,btn_detalles,btn_sincronizar;
    public static Retrofit retrofit;
    private ConexionSQLiteHelper conn;
    private SQLiteDatabase db;
    private ContentValues contentValues;
    private String V_URL_MON = "https://webapi.innova-id.com/api/cedulas";
    private List<String> datos = new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura_cedulas);

        codigoLeido = findViewById(R.id.codigo_leido);
        Numero_cedula = findViewById(R.id.numero_cedula);
        Nombre = findViewById(R.id.nombre);
        Fecha_Nacimiento = findViewById(R.id.fecha_nacimiento);
        Correo_Electronico = findViewById(R.id.t_fecha_nacimiento);
        Numero_Celular = findViewById(R.id.t_numero_celular);
        btn_guardar = findViewById(R.id.btn_guardar);
        btn_detalles = findViewById(R.id.btn_detalles);
        btn_sincronizar = findViewById(R.id.btn_sincronizar);
        
        //Se detectan los datos ingresados en el EditText automáticamente
        codigoLeido.setOnKeyListener((view, i, keyEvent) -> {
            //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
            if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                //Este contador se encarga de enviar los datos solo una vez
                    if (!codigoLeido.getText().toString().trim().isEmpty()) {
                        mostrar_Datos_Leidos(codigoLeido.getText().toString());
                    }
                return true;
            }
            return false;
        });

        //Iniciando los editext para evitar datos null
        Correo_Electronico.setText("");
        Numero_cedula.setText("");

        btn_guardar.setOnClickListener(view -> guardarDatosLeidos(Numero_cedula.getText().toString(), Nombre.getText().toString(), Fecha_Nacimiento.getText().toString(), Correo_Electronico.getText().toString(), Numero_Celular.getText().toString()));
        btn_detalles.setOnClickListener(view -> {
            Intent intent = new Intent(LecturaCedulas.this, DiamanteDetallesActivity.class);
            startActivity(intent);
        });

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(V_URL_MON + '/')
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        btn_sincronizar.setOnClickListener(view -> recuperarDatosLeidos());
    }

    private void mostrar_Datos_Leidos(String Codigo) {

        try {
            sNumero_Cedula = Codigo.substring(48, 58);

            primerNombre  =  Codigo.substring(104, 127);
            segundoNombre  = Codigo.substring(127, 150); //ok

            //sexo 150

            primerApellido = Codigo.substring(58, 80);
            segundoApellido =  Codigo.substring(81, 104); //ok

            sFecha_Nacimiento =  Codigo.substring(152, 160);


            Numero_cedula.setText(sNumero_Cedula);
            Nombre.setText(primerNombre.trim()  + " " + segundoNombre.trim() +" " +  primerApellido.trim() + " " + segundoApellido.trim());
            Fecha_Nacimiento.setText(sFecha_Nacimiento.substring(0, 4) + " - " +sFecha_Nacimiento.substring(4, 6) + "-" +sFecha_Nacimiento.substring(6, 8) );
        }catch (StringIndexOutOfBoundsException e){
            Toasty.error(getApplicationContext(), "Error al tratar de obtener el código", Toasty.LENGTH_SHORT).show();
            limpiarDatos();

        }



        codigoLeido.setText("");
    }

    private void guardarDatosLeidos(String NumeroCedula, String NombreCompleto, String FechaNacimiento, String CorreoElectronico, String NumeroCelular){
        if(!NumeroCedula.trim().isEmpty()){
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_CEDULAS, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            db.beginTransactionNonExclusive();


            try {
                if (CorreoElectronico.length() > 60){
                    CorreoElectronico = "";
                }
                if(NumeroCelular.length() > 60){
                    NumeroCelular = "";
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put(Utilidades.NUMERO_CEDULA , NumeroCedula.trim());
                contentValues.put(Utilidades.NOMBRE_COMPLETO, NombreCompleto);
                contentValues.put(Utilidades.FECHA_NACIMIENTO, FechaNacimiento.trim());
                contentValues.put(Utilidades.CORREO_ELECTRONICO, CorreoElectronico.trim());
                contentValues.put(Utilidades.NUMERO_CELULAR, NumeroCelular.trim());
                contentValues.put(Utilidades.FECHA_LECTURA, getFechaActual());
                contentValues.put(Utilidades.USUARIO_INGRESO, Cons.Usuario);
                db.insert(Utilidades.TABLA_CEDULAS, null, contentValues);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            Toasty.success(getApplicationContext(), "El registro fue guardado correctamente", Toasty.LENGTH_SHORT).show();
            limpiarDatos();
            codigoLeido.requestFocus();
            db.close();

            btn_sincronizar.setBackgroundColor(Color.BLUE);
        }else{
            Toasty.warning(getApplicationContext(), "Es necesario ingresar la cédula", Toasty.LENGTH_SHORT).show();
        }

    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formateador.format(ahora);
    }

    public void limpiarDatos(){
        Numero_cedula.setText("");
        Nombre.setText("");
        Fecha_Nacimiento.setText("");
        Correo_Electronico.setText("");
        Numero_Celular.setText("");
    }


    public void recuperarDatosLeidos(){
        Toasty.warning(getApplicationContext(), "Se están procesando los datos, espere el mensaje de confirmación", Toasty.LENGTH_LONG).show();
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_CEDULAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_CEDULAS; //Ingresa el nombre de tu tabla
        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " ORDER BY " + Utilidades.id + " DESC;", null);
        if (c.moveToFirst()) {
            do {
                if(!c.getString(5).equals("PROCESADO"))
                {
                SincronizarDatos(c.getString(0).trim(),  c.getString(1).trim(), c.getString(2).trim(),c.getString(3).trim(),c.getString(4).trim(),c.getString(5).trim(),c.getString(6).trim(), c.getInt(7));

                }
            } while (c.moveToNext());
        }
        db.close();
    }


    public void SincronizarDatos(String cedula, String nombre, String fecha_nacimiento,  String correo_electronico,String celular, String  fecha_entrada, String reponsable, int id){
        if(!cedula.trim().isEmpty()){
            JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
            Call<Cedulas> call = jsonPlaceHolderApi.postCedulas(cedula, nombre, fecha_nacimiento, fecha_entrada, correo_electronico, celular,reponsable);

            call.enqueue(new Callback<Cedulas>() {
                @Override
                public void onResponse(Call<Cedulas> call, Response<Cedulas> response) {
                    if (!response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    actualizarCampo(id);
                    btn_sincronizar.setText("Completado");
                    btn_sincronizar.setBackgroundColor(getResources().getColor(R.color.green));

                }

                @Override
                public void onFailure(Call<Cedulas> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), String.valueOf( t), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void actualizarCampo(int id){
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_CEDULAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_CEDULAS; //Ingresa el nombre de tu tabla
        Cursor c = db.rawQuery("UPDATE " + myTable + " SET " + Utilidades.FECHA_LECTURA +  " = 'PROCESADO' WHERE " + Utilidades.id + " = " + id, null);
        if (c.moveToNext()) {
            do {
                Toasty.success(this, "Registro " + id + " procesado", Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
        db.close();
    }
}


