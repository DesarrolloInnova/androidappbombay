package innobile.itraceandroid.Diamante;

public class ModeloDiamante {
    String Cedula;
    String Nombre;
    String FechaNacimiento;
    String Correo;
    String Celular;
    String FechaIngreso;
    String Usuario;

    public ModeloDiamante() {
    }

    public ModeloDiamante(String cedula, String nombre, String fechaNacimiento, String correo, String celular, String fechaIngreso, String usuario) {
        Cedula = cedula;
        Nombre = nombre;
        FechaNacimiento = fechaNacimiento;
        Correo = correo;
        Celular = celular;
        FechaIngreso = fechaIngreso;
        Usuario = usuario;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getFechaIngreso() {
        return FechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        FechaIngreso = fechaIngreso;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
}
