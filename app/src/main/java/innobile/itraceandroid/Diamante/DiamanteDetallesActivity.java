package innobile.itraceandroid.Diamante;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.SwipeController;
import innobile.itraceandroid.SwipeControllerActions;

public class DiamanteDetallesActivity extends AppCompatActivity {

    private static ConexionSQLiteHelper conn;
    public DeleteDiamanteAdapter mAdapter;
    SwipeController swipeController = null;
    private List<ModeloDiamante> detalles = new ArrayList<>();
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new DeleteDiamanteAdapter(detalles);
        recuperarDatosLeidos();
        setupRecyclerView();
    }


    public void recuperarDatosLeidos(){
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_CEDULAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_CEDULAS; //Ingresa el nombre de tu tabla
        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " ORDER BY " + Utilidades.id + " DESC;", null);
        if (c.moveToFirst()) {
            do {
                ModeloDiamante detalle = new ModeloDiamante();
                detalle.setCedula(c.getString(0));
                detalle.setNombre( c.getString(1));
                detalle.setFechaNacimiento( c.getString(2));
                detalle.setCorreo(c.getString(3));
                detalle.setCelular(c.getString(4));
                detalle.setFechaIngreso(c.getString(5));
                detalle.setUsuario(c.getString(6));
                detalles.add(detalle);
            } while (c.moveToNext());
        }
        db.close();
        mAdapter.notifyDataSetChanged();
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                Toasty.warning(getApplicationContext(), "Por seguridad no se pueden borrar los registros", Toasty.LENGTH_SHORT).show();
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }


}
