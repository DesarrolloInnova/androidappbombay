package innobile.itraceandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.MetodosGlobales.bodega;
import innobile.itraceandroid.TrasladosTM.DatosProductos;

public class InventarioActivosFijos extends AppCompatActivity {

    private static String V_URL, V_URL_MON = null,  V_TipoConexion  = null;
    private static TextView bodega_origen = null, tcantidad_leida = null, tcodigo_leido = null, tCodigo, nombre_bodega, tDescripcion = null, tMarca = null, tGrupo = null;
    private static int  NVecesComprobando = 0, cantidadPrueba = 0;
    private static ConexionSQLiteHelper conn;
    private static SQLiteDatabase db;
    private static Button btnDetalle, btnProcesar;
    private ImageButton btnSpeak = null;
    private static final String APP_ID = "1436ceb8d31220097ec6e298bf0084b6e";
    private TextToSpeech textToSpeech;
    private EditText tnuevo_codigo = null;
    //Creando las variables que serán guardadas en SQlite
    private static  String codigoProducto = "", numeroDocumento = "", codigoBodega = "", descripcion = "", marca ="", grupo = "", cantidadLeida = "", fecha = "";
    //Referenciando el objeto para obtener la bodega origen
    private innobile.itraceandroid.MetodosGlobales.bodega bodega = new bodega();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario_activos_fijos);

        //Referenciando los elementos de la vista
        bodega_origen = findViewById(R.id.bodega_origen);
        tcantidad_leida = findViewById(R.id.tcantidad_leida);
        tcodigo_leido = findViewById(R.id.eCodigoIngresado);
        tCodigo = findViewById(R.id.tCodigo);
        nombre_bodega = findViewById(R.id.nombre_bodega);
        tDescripcion = findViewById(R.id.tNombre);
        tMarca = findViewById(R.id.tCiudad);
        tGrupo = findViewById(R.id.tGrupo);
        tcantidad_leida = findViewById(R.id.tcantidad_leida);
        btnDetalle = findViewById(R.id.btnDetalle);
        btnProcesar = findViewById(R.id.btnGuardar);
        btnSpeak = findViewById(R.id.btnspeak1);
        tnuevo_codigo = findViewById(R.id.tnuevo_codigo);


        //Creando que convierte el texto a voz
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR){
                    textToSpeech.setLanguage( new Locale( "spa", "ESP" ) );
                }
            }
        });


        //LLamando el asistente de google
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comandosVoz();
            }


        });


        //Agregando funciona a los botones de procesar y detalle
        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Esperando función para el detalle", Toast.LENGTH_LONG).show();
            }
        });

        btnProcesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Esperando función para el botón de procesar", Toast.LENGTH_LONG).show();
            }
        });


        //Mostrando el código del documento en el campo tCodigo de la vista
        tCodigo.setText(getIntent().getExtras().getString("numero_documento"));

        //Instanciando la conexión a la base de datos Sqlite
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_INVENTARIOAF, null, 1);

        //Configurando URL de la conexión
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL=  t_configuraciones.getCon_URL();
        V_URL_MON =  t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();

        //Cargando el número de documento y la fecha
        numeroDocumento  = getIntent().getExtras().getString("numero_documento");
        fecha = getIntent().getExtras().getString("fecha");


        bodega.codigoBodega(getApplicationContext(), V_URL_MON,getIntent().getExtras().getString("Bodega_origen"), getString(R.string.InventarioActivosFijos));

        //Se encarga de ejecutar el método de buscar producto al percibir un enter
        tcodigo_leido.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
                if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB)  || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    //Este contador se encarga de enviar los datos solo una vez
                    NVecesComprobando++;
                    if(NVecesComprobando == 2){
                        String[] separandoCodigo =  tcodigo_leido.getText().toString().split(" ");
                        String codigo = separandoCodigo[0];
                        codigoProducto = codigo ;
                        bodega.datosProducto(getApplicationContext(), V_URL_MON, codigo,getString(R.string.InventarioActivosFijos));
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public static void cargarDatos () {
        bodega_origen.setText(DatosProductos.ID_ORIGEN);
        nombre_bodega.setText(DatosProductos.pNOMBRE_BODEGA);
        tDescripcion.setText(DatosProductos.acDESCRIPCION);
        tMarca.setText(DatosProductos.acMARCA);
        tGrupo.setText(DatosProductos.acGRUPO);
      //  guardarDatos();

    }

    /**
     * Se encarga de listar el total de items leidos dentro de un invetario de activos fijos
     * */

    public static void guardarDatos(){
        cantidadPrueba++;
        tcantidad_leida.setText(String.valueOf(cantidadPrueba));

        //Se guardan los datos cada vez que se hace una
        // lecturacodigoProducto = "", numeroDocumento = "", codigoBodega = "", descripcion = "", marca ="", grupo = "", cantidadLeida = "", fecha = "";private String numeroDocumento = "", codigoBodega = "", descripcion = "", marca ="", grupo = "", cantidadLeida = "", fecha = "";


        //Cargando los datos a insertar

        codigoBodega = DatosProductos.ID_ORIGEN;
        descripcion = tDescripcion.getText().toString();
        marca = tMarca.getText().toString();
        grupo = tGrupo.getText().toString();
        cantidadLeida = tcantidad_leida.getText().toString();

        tcodigo_leido.setText("");
        //Se llama el método para guardar los datos
        guardarLecturasSqlite(codigoProducto, numeroDocumento, codigoBodega, descripcion, marca, grupo, cantidadLeida, fecha);
    }

    /**
     * Se encarga de guardar los datos leidos en una basa se datos SQLITE
     * @param afNUMERO_DOCUMENTO = "numero_documento";
     * @param afCODIGO_BODEGA = "codigo_bodega";
     * @param afDESCRIPCION = "descripcion";
     * @param afMARCA = "marca";
     * @param afGRUPO = "grupo";
     * @param afCANTIDAD_LEIDA = "cantidad_leida";
     * @param afFECHA = "fecha";
     * */
    public static void guardarLecturasSqlite(String afCODIGO_PRODUCTO, String afNUMERO_DOCUMENTO, String afCODIGO_BODEGA, String afDESCRIPCION, String afMARCA, String afGRUPO, String afCANTIDAD_LEIDA, String afFECHA){

        //Se crea la conexión para guardar los datos
        db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Utilidades.afCODIGO_PRODUCTO,afCODIGO_PRODUCTO );
        values.put(Utilidades.afNUMERO_DOCUMENTO,afNUMERO_DOCUMENTO );
        values.put(Utilidades.afCODIGO_BODEGA,afCODIGO_BODEGA );
        values.put(Utilidades.afDESCRIPCION,afDESCRIPCION);
        values.put(Utilidades.afMARCA,afMARCA);
        values.put(Utilidades.afGRUPO,afGRUPO);
        values.put(Utilidades.afCANTIDAD_LEIDA ,afCANTIDAD_LEIDA );
        values.put(Utilidades.afFECHA,afFECHA);
        db.insert(Utilidades.TABLA_ENCABEZADO_TRASLADOS, null, values);

        //Reiniciando el contador
        NVecesComprobando = 0;
    }


    private void comandosVoz(){

        Intent voice =  new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        voice.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());
        voice.putExtra(RecognizerIntent.EXTRA_PROMPT, "Asistente Innova");
        startActivityForResult(voice, 1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == RESULT_OK && data != null){
            ArrayList<String> arrayList = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

            tnuevo_codigo.setText(arrayList.get(0).toString());

        }

    }



    private void speak( String str ) throws InterruptedException {
        textToSpeech.speak( str, TextToSpeech.QUEUE_FLUSH, null );
        textToSpeech.setSpeechRate( 0.0f );
        textToSpeech.setPitch( 0.0f );


        Thread.sleep(3000);
        comandosVoz();
    }

}
