package innobile.itraceandroid;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.RequestQueue;

import avd.api.core.exceptions.ApiScannerException;
import innobile.itrace.Activity.DetalleinventarioActivity;
import innobile.itrace.R;

public class Scanner extends SampleAppActivity {

    private SampleApplication application = null;
    public int ContarRegistros = 0;
    private RequestQueue queue;

    public SampleAppActivity currentActivity = null;
    public int cont = 0;
    private ToggleButton tbScanEngine = null;
    private TextView MostrarMetodo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_scanner);
        final EditText campo = findViewById(R.id.Ean_Leido);
        Button detalle = findViewById(R.id.btnDetalle);
        MostrarMetodo = findViewById(R.id.MostrarMetodoLectura);

        MostrarMetodo.setText(getIntent().getStringExtra("metodo_lectura"));
        detalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Scanner.this, ImpresionDetalle.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        application = (SampleApplication) getApplication();
        application.requiredDevice = application.ANY_DEVICE;
        application.lastActivityForAnyDevice = Scanner.class;

        this.setupDefaults();
    }

    @Override
    public void onPause() {
        unregisterReceiver(mDeviceSelectedReceiver);

        if (isFinishing()) {
            try {
                application.removeScannerActivity();
            } catch (ApiScannerException e) {
            } // No need to do anything here - we are closing the activity anyway
        }

        super.onPause();
    }

    private final BroadcastReceiver mDeviceSelectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (application.allDevicesSelected())
                tbScanEngine.setEnabled(false);
            else {
            }
        }
    };

    private void setupDefaults() {
//        application.initializeDeviceIndexButtons(this);

        IntentFilter filter = new IntentFilter(SampleApplication.INTENT_ACTION_UPDATE_DEVICE_SELECTION);
        registerReceiver(mDeviceSelectedReceiver, filter);
        sendBroadcast(new Intent(SampleApplication.INTENT_ACTION_UPDATE_DEVICE_SELECTION));


        try {
            application.addScannerActivity();
        } catch (ApiScannerException e) {
            showMessageBox("Adding listener failed",
                    String.format("Scanner listener has not been added. Error code is %d. Scan results processing is inactive.", e.getErrorCode()),
                    "Ok", null, null, null, application.getDevice());
        }
    }
}
