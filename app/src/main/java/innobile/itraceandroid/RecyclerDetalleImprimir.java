package innobile.itraceandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.modelo.ImpresionDetalleModelo;
import innobile.itraceandroid.Bombay.ConsultaProductos;


/**
 * Created by JUNED on 6/16/2016.
 */
public class RecyclerDetalleImprimir extends RecyclerView.Adapter<RecyclerDetalleImprimir.MyViewHolder> {

    private Context mContext;
    private List<ImpresionDetalleModelo> mData;
    RequestOptions option;


    public RecyclerDetalleImprimir(Context mContext, List<ImpresionDetalleModelo> mData) {
        this.mContext = mContext;
        this.mData = mData;
        // Request option for Glide
        option = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.descripcion_row_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, ConsultaProductos.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("anime_name", mData.get(viewHolder.getAdapterPosition()).getCod_Barras());
                i.putExtra("anime_description", mData.get(viewHolder.getAdapterPosition()).getDescripcion());
                mContext.startActivity(i);
            }
        });
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_name.setText(mData.get(position).getCod_Barras());
        holder.tv_rating.setText(mData.get(position).getDescripcion());
        holder.Cantidad.setText(mData.get(position).getCantidad());

        // Load Image from the internet and set it into Imageview using Glide
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;
        TextView tv_rating;
        LinearLayout view_container;
        TextView Cantidad;
        EditText IngresarValor;

        public MyViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            tv_name = itemView.findViewById(R.id.anime_name);
            tv_rating = itemView.findViewById(R.id.rating);
            Cantidad = itemView.findViewById(R.id.cantidad);

        }
    }
}
