package innobile.itraceandroid;



import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import innobile.itrace.R;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.ImpresionDetalleModelo;
import innobile.itrace.transport.T_Configuraciones;


public class ImpresionDetalle extends AppCompatActivity {

    private List<ImpresionDetalleModelo> lstAnime ;
    private RecyclerView recyclerView ;
    private  String dato;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impresion_detalle);
        lstAnime = new ArrayList<>() ;
        recyclerView = findViewById(R.id.recyclerviewid);



        try {
            jsonObjectRequest();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void jsonObjectRequest() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());

        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        JSONObject jsonBody = null;
        jsonBody = new JSONObject();

        jsonBody.put("Cod_Empresa", "01");
        jsonBody.put("Cod_Bodega", "BODGENERAL");

        String JSON_URL =  t_configuraciones.getCon_URL() + "/BuscarImpresiones";

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, JSON_URL, jsonBody ,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject jsonObject  = null ;



                        JSONArray mJsonArray = null;
                        try {
                            mJsonArray = response.getJSONArray("recordset");
                            for(int i=0; i < mJsonArray.length(); i++){
                                JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                ImpresionDetalleModelo anime = new ImpresionDetalleModelo();
                                anime.setCod_Barras(mJsonObject.getString("Cod_Barras"));
                                anime.setDescripcion(mJsonObject.getString("Descripcion"));
                                anime.setCantidad(mJsonObject.getString("Cantidad"));
                                lstAnime.add(anime);
                            }

                            for (int i = 0 ; i < response.length(); i++ ) {


                                try {
                                    jsonObject = response.getJSONObject("recordset");
                                    ImpresionDetalleModelo anime = new  ImpresionDetalleModelo();
                                    anime.setCod_Barras(jsonObject.getString("Cod_Barras"));
                                    anime.setDescripcion(jsonObject.getString("Descripcion"));
                                    anime.setCantidad(jsonObject.getString("Cantidad"));
                                    lstAnime.add(anime);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            setuprecyclerview(lstAnime);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
        );

        queue.add(getRequest);
    }


    private void setuprecyclerview(List<ImpresionDetalleModelo> lstAnime) {

        RecyclerDetalleImprimir myadapter = new RecyclerDetalleImprimir(getApplicationContext(),lstAnime);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myadapter);

    }


}

