package innobile.itraceandroid;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.modelo.BuscarReferenciamodelo;
import innobile.itrace.transport.T_Configuraciones;
import innobile.itraceandroid.SwipeController;


public class Consultar_Recepcion extends AppCompatActivity {

    public DeleteRequisicionData mAdapter;
    private SwipeController swipeController = null;
    private RequestQueue requestQueue;
    private FloatingActionMenu floatingActionButton;
    private RecyclerView recyclerView;
    private String V_URL = null;
    private String V_TipoConexion = null, V_URL_MON = null;
    private String tipo_Movimiento = "";
    private List<GetDataAdapter> mlstRequisicionData = new ArrayList<>();
    private TextView nombreMovimiento;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_requisicion);

        //Almacenando variable para comprobar el tipo de movimiento
        tipo_Movimiento = Global.TIPO_DOCUMENTO;

        //Creando el recyclerView
        recyclerView = findViewById(R.id.recyclerviewid);
        nombreMovimiento = findViewById(R.id.nombreMovimiento);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        floatingActionButton = findViewById(R.id.material_design_android_floating_action_menu);
        assert floatingActionButton != null;

        //Se encarga de crear el esquema solo una vez para guardar los datos en MongoDB
        try {
            Global.CREAR_REQUISICON_MONGO = true;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "El dato para crear la requisiciòn està vacio", Toast.LENGTH_SHORT).show();
        }


        if (requestQueue == null) {
            //Configurando la petición por Volley
            // Instantiate the cache
            Cache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024); // 1MB cap
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }


        //Se muestra un tìtulo diferente segun el tipo de movimientos
        if (tipo_Movimiento.equals("RQ")) {
            nombreMovimiento.setText("REQUISICIONES");
        } else if (tipo_Movimiento.equals("RM")) {
            nombreMovimiento.setText("RECIBO DE MERCANCIA");
        } else {
            Toast.makeText(getApplicationContext(), "El tipo de movimiento no es vàlido", Toast.LENGTH_SHORT).show();
        }


        //Pasando referencia de la lista global
        mAdapter = new DeleteRequisicionData(getApplicationContext(), mlstRequisicionData);


        //Configurando la URL local para realizar las peticiones y concatenar con cada uno de las direcciones en la nube
        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Cargando la nueva actividad usando el FloatingMenu
        floatingActionButton.findViewById(R.id.material_design_floating_action_menu_item1).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //Comprobando el tipo de movimiento para cargar el encabezado correspondiente
                if (tipo_Movimiento.equals("RQ")) {

                    Intent intent = new Intent(Consultar_Recepcion.this, encabezado_Requisiciones.class);
                    startActivity(intent);
                    finish();
                } else if (tipo_Movimiento.equals("RM")) {
                    Intent intent = new Intent(Consultar_Recepcion.this, encabezado_entregaMercancia.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "El tipo de movimiento no es vàlido", Toast.LENGTH_SHORT).show();
                }

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BuscarMovimientos();
            }
        });

        //Llamado el JSON para mostrar las requisiciones
        BuscarMovimientos();

        //Cargando la vista del RecyclerView

        //Creando la vista de RecyclerView
        setupRecyclerView();


    }

    /**
     * Se encarga de traer las requisiciones y los datos al momento de recibir la mercancia
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    private void BuscarMovimientos() {
        try {
            JSONObject jsonBuscarMovimiento = null;
            jsonBuscarMovimiento = new JSONObject();

            jsonBuscarMovimiento.put("Cod_Empresa", "01");
            jsonBuscarMovimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO);
            jsonBuscarMovimiento.put("Estado", "IN");


            // String JSON_URL =  t_configuraciones.getCon_URL() + "/GetReferenciaData";


            final JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarMovimientos", jsonBuscarMovimiento,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            // Esta es la lista global
                            mlstRequisicionData.clear();

                            JSONArray mJsonArray = null;
                            try {
                                mJsonArray = response.getJSONArray("recordset");
                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                    GetDataAdapter requisicionData = new GetDataAdapter();
                                    requisicionData.setCodigo(mJsonObject.getString("Nro_Dcto"));
                                    requisicionData.setFecha(mJsonObject.getString("Fecha"));
                                    mlstRequisicionData.add(requisicionData);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // Notifica los cambios al adaptador
                            mAdapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
            );

            getRequest.setShouldCache(false);
            getRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(getRequest);
        } catch (JSONException e) {
            e.printStackTrace();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Se añaden las funciones a los items del RecyclerView y permite borrar los datos
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    private void setupRecyclerView() {
        try {
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(mAdapter);

            swipeController = new SwipeController(new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {
                    try {
                        BorrarRegistro(mAdapter.players.remove(position).getCodigo());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Contiene la posicición del Item que fue eliminaod
                    mAdapter.notifyItemRemoved(position);
                    mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

                }
            });

            //Se crear el touch para añadir funcionalidad al presionar cada uno de los items
            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
            itemTouchhelper.attachToRecyclerView(recyclerView);

            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                    swipeController.onDraw(c);
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Se encarga de borrar los datos en al API al deslizar el item y presionar el botón ELIMINAR
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    public void BorrarRegistro(final String Codigo_Eliminar) throws JSONException {
        try {

            //Se crea el JSON que contiene el Codigo a eliminar
            JSONObject jsonEliminarMovimiento = null;
            jsonEliminarMovimiento = new JSONObject();
            jsonEliminarMovimiento.put("Cod_Empresa", "01");
            jsonEliminarMovimiento.put("Numero_Documento", Integer.parseInt(Codigo_Eliminar));
            jsonEliminarMovimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/EliminarMovimientos", jsonEliminarMovimiento, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        borrarMovimientoDB(Codigo_Eliminar);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar del borrar la recepcion", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Se encarga de borrar un documento en MongoDB
     * Autor: CarlosDnl
     * Fecha: 25/11/2019
     */

    public void borrarMovimientoDB(String Codigo_Eliminar) throws JSONException {
        try {
            //Se crea el JSON que contiene el Codigo a eliminar
            JSONObject jsonEliminarMovimiento = null;
            jsonEliminarMovimiento = new JSONObject();
            jsonEliminarMovimiento.put("Cod_Empresa", "01");
            jsonEliminarMovimiento.put("Numero_Documento", Integer.parseInt(Codigo_Eliminar));
            jsonEliminarMovimiento.put("Tipo_Documento", Global.TIPO_DOCUMENTO);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL_MON + "/api/Movimiento/eliminarMovimiento", jsonEliminarMovimiento, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar del borrar la recepcion", Toast.LENGTH_SHORT).show();
        }

    }
}