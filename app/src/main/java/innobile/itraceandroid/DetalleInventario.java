package innobile.itraceandroid;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.Cons;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.modelo.ModeloDeleteData;
import innobile.itrace.transport.T_Configuraciones;

public class DetalleInventario extends AppCompatActivity {

    public DeleteDataAdapter mAdapter;
    SwipeController swipeController = null;
    private String Codigo, pDescripcion, Unidad_Medida;
    private List<ModeloDeleteData> detalles = new ArrayList<>();
    private ModeloDeleteData productos = new ModeloDeleteData();
    private String Tipo_Docto;
    private ConexionSQLiteHelper conn;
    private String V_URL = null;
    private String V_TipoConexion = null, V_URL_MON = null;
    private RequestQueue requestQueue;
    private RecyclerView recyclerView;
    private String numero_documento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verdetallerq);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //Guardando el tipo de movimiento que se está ejecutando
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Pasando la estructura de la lista
        mAdapter = new DeleteDataAdapter(detalles);

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_URL_MON = t_configuraciones.getCon_URLMongo();  //Url que se encarga de la conexión desde la nube
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        //Obtengo el numero del documento
        try {
            numero_documento = getIntent().getExtras().getString("numero_documento");
        } catch (Exception e) {

        }

        if (requestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            // Instantiate the RequestQueue with the cache and network.
            requestQueue = new RequestQueue(new NoCache(), network);
            // Start the queue
            requestQueue.start();
        }


        //Comprobando el tipo de movimiento para ejecutar la carga de datos en la lista
        try {
            obtenerDetallesSqlite();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setupRecyclerView();
    }

    public void obtenerDetallesSqlite() throws JSONException {

        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        Cursor c = db.rawQuery("SELECT * FROM " + myTable + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = " + numero_documento + " AND " + Utilidades.vEstado + " = 'IN'  ORDER BY id DESC;", null);
        if (c.moveToFirst()) {
            do {
                ModeloDeleteData detalle = new ModeloDeleteData();
                detalle.setCodigo(c.getString(1));
                detalle.setpDescripcion(c.getString(2));
                detalle.setUnidad_Medida(c.getString(9));
                detalle.setId(c.getString(10));
                detalles.add(detalle);
            } while (c.moveToNext());
        }
        db.close();
        // Notifica los cambios al adaptador
        mAdapter.notifyDataSetChanged();

    }


    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                try {
                    borrarDatosSqlite(mAdapter.productos.remove(position).getId());
                } catch (Exception e) {
                    Toasty.warning(getApplicationContext(), "No se puedo eliminar el registro", Toasty.LENGTH_LONG).show();
                }

                //Toast.makeText(getApplicationContext(),  mAdapter.players.remove(position).getCodigo(), Toast.LENGTH_SHORT).show();
                // mAdapter.players.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());

            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    /**
     * Se encarga de borrar un registro de la base de datos
     */

    public void borrarDatosSqlite(String id) {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        Cursor c = db.rawQuery("DELETE FROM " + myTable + " WHERE " + Utilidades.vID + " = '" + id + "';", null);
        c.moveToFirst();
        db.close();
    }

}



