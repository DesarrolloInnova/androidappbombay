package innobile.itraceandroid.RetrofitAdapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class recordsets {

    @SerializedName("Num_Pedido")
    @Expose
    String Num_Pedido;
    @SerializedName("Nit_Cliente")
    @Expose
    String Nit_Cliente;
    @SerializedName("Cod_Producto")
    @Expose
    String Cod_Producto;
    @SerializedName("Cant_Pedido")
    @Expose
    String Cant_Pedido;
    @SerializedName("Bodega")
    @Expose
    String Bodega;
    @SerializedName("Prod_descripcion")
    @Expose
    String Prod_descripcion;
    @SerializedName("Fecha_Pedido")
    @Expose
    String Fecha_Pedido;
    @SerializedName("Und_Medida")
    @Expose
    String Und_Medida;

    public recordsets(String num_Pedido, String nit_Cliente, String cod_Producto, String cant_Pedido, String bodega, String prod_descripcion, String fecha_Pedido, String und_Medida) {
        Num_Pedido = num_Pedido;
        Nit_Cliente = nit_Cliente;
        Cod_Producto = cod_Producto;
        Cant_Pedido = cant_Pedido;
        Bodega = bodega;
        Prod_descripcion = prod_descripcion;
        Fecha_Pedido = fecha_Pedido;
        Und_Medida = und_Medida;
    }

    public recordsets() {
    }

    public String getNum_Pedido() {
        return Num_Pedido;
    }

    public void setNum_Pedido(String num_Pedido) {
        Num_Pedido = num_Pedido;
    }

    public String getNit_Cliente() {
        return Nit_Cliente;
    }

    public void setNit_Cliente(String nit_Cliente) {
        Nit_Cliente = nit_Cliente;
    }

    public String getCod_Producto() {
        return Cod_Producto;
    }

    public void setCod_Producto(String cod_Producto) {
        Cod_Producto = cod_Producto;
    }

    public String getCant_Pedido() {
        return Cant_Pedido;
    }

    public void setCant_Pedido(String cant_Pedido) {
        Cant_Pedido = cant_Pedido;
    }

    public String getBodega() {
        return Bodega;
    }

    public void setBodega(String bodega) {
        Bodega = bodega;
    }

    public String getProd_descripcion() {
        return Prod_descripcion;
    }

    public void setProd_descripcion(String prod_descripcion) {
        Prod_descripcion = prod_descripcion;
    }

    public String getFecha_Pedido() {
        return Fecha_Pedido;
    }

    public void setFecha_Pedido(String fecha_Pedido) {
        Fecha_Pedido = fecha_Pedido;
    }

    public String getUnd_Medida() {
        return Und_Medida;
    }

    public void setUnd_Medida(String und_Medida) {
        Und_Medida = und_Medida;
    }
}
