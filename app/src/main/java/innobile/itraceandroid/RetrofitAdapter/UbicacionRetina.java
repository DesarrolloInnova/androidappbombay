package innobile.itraceandroid.RetrofitAdapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UbicacionRetina {
    @SerializedName("UBICACION")
    @Expose
    String UBICACION;

    public UbicacionRetina(String UBICACION) {
        this.UBICACION = UBICACION;
    }

    public String getUBICACION() {
        return UBICACION;
    }

    public void setUBICACION(String UBICACION) {
        this.UBICACION = UBICACION;
    }
}
