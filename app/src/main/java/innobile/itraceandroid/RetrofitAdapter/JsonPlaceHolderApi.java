package innobile.itraceandroid.RetrofitAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface JsonPlaceHolderApi {
    @Headers("Content-Type:application/json")
    @GET("/api/ActivosFijos/obtenerDetalle")
    Call<List<Maquinas>> getMaquinas();

    @FormUrlEncoded
    @POST("/api/ActivosFijos/listarXBodega")
    Call<List<activos_bodegas>> getActivos_Bodegas(@Field("id_Bodega") String first);

    @Headers("Content-Type:application/json")
    @GET("/api/bodega/bodegas")
    Call<List<bodegas_id>> getBodegas();


    @FormUrlEncoded
    @POST("/tablaConteos")
    Call<insertar_datos> postConteoData(@Field("numero_Documento") String numero_documento,
                                        @Field("codigo_Barras") String codigo_Barras,
                                        @Field("codigo_sku") String codigo_sku,
                                        @Field("fecha_Lectura") String fecha_Lectura,
                                        @Field("cantidad") String cantidad,
                                        @Field("tipo_Lectura") String tipo_Lectura,
                                        @Field("bodega") String bodega,
                                        @Field("codigo_Bodega") String codigo_Bodega);


    @FormUrlEncoded
    @POST("/tablaPedidos")
    Call<List<recordsets>> postPedidos(@Field("numero_Pedido") String numero_Pedido);


    @FormUrlEncoded
    @POST("guardar")
    Call<Cedulas> postCedulas(@Field("Numero_Cedula") String Numero_Cedula,
                                    @Field("Nombre_Completo") String Nombre_Completo,
                                    @Field("Fecha_Nacimiento") String Fecha_Nacimiento,
                                    @Field("Fecha_Entrada") String Fecha_Entrada,
                                    @Field("Correo_Electronico") String Correo_Electronicoa,
                                    @Field("Celular") String Celular,
                                    @Field("Responsable") String Responsable
                                    );

    @Headers("Content-Type:application/json")
    @GET("api/tablaubicaciones")
    Call<List<UbicacionRetina>> getUbicaciones();


    @FormUrlEncoded
    @POST("tablaexistencias")
    Call<List<Producto_Existencia>> postBuscarProducto(@Field("Ubicacion") String Ubicacion,
                              @Field("Valor") String Valor
    );

    @FormUrlEncoded
    @POST("activosfijos/restarCantidad")
    Call<restar_cantidad> postRestarCantidad(@Field("_id") String _id,
                                                       @Field("cantidad") String cantidad
    );


    @Headers("Content-Type:application/json")
    @GET("api/equiposten/mostrardatos")
    Call<List<EquipoSten>> getEquipoEten();

    @PUT("api/equiposten/actualizarBodega")
    @FormUrlEncoded
    Call<SuccessResponse> updateBodegas(@Field("Producto") String Producto,
                          @Field("Bodega") String Bodega,
                          @Field("Cantidad") String Cantidad);

}
