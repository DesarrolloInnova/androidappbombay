package innobile.itraceandroid.RetrofitAdapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class productos {
    String Codigo;
    String Descripcion;

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
