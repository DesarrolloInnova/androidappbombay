package innobile.itraceandroid.RetrofitAdapter;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Producto_Existencia {

    @SerializedName("UBICACION")
    @Expose
    private String uBICACION;
    @SerializedName("BODEGA")
    @Expose
    private String bODEGA;
    @SerializedName("REFERENCIA")
    @Expose
    private String rEFERENCIA;
    @SerializedName("EXTENSION")
    @Expose
    private String eXTENSION;
    @SerializedName("SERIAL")
    @Expose
    private String sERIAL;
    @SerializedName("VENCIMIENTO")
    @Expose
    private String vENCIMIENTO;
    @SerializedName("CANTIDAD")
    @Expose
    private Integer cANTIDAD;
    @SerializedName("LOTE")
    @Expose
    private String lOTE;

    public String getUBICACION() {
        return uBICACION;
    }

    public void setUBICACION(String uBICACION) {
        this.uBICACION = uBICACION;
    }

    public String getBODEGA() {
        return bODEGA;
    }

    public void setBODEGA(String bODEGA) {
        this.bODEGA = bODEGA;
    }

    public String getREFERENCIA() {
        return rEFERENCIA;
    }

    public void setREFERENCIA(String rEFERENCIA) {
        this.rEFERENCIA = rEFERENCIA;
    }

    public String getEXTENSION() {
        return eXTENSION;
    }

    public void setEXTENSION(String eXTENSION) {
        this.eXTENSION = eXTENSION;
    }

    public String getSERIAL() {
        return sERIAL;
    }

    public void setSERIAL(String sERIAL) {
        this.sERIAL = sERIAL;
    }

    public String getVENCIMIENTO() {
        return vENCIMIENTO;
    }

    public void setVENCIMIENTO(String vENCIMIENTO) {
        this.vENCIMIENTO = vENCIMIENTO;
    }

    public Integer getCANTIDAD() {
        return cANTIDAD;
    }

    public void setCANTIDAD(Integer cANTIDAD) {
        this.cANTIDAD = cANTIDAD;
    }

    public String getLOTE() {
        return lOTE;
    }

    public void setLOTE(String lOTE) {
        this.lOTE = lOTE;
    }
}

