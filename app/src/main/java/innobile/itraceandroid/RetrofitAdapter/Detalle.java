package innobile.itraceandroid.RetrofitAdapter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detalle {

    @SerializedName("Cantidad")
    @Expose
    private Integer cantidad;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("Bodega")
    @Expose
    private Bodega bodega;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Bodega getBodega() {
        return bodega;
    }

    public void setBodega(Bodega bodega) {
        this.bodega = bodega;
    }

}