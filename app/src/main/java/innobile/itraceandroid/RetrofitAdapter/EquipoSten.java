package innobile.itraceandroid.RetrofitAdapter;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EquipoSten {

    @SerializedName("empresas")
    @Expose
    private String empresas;
    @SerializedName("Reservado")
    @Expose
    private Boolean reservado;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("Codigo")
    @Expose
    private String codigo;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("Marca")
    @Expose
    private String marca;
    @SerializedName("Grupo")
    @Expose
    private String grupo;
    @SerializedName("Detalles")
    @Expose
    private List<Detalle> detalles = null;
    @SerializedName("Fecha_Creacion")
    @Expose
    private String fechaCreacion;

    public String getEmpresas() {
        return empresas;
    }

    public void setEmpresas(String empresas) {
        this.empresas = empresas;
    }

    public Boolean getReservado() {
        return reservado;
    }

    public void setReservado(Boolean reservado) {
        this.reservado = reservado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public List<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Detalle> detalles) {
        this.detalles = detalles;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

}