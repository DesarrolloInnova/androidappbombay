package innobile.itraceandroid.MetodosGlobales

import android.content.Context
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonArrayRequest
import innobile.itrace.R
import innobile.itraceandroid.IngresarProducto
import innobile.itraceandroid.InventarioActivosFijos
import innobile.itraceandroid.TrasladosTM.DatosProductos

class bodega(){


    fun codigoBodega(context: Context,  V_URL_MON : String, bodega_Origen: String, actividad_recibida : String) {
        var bodega_origen: TextView? = null
        // Instantiate the cache
        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        // Instantiate the RequestQueue with the cache and network. Start the queue.
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        //Se codifica la URL para no recibir espacios en blanco
        val bEncode = java.net.URLEncoder.encode(bodega_Origen, "utf-8")
        val url = "$V_URL_MON/api/bodega/list?valor=$bEncode"

        // Request a string response from the provided URL.
        val req = JsonArrayRequest(Request.Method.GET, url,null,
                Response.Listener { response ->
                    val obj = response.getJSONObject(0)
                    DatosProductos.ID_ORIGEN = obj.getString("_id")
                    DatosProductos.pNOMBRE_BODEGA = obj.getString("Descripcion")
                    if(actividad_recibida == context.getString(R.string.InventarioActivosFijos)){
                        InventarioActivosFijos.cargarDatos()
                    }
                    else if(actividad_recibida == context.getString(R.string.ingresar_producto)){
                        IngresarProducto.guardarDatos(context, V_URL_MON)
                    }
                }, Response.ErrorListener {error ->
                     Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()

                }
        )
        requestQueue.add(req)
    }


    fun datosProducto(context: Context,  V_URL_MON : String, codigoProducto : String, actividad_recibida: String) {
        var bodega_origen: TextView? = null
        // Instantiate the cache
        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        // Instantiate the RequestQueue with the cache and network. Start the queue.
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        //Se codifica la URL para no recibir espacios en blanco

        val url = "$V_URL_MON/api/ActivosFijos/list?valor=$codigoProducto"

        // Request a string response from the provided URL.
        val req = JsonArrayRequest(Request.Method.GET, url,null,
                Response.Listener { response ->
                    val obj = response.getJSONObject(0)
                    DatosProductos.acDESCRIPCION = obj.getString("Descripcion")

                    if (obj.has("Marca")) {
                        DatosProductos.acMARCA = obj.getString("Marca")
                    }else{
                        DatosProductos.acMARCA = "Sin marca"
                    }
                    DatosProductos.acGRUPO = obj.getString("Grupo")

                    if(actividad_recibida == context.getString(R.string.InventarioActivosFijos)){
                        InventarioActivosFijos.cargarDatos()
                    }
                }, Response.ErrorListener {error ->
            Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()

        }
        )
        requestQueue.add(req)
    }
}






