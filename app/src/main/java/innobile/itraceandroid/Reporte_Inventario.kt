package innobile.itraceandroid

import android.app.AlertDialog
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.view.View
import android.widget.*
import es.dmoral.toasty.Toasty
import innobile.Adapter.AdapterReporte
import innobile.itrace.R
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades


class Reporte_Inventario : AppCompatActivity() {

    var lista: ListView? = null
    var context: Context? = null
    private var arrayListDescripcion: ArrayList<String> = ArrayList()
    private var arrayListSku: ArrayList<String> = ArrayList()
    private var arrayListCodigoBarras: ArrayList<String> = ArrayList()
    private var arrayListCantidadInventario: ArrayList<String> = ArrayList()
    private var ArrayListaCantidadMaestro: ArrayList<String> = ArrayList()
    private var ArrayListDiferencia: ArrayList<String> = ArrayList()
    var arrayListBodega: ArrayList<String> = ArrayList()
    var arrayListUbicacion: ArrayList<String> = ArrayList()
    private var conn: ConexionSQLiteHelper? = null
    private lateinit var lstBodegas: Array<String?>
    private lateinit var lstDocumento: Array<String?>
    var codigo = ""
    var descripcion = ""
    var cantidad = ""
    var codigo_barras = ""
    var bodega = ""
    var ubicacion = ""
    var numero_documento = ""
    var nombre_bodega = ""
    private var autoCompleteBodegas: AutoCompleteTextView? = null
    private var autoCompleteDocumento: AutoCompleteTextView? = null
    val cursor = ""
    private var datos: ArrayList<String> = ArrayList()
    private var documentos: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reporte__inventario)

        if (Global.COMPROBAR_REPORTE) {
            Global.COMPROBAR_REPORTE = false
            obtenerBodegaCodigo()
        } else {
            context = this
            lista = findViewById<ListView>(R.id.lvListado)
            val tnumeroInventario: TextView = findViewById<TextView>(R.id.numero_inventario)
            val tNombreBodega: TextView = findViewById<TextView>(R.id.nombre_bodega)

            numero_documento = intent.extras.getString("numero_documento")
            nombre_bodega = intent.extras.getString("nombre_Bodega")

            tnumeroInventario.text = numero_documento
            tNombreBodega.text = nombre_bodega
            leerTablaInventario()
        }


    }

    fun leerTablaInventario() {
        try {
            conn = ConexionSQLiteHelper(applicationContext, Utilidades.vTABLA_INVENTARIO, null, 1)
            val db: SQLiteDatabase = conn!!.readableDatabase
            val myTable = Utilidades.vTABLA_INVENTARIO //Set name of your table
            val c = db.rawQuery("SELECT SUM(" + Utilidades.vCANTIDAD + "), " + Utilidades.vCODIGO_SKU_LEIDO + "," + Utilidades.vDESCRIPCION + "," + Utilidades.vCODIGO_BARRAS + "," + Utilidades.vNOMBRE_BODEGA + "," + Utilidades.vESTANTE_INVENTARIO + " FROM " + myTable + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = " + numero_documento + " AND " + Utilidades.vNOMBRE_BODEGA + " = '" + nombre_bodega + "' GROUP BY " + Utilidades.vCODIGO_BARRAS, null)
            if (c.moveToFirst()) {
                do {
                    cantidad = c.getString(0).trim()
                    codigo = c.getString(1).trim()
                    descripcion = c.getString(2).trim()
                    codigo_barras = c.getString(3).trim()
                    bodega = c.getString(4).trim()
                    ubicacion = c.getString(5).trim()
                    compararTablaMaestra(codigo, cantidad, descripcion, codigo_barras, bodega, ubicacion);
                } while (c.moveToNext())
            }
            db.close()

            lista!!.adapter = AdapterReporte(this, arrayListDescripcion, arrayListSku, arrayListCodigoBarras, arrayListCantidadInventario, ArrayListaCantidadMaestro, ArrayListDiferencia, arrayListBodega, arrayListUbicacion)
        } catch (e: SQLiteException) {
            Toasty.error(applicationContext, e.toString(), Toasty.LENGTH_SHORT).show()
        }
    }

    fun compararTablaMaestra(SKU: String, cantidad: String, descripcion: String, codigo_barras: String, bodega: String, ubicacion: String) {
        try {
            conn = ConexionSQLiteHelper(applicationContext, Utilidades.TABLA_MAESTRO, null, 1)
            val db: SQLiteDatabase = conn!!.readableDatabase
            val myTable = Utilidades.TABLA_MAESTRO //Set name of your table


            val c = db.rawQuery("SELECT " + Utilidades.maestroCantidadInventario + " FROM " + myTable + " WHERE " + Utilidades.maestroSkuInventario + " = '" + SKU + "';", null)
            if (c.moveToFirst()) {
                do {

                    arrayListDescripcion.add(descripcion)
                    arrayListSku.add(SKU)
                    arrayListCodigoBarras.add(codigo_barras)
                    arrayListCantidadInventario.add(cantidad)
                    ArrayListaCantidadMaestro.add(c.getString(0))
                    ArrayListDiferencia.add(diferencia(cantidad, c.getString(0)).toString())
                    arrayListBodega.add(bodega)
                    arrayListUbicacion.add(ubicacion)

                } while (c.moveToNext())
            } else {
                arrayListDescripcion.add(descripcion)
                arrayListSku.add(SKU)
                arrayListCodigoBarras.add(codigo_barras)
                arrayListCantidadInventario.add(cantidad)
                ArrayListaCantidadMaestro.add("Sin dato")
                ArrayListDiferencia.add("Sin dato")
                arrayListBodega.add(bodega)
                arrayListUbicacion.add(ubicacion)
            }
            db.close()
        } catch (e: RuntimeException) {

        }
    }

    fun diferencia(cantidadinventario: String, cantidadMaestro: String): Int {
        var diferencia = 0;
        if (Integer.parseInt(cantidadinventario) > Integer.parseInt(cantidadMaestro)) {
            diferencia = Integer.parseInt(cantidadinventario) - Integer.parseInt(cantidadMaestro)
        } else if (Integer.parseInt(cantidadinventario) < Integer.parseInt(cantidadMaestro)) {
            diferencia = Integer.parseInt(cantidadMaestro) - Integer.parseInt(cantidadinventario)
        } else if (Integer.parseInt(cantidadinventario) == Integer.parseInt(cantidadMaestro)) {
            diferencia = 0;
        }

        return diferencia
    }


    fun obtenerBodegaCodigo() {
        val mBuilder = AlertDialog.Builder(this@Reporte_Inventario)

        val mView = layoutInflater.inflate(R.layout.datos_reportes, null)
        autoCompleteBodegas = mView.findViewById(R.id.autoCompleteBodega)
        autoCompleteDocumento = mView.findViewById(R.id.autoCompleteNumero)
        val documentos = mView.findViewById<ImageView>(R.id.ImageViewDocumento)
        val buscarInventario = mView.findViewById<Button>(R.id.btnBuscarInventario)
        val generarReporte = mView.findViewById<Button>(R.id.btnGenerarInventario)
        val mSalir = mView.findViewById<View>(R.id.btnSalir) as Button


        cargarSqliteBodegas()

        val adapterBodegas: ArrayAdapter<String> = ArrayAdapter<String>(this@Reporte_Inventario, android.R.layout.simple_spinner_dropdown_item, lstBodegas)
        autoCompleteBodegas!!.setAdapter(adapterBodegas)

        buscarInventario.setOnClickListener {
            if (autoCompleteBodegas!!.text.trim().isNotEmpty()) {

                cargarSqliteDocumentos(autoCompleteBodegas!!.text)
                val adapterDocumento = ArrayAdapter<String>(this@Reporte_Inventario, android.R.layout.simple_spinner_dropdown_item, lstDocumento)
                autoCompleteDocumento!!.setAdapter(adapterDocumento)

                documentos.setOnClickListener { autoCompleteDocumento!!.showDropDown() }

            } else {
                Toasty.warning(applicationContext, "Es necesario ingresar el nombre de una bodega", Toasty.LENGTH_LONG).show()
            }

        }





        mBuilder.setView(mView)
        val dialog = mBuilder.create()
        dialog.show()
        mSalir.setOnClickListener {
            dialog.cancel()
        }

        generarReporte.setOnClickListener {
            if (autoCompleteBodegas!!.text.trim().isNotEmpty() && autoCompleteDocumento!!.text.trim().isNotEmpty()) {
                dialog.cancel()
                context = this
                lista = findViewById<ListView>(R.id.lvListado)
                val tnumeroInventario: TextView = findViewById<TextView>(R.id.numero_inventario)
                val tNombreBodega: TextView = findViewById<TextView>(R.id.nombre_bodega)


                numero_documento = autoCompleteDocumento!!.text.toString()
                nombre_bodega = autoCompleteBodegas!!.text.toString()

                tnumeroInventario.text = numero_documento
                tNombreBodega.text = nombre_bodega
                leerTablaInventario()
            } else {
                Toasty.warning(applicationContext, "Es necesario ingresar todos los campos", Toasty.LENGTH_SHORT).show()
            }
        }
    }

    private fun cargarSqliteBodegas(): Array<String?> {
        conn = ConexionSQLiteHelper(applicationContext, Utilidades.TABLA_VBODEGAS, null, 1)
        val db = conn!!.readableDatabase
        val myTable = Utilidades.TABLA_VBODEGAS //Set name of your table
        val cursor = db.rawQuery("SELECT * FROM  $myTable", null)

        if (cursor.moveToFirst()) {
            do {
                val nombre_Bodega = cursor.getString(1)
                datos.add(nombre_Bodega)
            } while (cursor.moveToNext())
        }

        db.close()

        lstBodegas = arrayOfNulls<String>(datos.size)
        for (j in datos.indices) {
            lstBodegas[j] = datos[j]
        }
        return lstBodegas
    }

    private fun cargarSqliteDocumentos(nombre_Bodega: Editable): Array<String?>? {
        try {
            documentos.clear()
            conn = ConexionSQLiteHelper(applicationContext, Utilidades.vTABLA_INVENTARIO, null, 1)
            val db = conn!!.readableDatabase
            val myTable = Utilidades.vTABLA_INVENTARIO //Set name of your table
            val c = db.rawQuery("SELECT " + Utilidades.vNUMERO_DOCUMENTO + " FROM  " + myTable + " WHERE " + Utilidades.vNOMBRE_BODEGA + " = '" + nombre_Bodega + "' GROUP BY " + Utilidades.vNUMERO_DOCUMENTO + ";", null)
            if (c.moveToFirst()) {
                do {
                    val documento = c.getString(0)
                    documentos.add(documento)
                } while (c.moveToNext())
            } else {
                Toasty.info(applicationContext, "No se encontraron documentos en esa bodega", Toasty.LENGTH_SHORT).show()
            }

            lstDocumento = arrayOfNulls(documentos.size)
            for (j in documentos.indices) {
                lstDocumento[j] = documentos[j]
            }

        } catch (e: SQLiteException) {
            Toasty.error(applicationContext, "Error al tratar de cargar los documentos", Toasty.LENGTH_SHORT).show()
        }
        return lstDocumento
    }
}



