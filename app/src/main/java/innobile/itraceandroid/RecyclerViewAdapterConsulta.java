package innobile.itraceandroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.modelo.BuscarReferenciamodelo;
import innobile.itraceandroid.Bombay.ConsultaProductos;


/**
 * Created by JUNED on 6/16/2016.
 */
public class RecyclerViewAdapterConsulta extends RecyclerView.Adapter<RecyclerViewAdapterConsulta.MyViewHolder> {

    private Context mContext;
    private List<BuscarReferenciamodelo> mData;
    RequestOptions option;


    public RecyclerViewAdapterConsulta(Context mContext, List<BuscarReferenciamodelo> mData) {
        this.mContext = mContext;
        this.mData = mData;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.descripcion_row_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(mContext, ConsultaProductos.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("codigo_buscar", mData.get(viewHolder.getAdapterPosition()).getCodigo());
                    GlobalDatoDetalle.comprobarTraerDetalle = "1";
                    ((Activity) mContext).startActivityForResult(i, 1);
                } catch (Error e) {
                    Toast.makeText(mContext, "Ocurrio un error", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tCodigo.setText(mData.get(position).getCodigo());
        holder.tDescripcion.setText(mData.get(position).getDescripcion());

        // Load Image from the internet and set it into Imageview using Glide
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tCodigo;
        TextView tDescripcion;
        LinearLayout view_container;

        public MyViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            tCodigo = itemView.findViewById(R.id.tCodigo);
            tDescripcion = itemView.findViewById(R.id.tDescripcion);
        }
    }


}
