package innobile.itraceandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import innobile.itrace.R;
import innobile.itrace.view.MenuActivity;
import innobile.itraceandroid.Bombay.ConsultaProductos;

public class DefinirRuta extends AppCompatActivity {

    //Menu principal
    private Button btnMenuPrincipal, btnConsultarProducto, btnRecepcion, btnMenuRecepcion, btnRequisiciones, btnRecibirMercancia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definir_ruta);

        //Cargando los elementos
        btnMenuPrincipal = findViewById(R.id.btnMenuPrincipal);
        btnConsultarProducto = findViewById(R.id.btnConsultarProducto);
        btnRecepcion = findViewById(R.id.btnRecepcion);
        btnMenuRecepcion = findViewById(R.id.btnMenuRecepcion);
        btnRequisiciones = findViewById(R.id.btnRequisiciones);
        btnRecibirMercancia = findViewById(R.id.btnRecibirMercancia);


        //Configurando las rutas


        //Ruta al menù principal
        btnMenuPrincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //Ruta a consultar productos
        btnConsultarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, ConsultaProductos.class);
                GlobalDatoDetalle.comprobarTraerDetalle = "0";
                startActivity(intent);
                finish();
            }
        });

        //Ruta para la recepcion
        btnRecepcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, MenuRecepcion.class);
                startActivity(intent);
                finish();
            }
        });

        btnMenuRecepcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, MenuRecepcion.class);
                startActivity(intent);
                finish();
            }
        });


        btnRequisiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, Consultar_Recepcion.class);
                //Envia 1 si el tipo de movimiento es RQ
                Global.TIPO_DOCUMENTO = "RQ";
                startActivity(intent);
                finish();
            }
        });


        btnRecibirMercancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DefinirRuta.this, Consultar_Recepcion.class);
                //Envia 1 si el tipo de movimiento es RC
                Global.TIPO_DOCUMENTO = "RM";
                startActivity(intent);
                finish();
            }
        });

        /*

 ibtnrequision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuRecepcion.this, ConsultarRequisicion.class);
                //Envia 1 si el tipo de movimiento es RQ
                Global.TIPO_DOCUMENTO = "RQ";
                startActivity(intent);
            }
        });

        ibtnRecibirMercancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuRecepcion.this, ConsultarRequisicion.class);
                //Envia 1 si el tipo de movimiento es RC
                Global.TIPO_DOCUMENTO = "RM";
                startActivity(intent);
            }
        });        *


        **/


    }
}
