package innobile.itraceandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import innobile.itrace.R;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.Cons;
import innobile.itrace.modelo.BuscarReferenciamodelo;
import innobile.itrace.transport.T_Configuraciones;


public class ConsultarPorReferencia extends AppCompatActivity {

    private List<BuscarReferenciamodelo> lstProductos;
    private RecyclerView recyclerView;
    private String dato, comprobarExistencia = "";
    private int MY_TIMEOUT = 2000;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_por_referencia);
        dato = getIntent().getExtras().getString("data_buscar");
        lstProductos = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerviewid);

        try {
            if (requestQueue == null) {
                //Configurando la petición por Volley
                // Instantiate the cache
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
                // Set up the network to use HttpURLConnection as the HTTP client.
                Network network = new BasicNetwork(new HurlStack());
                // Instantiate the RequestQueue with the cache and network.
                requestQueue = new RequestQueue(cache, network);
                // Start the queue
                requestQueue.start();
            }
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de usar Volley", Toast.LENGTH_SHORT).show();
        }


        try {
            if (dato.length() != 0) {
                try {
                    jsonObjectRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getApplicationContext(), "No se ingreso ningun dato para buscar", Toast.LENGTH_SHORT).show();
            }
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de cargar los datos", Toast.LENGTH_SHORT).show();
        }


    }

    private void jsonObjectRequest() throws JSONException {
        try {
            T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
            T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

            JSONObject jsonBody = null;
            jsonBody = new JSONObject();
            jsonBody.put("Valor", dato);
            String JSON_URL = t_configuraciones.getCon_URL() + "/BuscarProductoDescripcion";


            //Definiendo la politica


            final JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, JSON_URL, jsonBody,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject = null;


                            JSONArray mJsonArray = null;
                            try {
                                mJsonArray = response.getJSONArray("recordset");
                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                                    BuscarReferenciamodelo producto = new BuscarReferenciamodelo();
                                    producto.setCodigo(mJsonObject.getString("CODIGO"));
                                    comprobarExistencia = mJsonObject.getString("DESCRIPCION");
                                    producto.setDescripcion(comprobarExistencia);
                                    lstProductos.add(producto);
                                }

                                setuprecyclerview(lstProductos);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "No se encontró ningún registro", Toast.LENGTH_SHORT).show();
                        }
                    }
            );


            getRequest.setShouldCache(false);
            getRequest.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT * 5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(getRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de traer los datos", Toast.LENGTH_SHORT).show();
        }
    }


    private void setuprecyclerview(List<BuscarReferenciamodelo> lstAnime) {
        try {
            RecyclerViewAdapterConsulta myadapter = new RecyclerViewAdapterConsulta(getApplicationContext(), lstAnime);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(myadapter);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Se generó un error al tratar de mostrar los datos", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            finish();
        }
    }
}

