package innobile.itraceandroid;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import innobile.itrace.R;
import innobile.itrace.modelo.ModeloDeleteData;


class DeleteDataAdapter extends RecyclerView.Adapter<DeleteDataAdapter.PlayerViewHolder> {
    public List<ModeloDeleteData> productos;


    public class PlayerViewHolder extends RecyclerView.ViewHolder {
        private TextView codigoBarras, SKU, Descripcion, id;

        public PlayerViewHolder(View view) {
            super(view);
            codigoBarras = (TextView) view.findViewById(R.id.cBarras);
            SKU = (TextView) view.findViewById(R.id.sku);
            Descripcion = (TextView) view.findViewById(R.id.descripcion);
            id = (TextView) view.findViewById(R.id.id);
        }
    }

    public DeleteDataAdapter(List<ModeloDeleteData> productos) {
        this.productos = productos;
    }

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delete_row, parent, false);

        return new PlayerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        ModeloDeleteData productos = this.productos.get(position);
        holder.codigoBarras.setText(productos.getCodigo());
        holder.SKU.setText(productos.getpDescripcion());
        holder.Descripcion.setText(productos.getUnidad_Medida());
        holder.id.setText(productos.getId());
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}


