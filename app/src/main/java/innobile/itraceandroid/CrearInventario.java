package innobile.itraceandroid;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itraceandroid.Fajate.configuracion_inventario;

public class CrearInventario extends AppCompatActivity {

    private TextView tCodigoDocumento, tConteo, tSKU, tDescripcion, tCantidad_Leidos;
    private Button btnDetalle, btnTerminarInventario, btnReportes;
    private EditText codigo_Barras, estanteUbicacion;
    private int comprobarIngreso = 0;
    private ConexionSQLiteHelper conn;
    private String CODIGO_SKU = "", DESCRIPCION;
    private ImageView ibtnBuscar;
    private String numero_Documento, fecha_Lectura, nombre_Bodega, codigo_Bodega, tipo_Lectura, sqCodigo_Barras, sqSKU;
    private int sqCantidad;
    private Long cn;
    private String descripcion_Producto;
    private int cantidad = 0;
    private Button btnContinuar;
    private AutoCompleteTextView autoCompleConteo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_inventario);
        tCodigoDocumento = findViewById(R.id.tCodigo_Documento);
        tConteo = findViewById(R.id.tConteo);
        tSKU = findViewById(R.id.txtSKU);
        tCantidad_Leidos = findViewById(R.id.tTotalRegistros);
        tDescripcion = findViewById(R.id.tDescripcion);
        btnDetalle = findViewById(R.id.btnDetalle);
        ibtnBuscar = findViewById(R.id.ibtnBuscar);
        btnTerminarInventario = findViewById(R.id.btnTerminarInventario);
        btnReportes = findViewById(R.id.btnReportes);
        codigo_Barras = findViewById(R.id.codigo_barras);
        estanteUbicacion = findViewById(R.id.estante_ubicacion);

        //Evita error al tratar de obtener dato null
        estanteUbicacion.setText("");

        //Datos SQlite encabezado
        numero_Documento = getIntent().getExtras().getString("tCodigoDocumento");
        tipo_Lectura = getIntent().getExtras().getString("tConteo");
        fecha_Lectura = getIntent().getExtras().getString("Fecha_Lectura");
        nombre_Bodega = getIntent().getExtras().getString("Nombre_Bodega");
        codigo_Bodega = codigoBodega(nombre_Bodega);

        //Si el inventario ya está creado permite modificar el conteo
        if (Global.CAMBIAR_CONTEO) {
            cambiarConteo();
            Global.CAMBIAR_CONTEO = false;

        }

        try {
            //Cargando el còdigo del documento y el tipo de conteo
            tCantidad_Leidos.setText(tipo_Lectura);
            tCodigoDocumento.setText(numero_Documento);
            tConteo.setText(String.valueOf(cantidaRegistroInventario()));
        } catch (Exception ex) {
            Toasty.error(getApplicationContext(), "Uno de los campos no tiene datos.", Toast.LENGTH_SHORT, true).show();
            ex.printStackTrace();
        } catch (Error e) {
            Toasty.error(getApplicationContext(), "Error dentro de la aplicacion", Toast.LENGTH_SHORT, true).show();
        }


        //Boton que carga la activity del detalle
        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CrearInventario.this, DetalleInventario.class);
                intent.putExtra("numero_documento", numero_Documento);
                startActivity(intent);
            }
        });

        btnReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CrearInventario.this, Reporte_Inventario.class);
                intent.putExtra("numero_documento", numero_Documento);
                intent.putExtra("nombre_Bodega", nombre_Bodega);
                startActivity(intent);
            }
        });


        btnTerminarInventario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                terminarInventario();
            }
        });


        //Se encarga de buscar un código al presionar la lupa
        ibtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!codigo_Barras.getText().toString().trim().isEmpty()) {
                    consultarProductoSqlite(codigo_Barras.getText().toString().trim());
                }
            }
        });


        //Se detectan los datos ingresados en el EditText automáticamente
        codigo_Barras.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                //La terminal debe ser configurada con TAB o con ENTER para poder ejecutar este procedimiento
                if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    //Este contador se encarga de enviar los datos solo una vez
                    comprobarIngreso++;
                    if (comprobarIngreso == 2) {
                        if (!codigo_Barras.getText().toString().trim().isEmpty()) {
                            consultarProductoSqlite(codigo_Barras.getText().toString().trim());
                        }
                    }
                    return true;
                }
                return false;
            }
        });

    }

    private void cambiarConteo() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(CrearInventario.this);
        View mView = getLayoutInflater().inflate(R.layout.cambiar_conteo, null);
        btnContinuar = mView.findViewById(R.id.btnContinuar);
        autoCompleConteo = mView.findViewById(R.id.autoCompleConteo);

        //Se carga la vista dentro de la actividad actual
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!autoCompleConteo.getText().toString().trim().isEmpty()) {
                    tipo_Lectura = autoCompleConteo.getText().toString().trim();
                    tCantidad_Leidos.setText(tipo_Lectura);
                    dialog.cancel();
                } else {
                    Toasty.warning(getApplicationContext(), "Es necesario ingresar los campo correctamente", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    /**
     * Se encarga de buscar el código ingresado en Sqlite
     * Autor:CarlosDnl
     * Fecha: 26-02-2020
     */

    private void consultarProductoSqlite(String codigo) {

        try {
            if (!estanteUbicacion.getText().toString().trim().isEmpty()) {
                conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VPRODUCTOS, null, 1);
                SQLiteDatabase db = conn.getReadableDatabase();
                String myTable = Utilidades.TABLA_VPRODUCTOS;//Set name of your table
                Cursor c = db.rawQuery("SELECT " + Utilidades.CODIGO_SKU + " , " + Utilidades.VDESCRIPCION_PRODUCTO + " FROM " + myTable + " WHERE " + Utilidades.VCODIGO_BARRAS + " = '" + codigo + "';", null);
                if (c.moveToFirst()) {
                    tConteo.setText(String.valueOf(cantidaRegistroInventario()));
                    do {
                        CODIGO_SKU = c.getString(0);
                        DESCRIPCION = c.getString(1);
                    } while (c.moveToNext());
                }
                db.close();
                tSKU.setText(CODIGO_SKU);
                tDescripcion.setText(DESCRIPCION);

                sqCodigo_Barras = codigo_Barras.getText().toString();
                sqSKU = CODIGO_SKU;
                sqCantidad = 1;
                descripcion_Producto = tDescripcion.getText().toString();

                codigo_Barras.setText("");
                comprobarIngreso = 0;


                //Se ingresan los datos en la tabla de inventario
                guardarDatosSqlite(numero_Documento, sqCodigo_Barras, sqSKU, fecha_Lectura, sqCantidad, tipo_Lectura, nombre_Bodega, codigo_Bodega, descripcion_Producto, estanteUbicacion.getText().toString());
            } else {
                Toasty.warning(getApplicationContext(), "Ingresa la ubicación de la bodega", Toasty.LENGTH_SHORT).show();
                codigo_Barras.setText("");
                comprobarIngreso = 0;
            }

        } catch (SQLiteException e) {

        } catch (Exception e) {

        }
    }

    /**
     * Se encarga de guargar los datos leidos
     * Autor:CarlosDnl
     * Fecha:26-02-2020
     * <p>
     * <p>
     * public static String vNUMERO_DOCUMENTO = "numero_documento";
     * public static String vCODIGO_BARRAS = "codigo_barras";
     * public static String vCODIGO_SKU_LEIDO = "codigo_sku";
     * public static String vFECHA_LECTURA = "fecha_lectura";
     * public static String vCANTIDAD = "cantidad";
     * public static String vTIPO_LECTURA = "tipo_lectura";
     * public static String vNOMBRE_BODEGA = "bodega";
     * public static String vCodigo_Bodega = "codigo_bodega";
     */

    private void guardarDatosSqlite(String vNumero_Documento, String vCodigo_Barras, String vCodido_Sku_Leido, String vFecha_Lectura, int vCantidad, String vTipo_Lectura, String vNombreBodega, String vCodigo_bodega, String vDescripcion, String vUbicacion) {

        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            //Reinicio de la base de datos
            db.beginTransactionNonExclusive();
            try {

                ContentValues contentValues = new ContentValues();

                contentValues.put(Utilidades.vNUMERO_DOCUMENTO, vNumero_Documento.trim());
                contentValues.put(Utilidades.vCODIGO_BARRAS, vCodigo_Barras);
                contentValues.put(Utilidades.vCODIGO_SKU_LEIDO, vCodido_Sku_Leido);
                contentValues.put(Utilidades.vFECHA_LECTURA, vFecha_Lectura);
                contentValues.put(Utilidades.vCANTIDAD, vCantidad);
                contentValues.put(Utilidades.vTIPO_LECTURA, vTipo_Lectura);
                contentValues.put(Utilidades.vNOMBRE_BODEGA, vNombreBodega);
                contentValues.put(Utilidades.vCodigo_Bodega, vCodigo_bodega);
                contentValues.put(Utilidades.vDESCRIPCION, vDescripcion);
                contentValues.put(Utilidades.vESTANTE_INVENTARIO, vUbicacion);


                db.insert(Utilidades.vTABLA_INVENTARIO, null, contentValues);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            //Se cierra la conexión con la base de datos
            db.close();
        } catch (SQLiteException e) {

        }
    }

    private String codigoBodega(String nombre_Bodega) {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_VBODEGAS, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.TABLA_VBODEGAS;//Set name of your table
        Cursor c = db.rawQuery("SELECT " + Utilidades.CODIGO_BODEGA + " FROM " + myTable + " WHERE " + Utilidades.NOMBRE_BODEGA + " = '" + nombre_Bodega + "';", null);
        if (c.moveToFirst()) {
            do {
                nombre_Bodega = c.getString(0);
            } while (c.moveToNext());
        }
        db.close();
        return nombre_Bodega;
    }

    private int cantidaRegistroInventario() {
        try {
            conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
            Cursor c = db.rawQuery("SELECT * FROM " + myTable + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = '" + tCodigoDocumento.getText().toString() + "' AND " + Utilidades.vEstado + " = 'IN'", null);
            cantidad = c.getCount();
        } catch (SQLiteException exception) {
            Log.i("error la inserare child", "on the next line");
            exception.printStackTrace();
        }
        return cantidad;
    }

    public void terminarInventario() {
        conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.vTABLA_INVENTARIO, null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String myTable = Utilidades.vTABLA_INVENTARIO;//Set name of your table
        Cursor c = db.rawQuery("UPDATE " + myTable + " SET " + Utilidades.vEstado + " = 'PR' " + " WHERE " + Utilidades.vNUMERO_DOCUMENTO + " = " + tCodigoDocumento.getText().toString(), null);
        c.moveToNext();
        new SweetAlertDialog(CrearInventario.this)
                .setTitleText("El inventario fue terminado correctamente")
                .show();
        db.close();
    }
}
