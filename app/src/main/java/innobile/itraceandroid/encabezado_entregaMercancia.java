package innobile.itraceandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.CertPathBuilderSpi;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itrace.data.DataBaseManager.ConexionSQLiteHelper;
import innobile.itrace.data.DataBaseManager.T_Configuraciones_DBManager;
import innobile.itrace.data.DataBaseManager.utilidades.Utilidades;
import innobile.itrace.data.Cons;
import innobile.itrace.transport.T_Configuraciones;

public class encabezado_entregaMercancia extends AppCompatActivity {


    private AutoCompleteTextView CentralCosto, V_Nit_Tercero;
    private EditText V_Nro_Dcto_Tercero;
    private ImageView ImageCentralCosto, ImagenResponsable;
    private String[] lstCentroCostos = new String[]{"Sin datos"};
    private String[] lstResponsable = new String[]{"Sin datos"};
    private String[] lstPrioridad = new String[]{"Normal", "Urgente"};
    private Button btnDetalle;
    private TextView Nro_Docto;
    private String Cod_Empresa = "01";
    private String Tipo_Docto = "";
    private Boolean band = true, ComprobarData;
    private TextView fecha;
    private String V_URL = null;
    private String comprobarConsulta = "";
    private String V_TipoConexion = null;
    private ProgressBar pgsBar;
    private Button btnNavegacion;
    private RequestQueue requestQueue;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encabezado_entrega_mercancia);


        V_Nro_Dcto_Tercero = findViewById(R.id.NroFactura);
        CentralCosto = findViewById(R.id.autoCompleteCentroCosto);
        V_Nit_Tercero = findViewById(R.id.autoCompleteResponsable);
        ImageCentralCosto = findViewById(R.id.ImagenCentralCosto);
        ImagenResponsable = findViewById(R.id.ImagenResponsable);
        Nro_Docto = findViewById(R.id.txtNroDocto);
        btnDetalle = findViewById(R.id.Buttondetalle);
        CentralCosto.setThreshold(2);
        CentralCosto.setThreshold(1);
        fecha = findViewById(R.id.MostrarFecha);
        btnNavegacion = findViewById(R.id.btnNavegacion);


        //Activando la opciones para navegar desde el encabezado
        btnNavegacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(encabezado_entregaMercancia.this, DefinirRuta.class);
                startActivity(intent);
                finish();
            }
        });

        try {
            if (requestQueue == null) {
                //Configurando la petición por Volley
                // Instantiate the cache
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
                // Set up the network to use HttpURLConnection as the HTTP client.
                Network network = new BasicNetwork(new HurlStack());
                // Instantiate the RequestQueue with the cache and network.
                requestQueue = new RequestQueue(new NoCache(), network);
                // Start the queue
                requestQueue.start();
            }
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de crear la instancia de Volley", Toast.LENGTH_SHORT).show();
        }


        //Progress bar que se encarga de mostrar el progreso al traer los datos de los proveedores
        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        //Controlar que se muestre el ProgressBar
        Toasty.info(encabezado_entregaMercancia.this, "Cargando los datos de proveedores", Toast.LENGTH_SHORT, true).show();
        pgsBar.setVisibility(View.VISIBLE);


        //Configurando el tipo de documento
        Tipo_Docto = Global.TIPO_DOCUMENTO;

        //Configurando la fecha
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        fecha.setText(formatter.format(date));

        T_Configuraciones_DBManager t_configuraciones_dbManager = new T_Configuraciones_DBManager(getApplicationContext());
        T_Configuraciones t_configuraciones = t_configuraciones_dbManager.ConsultarConfiguracion(Cons.CONFIGURACION_URL);

        V_URL = t_configuraciones.getCon_URL();
        V_TipoConexion = t_configuraciones.getCon_TipoConexion();


        try {
            ConsultarNuevoNroDcto();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            CargarCentroCostos("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            CargarProveedores("*");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ImageCentralCosto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CentralCosto.showDropDown();
            }
        });

        ImagenResponsable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                V_Nit_Tercero.showDropDown();
            }
        });


        btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ComprobarData = comprobarDatosIngresados();
                    //COMPROBAR QUE TODOS LOS CAMPOS FUERON SELECCIONADOS:  PRIORIDAD, CENTRO DE COSTOS Y RESPONSABLE
                    if (ComprobarData) {


                        GuardarEncabezadoLocalmente(Tipo_Docto.toString(), Integer.parseInt(Nro_Docto.getText().toString()), V_Nro_Dcto_Tercero.getText().toString(), CentralCosto.getText().toString(), V_Nit_Tercero.getText().toString());
                        //LLAMA FORMULARION Y MANDA PARAMETROS

                        if (Tipo_Docto.equals("RM")) {
                            Intent intent = new Intent(encabezado_entregaMercancia.this, OrdeCompra.class);
                            intent.putExtra("codigo", Nro_Docto.getText().toString());
                            intent.putExtra("V_Nro_Dcto_Tercero", V_Nro_Dcto_Tercero.getText().toString());
                            intent.putExtra("central_costo", CentralCosto.getText().toString());
                            intent.putExtra("V_Nit_Tercero", V_Nit_Tercero.getText().toString());
                            intent.putExtra("comprobarConsulta", comprobarConsulta);
                            startActivity(intent);
                        }


                    } else {
                        Toasty.warning(getApplicationContext(), "Es necesario ingresar todos los datos", Toast.LENGTH_SHORT, true).show();
                        band = true;
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error al tratar de obtener datos", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void CargarCentroCostos(String centralCostoDato) throws JSONException {

        try {
            JSONObject jsonBuscarCentroCostos = null;
            jsonBuscarCentroCostos = new JSONObject();
            jsonBuscarCentroCostos.put("Valor", centralCostoDato);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarCentroCostos", jsonBuscarCentroCostos, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                        String centralCosto;  //Esta varaible va a quitar los espacions de la respuesta del json


                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstCentroCostos = new String[preguntasJA.length()];


                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            centralCosto = mJsonObject.getString("NOMBRE");
                            lstCentroCostos[i] = centralCosto.trim();

                        }
                        ArrayAdapter<String> adapterCentralCosto = new ArrayAdapter<String>(encabezado_entregaMercancia.this, android.R.layout.simple_spinner_dropdown_item, lstCentroCostos);
                        CentralCosto.setAdapter(adapterCentralCosto);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_entregaMercancia.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al cargar el centro de costos", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Se encarga de traer los datos de los proveedores
     * Fecha: 18/12/2019
     * Autor: CarlosDnl
     */

    public void CargarProveedores(String datoProveedor) throws JSONException {
        try {

            JSONObject jsonBuscarTerceros = null;
            jsonBuscarTerceros = new JSONObject();

            jsonBuscarTerceros.put("Valor", datoProveedor);

            // String url =   V_URL + "/datosProveedores";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarTerceros", jsonBuscarTerceros, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String texto;

                        JSONArray preguntasJA = response.getJSONArray("recordset");

                        lstResponsable = new String[preguntasJA.length()];

                        for (int i = 0; i < preguntasJA.length(); i++) {
                            JSONObject mJsonObject = preguntasJA.getJSONObject(i);
                            texto = mJsonObject.getString("NOMBRE");
                            lstResponsable[i] = texto.trim();
                        }


                        pgsBar.setVisibility(View.GONE);
                        Toasty.success(getApplicationContext(), "Los datos fueron cargados correctamente", Toast.LENGTH_SHORT, true).show();
                        ArrayAdapter<String> adapterResponsables = new ArrayAdapter<String>(encabezado_entregaMercancia.this, android.R.layout.simple_spinner_dropdown_item, lstResponsable);
                        V_Nit_Tercero.setAdapter(adapterResponsables);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(encabezado_entregaMercancia.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al cargar los datos de los proveedores", Toast.LENGTH_SHORT).show();
        }
    }


    public void GuardarEncabezadoLocalmente(String Tipo_Dcto, int Nro_Docto, String prioridad, String central_costo, String responsable) {
        try {
            ConexionSQLiteHelper conn = new ConexionSQLiteHelper(getApplicationContext(), Utilidades.TABLA_ENCABEZADO, null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(Utilidades.TIPO_DOCUMENTO, Tipo_Dcto);
            values.put(Utilidades.NUMERO_DOCUMENTO, Nro_Docto);
            values.put(Utilidades.PRIORIDAD, prioridad);
            values.put(Utilidades.CENTRAL_COSTO, central_costo);
            values.put(Utilidades.RESPONSABLE, responsable);


            db.insert(Utilidades.TABLA_ENCABEZADO, null, values);
            db.close();
        } catch (Error e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Este método se encarga de mostrar el código actual a mostrar en un tipo de movimiento RQ o RC
     * Autor: CarlosDnl
     * Fecha: 05/12/2019
     */


    public void ConsultarNuevoNroDcto() throws JSONException {
        try {
            //Creando el json con los parametros que va a recibir el API
            JSONObject jsonBuscarConsecutivoMov = null;
            jsonBuscarConsecutivoMov = new JSONObject();

            jsonBuscarConsecutivoMov.put("Cod_Empresa", Cod_Empresa);
            jsonBuscarConsecutivoMov.put("Tipo_Documento", Tipo_Docto);

            //URL a la que será realizada la petición
            // String url =   V_URL + "/selectCode";


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, V_URL + "/BuscarConsecutivoMov", jsonBuscarConsecutivoMov, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray mJsonArray = response.getJSONArray("recordset");

                        // for(int i=0; i < mJsonArray.length(); i++){
                        if (mJsonArray.length() >= 1) {
                            JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                            String referencia = mJsonObject.getString("Consecutivo");
                            int nuevo_valor = 1;
                            nuevo_valor = nuevo_valor + Integer.parseInt(referencia.trim());

                            Nro_Docto.setText(String.valueOf(nuevo_valor));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al consultar el número del documento", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Se encarga de comprobar que todos los datos esten cargados
     * Fecha: 19/12/2019
     * Autor: CalorDnl
     */

    public Boolean comprobarDatosIngresados() {
        try {
            String documento, central, proveedor;
            documento = V_Nro_Dcto_Tercero.getText().toString();
            central = CentralCosto.getText().toString();
            proveedor = V_Nit_Tercero.getText().toString();

            //Comprueba si alguno de los datos está vacio
            if (documento.equals("") || central.equals("") || proveedor.equals("")) {
                return band = false;
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al tratar de comprobar los datos ingresados", Toast.LENGTH_SHORT).show();
        }


        return band;
    }


}



