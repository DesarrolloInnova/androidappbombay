package innobile.Adapter;

public class GetBodegaAdapter {
    String  CODIGO_BODEGA;
    String NOMBRE_BODEGA;

    public GetBodegaAdapter(String CODIGO_BODEGA, String NOMBRE_BODEGA) {
        this.CODIGO_BODEGA = CODIGO_BODEGA;
        this.NOMBRE_BODEGA = NOMBRE_BODEGA;
    }

    public GetBodegaAdapter() {
    }

    public String getCODIGO_BODEGA() {
        return CODIGO_BODEGA;
    }

    public void setCODIGO_BODEGA(String CODIGO_BODEGA) {
        this.CODIGO_BODEGA = CODIGO_BODEGA;
    }

    public String getNOMBRE_BODEGA() {
        return NOMBRE_BODEGA;
    }

    public void setNOMBRE_BODEGA(String NOMBRE_BODEGA) {
        this.NOMBRE_BODEGA = NOMBRE_BODEGA;
    }
}
