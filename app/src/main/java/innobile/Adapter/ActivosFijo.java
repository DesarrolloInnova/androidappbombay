package innobile.Adapter;

public class ActivosFijo {
        String Codigo;
        String Descripcion;
        String Marca;
        String Grupo;


    public ActivosFijo(String codigo, String descripcion, String marca, String grupo) {
        Codigo = codigo;
        Descripcion = descripcion;
        Marca = marca;
        Grupo = grupo;
    }

    public ActivosFijo() {
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }
}
