package innobile.Adapter;

public class GetInventarioAdapter {
    String codigo_Documento;
    String fecha;
    String tipo_Conteo;
    String bodega_Nombre;



    public GetInventarioAdapter(String codigo_Documento, String fecha, String tipo_Conteo, String bodega_Nombre) {
        this.codigo_Documento = codigo_Documento;
        this.fecha = fecha;
        this.tipo_Conteo = tipo_Conteo;
        this.bodega_Nombre = bodega_Nombre;
    }

    public GetInventarioAdapter() {

    }

    public String getCodigo_Documento() {
        return codigo_Documento;
    }

    public String getFecha() {
        return fecha;
    }

    public String getTipo_Conteo() {
        return tipo_Conteo;
    }

    public String getBodega_Nombre() {
        return bodega_Nombre;
    }

    public void setCodigo_Documento(String codigo_Documento) {
        this.codigo_Documento = codigo_Documento;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setTipo_Conteo(String tipo_Conteo) {
        this.tipo_Conteo = tipo_Conteo;
    }

    public void setBodega_Nombre(String bodega_Nombre) {
        this.bodega_Nombre = bodega_Nombre;
    }



}
