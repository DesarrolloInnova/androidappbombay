package innobile.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import innobile.itrace.R;


public class AdapterRequisicion extends RecyclerView.Adapter <AdapterRequisicion.ViewHolderDatos> {


    ArrayList<String> ListRequisicion;

    public AdapterRequisicion(ArrayList<String> ListRemisionEspecial){
        this.ListRequisicion = ListRemisionEspecial;
    }

    @Override
    public ViewHolderDatos onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_requisicion,null,false);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderDatos viewHolderDatos, int i) {

        viewHolderDatos.CargarLista(ListRequisicion.get(i));

    }

    @Override
    public int getItemCount() {
        return ListRequisicion.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder{

        TextView txtNroRequisicion, txtCliente, txtEstado;

        public ViewHolderDatos(View itemView) {
            super(itemView);
            txtNroRequisicion = itemView.findViewById(R.id.txtNroRequisicion);
        }

        public void CargarLista(String s) {

            txtNroRequisicion.setText(s);
        }
    }


}
