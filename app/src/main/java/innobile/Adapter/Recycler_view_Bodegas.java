package innobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import es.dmoral.toasty.Toasty;
import innobile.itrace.R;
import innobile.itraceandroid.TrasladosTM.Reportes_Traslados;

/**
 * Created by Aws on 11/03/2018.
 */

public class Recycler_view_Bodegas extends RecyclerView.Adapter<Recycler_view_Bodegas.MyViewHolder> {

    private Context mContext;
    private List<ActivosFijo> mData;


    public Recycler_view_Bodegas(Context mContext, List<ActivosFijo> mData) {
        this.mContext = mContext;
        this.mData = mData;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.bodegas_row_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toasty.info(mContext, mData.get(viewHolder.getAdapterPosition()).getDescripcion(), Toasty.LENGTH_SHORT).show();
         /*       Intent i = new Intent(mContext, Reportes_Traslados.class);
                i.putExtra("anime_name",mData.get(viewHolder.getAdapterPosition()).getDescripcion());
                i.putExtra("anime_description",mData.get(viewHolder.getAdapterPosition()).getDescription());
                i.putExtra("anime_studio",mData.get(viewHolder.getAdapterPosition()).getStudio());
                i.putExtra("anime_category",mData.get(viewHolder.getAdapterPosition()).getCategorie());
                i.putExtra("anime_nb_episode",mData.get(viewHolder.getAdapterPosition()).getNb_episode());
                i.putExtra("anime_rating",mData.get(viewHolder.getAdapterPosition()).getRating());
                i.putExtra("anime_img",mData.get(viewHolder.getAdapterPosition()).getImage_url());

                mContext.startActivity(i);*/

            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tDescripcion.setText(mData.get(position).getDescripcion());
        holder.tCodigo.setText(mData.get(position).getCodigo());
        holder.tMarca.setText(mData.get(position).getMarca());
        holder.tGrupo.setText(mData.get(position).getGrupo());


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tDescripcion;
        TextView tCodigo;
        TextView tMarca;
        TextView tGrupo;
        LinearLayout view_container;


        public MyViewHolder(View itemView) {
            super(itemView);

            view_container = itemView.findViewById(R.id.container);
            tDescripcion = itemView.findViewById(R.id.maquina_descripcion);
            tCodigo = itemView.findViewById(R.id.maquina_codigo);
            tMarca = itemView.findViewById(R.id.maquina_marca);
            tGrupo = itemView.findViewById(R.id.maquina_grupo);

        }
    }

}