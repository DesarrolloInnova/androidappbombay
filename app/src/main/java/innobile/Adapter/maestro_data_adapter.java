package innobile.Adapter;

public class maestro_data_adapter {
    String  maestroCODIGO_BODEGA;
    String maestroSkuInventario;
    String maestroCantidadInventario;


    public maestro_data_adapter(String maestroCODIGO_BODEGA, String maestroSkuInventario, String maestroCantidadInventario) {
        this.maestroCODIGO_BODEGA = maestroCODIGO_BODEGA;
        this.maestroSkuInventario = maestroSkuInventario;
        this.maestroCantidadInventario = maestroCantidadInventario;
    }

    public maestro_data_adapter() {
    }

    public String getMaestroCODIGO_BODEGA() {
        return maestroCODIGO_BODEGA;
    }

    public void setMaestroCODIGO_BODEGA(String maestroCODIGO_BODEGA) {
        this.maestroCODIGO_BODEGA = maestroCODIGO_BODEGA;
    }

    public String getMaestroSkuInventario() {
        return maestroSkuInventario;
    }

    public void setMaestroSkuInventario(String maestroSkuInventario) {
        this.maestroSkuInventario = maestroSkuInventario;
    }

    public String getMaestroCantidadInventario() {
        return maestroCantidadInventario;
    }

    public void setMaestroCantidadInventario(String maestroCantidadInventario) {
        this.maestroCantidadInventario = maestroCantidadInventario;
    }
}
