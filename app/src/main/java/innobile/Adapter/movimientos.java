package innobile.Adapter;

public class movimientos {
    String codigo;
    String bodega_Origen;
    String bodega_Destion;
    String responsable;
    String Observaciones;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getBodega_Origen() {
        return bodega_Origen;
    }

    public void setBodega_Origen(String bodega_Origen) {
        this.bodega_Origen = bodega_Origen;
    }

    public String getBodega_Destion() {
        return bodega_Destion;
    }

    public void setBodega_Destion(String bodega_Destion) {
        this.bodega_Destion = bodega_Destion;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }
}
