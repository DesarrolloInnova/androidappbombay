package innobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import innobile.itrace.R;
import innobile.itraceandroid.CrearInventario;
import innobile.itraceandroid.Global;

public class InventarioModelData extends RecyclerView.Adapter<InventarioModelData.PlayerViewHolder> {
    public List<GetInventarioAdapter> inventario;
    private Context mContext;


    public InventarioModelData(Context mContext, List<GetInventarioAdapter> inventario) {
        this.inventario = inventario;
        this.mContext = mContext;

    }

    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.inventario_row_item, parent, false);
        final InventarioModelData.PlayerViewHolder viewHolder = new InventarioModelData.PlayerViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent().setClass(mContext, CrearInventario.class);
                Global.CREAR_REQUISICON_MONGO = false;
                Global.CAMBIAR_CONTEO = true;
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                i.putExtra("tCodigoDocumento", inventario.get(viewHolder.getAdapterPosition()).getCodigo_Documento());
                i.putExtra("tConteo", inventario.get(viewHolder.getAdapterPosition()).getTipo_Conteo());
                i.putExtra("Fecha_Lectura", inventario.get(viewHolder.getAdapterPosition()).getFecha());
                i.putExtra("Nombre_Bodega", inventario.get(viewHolder.getAdapterPosition()).getBodega_Nombre());
                mContext.startActivity(i);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(InventarioModelData.PlayerViewHolder holder, int position) {
        GetInventarioAdapter datos = inventario.get(position);
        holder.codigo.setText(datos.getCodigo_Documento());
        holder.fecha.setText(datos.getFecha());
        holder.tipo.setText(datos.getTipo_Conteo());
        holder.bodega.setText(datos.getBodega_Nombre());
    }


    @Override
    public int getItemCount() {
        return inventario.size();
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder {

        TextView codigo;
        TextView fecha;
        TextView tipo;
        TextView bodega;
        LinearLayout view_container;

        public PlayerViewHolder(View itemView) {
            super(itemView);
            view_container = itemView.findViewById(R.id.container);
            codigo = itemView.findViewById(R.id.tDocumento);
            fecha = itemView.findViewById(R.id.tFecha);
            tipo = itemView.findViewById(R.id.tConteo);
            bodega = itemView.findViewById(R.id.tBodega);
        }
    }

}
