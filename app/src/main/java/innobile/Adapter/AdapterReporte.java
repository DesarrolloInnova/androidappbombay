package innobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import innobile.itrace.R;
import innobile.itraceandroid.ContextCompat;
import innobile.itraceandroid.Reporte_Inventario;

public class AdapterReporte extends BaseAdapter {
    ArrayList<String> resultado, skuArray, codigoBarrasArray,cantidadInventario, cantidadMaestro, diferencia, bodegas, ubicacion;
    int[] imgId;
    Context contexto;
    private static LayoutInflater inflater= null;
    public AdapterReporte (Reporte_Inventario reporte_inventario, ArrayList<String> descripcionArray,ArrayList<String> skuArray,ArrayList<String> codigoBarrasArray,ArrayList<String> cantidadInventario,ArrayList<String> cantidadMaestro,ArrayList<String> diferencia,ArrayList<String> bodegas,ArrayList<String> ubicacion ) {
        this.resultado = descripcionArray;
        this.skuArray = skuArray;
        this.codigoBarrasArray = codigoBarrasArray;
        this.cantidadInventario = cantidadInventario;
        this.cantidadMaestro = cantidadMaestro;
        this.diferencia = diferencia;
        this.bodegas = bodegas;
        this.ubicacion = ubicacion;
        contexto = reporte_inventario;
        inflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount(){
        return this.resultado.size();
    }
    @Override
    public  Object getItem(int posicion) {
        return posicion;
    }
    @Override
    public  long  getItemId(int posicion) {
        return posicion;
    }
    public class Holder
    {
        TextView tDescripcion, tSKU, tCodigoBarras, tCantidadInventario, tCantidadMaestro, tDiferencia, tBodega, tUbicacion;
        LinearLayout inventario_info;
    }
    @Override
    public View getView(final int posicion, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View fila;
        fila = inflater.inflate(R.layout.lista_reporte, null);
        holder.tDescripcion=(TextView) fila.findViewById(R.id.tDescripcion);
        holder.tSKU=(TextView) fila.findViewById(R.id.tSKU);
        holder.tCodigoBarras=(TextView) fila.findViewById(R.id.tCodigoBarras);
        holder.tCantidadInventario=(TextView) fila.findViewById(R.id.tCantidadInventario);
        holder.tCantidadMaestro=(TextView) fila.findViewById(R.id.tCantidadMaestro);
        holder.tDiferencia=(TextView) fila.findViewById(R.id.tDiferencia);
        holder.tBodega=(TextView) fila.findViewById(R.id.tBodega);
        holder.tUbicacion=(TextView) fila.findViewById(R.id.tUbicacion);
        holder.inventario_info = fila.findViewById(R.id.inventario_info);


        if(this.cantidadMaestro.get(posicion).equals("Sin dato")){
            holder.inventario_info.setBackgroundColor(ContextCompat.getColor(contexto, R.color.warning));
        }else if (!this.cantidadMaestro.get(posicion).equals(this.cantidadInventario.get(posicion))){
            holder.inventario_info.setBackgroundColor(ContextCompat.getColor(contexto, R.color.error));
        }else if (this.cantidadMaestro.get(posicion).equals(this.cantidadInventario.get(posicion))){
            holder.inventario_info.setBackgroundColor(ContextCompat.getColor(contexto, R.color.correcto));
        }


        holder.tDescripcion.setText(resultado.get(posicion));
        holder.tSKU.setText(this.skuArray.get(posicion));
        holder.tCodigoBarras.setText(this.codigoBarrasArray.get(posicion));
        holder.tCantidadInventario.setText(this.cantidadInventario.get(posicion));
        holder.tCantidadMaestro.setText(this.cantidadMaestro.get(posicion));
        holder.tDiferencia.setText(this.diferencia.get(posicion));
        holder.tBodega.setText(this.bodegas.get(posicion));
        holder.tUbicacion.setText(this.ubicacion.get(posicion));

        fila.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(contexto,  resultado.get(posicion),
                        Toast.LENGTH_LONG).show();
            }
        });
        return fila;
    }

}
