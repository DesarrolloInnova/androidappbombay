package innobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import innobile.itrace.R;
import innobile.itraceandroid.TrasladosTM.AsistenteApp;
import innobile.itraceandroid.TrasladosTM.DetallesMovimientos;

public class movimientos_adapter extends RecyclerView.Adapter<movimientos_adapter.MyViewHolder> {

    private Context mContext ;
    private List<movimientos> mData ;
    RequestOptions option;


    public movimientos_adapter(Context mContext, List<movimientos> mData) {
        this.mContext = mContext;
        this.mData = mData;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.movimiento_recycler,parent,false) ;
        final MyViewHolder viewHolder = new MyViewHolder(view) ;
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, DetallesMovimientos.class);
                i.putExtra("codigo",mData.get(viewHolder.getAdapterPosition()).getCodigo());
                mContext.startActivity(i);

            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tCodigo.setText(mData.get(position).getCodigo());
        holder.tBodegaOrigen.setText(mData.get(position).getBodega_Origen());
        holder.tBodegaDestino.setText(mData.get(position).getBodega_Destion());
        holder.tResponsable.setText(mData.get(position).getResponsable());
        holder.tObservaciones.setText(mData.get(position).getObservaciones());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tCodigo ;
        TextView tBodegaOrigen;
        TextView tBodegaDestino ;
        TextView tResponsable;
        TextView tObservaciones;
        LinearLayout view_container;

        public MyViewHolder(View itemView) {
            super(itemView);

            view_container = itemView.findViewById(R.id.container);
            tCodigo = itemView.findViewById(R.id.tnumero_documento);
            tBodegaOrigen = itemView.findViewById(R.id.tbodega_origen);
            tBodegaDestino = itemView.findViewById(R.id.tbodega_destino);
            tResponsable = itemView.findViewById(R.id.t_responsable);
            tObservaciones = itemView.findViewById(R.id.t_observaciones);
        }
    }

}
