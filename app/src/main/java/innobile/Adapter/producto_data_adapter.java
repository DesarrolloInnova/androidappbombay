package innobile.Adapter;

public class producto_data_adapter {
    String CODIGO_SKU;
    String VCODIGO_BARRAS;
    String DESCRIPCION_PRODUCTO;

    public producto_data_adapter(String CODIGO_SKU, String VCODIGO_BARRAS, String DESCRIPCION_PRODUCTO) {
        this.CODIGO_SKU = CODIGO_SKU;
        this.VCODIGO_BARRAS = VCODIGO_BARRAS;
        this.DESCRIPCION_PRODUCTO = DESCRIPCION_PRODUCTO;
    }

    public producto_data_adapter() {
    }

    public String getCODIGO_SKU() {
        return CODIGO_SKU;
    }

    public void setCODIGO_SKU(String CODIGO_SKU) {
        this.CODIGO_SKU = CODIGO_SKU;
    }

    public String getVCODIGO_BARRAS() {
        return VCODIGO_BARRAS;
    }

    public void setVCODIGO_BARRAS(String VCODIGO_BARRAS) {
        this.VCODIGO_BARRAS = VCODIGO_BARRAS;
    }

    public String getDESCRIPCION_PRODUCTO() {
        return DESCRIPCION_PRODUCTO;
    }

    public void setDESCRIPCION_PRODUCTO(String DESCRIPCION_PRODUCTO) {
        this.DESCRIPCION_PRODUCTO = DESCRIPCION_PRODUCTO;
    }
}
