package innobile.itrace.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Administrador de Bases de datos SQLite
 */
public class DbHelper extends SQLiteOpenHelper {

    //Nombre del archivo de la base de datos
    private static final String DB_NAME = "iTrace.sqlite";
    //Versión del esquema de la base de datos
    private static final int DB_SCHEMA_VERSION = 1;

    private String createTableSQL = "";

    /**
     * Constructor de la clase
     *
     * @param context
     */
    public DbHelper(Context context, String createTableSQL) {
        super(context, DB_NAME, null, DB_SCHEMA_VERSION);
        setCreateTableSQL(createTableSQL);
    }

    @Override
    /**
     * Se ejecuta cuando se crea la base de datos.
     * Se le pasa como parámetro una base de datos SQLite
     */
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la consulta sql
        db.execSQL(getCreateTableSQL());

    }

    @Override
    /**
     * Se ejecuta cuando hay una modificación en la base de datos SQLite
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    //Propiedades de los atributos
    public String getCreateTableSQL() {
        return createTableSQL;
    }

    public void setCreateTableSQL(String createTableSQL) {
        this.createTableSQL = createTableSQL;
    }
}
